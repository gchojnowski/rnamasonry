# Copyright (C) 2023 Grzegorz Chojnowski

import os,sys
import glob
from setuptools import setup
import setuptools
import subprocess
import distutils.log
import distutils.command.build
from distutils.command.install import install as DistutilsInstall

print("***** Compiling additional modules")
ret=subprocess.check_call('scons', cwd=os.path.join("rnaMasonryLib","pysimrna"))

print("***** Compiling additional modules")
ret=subprocess.check_call('scons', cwd=os.path.join("rnaMasonryLib","pysimrna"))


_ext_modules=[]
_package_data={'rnaMasonryLib':[os.path.join("pysimrna", "pysimrna.so"), \
                                os.path.join("pysimrna", "data", "*"), \
                                os.path.join("moderna_source_1.7.1_py3",'*','*','*'),\
                                os.path.join("moderna_source_1.7.1_py3",'*','*'),\
                                os.path.join("moderna_source_1.7.1_py3",'*',),\
                                os.path.join("k2n_standalone",'*',),\
                                os.path.join("*"),\
                                os.path.join("ClaRNA",'*'),\
                                os.path.join("ClaRNA",'*', '*'),\
                                os.path.join('..','local_db','*'),\
                                os.path.join('..','local_db','*', '*'),\
                                os.path.join('..','examples','*'),\
                                os.path.join('..','data','mon_lib','*','*'),\
                                os.path.join('..','data','geostd','*','*'), ]}


def get_version():
    import rnamasonry.version
    return rnamasonry.version.__version__


setup(
    cmdclass={},
    name="RNAMasonry",
    version=get_version(),
    packages=['rnamasonry', 'rnaMasonryLib'],
    package_data=_package_data,
    url="https://gitlab.com/gchojnowski/rnamasonry",
    license="BSD",
    author="Grzegorz Chojnowski",
    author_email="gchojnowski@embl-hamburg.de",
    description="RNA fragment assembly with secondary structure and experimental restraints",
    zip_safe=False,
    entry_points={
          "console_scripts": [
            "rnamasonry = rnamasonry.__main__:main",
            ],
    }
)


