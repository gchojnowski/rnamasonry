# RNA Masonry:  RNA fragment assembly with experimental restraints

RNA Masonry is a program for RNA structure prediction with secondary structure and Small Angle Scattering (SAS) restraints. The program assemblies 
models using a over 50k RNA motifs extracted from high-quality experimnental structures using [RNABricks2](http://iimcb.genesilico.pl/rnabricks2/). Tentative models are validated with [SimRNA](http://genesilico.pl/simrna/) scoring function to ensure plausibility of model geometry. Have a look at a related publication if you want to learn more.

- [How to cite](#how-to-cite)
- [Webserver](#webserver)
- [Installation](#installation)
    - [Troubleshooting](#troubleshooting)
- [How to use RNA Masonry](#how-to-use-rna-masonry)
    - [Starting simulation from secondary structure](#starting-simulation-from-secondary-structure)
    - [Starting simulation from an existing model](#starting-simulation-from-an-existing-model)
    - [Using SAS restraints](#using-sas-restraints)
<br/> 

# How to cite

Grzegorz Chojnowski, Rafał Zaborowski, Marcin Magnus, Sunandan Mukherjee, and Janusz M. Bujnicki (2021)\
*RNA 3D structure modeling by fragment assembly with small-angle X-ray scattering restraints* [Bioinformatics, 2023, btad527](https://doi.org/10.1093/bioinformatics/btad527)

<br/> 

# Webserver

You are welcome to use the RNA Masonry webserver available at

http://iimcb.genesilico.pl/rnamasonry 

As with any shared resource, the number of tasks that can be run simultaneously is limited. Therefore, if you plan to run a large number of jobs I suggest you to install the software locally, it's really easy!


<br/> 

# Installation

The program was tested mostly on Linux and MacOS. It will also work on MS Windows, but I recommend to use it within WSL as satisfying external dependencies (eg cctbx) may be otherwise a pain.

For handling software dependencies RNA Masonry requires **conda** or **miniconda** environments. Detailed installation instructions for each of them can be found [here](https://docs.conda.io/en/latest/index.html) and [here](https://docs.conda.io/en/latest/miniconda.html). 

**M1/M2 users:** please install intel version of conda/miniconda (it's fully compatible with your system). Not all RNA Masonry dependencies are currently available for arm architecture.

After installing (if needed) conda/miniconda create and activate an enironment:

```
conda create -n rnamasonry
conda activate rnamasonry
```

or with miniconda e.g.

```
bash Miniconda3-latest-MacOSX-x86_64.sh -b -p conda_rms
. conda_rms/bin/activate 
```

next, install dependencies

```
conda install -c conda-forge cctbx-base numpy biopython networkx scipy simplejson scons boost
```

and clone the repository

```
git clone --single-branch https://gitlab.com/gchojnowski/rnamasonry.git
```

you must also download a **library of RNA fragments**. Otherwise, the program will use only a small, hard-coded set of fragments and give less satisfactory results (only useful for testing).

```
cd rnamasonry
curl -O https://iimcb.genesilico.pl/rnamasonry/media/local_db.tar
tar xvf local_db.tar
```

finally, install RNA Masonry with

```
python -m pip install .
```

Now, you can test your installation with the following command (see below for more usage examples). If it fails, follow to the next section [troubleshooting](#troubleshooting)

```
rnamasonry -i examples/tRNA.txt --fast
```

<br/> 

# Troubleshooting

If the standard installation procedure fails (can happen on older Linux installations), try to install latest c/c++ compilers with conda 

```
conda install -c conda-forge gcc gxx
```

and rerurn the instalation script

```
python -m pip install .
```
<br/> 


# How to use RNA Masonry

RNA Masonry assemblies models from fragments strictly preserving input secondary structure. The input secondary structure can be privided:
 - in a [dot bracket format](#starting-simulation-from-secondary-structure)
 - as an [exisitng RNA model](#starting-simulation-from-an-existing-model)
<br/> 


## Starting simulation from secondary structure
To start a folding simulation you need to provide an input file with a target sequence and secondary structure in a dot-bracket format.
Pseudo-knots may be added in separate (possibly multiple) lines, like in an example below. First line must define nested structure of the target.

    GCGGAUUUAGCUCAGUUGGGAGAGCGCCAGACUGAAGAUCUGGAGGUCCUGUGUUCGAUCCACAGAAUUCGCACCA
    (((((((..((((........))))(((((.........)))))....(((((.......))))))))))))....
    ..................(....................................)....................

As an example, you can start a simulation with exemplary input from the project directory. The *--fast* keyword will reduce pool of fragments available during simulation for quicker results. It is not recomemnded to use this option except tests.

```
rnamasonry -i examples/tRNA.txt --fast
```
<br/> 


## Starting simulation from an existing model

Alternatively, the simulation can be initiated with a model in PDB/mmCIF format. In that case secondary structure will be assigned automatically using [ClaRNA](http://genesilico.pl/clarna/) and used to restrain the simulation. For example, you can start a simulation with a provided tRNA model:

```
rnamasonry -i examples/1EHZ.pdb --fast
```
<br/> 


## Using SAS restraints

You can restrain the folding simulation (initiated either with secondary structure or input model) with SAXS or SANS data. During simulation RNA Masonry will score the agreement of tentative models with experimental scattering curve. The program accepts processed data in a format recognized by [CRYSOL](https://www.embl-hamburg.de/biosaxs/crysol.html) and [FOXS](https://modbase.compbio.ucsf.edu/foxs/). You can decide, whether to use CRYSOL or FOXS for computations (the latter may be faster, but is available only for Linux). To start a simulation for tRNA using FOXS and a scatrering curve provided with RNA Masonry run

```
rnamasonry -i examples/tRNA.txt --saxs-data examples/tRNA_saxs.dat
```    

similarly, to start a simulation from an existing model using CRYSOL for scoring run

```
rnamasonry -i examples/1EHZ.pdb --saxs-data examples/tRNA_saxs.dat --crysol
``` 

**IMPORTANT:** to use CRYSOL, you must ensure that a fully functional version is available from the command line before running RNA Masonry. The program will work both with CRYSOL 2.8.x and 3.x, but the use of the most recent version is recommended.


(C) 2014-2023 Grzegorz Chojnowski
