#! /usr/bin/env libtbx.python
# -*- coding: utf-8 -*-
# =============================================================================
# Created at:
#
# Author:
#  Grzegorz Chojnowski - gchojnowski@gmail.com
#  Rafał Zaborowski
# =============================================================================

## @file rnamasonry.py
#
#  A program matching recurrent RNA motifs from RNABricks database to input RNA models.
#  to validate plausibility of base-pairs and genberate geometry restraints for resinement
#  with Refmac5, phenix, and for display in Pymol.
#  The package is based on a smatch_cpp project


__author__ = "Grzegorz Chojnowski, Rafał Zaborowski"
__author_email__ = "gchojnowski@gmail.com"
__date__ = "2013-12-30"
__updated__ = "2023-06-16"


import sys, os
import json
import shutil,glob
import traceback
import urllib.request, urllib.error, urllib.parse, urllib.request, urllib.parse, urllib.error
import string
import locale
import tempfile

from optparse import OptionParser, OptionGroup, SUPPRESS_HELP

# ============================== local imports ================================

ROOT = os.path.dirname(os.path.realpath(__file__))
sys.path.insert(2, os.path.join(ROOT, '..', 'rnaMasonryLib') )
sys.path.insert(2, os.path.join(ROOT, '..') )

from rnamasonry_config import ROOT, RNABRICKS2_URL, MIN_MODERNA_NFRAGS, FRAG_TYPES, FOXS_PATH
sys.path.append(os.path.join(ROOT, "lib"))

from ph_parser import ph_parser
from motifs import IncorrectInputDataError

# gchojnowski
from update_db import db_sync
from metropolis_hastings import MCMC
from replica_exchange import ReplicaExchange
from rnamasonry_utils import print_stats
from pysaxs import pysaxs
from blobfit import blobfit

from tree_traverser import TreeTraverser
from datetime import datetime
import time

# ============================== local imports ================================
try:
    import rnamasonry.version
    rnamasonry_version = rnamasonry.version.__version__
except:
    rnamasonry_version="dev"

rnamasonry_header=f"""
# RNA Mamasonry ({rnamasonry_version}): RNA fragment assembly with experimental restraints

"""


# =============================================================================

def save_branch_to_disk(branch, write_path, cut_joints=True, one_chain_only=True):
    """Multiprocessing method to create branch object based on information which fragments to
    use and save branch hierarchy to disk."""
    hierarchy = branch.get_hierarchy(cut_joint_nucleotides=cut_joints)
    if one_chain_only:
        hierarchy = ph_parser().parse(hierarchy)
    hierarchy.write_pdb_file(write_path)
    return hierarchy



# -----------------------------------------------------------------------------


def parse_args(examples=False, expert=False):

    parser=OptionParser()
    parser.set_usage("""rnamasonry [options]\n\n""")

    main_group = OptionGroup(parser, "Moudus operandi")
    parser.add_option_group(main_group)


    main_group.add_option("--input_file", "-i", dest="input_file", action="store", type="string",\
                        help="Input file containing RNA sequence and secondary structure or a PDB/mmCIF file. Examples: "
                            "examples/tRNA.txt,1EHZ.pdb,1EHZ.cif", \
                            metavar="FILENAME")

    main_group.add_option("--refine", action="store_true", dest="refine", default=False, \
                            help="Perform a simple simulated annealing refinement only "
                                    " [default: replica exchange]")

    main_group.add_option("--test", action="store_true", dest="test", default=False, \
                            help=SUPPRESS_HELP if 1 else "Run a basic test")


    # SAXS curve restrains
    saxs_opts_group = OptionGroup(parser, "SAS controls")
    parser.add_option_group(saxs_opts_group)

    saxs_opts_group.add_option("--saxs-data", action="store", type="string", dest="saxs_data", \
                            default=None, help=SUPPRESS_HELP, metavar="FILENAME")

    saxs_opts_group.add_option("--sas-data", action="store", type="string", dest="sas_data", \
                            default=None,
                            help="Experimental SAXS/SANS curve for scoring tentative models. "
                                    "Uses FoXs on Linux systems (SAXS only), otherwise requires CRYSOL/CRYSON", \
                            metavar="FILENAME")

    saxs_opts_group.add_option("--crysol", action="store_true", dest="crysol", default=False, \
                            help="Use CRYSOL instead of a local FOXS copy (not available on Mac) "
                                    "NOTE: CRYSOL must be available from command line")

    saxs_opts_group.add_option("--cryson_opts", action="store", dest="cryson_opts", type="string", default=None, \
                            help="Use CRYSON for neutron scattering data. This option should define all"
                            " additional cryson options defining your sample like D2O "
                            " and sample perdeuteration fractions e.g. '-D2O 0.5 -per 1.0'"
                            " NOTE: CRYSON must be available from command line")


    # IO opts
    io_opts_group = OptionGroup(parser, "IO controls")
    parser.add_option_group(io_opts_group)
    io_opts_group.add_option("--project_dir", action="store", dest="project_dir", type="string",
                            metavar="DIRNAME", \
                            help="Change default project directory name", \
                            default=None)


    io_opts_group.add_option("--freeze", action="store", type="string", dest="freeze", \
                            default=None,
                            help="Keep all fragment with selected nucleotides frozen\n"
                                    " [e.g. --freeze=12:25,30:39]", metavar="SELECTION")

    # EXPERT opts
    expert_opts_group = OptionGroup(parser, "Expert parameters",
                                            "Defaults are optimal in most of "
                                            " the cases. Enable with -e/--expert")
    parser.add_option_group(expert_opts_group)








    expert_opts_group.add_option("--n_threads", "-n", action="store", type="int", \
                            dest="threads_number", default=4, metavar="4",\
                            help=SUPPRESS_HELP if not expert else "Number of threads [default: %default]")

    expert_opts_group.add_option("--local_db", action="store", type="string", dest="local_db", \
                            default=os.path.join(ROOT, 'local_db'),
                            help=SUPPRESS_HELP if not expert else "Path to the fragment database directory"
                                    " [default: %default]", metavar="PATH")


    expert_opts_group.add_option("--refine_kT", action="store", type="float", \
                            dest="refine_kT", default=2.0, metavar="FLOAT",\
                            help=SUPPRESS_HELP if not expert else "Simulated annealing kT value. "
                                 "If input model is a rock-solid xtal structure you may "
                                 "want to meake it really big e.g. 50. But "
                                 "kT<=5 should be fine on most of the cases"
                                    " [default: %default]")


    expert_opts_group.add_option("--minmax_kT", action="store", type="string", \
                            dest="minmax_kT", default="1,10", metavar="MIN,MAX", \
                            help=SUPPRESS_HELP if not expert else "Defines minimum and maximum temparature "
                                 "for replica exchange simulation."
                                    " [default: %default]")


    expert_opts_group.add_option("--wx", action="store", type="float", \
                            dest="blobfit_weight", default=0.5, metavar="FLOAT",\
                            help=SUPPRESS_HELP if not expert else "Weight of the experimental restraints in the scoring function "
                                    "[default: %default]")





    expert_opts_group.add_option("--sync", action="store_true", dest="sync", default=False, \
                            help=SUPPRESS_HELP if not expert else "Sync local list of RNA fragments"
                                    " with RNA Bricks2 database (updated once a week)")

    expert_opts_group.add_option("--steps", action="store", type="int", dest="steps", default=100, metavar="100",\
                            help=SUPPRESS_HELP if not expert else "Number of MCMC refinement or"
                                    " replica exchange steps [default: %default]")

    expert_opts_group.add_option("--fast", "-f", action="store_true", dest="fast", default=False, \
                            help=SUPPRESS_HELP if not expert else "Use no more that 10 frags for each node [default: 100]."
                                    " For testing purposes only.")





    expert_opts_group.add_option("--start-model", action="store", type="string", dest="start_project", \
                            default=None,
                            help=SUPPRESS_HELP if not expert else "Path to a json file with dictionary defining starting"
                                    " nodes configuration [default: %default]", metavar="FILENAME")


    expert_opts_group.add_option("--save-decoys", action="store", type="float", \
                            dest="save_decoys_fraction", default=0.0, metavar="FLOAT",\
                            help=SUPPRESS_HELP if not expert else "Save given fraction of lowest-energy replica exchange "
                                    "decoys in PDB format [default: %default]")


    expert_opts_group.add_option("--rg", action="store", type="float", \
                            dest="rg_user", default=None, metavar="FLOAT",\
                            help=SUPPRESS_HELP if not expert else "Target structure radius of gyration "
                                    "[default: %default]")





    expert_opts_group.add_option("--reference-model", action="store", type="string", dest="reference_model_fname", \
                            default=None,
                            help=SUPPRESS_HELP if not expert else "Reference model to calculate plot of rmsd vs energy.", \
                            metavar="FILENAME")

    expert_opts_group.add_option("--verbose", "-v", action="store_true", dest="verbose", default=False, \
                            help=SUPPRESS_HELP if not expert else "Print lots of useless stuff on output" )

    expert_opts_group.add_option("--debug", "-d", action="store_true", dest="debug", default=False, \
                            help=SUPPRESS_HELP if not expert else "Prints even more useless stuff on output" )

    expert_opts_group.add_option("-e", "--expert", action="store_true", dest="expert", default=None, \
                  help=SUPPRESS_HELP)

    #enables various opts that restric usage of resources when started from the webservice (e.g. seqin/pdb length)
    expert_opts_group.add_option("--webservice", action="store_true", dest="webservice", default=False, \
                            help=SUPPRESS_HELP)


    # DUMMY opts
    dummy_opts_group = OptionGroup(parser, " DUMMY atom fitting, not tested well enough...")
    #parser.add_option_group(dummy_opts_group)




    # fit model to a map or a structure (dummy/full-atom)
    dummy_opts_group.add_option("--mapin", action="store", dest="mapin", type="string", \
                            metavar="FILENAME", \
                            help="Target map in CCP4 format", \
                            default=None)

    dummy_opts_group.add_option("--refin", action="store", dest="refin", type="string",
                            metavar="FILENAME", \
                            help="Target model in PDB format (full-atom or dummy, anything works)", \
                            default=None)

    dummy_opts_group.add_option("--dummy", action="store", dest="dummy_ofname", type="string",
                            metavar="FILENAME", \
                            help="Write a PDB file with the dummy model (if applicable)"
                                    "It is generally a good idea to have a look at that.", \
                            default=None)

    dummy_opts_group.add_option("--dummy_xyz", action="store", dest="dummy_xyz", type="string",
                            metavar="X,Y,Z", \
                            help="Start growing dummy atom at specific position (works with mapin only)"
                                    "It can be used to select specific blob."
                                    " May not wotk if the input density is almost flat and continuous.", \
                            default="")



    dummy_opts_group.add_option("--target_model", action="store", dest="target_model", type="string",
                            metavar="PDB.CHAIN", \
                            help="Exclude selected chain and similar ones from fragments pool", \
                            default="")




    (options, _args)  = parser.parse_args()

    return (parser, options)


# -----------------------------------------------------------------------------


def prepare_project_dir(input_fname=None, project_dir=None):
    """
        prepare tmp directory for the project based on the input filename
        or user-provided project_dir name
        use numerical suffix to avoid overriding existing ones
    """

    assert [input_fname, project_dir].count(None)<2

    if not project_dir:
        project_dir = "rms_"+os.path.basename(input_fname).replace('.', '_')

    if os.path.exists(project_dir):
        idx=1
        while os.path.exists(project_dir+"_%i"%idx):
            idx+=1
        project_dir = project_dir+"_%i"%idx

    try:
        os.mkdir(project_dir)
        print(" ==> Created new project dir at %s" % project_dir)
        print()
    except:
        print(" --> Failed to create project dir. Check your settings")


    return project_dir


# -----------------------------------------------------------------------------

def process_blobfit_input(mapin=None, refin=None, n_residues=None, dummy_ofname=None, dummy_xyz=None):

    assert n_residues is not None, Exception("Blobfit.__init__: number of target residues missing")

    assert [mapin,refin].count(None) > 0, Exception("Blobfit.__init__: cannot process mapin and refin simultaneously")

    if mapin:
        bfit = blobfit(map_fname=mapin, debug=False)

    else:
        with open(refin, 'r') as ifile:
            bfit = blobfit(pdb_string=ifile.readlines(), debug=False, debug_ofname_base=None)

    # following step may take some time. that's why it's good to make it only once
    bfit.scorch(na_number=n_residues, seed_xyz=dummy_xyz, debug=False)
    if dummy_ofname: bfit.dummy_model.write_pdb_file( dummy_ofname )
    return bfit



# -----------------------------------------------------------------------------

def test():
    return 0

# -----------------------------------------------------------------------------

def main():

    print(rnamasonry_header)

    (parser, options) = parse_args()

    if options.expert:
        (parser, options) = parse_args(expert = options.expert)
        parser.print_help()
        print()
        exit(0)

    if not options.debug:
        sys.tracebacklimit=0

    if len(sys.argv)>1:
        time_started = datetime.now()
        print(" ==> Time started %s\n" % time_started.strftime("%m/%d/%Y, %H:%M:%S"))
        print(" ==> Command line: rnamasonry %s" % (" ".join(sys.argv[1:])))
        print()
        print()


    if options.test:
        ret = test()
        return ret

    if options.sync:
        dbo = db_sync(db_dir = options.local_db, rb2_url=RNABRICKS2_URL)
        if  options.target_model and not len(options.target_model.split("."))==2:
            raise Exception("ERROR: please provide target model in a format PDB.CHAIN")


        try:
            dbo.sync_local_db(options.target_model)
        except:
            raise Exception("RNA Bricks error. Check internet connection and target_model details.")

        return(0)


    # check if CRYSOL is available
    if options.saxs_data or options.sas_data:
        # kept for backward compatibility
        if options.saxs_data:
            saxs_data_str = open(options.saxs_data, 'r').read()
        else:
            saxs_data_str = open(options.sas_data, 'r').read()

        co = pysaxs(foxs_path=None if (options.crysol or options.cryson_opts) else FOXS_PATH, \
                    cryson_opts=options.cryson_opts)

        v = co.check(with_dummy_data=True, debug=options.debug)
        if v:
            print(" --> Found compatible version of %s" % \
                    ("CRYSOL" if options.crysol else "CRYSON" if options.cryson_opts else "FOXS"))
        else:
            if options.cryson_opts:
                print(" ERROR: Got SANS data on input but CRYSON is not available")
            else:
                print(" ERROR: Got SAXS data on input but %s is not available"  % \
                        ("CRYSOL" if options.crysol else "FOXS"))
            exit(1)

    else:
        saxs_data_str = None



    if not options.input_file:
        parser.print_help()
        return(1)


    #global THREADS_NUMBER
    #THREADS_NUMBER = options.threads_number

    dry_run = False

    """
        one_chain_only:
            Each motif in resulting structure is separate chain - pass 0.
            Structure consist of one chain - default (1). Default option
            together with default option of cut_joint_nucleotides flag
            produces output structures dedicated for QRNA and SimRNA refinement.
    """

    try:
        kTmin,kTmax = list(map(locale.atof, options.minmax_kT.split(',')))
    except:
        print("rnamasonry error: --minmax_kT option requires two comma-separated FLOATS")
        exit(1)

    if options.start_project:

        try:
            with open(os.path.expanduser(options.start_project), 'r') as f:
                _branch_dict = json.loads(f.read())
        except:
            print(" --> Problem opening start model file, exitting...")
            exit(1)
        _b={}
        for k,v in list(_branch_dict.items()):
            _b[int(k)] = v
        branches = [_b]

        project_dir = prepare_project_dir(options.input_file, options.project_dir)

        start_dir = os.path.dirname(os.path.expanduser(options.start_project))
        os.mkdir( os.path.join(project_dir, "fake_fragments") )
        os.mkdir( os.path.join(project_dir, "input_fragments") )
        os.mkdir( os.path.join(project_dir, "fragment_objects") )

        for fname in glob.glob( os.path.join(start_dir, 'input_fragments', '*') ):
            shutil.copy( fname, os.path.join(project_dir, "input_fragments") )

        for fname in glob.glob( os.path.join(start_dir, 'fake_fragments', '*') ):
            shutil.copy( fname, os.path.join(project_dir, "fake_fragments") )

        for fname in glob.glob( os.path.join(start_dir, 'fragment_objects', '*') ):
            shutil.copy( fname, os.path.join(project_dir, "fragment_objects") )

    else:
        branches = []
        project_dir = prepare_project_dir(options.input_file, options.project_dir)


    # fast or not
    if options.fast:
        global MIN_MODERNA_NFRAGS
        MIN_MODERNA_NFRAGS = 10
        max_frag_counts = ((ft,10) for ft in FRAG_TYPES)
        options.steps=min(5, options.steps)
    else:
        max_frag_counts = ((ft, 100) if ft=='stem' else (ft,1500) for ft in FRAG_TYPES)




    args = {'max_clash_percentage' : 2.0, \
            'rmsd_distance' : 1.0, \
            'nr_list' : 'ignore', \
            'frag_types_rmsd' : {}, \
            'p_value' : 0.01, \
            'threshold_rmsd' : 1.0, \
            'cut_joint_nuc' : True, \
            'one_chain_only' : True, \
            'exclude_pdb_id' : None, \
            'local_db' : options.local_db, \
            'project_dir': project_dir, \
            'max_frag_counts' : dict(max_frag_counts),
            'freeze' : options.freeze, \
            'verbose' : options.verbose}

    # create tree object
    tv = TreeTraverser(options.input_file, debug=options.debug, threads_number=int(options.threads_number), **args)


    # blobfit; preprocess mapin/refin
    if options.mapin or options.refin:

        try:
            dummy_xyz = list(map(string.atof, options.dummy_xyz.split(',')))
        except:
            dummy_xyz = None


        blobfit_obj = process_blobfit_input(mapin=options.mapin, \
                                            refin=options.refin, \
                                            n_residues=len(tv.sequence), \
                                            dummy_ofname=options.dummy_ofname, \
                                            dummy_xyz = dummy_xyz)
    else:
        blobfit_obj = None



    # input is a PDB file: start with frags extracted from the input model (0th)
    if tv.ipdb:
        branches.append({})
        for i in range( len(tv.association_graph.nodes()) ): branches[0][i]=0

    mcmc_obj = MCMC(tv.association_graph, \
                    tv.cut_joint_nuc, \
                    tv.one_chain_only, \
                    ss = tv.input_dotbracket, \
                    seq = tv.sequence, \
                    rg = options.rg_user, \
                    project_dir = tv.project_dir, \
                    local_db = tv.local_db, \
                    blobfit_obj = blobfit_obj, \
                    blobfit_weight = options.blobfit_weight, \
                    saxs_data_str = saxs_data_str, \
                    foxs_path = None if (options.crysol or options.cryson_opts) else FOXS_PATH, \
                    cryson_opts = options.cryson_opts)



    if not branches:

        if not dry_run:
            try:
                branches = tv.build()
            except:
                print(traceback.format_exc())
                sys.exit()

            for i in range( int(options.threads_number) ):
                tv.queue.put(None)

        print()

        # store starting models in the project directory
        print(" --> Found %i starting structures with reasonable geometry" % len(branches))

        branches_with_E = sorted([(_b, mcmc_obj.get_score(_b)) for _b in branches], key=lambda x: x[1], reverse=False)
        for i,(_b, _e) in enumerate(branches_with_E):
            print("        %-3i: E=%.2f"% (i+1, 0 if _e is None else _e))

    else:
        print(" --> Processing input starting structure")


    print()

    for _i,b in enumerate(branches):
        with open(os.path.join(project_dir, 'start_model_%i.json' % _i), 'w') as f:
            f.write(json.dumps(b))

        _branch = tv.build_branch_from_dictionary(b, tv.association_graph, force_build=True)
        save_branch_to_disk(_branch, os.path.join(project_dir, 'start_model_%i.pdb'%_i))



    if options.refine:

        decoys = mcmc_obj.refine(branches[0], max_steps=options.steps, inpkT=options.refine_kT, fix_stems=False, verbose=True)

        d_min = sorted(decoys, key=lambda x:x[1])[0]

        mcmc_obj.dump_branch(d_min[0],
                             os.path.join(project_dir,"final_model_refined.pdb"),
                             energy=d_min[1],
                             verbose=False)


        for idecoy, decoy in enumerate(sorted(decoys, key=lambda x:x[1])):

            if idecoy > (options.steps * options.save_decoys_fraction): break

            mcmc_obj.dump_branch(decoy[0], os.path.join(project_dir,"decoy_%05i.pdb" % idecoy), \
                                        energy=decoy[1], verbose=False)




    else:

        if options.debug:
            import cProfile, pstats, io
            pr = cProfile.Profile()
            pr.enable()

        re_obj = ReplicaExchange(tv, \
                                 branches[0], \
                                 threads=options.threads_number, \
                                 kTmin=kTmin, \
                                 kTmax=kTmax, \
                                 max_steps=options.steps, \
                                 rg = options.rg_user, \
                                 save_decoys_fraction=options.save_decoys_fraction, \
                                 blobfit_obj = blobfit_obj, \
                                 blobfit_weight = options.blobfit_weight, \
                                 saxs_data_str = saxs_data_str, \
                                 foxs_path=None if (options.crysol or options.cryson_opts) else FOXS_PATH, \
                                 cryson_opts = options.cryson_opts, \
                                 fix_stems = False)

        if options.reference_model_fname:
            re_obj.run( os.path.expanduser(options.reference_model_fname) )
        else:
            re_obj.run()


        # do the refinement with all the stems and loops available

        mcmc_obj = MCMC(tv.association_graph, \
                        tv.cut_joint_nuc, \
                        tv.one_chain_only, \
                        ss = tv.input_dotbracket, \
                        seq = tv.sequence, \
                        rg = options.rg_user, \
                        project_dir = tv.project_dir, \
                        local_db = tv.local_db, \
                        blobfit_obj = blobfit_obj, \
                        blobfit_weight = options.blobfit_weight, \
                        saxs_data_str = saxs_data_str, \
                        foxs_path = None if (options.crysol or options.cryson_opts) else FOXS_PATH, \
                        cryson_opts = options.cryson_opts)

        decoys = mcmc_obj.refine(re_obj.min_e_decoy[0], max_steps=100, inpkT=options.refine_kT, fix_stems=False, verbose=True)

        d_min = sorted(decoys, key=lambda x:x[1])[0]

        final_model_fname = os.path.join(project_dir,"final_model_refined.pdb")

        mcmc_obj.dump_branch(d_min[0], final_model_fname, energy=d_min[1], verbose=False, renumber_resseqs_from_one=True)

        print()
        print(( " ==> Wrote final model: %s" % os.path.expanduser(final_model_fname) ))
        print()

        if 0:#options.debug:
            pr.disable()
            s = io.StringIO()
            sortby = 'cumulative'
            ps = pstats.Stats(pr, stream=s).sort_stats(sortby)
            ps.print_stats()
            print(s.getvalue())




# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------

if __name__=="__main__":
    start_time = datetime.now()

    ret = main()

    if not ret:
        print()
        td = (datetime.now() - start_time)
        print("     Elapsed time %02i:%02i:%02i" % (td.total_seconds()//3600,
                                                   (td.total_seconds()%3600)//60,
                                                   td.total_seconds()%60))

        print(" ==> Time finished %s" % datetime.now().strftime("%m/%d/%Y, %H:%M:%S"))
        print()
        print("Normal termination of RNA Masonry")

