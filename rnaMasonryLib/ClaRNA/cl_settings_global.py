"""
@author walen
"""
import os
CLARNA_DIR = os.path.dirname(os.path.abspath(__file__))
CACHE_DIR = os.path.dirname(os.path.abspath(__file__))+"/.cache"
DATA_DIR = os.path.dirname(os.path.abspath(__file__))+"/gc-data"

PYTHON_CMD = "/usr/bin/python"
if os.path.isfile("/opt/local/bin/python"):
    PYTHON_CMD = "/opt/local/bin/python"

RNAVIEW_HOME=CLARNA_DIR + "/RNAVIEW"
MCA_HOME=CLARNA_DIR + "/MC-Annotate"
FR3D_HOME=CLARNA_DIR + "/FR3D"

MODERNA_STACKINGS_HOME = os.path.dirname(os.path.abspath(__file__))
