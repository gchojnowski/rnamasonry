#! /usr/bin/env libtbx.python
# -*- coding: utf-8 -*-
# =============================================================================
# Created at:
#  International Institute of Molecular and Cell Biology in Warsaw, Poland
# Author:
#  Grzegorz Chojnowski - gchojnowski@genesilico.pl, gchojnowski@gmail.com
# =============================================================================


import urllib.request, urllib.error, urllib.parse, urllib.request, urllib.parse, urllib.error
import string
import os, sys
from io import StringIO
import gzip
import json
import tarfile

try:
    from rnamasonry_config import *
except:
    sys.path.append("..")
    from rnamasonry_config import *


sys.path.append(os.path.join(ROOT, "lib"))

from progressbar import ProgressBar, Percentage, Bar, ETA
from motifs import Branch
from ph_parser import ph_parser


from dbutils import get_fobj_from_database, ls_database, update_gzip


RNABRICKS2_URL = "http://iimcb.genesilico.pl/rnabricks2"


# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------


class db_sync:


    def __init__(self, db_dir = 'local_db', rb2_url=RNABRICKS2_URL):
        self.db_dir = db_dir
        self.rb2_url = rb2_url

        if not os.path.exists(self.db_dir):
            os.mkdir(self.db_dir)


    # -------------------------------------------


    def get_pdb(self, fid):

        try:

            url = urllib.request.urlopen(url="%s/site_media/pdb/frags/%s.pdb.gz" % \
                                        (self.rb2_url, fid) )

            url_f = StringIO(url.read())
            gzipfile = gzip.GzipFile(fileobj=url_f)
            data = gzipfile.read()
        except:
            url = urllib.request.urlopen(url="%s/fragments/get_fragment_by_id/frag_id=%s" % \
                                        (self.rb2_url, fid) )

            data = url.read()

        return data


    # -------------------------------------------


    def download_list_if_medoids(self, target_model=""):

        print(" --> Downloading recent list of medoids")
        url = urllib.request.urlopen( url="%s/api/get_medoids_list/%s" % (self.rb2_url, target_model) )


        data = []
        for line in url.readlines():
            d = line.decode('utf-8').split()
            ftype = d[1]
            fid = d[0]
            strings  = '_%s_'%'_'.join([str(len(_)) for _ in [_f for _f in d[2].split('_') if _f]])

            data.append( (ftype, strings, fid) )

        return sorted(data, key=lambda x:x[1])


    # -------------------------------------------


    def sync_local_db(self, target_model):


        db_info = []
        broken_fids = []

        try:
            nfrags=0
            with open(os.path.join(self.db_dir,'db_info'), 'r') as f:
                for line in f.readlines():
                    if line.startswith("#"): broken_fids.append( line.split()[-1] )
                    nfrags+=1
        except:
            nfrags = 0

        data = self.download_list_if_medoids(target_model)

        new_frags_to_check = len(data)-nfrags
        if not new_frags_to_check:
            print()
            print(" ==> Nihil novi, sorry!")
            return

        widgets = [' --> Downloading new fragments [%i]: '%new_frags_to_check, \
                                Percentage(), ' ', Bar(), ' ', ETA()]

        files_in_db = ls_database()

        pbar = ProgressBar(widgets=widgets, maxval=len(data), fd=sys.stdout).start()

        new_count = 0
        new_data = {}

        for idx,item in enumerate(data):

            pbar.update(idx+1)
            ftype = item[0]
            fid = item[2]
            path = os.path.join(self.db_dir, ftype)

            fname = os.path.join( ftype, 'structure_%s.pdb'%str(fid) )

            if fid in broken_fids:
                db_info.append( "#%s" % (" ".join(item)) )
                continue

            if fname in files_in_db:
                db_info.append( " ".join(item) )
                continue

            if os.path.isfile(path+'/structure_'+str(fid)+'.pdb'):
                db_info.append( " ".join(item) )
                continue

            # gchojnowski: skip frags previously identified broken frags
            if os.path.isfile( os.path.join(self.db_dir, "broken", 'structure_%s.pdb' % str(fid)) ):
                db_info.append( "#%s" % (" ".join(item)) )
                continue

            s=self.get_pdb(fid)
            _ph = ph_parser().string2model(s.decode('utf-8'), reject_broken=True)

            if _ph:
                new_data[fname] = _ph.as_pdb_string()
                db_info.append(" ".join(item))
            else:
                #print '  FUBAR residue, skipping fragment [%s]' % fid
                db_info.append("#%s" % (" ".join(item)))
                continue

            new_count += 1

        # update DB .dat files
        update_gzip(new_data=new_data)

        with open(self.db_dir+'/db_info','w') as handle:
            handle.write("\n".join(db_info))

        if new_count>0: print(" --> Successfully added %s new fragments\n" % str(new_count))


# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------


def tests(fid = 40287, ofname = None):

    # test the pdb_string parser and cleaner

    dbo = db_sync(db_dir = 'local_db')

    #with open("25341.pdb", "r") as fin:
    #    s = fin.read()
    s=dbo.get_pdb(fid)
    print(s)
    _ph = ph_parser().string2model(s)
    print(fid)

    _ph.write_pdb_file("xxx.pdb")

    #self.mutate.mutate2seq(hierarchy.only_chain(), target_seq = self.sequence)





    if ofname:
        _ph.write_pdb_file(ofname)



def main():
    dbo = db_sync(db_dir = 'local_db')
    dbo.sync_local_db()

if __name__=="__main__":


    if len(sys.argv)>1:
        tests(fid=sys.argv[1])
    else:
        main()
