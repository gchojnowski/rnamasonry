#! /usr/bin/env python
# -*- coding: utf-8 -*-
# =============================================================================
# Created at:
#  International Institute of Molecular and Cell Biology in Warsaw, Poland
# Author:
#  Grzegorz Chojnowski - gchojnowski@genesilico.pl, gchojnowski@gmail.com
# =============================================================================

import sys


try:
    from rnamasonry_config import *
except:
    sys.path.append("..")
    from rnamasonry_config import *

# -------------------- K2N ---------------------------------------------------
sys.path.append(os.path.join(ROOT, "rnaMasonryLib", "k2n_standalone"))

from knots import opt_all, num_bps
from rna2d import Pairs
# -------------------- K2N ---------------------------------------------------



# -----------------------------------------------------------------------------

class nested_ss:


    def __init__(self):
        pass


    # -------------------------------------------------------------------------


    def get_nested_dotbracket(self, inp_basepairs, inp_sequence):

        bps = inp_basepairs
        dotbracket = []
        while True:
            nested = self.get_nested(bps)
            rejected = []
            dotb = ['.']*len(inp_sequence)
            for bp in bps:
                if bp in nested:
                    dotb[bp[0]] = '('
                    dotb[bp[1]] = ')'
                else:
                    rejected.append(bp)
            dotbracket.append("".join(dotb))
            if not rejected: break
            bps = rejected

        return dotbracket


    # -------------------------------------------------------------------------


    def get_nested(self, inp_basepairs):

        """
            helper function, removes pseudoknots
        """


        def get_k2n_pairs(basepairs):
            pairs = Pairs()
            resi_list = []
            for bp in basepairs:
                if not bp[0] in resi_list and not bp[1] in resi_list:
                    pairs.append(bp)
                    resi_list.append(bp[0])
                    resi_list.append(bp[1])
                else:
                    pass
            pairs = pairs.directed()
            pairs.sort()
            return pairs

        def filter_knotted_pairs(basepairs, nested_basepairs):
            nested_structure = []
            for bp in basepairs:
                if bp in nested_basepairs:
                    nested_structure.append(bp)
            return nested_structure


        def get_ss(chain_length, basepairs):
            ss = ['.']*chain_length
            for resi_1, resi_2 in basepairs:
                if resi_1>resi_2:
                    resi_1, resi_2 = resi_2, resi_1
                ss[resi_1]='('
                ss[resi_2]=')'
            return "".join(ss)

        # ---- get nested
        nested_result = opt_all(get_k2n_pairs(inp_basepairs),\
                                goal='max', scoring_function=num_bps,\
                                return_removed=True)
        nested_structure = filter_knotted_pairs(inp_basepairs, list(nested_result)[0][0])

        return nested_structure

# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------

