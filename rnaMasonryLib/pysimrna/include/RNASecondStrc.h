/*
 * RNASecondStrc.h
 *
 *  Created on: 2011-07-25
 *      Author: Michal Boniecki
 */

#ifndef RNASecondStrc_H_
#define RNASecondStrc_H_


#include <iostream>
#include <list>

using namespace std;

typedef struct{

    int nucl_index_1, nucl_index_2;
    int chain_index_1, chain_index_2;

} T_WW_Pair;


class RNASecondStrc{

private:

    int parseSSLine_appendData(const char** splittedLineToParse);

protected:

    list<T_WW_Pair> v_WW_Pairs; // ()
    list<T_WW_Pair> v_WW_Repulsing_Nucls; //x //not active, to be implelemted
    list<T_WW_Pair> v_BaseBaseSwichedOff_Nucls; //o //not active, to be implelemted
    list<T_WW_Pair> v_StackingSwitchedOff_Nucls; //| //not active, to be implelemted

//    list<int> v_WW_void_Nucls;
//    list<int> v_Stacking_void_Nucls;

public:

    list<T_WW_Pair> &get_list_WW_Pairs();

    RNASecondStrc(const char *filename);
    //XXX gchojnowski
    RNASecondStrc(const char *filename, std::string ss_string);
    void RNASecondStrc_string(const char *filename, std::string ss_string);
    ~RNASecondStrc();

};

#endif
