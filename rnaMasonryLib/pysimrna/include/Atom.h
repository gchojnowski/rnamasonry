/*
 * Atom.h
 *
 *  Created on: 2009-11-03
 *      Author: Konrad Tomala
 *		Michal Boniecki
 */

#include "Dye.h"

#ifndef ATOM_H_
#define ATOM_H_

class Atom {
//class represents one atom of any type
// class will surely grow in the future
public:
	Point3D coord;
	Point3D coordOfMinE;
	Point3D coordBackup;
	Point3D *coordInitial_ptr;
	char atomName[5],nuclName[5],chainId;
	unsigned char fixed;
	int atomNumber, nuclNumber;
	double occupancy, bfactor;
	double calcPositionConstraintsEnergyBefore();
	double calcPositionConstraintsEnergyAfter();
	Atom();
	~Atom();
};

#endif /* ATOM_H_ */
