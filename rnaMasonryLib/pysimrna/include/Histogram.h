/*
 * Histogram.h
 *
 *  Created on: 2009-11-03
 *      Author: Konrad Tomala
 *		Michal Boniecki
 */
/** @file Histogram.h
*
* @brief modified Histograms3D_ReadAndStore::Histograms3D_ReadAndStore
*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <fcntl.h>
#include <iostream>
#include <fstream>

#include "Point3D.h"

#define		LINE	200

#ifndef HISTOGRAM_H_
#define HISTOGRAM_H_


class Histogram {
// abstract class represents histogram of potential
public:
	char *name;
	float minX, maxX, binWidth;
	int numberOfBins;
	virtual void initialize(const char*);
	virtual void init_data_strc(FILE *inpfile, const char*) = 0;
	virtual void load_data_lines(FILE *inpfile, const char*) = 0;
	Histogram();
	virtual ~Histogram();
};

class Histogram1D: public Histogram {
// class represents 1D histogram
public:
	float* gridData;
//	void initialize(char*);
	float get_value(float in_x);
	float get_value_linear_extrapol(float in_x);
	void scale(float in_scale_factor);
	void init_data_strc(FILE *inpfile, const char*);
	void load_data_lines(FILE *inpfile, const char*);
	Histogram1D& operator=(const Histogram1D&);
	Histogram1D(const Histogram1D&);
	Histogram1D();
	~Histogram1D();
};

class Histogram2D: public Histogram {
	// class represents 2D histogram
public:
	float minY, maxY;
	float** gridData;
	float get_value(float in_x, float in_y);
	void scale(float in_scale_factor);
//	void initialize(char*);
	void init_data_strc(FILE *inpfile, const char*);
	void load_data_lines(FILE *inpfile, const char*);
	Histogram2D();
	~Histogram2D();
};

class Histogram3D: public Histogram {
	// class represents 3D histogram
private:
	void allocate_grid();
public:
	float minY, maxY, minZ, maxZ, maxValue;
	float*** gridData;
	float get_value(float in_x, float in_y, float in_z);
	float get_value(const Point3D &in_xyz);
	void scale(float in_scale_factor);
//	void initialize(char*);
	void init_data_strc(FILE *inpfile, const char*);
	void load_data_lines(FILE *inpfile, const char*);
	Histogram3D& operator=(const Histogram3D&);
	Histogram3D  operator+(const Histogram3D&);
	Histogram3D&  operator+=(const Histogram3D&);
	Histogram3D  operator*(float in_scale_factor);
	Histogram3D();
	~Histogram3D();
};

class Histograms3D_ReadAndStore {

public:

    Histogram3D **v_histogram_ptrs;
    int n_histograms;

    Histograms3D_ReadAndStore(std::string path_to_data, char* filename);
//    ~Histograms3D_ReadAndStore();

    Histogram3D *getHistogramPtrByName(const char *inp_name);
};

#endif /* HISTOGRAM_H_ */
