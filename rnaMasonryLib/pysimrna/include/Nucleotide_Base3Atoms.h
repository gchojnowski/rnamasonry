/*
 * Nucleotide_Base3Atoms.h
 *
 *  Created on: 2011-12-30
 *      Author: Michal Boniecki
*/

#include "Nucleotide.h"

#ifndef _NUCLEOTIDE_BASE3ATOMS_
#define _NUCLEOTIDE_BASE3ATOMS_

class Nucleotide_Base3Atoms : public Nucleotide {

private:

	void append_currPoint3DToFile(const char *filename, Point3D inpPoint); //for diagnostics, for testing
	void append_currPoint3DToFile(int that_baseType, Point3D inpPoint);    //for diagnostics, for testing
	double calc_WW_AberrancePenalty(double dist_C4_C4, double dist_N_N, double dist_B1_B1, double dist_B2_B2);

public:
	Atom B1, B2, middlePoint;
	Point3D localB1, localB2, localMiddlePoint;
	Point3D localB1_old, localB2_old;
//	Nucleotide_Base3Atoms *next, *prev;

	void initialize();
	char getVersionId();
	void reCalc();
	void calcMiddlePoint();
	void calcPairingVectors();
	void calcGlobalBaseFromLocal();
	void calcLocalBaseFromGlobal();
	void changeBaseConformer();
	int isBaseFixed();
	void confirm();
	void turnBack();
	void writeToTrajectory(FILE*);
	void writeToPDB(FILE*);
	void addConnectsToPDB(FILE*); //to change
	void addBondsForProfl(FILE*, int);
	void saveCoordinates();
	Point3D transfPointToLocalUsingParVectors(Point3D inpPoint);
	Point3D transfPointToLocalUsingParVectorsOld(Point3D inpPoint);
	void transfPointToLocalUsingParVectors(Point3D* outPoint, Point3D* inpPoint);
	void transfPointToLocalUsingParVectorsOld(Point3D* outPoint, Point3D* inpPoint);

	double calc_BaseBaseEnergyBefore(Nucleotide_Base3Atoms *that_nucl, int symmetryFlag);
	double calc_BaseBaseEnergyAfter(Nucleotide_Base3Atoms *that_nucl, int symmetryFlag);
	double calc_BaseC4_BaseP_EnergyBefore(Nucleotide_Base3Atoms *that_nucl);
	double calc_BaseC4_BaseP_EnergyAfter(Nucleotide_Base3Atoms *that_nucl);
	double calc_WW_AberranceEnergyBefore(Nucleotide_Base3Atoms *that_nucl, int symmetryFlag);
	double calc_WW_AberranceEnergyAfter(Nucleotide_Base3Atoms *that_nucl, int symmetryFlag);
	virtual int isInWWContact(Nucleotide_Base3Atoms *that_nucl, int symmetryFlag);
	Atom* getAtomPtr(const char* atomName);
};

#endif // _NUCLEOTIDE_BASE3ATOMS_
