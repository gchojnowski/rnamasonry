/*
 * Nucleotide.h
 *
 *  Created on: 2009-11-03
 *      Author: Konrad Tomala
 *		Michal Boniecki
 */

#include <string.h>

#include "Histogram.h"
#include "Restraint.h"
#include "EllipsoidGauss3D.h"
#include "RNAConformers.h"

#define		DUMMY			-1
#define		ADENINE			0
#define     	CYTOSINE		1
#define		GUANINE			2
#define		URACIL			3

#define		N_BASETYPES		4 //this doesn't include DUMMY, it should be like this

#define 	LOCAL_C5_POSITION_X -0.28
#define 	LOCAL_C5_POSITION_Y -1.7
#define 	LOCAL_C5_POSITION_Z -5.02
#define		DYE_SHIFT_1			 3.5
#define		DYE_SHIFT_2			 6.0
#define		DYE_ANGLE			 1.0472

#ifndef NUCLEOTIDE_H_
#define NUCLEOTIDE_H_

char conv_baseType2baseName(int inp_baseType);

class Nucleotide {
// class represents nucleotide, it has three atoms - phosphorus, carbon and nitrogen
public:
	int baseType; // A=0 C=1 G=2 U=3
	Dye* attachedDye;
	list<Restraint*> restraints;
	int number;
	Atom C4, P, N, redNitrogen;
	Point3D localN, localN_old;
	Nucleotide* myChain;
	Histogram3D **v_BaseBase_histogram_ptrs; //total histrogram which should a sum of BaseBase, CoPlanar, 3p_Stacking, 5p_Stacking
	Histogram3D *v_BaseC4_histogram_ptr;
	Histogram3D *v_BaseP_histogram_ptr;
	Histogram3D *ExVolume_histogram_ptr;
	EllipsoidGauss3D **v_WW_forSSRestraints_ellipsoid_ptrs;
	RNAConformers_ReadAndStore *conformers_store_ptr;
	double angleCold, angleCnew, anglePold, anglePnew;
	double distanceCPold, distanceCPnew, distancePCold, distancePCnew;
	double distanceFromInitial_old, distanceFromInitial_new;
	//double torsCold, torsCnew, torsPold, torsPnew;
	Point3D w1old, w2old, w3old, w1_par_old, w2_par_old, w3_par_old;
	Point3D w1new, w2new, w3new, w1_par, w2_par, w3_par;
	Point3D v1old, v2old, v1new, v2new;
	Nucleotide *next, *prev;

	int stiffnessFlag;
	double stiffnessValue;

	void initialize();
	virtual char getVersionId();
	virtual void reCalc();
	virtual void calcVectors();
	void initBaseCoordsFromName();
	virtual void calcPairingVectors();
	void calcGlobalNitrogenFromLocal();
	void calcLocalNitrogenFromGlobal();
	virtual void changeBaseConformer();
	virtual int isBaseFixed();
	virtual void set_stiffnessValue(double inp_stiffness);
	virtual double get_stiffnessValue();
	virtual int get_stiffnessFlag();
	void addDye();
	void saveLocalDyePosition();
	void countDyePosition();
	void calcLocalBasePositionFromGlobal();
	void recountDyePosition();
	void confirm();
	void turnBack();
	void writeToTrajectory(FILE*);
	void writeToPDB(FILE*);
	void addConnectsToPDB(FILE*);
	void addBondsForProfl(FILE*, int);
	void writeDyeToPDB(FILE*, int);
	void saveCoordinates();
	double calcAngleEta();
	double calcAngleTheta();
	double calc_BaseBaseEnergyBefore(Nucleotide*, int symmetryFlag);
	double calc_BaseBaseEnergyAfter(Nucleotide*, int symmetryFlag);
	double calc_WW_AberranceEnergyBefore(Nucleotide*, int symmetryFlag);
	double calc_WW_AberranceEnergyAfter(Nucleotide*, int symmetryFlag);
	virtual int isInWWContact(Nucleotide *that_nucl, int symmetryFlag);
	Atom* getAtomPtr(const char* atomName);
	int assignHistogramPtrs(Histograms3D_ReadAndStore *histograms_store);
	int assignEllipsoidPtrs(Ellipsoids_ReadAndStore *ellipsoids_store);
	void assignConformersPtr(RNAConformers_ReadAndStore **conformers_store);

	Nucleotide();
	~Nucleotide();
};

#endif /* NUCLEOTIDE_H_ */
