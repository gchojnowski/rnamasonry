/** @file read_pdb.h 
* @brief mod: T_read_pdb::read_file <br>
* added: T_read_pdb::parse_pdb_string
*/


#ifndef _READ_PDB_
#define _READ_PDB_

#define MAX_N_ATOMS_IN_PDB 16000 //it is not limiting constant any more, since chainging static tables to C++ vectors

#include"Atom.h"

#include <vector>


typedef struct
{
  int start_index;
  int n_atoms;
} T_residue_pointer;

typedef struct
{
  int start_index;
  int n_residues,n_residues_sum;
} T_chain_pointer;

class T_read_pdb
{
  protected:
    vector<Atom> v_atoms;
    vector<T_residue_pointer> v_residue_pointers;
    vector<T_chain_pointer> v_chain_pointers;
//    Atom v_atoms[MAX_N_ATOMS_IN_PDB]; //old ver
//    T_residue_pointer v_residue_pointers[MAX_N_ATOMS_IN_PDB]; //old ver
//    T_chain_pointer v_chain_pointers[MAX_N_ATOMS_IN_PDB];  //old ver
    int n_atoms,n_residues,n_chains,selected_chain;
    int fill_atom_strc(Atom *atom, char *inp_bufor);
    int split_into_names(char **v_names, const char *i_names);
  public:
    void display_line(int i_line);
    void display_residue_lines(int i_res);
    
    int get_atom(Atom *i_atom, int i_res, const char *i_name);
    int get_atom(Atom *i_atom, int i_res, const char *i_name, int i_chain);
    int get_atom_from_selected_chain(Atom *i_atom, int i_res, const char *i_name);
    
    int select_chain_for_reading(int i_chain);
    
    // XXX gchojnowski
    int read_file(const char *filename);
    int parse_pdb_string(const char *filename, std::string pdb_string);
    // XXX

    int get_n_atoms(){return n_atoms;}
    int get_n_chains(){return n_chains;}
    int get_n_residues(){return n_residues;}
    int get_n_residues_in_chain(int i_chain);
    char get_chain_id(int i_chain);
    
    void test_split_string(char *inp_string);
};

#endif
