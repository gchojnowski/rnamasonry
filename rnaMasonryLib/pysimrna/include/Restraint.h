/*
 * Restraint.h
 *
 *  Created on: 2009-11-10
 *      Author: Konrad Tomala
 *		Michal Boniecki
 */

#include "Atom.h"


#define		AFTER_MOVE			    0
#define		BEFORE_MOVE			    1
#define		ENERGY_RESTR_MIN		0.0
#define		RES5				1.0
#define		RESTRAINTS_RATIO		1.0

#define		SLOPE_RESTRS_MODE		1
#define		WELL_RESTRS_MODE		2

#define		SS_SEQ_SEPAR_WO_REDUCTION	10

#ifndef RESTRAINT_H_
#define RESTRAINT_H_

class Nucleotide;
class Nucleotide_Base3Atoms;

class Restraint {
// abstract class representing restraint of any type
protected:
	double startBorder;
	double endBorder;
	double weight;
	int restrsMode;
	double well(double);
	double slope(double);
	double calcFinal(double);
public:
	int firstNuclNum;
	int secondNuclNum;
	virtual void showRestrDist() = 0;
	virtual double calcEnergyAfter() = 0;
	virtual double calcEnergyBefore() = 0;
	virtual ~Restraint();
};

class AtomRestraint: public Restraint {
// class representing restraint between two objects of type (class) Atom
private:
	Atom* atom1;
	Atom* atom2;
public:
	void showRestrDist();
	double calcEnergyAfter();
	double calcEnergyBefore();
//	AtomRestraint(double minUnrestricted, double maxUnrestricted, int firstNucl, int secondNucl, Atom* a1, Atom* a2, double in_weight);
	AtomRestraint(double minUnrestricted, double maxUnrestricted, int firstNucl, int secondNucl, Atom* a1, Atom* a2, double in_weight, int in_restrsMode);
	~AtomRestraint();
};

class Point3DRestraint : public Restraint {
// class representing restraint between two objects of type (class) Point3D
private:
	Point3D *point1, *point1_before;
	Point3D *point2, *point2_before;
	char *point1_name, *point2_name;
public:
	void showRestrDist();
	double calcEnergyAfter();
	double calcEnergyBefore();
//	Point3DRestraint(double minUnrestricted, double maxUnrestricted, int firstNucl, int secondNucl, Point3D *p1, Point3D *p1_before, char *p1_name, Point3D *p2, Point3D *p2_before, char *p2_name, double in_weight);
	Point3DRestraint(double minUnrestricted, double maxUnrestricted, int firstNucl, int secondNucl, Point3D *p1, Point3D *p1_before, char *p1_name, Point3D *p2, Point3D *p2_before, char *p2_name, double in_weight, int in_restrsMode);
	~Point3DRestraint();
};

class SecStrcRestraint: public Restraint {
// class representing Watson-Cric base-pairing restraint
private:
#ifdef SimRNA3
	Nucleotide_Base3Atoms* nucl1_ptr;
	Nucleotide_Base3Atoms* nucl2_ptr;
#else
	Nucleotide* nucl1_ptr;
	Nucleotide* nucl2_ptr;
#endif
public:
	void showRestrDist();
	double calcEnergyAfter();
	double calcEnergyBefore();
//	SecStrcRestraint(int, int, double in_weight);
#ifdef SimRNA3
	SecStrcRestraint(int nuclIndex_1, Nucleotide_Base3Atoms* nucl1_ptr, int nuclIndex_2, Nucleotide_Base3Atoms* nucl2_ptr, double in_weight);
#else
	SecStrcRestraint(int nuclIndex_1, Nucleotide* nucl1_ptr, int nuclIndex_2, Nucleotide* nucl2_ptr, double in_weight);
#endif
	~SecStrcRestraint();
};

/*
class DyeRestraint: public Restraint {
	// class representing restraint between two dyes (attached to atoms)
private:
	Dye* dye1;
	Dye* dye2;
public:
	void showRestrDist();
	double calcEnergyAfter();
	double calcEnergyBefore();
	DyeRestraint(double,double,int, int, Dye*, Dye*);
	~DyeRestraint();
};
*/

#endif /* RESTRAINT_H_ */
