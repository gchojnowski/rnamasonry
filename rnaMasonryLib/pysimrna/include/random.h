/*
 * random.h
 *
 *  Created on: 2014-03-19
 *      Author: Michal Boniecki
 *              Grzegorz Lach
 *
 *  This is simple implementation of rand48, actually only srand48() and drand48().
 *  We need this because in parallel execution of SimRNA (using OpenMP) we can't rely on global (standard) drand48() function, because we loose reproducibility of data.
 *  In order to overcome that problem each separate simulation (class) has to have its own random number generator (to be really independent), for example as a class, it's done below.
 *  This generator is tested to generate same string of numbers as standard drand48().
*/

#ifndef RANDOM_H_
#define RANDOM_H_

const unsigned long long m = 1LL<<48; //m is acutally 2**48
const unsigned long long a = 0x5DEECE66DLL; // in decimal is 25214903917LL;
const unsigned long long c = 0xB; // in decimal is 11;

const unsigned long long low_16bits_init = 0x330E;

class Rand48{

private:
    unsigned long long curr_seed;

public:
    double drand48();
    void srand48(unsigned long long init_seed);
};

inline void Rand48::srand48(unsigned long long init_seed){

    curr_seed = low_16bits_init + (init_seed<<16);
}

inline double Rand48::drand48(){

    curr_seed = (a*curr_seed + c) % m;
    return double(curr_seed)/double(m);
}

#endif
