/*
 * RNAStructure.h
 *
 *  Created on: 2009-11-03
 *      Author: Konrad Tomala
 *		Michal Boniecki
 */
/** @file RNAStructure.h
*
* @brief mod: RNAStructure::createFromPDB <br>
* added: RNAStructure::createFromPDB_string <br>
* added: RNAStructure::cleanTabs <br>
* new var: path_to_data

*/

#include <fstream>

#include "Nucleotide.h"
#include "Nucleotide_Base3Atoms.h"
#include "Histogram.h"
#include "structures.h"
#include "RNAConformers.h"
#include "RNASecondStrc.h"
#include "EllipsoidGauss3D.h"
#include "random.h"
#include <float.h>

#define		CAPTION			30
//#define		HIST_1D_NUMBER	16
//#define		HIST_2D_NUMBER	1
//#define		HIST_3D_NUMBER	16

#define		DUMMY			-1
#define		ADENINE			0
#define		CYTOSINE		1
#define		GUANINE			2
#define		URACIL			3

#define		MAX_N_CHAINS		52 //from A to Z and from a to z

#define		MAX_ATOM_MOVE					0.9
#define  	MAX_TWO_ATOMS_MOVE				0.9
#define		MAX_FRAGMENT_DIRECTION_CHANGE_ATOMS_MOVE	0.9
#define		MAX_FRAGMENT_DIRECTION_CHANGE_SEQ_SEPARATION	40

#define		MAX_ENERGY		DBL_MAX
#define		DEFAULT_BOND_DIST	3.84
#define		DEFAULT_BOND_ANGLE  0.85
#define		CHAIN_SHIFT_Z		6.0
#define		MAX_RNA_SIZE		500
#define		RADIUS_POTENTIAL_RATIO 1.0

//excluded volume cut-off distances
#define		EX_VOLUME_P_P	5.00 //without neighbours
#define		EX_VOLUME_C4_C4	3.40 //without neighbours
#define		EX_VOLUME_N_N	3.50
#define		EX_VOLUME_P_N	4.40
#define		EX_VOLUME_P_C4	3.40 //without neighbours
#define		EX_VOLUME_C4_N	3.70
/*
#define		EX_VOLUME_B1_B1	3.5
#define		EX_VOLUME_B2_B2	3.5
#define		EX_VOLUME_B1_B2	3.5
#define		EX_VOLUME_B1_N	3.5
#define		EX_VOLUME_B2_N	3.5
*/
#define		EX_VOLUME_P_B1	3.0
#define		EX_VOLUME_P_B2	3.0
#define		EX_VOLUME_C4_B1	3.0
#define		EX_VOLUME_C4_B2	3.0
//---------------------------------
#define		EX_VOLUME_HARD_CORE_RATIO	0.7 //0.7 means that inner 70% of radius corresponds to hard core, while outher 30% is soft (penalty value linearly grows from 0 to max, that corresponds to hard core penatly)

#define		SQR_EX_VOLUME_P_P	17.64 //sqr(4.20)
#define		SQR_EX_VOLUME_C4_C4	10.89 //sqr(3.30)
#define		SQR_EX_VOLUME_N_N	12.25 //sqr(3.50)
#define		SQR_EX_VOLUME_P_N	19.36 //sqr(4.40)
#define		SQR_EX_VOLUME_P_C4	7.29  //sqr(2.70)
#define		SQR_EX_VOLUME_C4_N	13.69 //sqr(3.70)

#define		EX_VOLUME_PENALTY	6.0

#define		CUTOFF_PUSHING		1
#define		CONTACT_PUSHING     3.0
#define		CONTACT_PUSHING_SQR     9.0
#define		CARBON_CONTACT_DIST	135.0 //here name should be also SQR
#define		NITROGEN_CONTACT_DIST_SQR 225.0
#define     	BASES_CONTACT_DIST	289 //previos cutoff maybe was too small 317.0
#define		BASES_MIDDLE_POINT_DIST 225
//#define		ENERGY_PUSHING		3.0 //3.44

#define		TOLERANCE_MIN		3.0
#define		TOLERANCE_MAX		4.32

#define 	SAMPLE_NITROGEN_LOCATION 0
#define		MOVE_ONE_ATOM		1
#define		MOVE_TWO_ATOMS		2
#define		MOVE_FRAGMENT		3
#define		ROTATE_END		4
#define		ROTATE_INSIDE		5

#define		FRACTION_OF_NITROGEN_ATOM_MOVES	0.10
#define		FRACTION_OF_ONE_ATOM_MOVES	0.45
#define		FRACTION_OF_TWO_ATOMS_MOVES	0.44
#define		FRACTION_OF_FRAGMENT_MOVES	0.01

#define		RIGHT			    1
#define		LEFT			    0
#define 	FAULT				-1
#define 	END					0

#define		DEFAULT_OUTPUT			0
#define		OUTPUT_NAME			1
#define		SEQUENCE_INPUT			0
#define		PDB_INPUT			1
#define		PDB_LIST_FILE_INPUT		2

#define		MAX_FILENAME_LENGTH		512
#define		MAX_OUTPUT_SS_LINES 8

#undef		ONE_ATOM_MOVE__TABLE_EXCEEDS_CHECK         //if you want to turn it on change "#undef" to "#define"
#undef		ONE_NUCLEOTIDE_MOVE__TABLE_EXCEEDS_CHECK   //if you want to turn it on change "#undef" to "#define"

#define		BOND_LENGTHS_CORRECTION_AFTER_NUCL_MOVE
#define		BOND_CONVERG_TOLERANCE 		0.002

#define		LIMITED_BOND_LENGTH_DISTORSION_IN_ONE_MOVE
#define		LIMITED_BOND_LENGHTS
#define		MAX_ALLOW_BOND_LENGHT_DISTORSION 0.5

#define		BOND_P_C4_LOWER_BOUND	3.3
#define		BOND_P_C4_UPPER_BOUND	4.0
#define		BOND_C4_P_LOWER_BOUND	3.0
#define		BOND_C4_P_UPPER_BOUND	4.0


#ifndef RNAStructure_H_
#define RNAStructure_H_

class PairWiseEXVolume {
private:
    double outher_cutoff, inner_cutoff;
    double outher_minus_inner;
    double sqr_outher_cutoff, sqr_inner_cutoff;
    double energy_value;
public:
    PairWiseEXVolume(double inp_outher_cutoff, double inp_hard_core_ratio, double inp_energy_value);
    double getEnergyValueFromSqrDist(double inp_sqr_dist);
};

class RNAStructure {
// class represents RNA chain, it has array of nucleotides and array or pointers to all RNA atoms
private:
	Point3D centerOfMass;
	PairWiseEXVolume *PairWiseEXVolume_C4_C4, *PairWiseEXVolume_N_N, *PairWiseEXVolume_C4_N;
	PairWiseEXVolume *PairWiseEXVolume_P_N, *PairWiseEXVolume_P_C4, *PairWiseEXVolume_P_P;
#ifdef SimRNA3
	PairWiseEXVolume *PairWiseEXVolume_P_B1, *PairWiseEXVolume_P_B2;
	PairWiseEXVolume *PairWiseEXVolume_C4_B1, *PairWiseEXVolume_C4_B2;
#endif
    std::string path_to_data;
/*
#ifdef SimRNA3
	PairWiseEXVolume *PairWiseEXVolume_B1_B1, *PairWiseEXVolume_B2_B2, *PairWiseEXVolume_B1_B2;
	PairWiseEXVolume *PairWiseEXVolume_B1_N, *PairWiseEXVolume_B2_N;
#endif
*/

	void allocateTables();

	double angleEtaStiffnessFunc(double x){
	    return 6 + x*(-4.274735055019605 + (0.82975430331694 - 0.023779195041667797*x)*x);
	}
	
	double angleThetaStiffnessFunc(double x){
	    return 6 + x*(-2.239040864782529 + (-0.10453598251346082 + 0.0733529860088176*x)*x);
	}

protected:
//XXX gchojnowski
	double secondStrcWeight;
	double restraintsEnergy, restraintsEnergyOld;
	Rand48 myRand; //this is needed since REMC method is executed in parallel mode, using OpenMP. In this case every replica has to have its own random numb. generator, otherwise reproducibility is lost.
	unsigned long long randomSeed;
public:
    /** @brief new variable storing path (abs/rel) to SimRNA3-current
    * (XXX gchojnowski)
    */
    void set_path_to_data( std::string path );
    /** @brief new function that prunes all data relatred to a given RNA structure (NOT histograms) (XXX gchojnowski)*/
    void cleanTables();

	double radiusOfLimitingSphere, radiusOfLimitingSphereSqr, radiusOfLimitingSphereWeight;
	long int  /*it,*/ confirmedFirst, confirmedLater; /*, numberOfBest;*/
	int  basesInContactNumBefore,  basesInContactNumAfter;//, purineNCoordSize, pyrimidNCoordSize; to remove as replaces by RNAconformers
	int  numberOfNucleotides, numberOfNucleotides_with_dummies;
	struct Pair* basesInContactAfter;
	struct Pair* basesInContactBefore;
	struct Range* chains;
	int numberOfChains;
	int replicaId;
#ifdef SimRNA3
	Nucleotide_Base3Atoms* nucleotides;
#else
	Nucleotide* nucleotides;
#endif
//	Point3D* localNCoordPurine; 
//	Point3D* localNCoordPyrimidine; to remove as replaces by RNAconformers
	FILE* trajectoryFile;
	char* outputPDB;
	Atom** backbone;
	Atom** atoms;
	Histogram1D hist_distance_PC, hist_distance_CP;
	Histogram1D hist_angle_PCP, hist_angle_CPC;
//	Histogram1D hist_tors_CPCP, hist_tors_PCPC;
	Histogram2D hist_distance_CPCP_vs_eta, hist_distance_PCPC_vs_theta;
	Histogram2D hist_eta_vs_theta;
	Histograms3D_ReadAndStore *histograms_store;
	Ellipsoids_ReadAndStore *ellipsoids_store;
	RNAConformers_ReadAndStore **v_conformers_stores;

	double kT;
	int confirmCounter, writeCounter, writeFrequency;
	double chainEnergy, minEnergy;
	double chainEnergyOld;
	double pairingEnergy;
	double pairingEnergyOld;
	double excludedVolumeEnergy, excludedVolumeEnergyOld;
	double limitingSphereEnergy, limitingSphereEnergyOld;
	double locGeometryEnergy, locGeometryEnergyOld;
	double fractionOfOneAtomMoves, fractionOfTwoAtomsMoves, fractionOfNitrogenAtomMoves, fractionOfFragmentMoves;

	void (RNAStructure::*calcPartialChainEnergy)(Range);
	void (RNAStructure::*calcTotalChainEnergy)();
	int (RNAStructure::*chooseMove)();

	void createFromSequence(char*);

    /** @brief createFromPDB splitted into two: old filename-based and all the guts that can accept pdb_string
    * (XXX gchojnowski)
    */
    void createFromPDB_string(char *path, std::string pdb_string, int atomsConstraintsFlag, int verbose);

    /** @brief keept to maintain code integrity
    * (XXX gchojnowski)
    */
    void createFromPDB(char *path, int atomsConstraintsFlag);

    /** @brief get chainEnergy value
    * (XXX gchojnowski)
    */
    double getChainEnergy();

    void set_kT(double inp_kT);


	virtual void initializeChain(struct InputParam);
	int setCoordsFromTrascFile(FILE*);
	void initFromTrascFrame(char*);
	void initFractionsOfMoves(struct InputParam);

	void reWeightBonds(double bondsWeight);
	void reWeightAngles(double anglesWeight);
	void reWeightTorsAngles(double torsAnglesWeight);
	void reWeightCorrEtaTheta(double torsEtaTheta);
	void setLimitingSphereRadius(double limitingSphereRadius);
	void setLimitingSphereWeight(double limitingSphereWeight);

    /** @brief now uses abs path to the data dir  - path_to_data
    * (XXX gchojnowski)
    */
	void readHistograms();
	void initHistogramInfo();
	void readNitrogenSamplingData();
	void createProflBondsFile();
	void initializeOutput(char*);
	void initializeOutput(char*, int curr_ReplicaIndex);
	virtual void writeRNAStructure();
	virtual void saveBestStructure();
	void writeOutputToPDB();
	void writeSecondStrcToFile();
	virtual void terminateTasks();
	void displayEnergyOfTrascFrame();


	int chooseLocalMoves();
	int chooseAllMoves();
	struct Range moveChain();
	int getChainForMove();

	virtual void revertMove(struct Range);
	virtual void confirmMove(struct Range);

	Point3D getVector(double);
//	void oneAtomMove(int);
	Point3D getLocalNitrogenSample(int); //to remove?
	struct Range sampleNitrogenLocation();
	struct Range oneAtomMove();
	struct Range oneAtomMove(int);
	struct Range twoAtomsMove();
	struct Range twoAtomsMove_P_C4(int);
	struct Range twoAtomsMove_C4_P(int);
	struct Range fragmentMove();
//	struct Range oneNucleotideMove(); //to remove
//	struct Range oneNucleotideMove(int); //to remove
	struct Range rotateEnd(); //to remove
	struct Range rotateInside(); //to remove


//XXX gchojnowski
	void initSecondStrcRestraints(const char* path, float ssWeight, bool verbose);
	void initSecondStrcRestraints_string(const char* path, std::string ss_string, float ssWeight, bool verbose);

	void calcTotalRestraintsEnergy();

	void calcTotalBasePairingEnergy();
	void calcPartialBasePairingEnergy();
	double calcBasePairingEnergyBefore(int, int);
	double calcBasePairingEnergyAfter(int, int);

	double calcTotalShortStackingEnergy();

	void calcTotalExVolumeEnergy();
	void calcPartialExVolumeEnergy(struct Range);
	double calcExVolumeEnergyBefore(struct Range);
	double calcExVolumeEnergyAfter(struct Range);

	void calcTotalLocalGeomEnergy();
	void calcPartialLocalGeomEnergy(struct Range);
	double calcLocalGeomEnergyBefore(struct Range);
	double calcLocalGeomEnergyAfter(struct Range);

	void prepareOneChain();
	void prepareMultiChains();
	void calcCenterOfMass();
	void calcRadiusOfLimitingSphere();
	void calcRadiusOfGyrationParams();
	double limitingSpherePenalty(Point3D);
	void calcTotalRadiusGyrationEnergy();
	void calcPartialRadiusGyrationEnergy(struct Range);
	double calcRadiusGyrationEnergyBefore(struct Range);
	double calcRadiusGyrationEnergyAfter(struct Range);

	virtual void calcTotalBasicChainEnergy();
	virtual void calcPartialBasicChainEnergy(struct Range);
	virtual void calcTotalExtendedChainEnergy();
	virtual void calcPartialExtendedChainEnergy(struct Range);
	virtual void showEnergyDetails();

	int makeDecision();
	void simulationStep();

	int findChainIndex(char chainId);
	int findNuclIndex(int chainIndex, char *str_NuclNumber);

	RNAStructure();
//	RNAStructure(int);
	~RNAStructure();
};

class RNAStructureWithRestraints: public RNAStructure {

protected:
	double restraintsEnergy, restraintsEnergyOld;
	double restraintsEnergy_minEnergy;
	double secondStrcWeight;
public:
	void initializeChain(struct InputParam);
	void terminateTasks();

	virtual void writeRNAStructure();
	virtual void saveBestStructure();

	void initDistanceRestraints(const char* path);
	void initDyeRestraints(const char* path);
	void initSecondStrcRestraints(const char* path);
//	int countAtomNum(int, char*); --- to remove
	void calcRestraintsEnergy(struct Range);
	void calcTotalRestraintsEnergy();
	double calcRestraintsEnergyBefore(struct Range);
	double calcRestraintsEnergyAfter(struct Range);
	void checkRestraintsDist();
	virtual void confirmMove(struct Range);
	virtual void revertMove(struct Range);
	virtual void calcTotalBasicChainEnergy();
	virtual void calcPartialBasicChainEnergy(struct Range);
	virtual void calcTotalExtendedChainEnergy();
	virtual void calcPartialExtendedChainEnergy(struct Range);
	virtual void showEnergyDetails();
};

#endif /* RNAStructure_H_ */
