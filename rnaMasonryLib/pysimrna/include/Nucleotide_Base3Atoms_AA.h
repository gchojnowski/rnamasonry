/*
 * Nucleotide_Base3Atoms_AA.h
 *
 *  Created on: 2013-03-28
 *      Author: Michal Boniecki
*/

#include "Nucleotide_Base3Atoms.h"
#include "Point3D.h"
#include <float.h>
#include <vector>

#ifndef _NUCLEOTIDE_BASE3ATOMS_AA_
#define _NUCLEOTIDE_BASE3ATOMS_AA_

class Nucleotide_Base3Atoms_AA : public Nucleotide_Base3Atoms {

public:
	vector<Atom> v_backboneAtoms;
	vector<Atom> v_baseAtoms;

	RNAConformers_ReadAndStore *backbone_conformers_store_ptr;
	RNAConformers_ReadAndStore *baseCoords_selfLC_store_ptr;

	void init_typeRelated(Nucleotide_Base3Atoms *inp_nucl);
	void init_coordsRelated(Nucleotide_Base3Atoms *inp_nucl);

	void fitBackboneAtoms();
	void restoreBaseAtoms();
	void assignAtomsProperties();
	int renumberAtoms(int initNumber);
	void writeToPDB(FILE *fp);

	Point3D calcGlobalPointFromLocal(Point3D inpPoint);
	Point3D calcLocalPointFromGlobal(Point3D inpPoint);

	Point3D calcGlobalPointFromLocalUsingParVectors(Point3D inpPoint);
};

#endif // _NUCLEOTIDE_BASE3ATOMS_AA_
