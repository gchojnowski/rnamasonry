/*
 * Dye.h
 *
 *  Created on: 2009-11-10
 *      Author: konrad
 */

#include "Point3D.h"

#ifndef DYE_H_
#define DYE_H_

struct Dye {
	Pair helicsBegin;
	Pair helicsEnd;
	Point3D locCoord;
	Point3D globCoord;
	Point3D globCoordOld;
};

#endif /* DYE_H_ */
