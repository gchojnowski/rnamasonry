/*
 * RNAConformers.h
 *
 *  Created on: 2012-01-27
 *      Author: Michal Boniecki
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "Point3D.h"
#include "random.h"

#include <vector>

#ifndef RNACONFORMERS_H_
#define RNACONFORMERS_H_

const double minEta = 0; //because X, Y it considers angles, they vary from 0 to 2*M_PI
const double minTheta = 0;
const double maxEta = 2*M_PI;
const double maxTheta = 2*M_PI;

const double default_bbAngleEta = 2.9;
const double default_bbAngleTheta = 3.7;

class RNAConformer{

protected:

    vector<Point3D> v_points;
    double bbAngleEta, bbAngleTheta;

public:

    RNAConformer(Point3D *v_points, int n_points);
    RNAConformer(Point3D *v_points, int n_points, double in_bbAngleEta, double in_bbAngleTheta);

    vector<Point3D> *getPointsPtr();
    double get_bbAngleEta();
    double get_bbAngleTheta();
};

class RNAConformers_ReadAndStore{

private:

    int calc_i_bbAngleEta(double curr_bbAngleEta);
    int calc_i_bbAngleTheta(double curr_bbAngleTheta);

protected:

    vector<RNAConformer*> all_conformer_ptrs;
    vector<RNAConformer*> **v2_conformer_ptrs_binned;

    int numberOfEtaBins, numberOfThetaBins;
    double widthBinEta, widthBinTheta;

    Rand48 myRand; //this is important for parallel (OpenMP) execution in Replica Exchange scheme

public:

    RNAConformers_ReadAndStore(const char *filename);
    RNAConformers_ReadAndStore(const char *filename, int nEtaBins, int nThetaBins);
    ~RNAConformers_ReadAndStore();

    RNAConformer *getRandomConformer();
    RNAConformer *getRandomConformer(double inp_bbAngleEta, double inp_bbAngleTheta);

    vector<RNAConformer*> getConformersVector();
    vector<RNAConformer*> getConformersVector(double inp_bbAngleEta, double inp_bbAngleTheta);

    void seed_myRand(unsigned long int inp_seed);
};

#endif //RNACONFORMERS_H_
