/*
 * 3DGaussEllipsoid.h
 *
 *  Created on: 2011-07-20
 *      Author: Michal Boniecki
 *		Grzegorz Lach
 */

#ifndef EllipsoidGauss3D_H_
#define EllipsoidGauss3D_H_

#define ELLIPSOID_NAME_LENGTH	12
#define FILE_LINE_LENGTH	1024

#include "Point3D.h"
#include <string.h>

class EllipsoidGauss3D{

protected:

    char name[ELLIPSOID_NAME_LENGTH]; //be careful
    Point3D vector2Center;
    Point3D matx_row1, matx_row2, matx_row3;
    double weight;

    double sqr_innerCutOff;
    double sqr_outherCutOff, outherCutOff;
    
    EllipsoidGauss3D *next;
    int identBit;
    
public:

//    int init_nextANDidentBit(EllipsoidGauss3D *next, int BitToSet);
    int set_next(EllipsoidGauss3D *next);
    int set_identBit(int whichBitToSet);
    char* getName();
    int get_identBit();
    double getValue(double x, double y, double z);
    double getValue_usingCutOff(double x, double y, double z);
    double getValue(Point3D inp_point_XYZ);
    double getValue_usingCutOff(Point3D inp_point_XYZ);
    double get_outherCutOff();
    Point3D get_Center();
    int isInInnerCutOff(double x, double y, double z);
    int isInInnerCutOff(Point3D inp_point_XYZ);
    int printEValNameWithIdentBit(FILE *output_file,double int_evalue);

    EllipsoidGauss3D(const char *name, double weight, Point3D vector2Center, Point3D matx_row1, Point3D matx_row2, Point3D matx_row3, double outherCutOff, double innerCutOff);
//    EllipsoidGauss3D();
};

class Ellipsoids_ReadAndStore{

protected:

    EllipsoidGauss3D **v_ellipsoid_ptrs;
    int n_ellipsoids;

public:

    Ellipsoids_ReadAndStore(const char *filename);
    EllipsoidGauss3D *getEllipsoidPtrByName(const char *inp_name);
};

#endif
