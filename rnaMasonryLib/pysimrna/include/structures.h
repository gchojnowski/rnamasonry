/*
 * structures.h
 *
 *  Created on: 2009-11-03
 *      Author: Konrad Tomala
 *		Michal Boniecki
 */

#ifndef STRUCTURES_H_
#define STRUCTURES_H_

struct Range {
	int start;
	int term;
	int chainNum;
	
	int startBase;
	int termBase;
};

struct Pair {
	int first;
	int second;
};

struct InputParam {
	int inputType;
	char* inputPath;
	char* ouputPath;
	int atomsConstraintsFlag;
	long int numberOfIterations;
	char* restrPath;
	char* secondStrcPath;
	char* dyePath;
	char* trascPath;
	int allMoves;
	char* confPath;
	int pdbListFlag;
	int numberOfReplicas;
	int curr_ReplicaIndex;
	unsigned long long randomSeed;

    // gchojnowski: enables pdbstring-based simul initialization
    std::string pdb_string;


	double *RNABondsWeight;
	double *RNAAnglesWeight;
	double *RNATorsAnglesWeight;
	double *RNAEtaThetaWeight;
	double *RNASecondStrcWeight;
	double *RNALimitingSphereRadius;
	double *RNALimitingSphereWeight;

	double *RNAfractionOfOneAtomMoves;
	double *RNAfractionOfTwoAtomsMoves;
	double *RNAfractionOfNitrogenAtomMoves;
	double *RNAfractionOfFragmentMoves;
};


#endif /* STRUCTURES_H_ */
