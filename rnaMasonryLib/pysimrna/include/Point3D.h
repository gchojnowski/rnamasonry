/*
 * Point3D.h
 *
 *  Created on: 2009-11-03
 *      Author: Konrad Tomala
 *		Michal Boniecki
 */
/** @file Point3D.h
*
* @brief modified Point3D::flatAngle
*/

#include "functions.h"
#include "structures.h"

#undef TESTING

#ifndef POINT3D_H_
#define POINT3D_H_

class Point3D {
// class represents point in 3D and has implemented operations on vectors in 3D
public:
	double x;
	double y;
	double z;
	Point3D& operator=(const Point3D&);
	Point3D operator+(const Point3D&);
	Point3D operator-(const Point3D&);
	Point3D operator/(double);
	Point3D operator*(double);
	Point3D crossProduct(const Point3D&);
	Point3D multiplyMatrixBy(const Point3D& row1, const Point3D& row2, const Point3D& row3); //multiply matrix given by 3 rows by this vector
	double dotProduct(const Point3D&);
	double distance(const Point3D&);
	double distanceSqr(const Point3D&);
	void normalize();
	double length();
	double lengthSqr();
	double flatAngle(Point3D&);
	double anglePrecise(Point3D&);
	double torsionalAngle(const Point3D&, const Point3D&, const Point3D&);
	void change_distance_symmetric(Point3D&, double target_distance);
	void change_distance_asymmetric(const Point3D&, double target_distance);
	Point3D(const Point3D&);
	Point3D();
	~Point3D();
};

inline double Point3D::length(){
	return sqrt(x*x + y*y + z*z);
}

inline double Point3D::lengthSqr(){
	return (x*x + y*y + z*z);
}

inline void Point3D::normalize(){
	double mult = 1.0/length();
	x *= mult;
	y *= mult;
	z *= mult;
}

inline double Point3D::distance(const Point3D &p2) {
	double x3, y3, z3;

	x3 = x - p2.x;
	y3 = y - p2.y;
	z3 = z - p2.z;

	return sqrt(x3*x3 + y3*y3 + z3*z3);
}

inline double Point3D::distanceSqr(const Point3D &p2){
	double x3, y3, z3;

	x3 = x - p2.x;
	y3 = y - p2.y;
	z3 = z - p2.z;

	return (x3*x3 + y3*y3 + z3*z3);
}

inline double Point3D::dotProduct(const Point3D &p2){
	return (x*p2.x + y*p2.y + z*p2.z);
}

inline Point3D Point3D::crossProduct(const Point3D &p2){

	Point3D p;

	p.x = (y * p2.z) - (z * p2.y);
	p.y = (z * p2.x) - (x * p2.z);
	p.z = (x * p2.y) - (y * p2.x);

	return p;
}

inline Point3D Point3D::multiplyMatrixBy(const Point3D& row1, const Point3D& row2, const Point3D& row3){ //multiply matrix given by 3 rows by this vector

	Point3D result;
	
	result.x = dotProduct(row1);
	result.y = dotProduct(row2);
	result.z = dotProduct(row3);

	return result;
}

/** @brief replaced local arccos implementation with math.h::acos
*
* arccos is array-based, with a yet another link to a local
* file: ./data/arccos and spooky implementation in functions.cpp
*
* (XXX gchojnowski)
*/
inline double Point3D::flatAngle(Point3D &p2){

	double cosAngle = dotProduct(p2) / (length() * p2.length());
	if(cosAngle > -1.0 && cosAngle < 1.0){
        return acos(cosAngle);
	}
	else if(cosAngle < -1.0)
	    return M_PI;
	else
	    return 0;
/*
	if (pom < -1.0) return M_PI;
	else {
		if (pom > 1.0) return 0.0;
		else
		{
			return arccos(pom);
//			return acos(pom);  //regular version of arcus cosinus is employed, because of REMC implementation (avoiding function initRemoteFunctions())
		}
	}
*/
}

inline double Point3D::torsionalAngle(const Point3D &p2, const Point3D &p3, const Point3D &p4){

	Point3D v1, v2, v3, vn1, vn2;
	double angleTors;

	v1.x = p2.x - x;
	v1.y = p2.y - y;
	v1.z = p2.z - z;

	v2.x = p3.x - p2.x;
	v2.y = p3.y - p2.y;
	v2.z = p3.z - p2.z;

	v3.x = p4.x - p3.x;
	v3.y = p4.y - p3.y;
	v3.z = p4.z - p3.z;

	vn1 = v1.crossProduct(v2);
	vn2 = v2.crossProduct(v3);

	angleTors = vn1.flatAngle(vn2);

	if (vn1.dotProduct(v3) < 0)
		angleTors = 2 * M_PI - angleTors;

	return angleTors;

}

#endif /* POINT3D_H_ */
