/*
 * functions.h
 *
 *  Created on: 2009-11-03
 *      Author: Konrad Tomala
 *		Michal Boniecki
 */

#include <iostream>
#include <list>

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <fcntl.h>

using namespace std;

#define		ARCCOS_SIZE		501
#define		ARCCOS_RATIO		4.0


#ifndef FUNCTIONS_H_
#define FUNCTIONS_H_

double arccos(double);
double radians(double degrees);
double degrees(double radians);
double sqr(double x);

char *del_spaces(char *out_str, const char *inp_str);
int split_string(char *string2parse, const char *delim_string, char **output_list, int max_n_items);
double convert_string2double(const char *inp_string, const char *inp_filename, int curr_file_line_nr);

void initRemoteFunctions();


#endif /* FUNCTIONS_H_ */
