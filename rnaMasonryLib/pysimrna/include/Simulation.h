/*
 * Simulation.h
 *
 *  Created on: 2009-11-03
 *      Author: Konrad Tomala
 *		Michal Boniecki
 */

#include "RNAStructure.h"

#define 	MIN_WRITE_FREQ		2
#define		NUMBER_OF_REPLICS	8
#define		DEFAULT_ITER_NUMBER	0
#define 	TEMP_RATIO		0.5
#define 	MIN_INITIAL_TEMP	0.950
#define		BORDER_CHAIN_LENGTH	10
#define		TRA_WRITE_IN_EVERY_N_ITERATIONS	10

#define		N_REPLICAS_LIMIT_WARNING	40

#define		INITIAL_TEMP		0.950
#define		FINAL_TEMP		0.500

#ifndef SIMULATION_H_
#define SIMULATION_H_

class Simulation {
// abstract class represents simulation of RNA folding
protected:
	long int numberOfIterations;
	RNAStructure* rnaStruct;
	virtual void setSimulationParam(struct InputParam) = 0;
	virtual int getNumberOfIterations(struct InputParam) = 0;
public:
	long int get_numberOfIterations(){return numberOfIterations;}
	double get_systemEnergy(){return rnaStruct->chainEnergy;}
	virtual void makeSimulation() = 0;
	virtual ~Simulation();
};

class SimulatedAnnealing: public Simulation {
	// class represents simulation of RNA folding with simulated annealing method
private:
	int pdbListFlag;
	double /*mb initialTemperature,*/ tempStep, initTemp, finalTemp;
	void readConfigFile(struct InputParam*);
	void chooseTypeOfChain(struct InputParam);
	double getInitialTemp();
	int getDefaultNumberOfIterations(int);
public:
	int traWriteInEveryNIterations;
	int getNumberOfIterations(struct InputParam);
	void setSimulationParam(struct InputParam);
	void setSimulationParamAsReplica(struct InputParam, int curr_ReplicaIndex);
	void makeNSimulationSteps(int n_steps);
	void makeSimulation();
	void writeTraflPdbData(int curr_tempShelfIndex, int symulStart);
	void displayEndingInfo(int curr_tempShelfIndex);
	double getSystemTemp();
	void setSystemTemp(double curr_systemTemp){rnaStruct->kT = curr_systemTemp;}

	SimulatedAnnealing();
	~SimulatedAnnealing();
};

class ReplicaExchangeMethod {
// class represents simulation of RNA folding with replica exchange method
private:
	SimulatedAnnealing **tempShelfs;//in this case (ReplicaMethod) usually at each temperature shelf there is isothermal simulation
					//but class SimulatedAnnealing also allows for this task, when initTemp == finalTemp
					//each temperature shelf is one replica, running in temperature according temperatures[] table
					//each temp. shelf stores pointer to the simulation running in this termperature

	long int numberOfIterations;
	int numberOfReplicas; // means also number of temperature shelfs (tempShelfs).
		
	double *temperatures;
public:	
	void setSimulationParam(struct InputParam);
	void makeSimulation();

	ReplicaExchangeMethod(int in_numberOfReplicas);
	~ReplicaExchangeMethod();
};

#endif /* SIMULATION_H_ */
