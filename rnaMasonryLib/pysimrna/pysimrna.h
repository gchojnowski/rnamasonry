#include "RNAStructure.h"
#include "read_pdb.h"
#include "Simulation.h"

/**
 * @file pysimrna.h
 * @brief class with the bindings defs
 * @author Grzegorz Chojnowski
 */

#ifndef PYSIMRNA_H_
#define PYSIMRNA_H_



class pysimrna{

    private:
        RNAStructure* rnaStruct;
        std::string simrna_path; //!< stores path to the SimRNA3-current dir
        float initTemp; //!< initial temperature, it has no effect at this stage

        std::string _ss_string;
        float _ss_weight;

        float _rg;
        float _rg_weight;

        bool _verbose;

    public:
        pysimrna(); //!< def constructor, assumes that ./lib/SimRNA3-current/data exists

        /** @brief alt constructor, redefines default path to the SimRNA3-current
        * @param path2simrna e.g. /home/xmen/xdata/SimRNA3-current
        */
        pysimrna(std::string path2simrna);

        ~pysimrna(); //!< default detructor. does literally nothing

        /** @brief process RNA structure form a given text file
        * @param pdb_fname
        * @return nothing
        */
        void process_pdbfile(std::string pdb_fname);



        /** @brief set radius iof gyration and corresponding weight
        * @param rg
        * @param weight
        * @return nothing
        */
        void set_rg(float rg, float weight);



        /** @brief set secondary structure
        * @param ss_string
        * @param weight
        * @return nothing
        */
        void set_ss(std::string ss_string, float weight);



        /** @brief parse PDB stucture from a string
        * @param pdb_fname dummy file name, SimRNA needs that internally
        * @param pdb_string
        * @return nothing
        */
        void process_pdbstring(std::string pdb_fname, std::string pdb_string);



        float get_energy(); //!< returns energy of the model
        void show_energy_details(); //!< prints detailed list of energy terms


        void simTest(std::string pdb_string);
        struct InputParam getParams();
};

#endif /* PYSIMRNA_H_ */
