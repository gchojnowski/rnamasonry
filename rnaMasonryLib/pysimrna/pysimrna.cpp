// =============================================================================
// Created at:
//  International Institute of Molecular and Cell Biology in Warsaw, Poland
// Author:
//  Grzegorz Chojnowski - gchojnowski@genesilico.pl, gchojnowski@gmail.com
// =============================================================================

#include "pysimrna.h"
#include "Simulation.h"

//#include <boost/python/numeric.hpp>

#include <boost/python/init.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>
#include <boost/python/module.hpp>


namespace bp = boost::python;

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

pysimrna::pysimrna(){
    initTemp = 0.95;
    rnaStruct = new RNAStructure;
    simrna_path = "./lib/SimRNA3-current/";

    _rg = 0.0;
    _rg_weight = 0.0;

    _ss_string = "";
    _ss_weight = 0.0;

    _verbose = false;

    rnaStruct -> set_path_to_data( simrna_path );
    rnaStruct -> set_kT( initTemp );
    rnaStruct -> readHistograms();
};


pysimrna::pysimrna(std::string path2simrna){
    initTemp = 0.95;

    _rg = 0.0;
    _rg_weight = 0.0;

    _ss_string = "";
    _ss_weight = 0.0;

    _verbose = false;


    rnaStruct = new RNAStructure;
    rnaStruct -> set_path_to_data( path2simrna );
    rnaStruct -> set_kT( initTemp );
    rnaStruct -> readHistograms();
    rnaStruct -> setLimitingSphereWeight(_rg_weight);
    rnaStruct -> setLimitingSphereRadius(_rg);
};




void pysimrna::process_pdbfile(std::string pdb_fname){
    std::string pdb_string;
    std::ifstream ifile;
    ifile.open(pdb_fname.c_str());
    pdb_string.assign((std::istreambuf_iterator<char>(ifile)), std::istreambuf_iterator<char>());
    ifile.close();
    process_pdbstring(pdb_fname, pdb_string);
};

void pysimrna::process_pdbstring(std::string pdb_fname, std::string pdb_string){

    //std::string ss_string = "..(...((..(......).))....)........(((((.(.((.(((((((.((((((((((((((.(...).))))....))))))))))).)))).))).)).).)))).\n........(((((((((((((((((((((((((((((((((.......................................)))))))))))))))))))))))))))))))))";


    //ss_string = "..(...((..(......).))....)........(((((.(.((.(((((((.((((((((((((((.(...).))))....))))))))))).)))).))).)).).)))).";


    rnaStruct->cleanTables();
    rnaStruct->createFromPDB_string((char *)pdb_fname.c_str(), pdb_string, 0, 0);
    // this step is needed for rg-like energy term
    rnaStruct->calcCenterOfMass();
    //rnaStruct->initSecondStrcRestraints("data/meta_thrs8.00A_clust156-000001_AA.txt", 1.0, false);
    rnaStruct->initSecondStrcRestraints_string("txt", _ss_string, _ss_weight, _verbose);
    //XXX gchojnowski: should handle ss restrains and radius of gyration
    rnaStruct->calcTotalExtendedChainEnergy();
    //rnaStruct->calcTotalBasicChainEnergy();
};


float pysimrna::get_energy(){
    return rnaStruct->getChainEnergy();
};


void pysimrna::show_energy_details(){
    rnaStruct->showEnergyDetails();
};

void pysimrna::set_rg(float rg, float weight){
    _rg = rg;
    _rg_weight = weight;
    rnaStruct->setLimitingSphereWeight(_rg_weight);
    rnaStruct->setLimitingSphereRadius(_rg);
};

void pysimrna::set_ss(std::string ss_string, float weight){
    _ss_string = ss_string;
    _ss_weight = weight;
};



struct InputParam pysimrna::getParams() {
    struct InputParam input;
    int seqFlag = 0, pdbFlag = 0, outFlag = 0, ch, numIterFlag = 0, restrFlag = 0, secondStrcFlag = 0;
    int dyeFlag = 0, trascFlag = 0, movesFlag = 0, confFlag = 0, randomSeedFlag = 0;
    int atomsConstraintsFlag = 0, pdbListFlag = 0, replicaExchangeFlag = 0;
    char *pdbArg = NULL, *seqArg = NULL, *outArg = NULL, *numIterArg = NULL, *restrArg = NULL, *secondStrcArg = NULL;
    char *dyeArg = NULL, *trascArg = NULL, *confArg = NULL, *randomSeedArg = NULL, *replicaExchangeArg = NULL;

    input.confPath = NULL;

    input.dyePath = NULL;
    input.restrPath = NULL;
    input.secondStrcPath = NULL;

    input.RNABondsWeight = NULL;
    input.RNAAnglesWeight = NULL;
    input.RNATorsAnglesWeight = NULL;
    input.RNAEtaThetaWeight = NULL;
    input.RNASecondStrcWeight = NULL;

    input.RNALimitingSphereRadius = NULL;
    input.RNALimitingSphereWeight = NULL;

    input.RNAfractionOfOneAtomMoves = NULL;
    input.RNAfractionOfTwoAtomsMoves = NULL;
    input.RNAfractionOfNitrogenAtomMoves = NULL;
    input.RNAfractionOfFragmentMoves = NULL;

    input.numberOfIterations = DEFAULT_ITER_NUMBER;


    return input;
}




void pysimrna::simTest(std::string pdb_string){

    struct InputParam input = getParams();
    input.numberOfReplicas = 1;
    input.pdb_string = pdb_string;

    SimulatedAnnealing singleSim;
    singleSim.setSimulationParam(input);
    singleSim.makeSimulation();


};




pysimrna::~pysimrna(){};


// ----------------------------------------------------------------------------


// MUST match the module name
BOOST_PYTHON_MODULE(pysimrna){

    //bp::numeric::array::set_module_and_type("numpy", "ndarray");

    // pysimrna
    bp::class_<pysimrna> srna( "pysimrna", bp::init<>());
    srna.def( bp::init< std::string >() );
    srna.def( "process_pdbfile", &pysimrna::process_pdbfile );
    srna.def( "process_pdbstring", &pysimrna::process_pdbstring );
    srna.def( "get_energy", &pysimrna::get_energy );
    srna.def( "show_energy_details", &pysimrna::show_energy_details );
    srna.def( "simTest", &pysimrna::simTest );
    srna.def( "set_rg", &pysimrna::set_rg );
    srna.def( "set_ss", &pysimrna::set_ss );

    /*
    sm.def( "find_matches", &supermatch::find_matches);

    sm.def( "finetune_match", &supermatch::finetune_match );
    sm.def( "finetune_match_symm", &supermatch::finetune_match_symm );
    sm.def( "finetune_match_wrapper", &supermatch::finetune_match_wrapper );

    sm.def( "get_reference_size", &supermatch::get_reference_size);

    bp::def( "superpose", superpose, ( bp::arg("reference"), bp::arg("moving")) );

    bp::def( "test_array", &test_array );
    */
}

