'''
Created on Dec 30, 2013

@author: rafal


Rewritten on Apr-Jun 2015
@author: gchojnowski
'''
import os,sys
import json
import math
import copy
from collections import deque
from itertools import *
from operator import *
import tarfile

import networkx as nx

# ========================== CCTBX ====================================
from scitbx.math import superpose
from scitbx import matrix
from scitbx.array_family import flex
from scitbx.array_family.flex import vec3_double

from iotbx import pdb
from iotbx.pdb import input as pdb_input
from annlib_ext import AnnAdaptor
# ========================== CCTBX ====================================
import pickle, gzip



try:
    from rnamasonry_config import *
except:
    sys.path.append("..")
    from rnamasonry_config import *

sys.path.append(os.path.join(ROOT, "lib"))
from rnamasonry_utils import rmsd_stats

from dbutils import get_fobj_from_database

'''
Classes for rna motif representation, i.e. strand, loop or stem. Consists of three classes:
1. base class (BasicMotif) - allows for rebuilding full atom structure based on information on motif id (in rna3db), motif residue groups numbers and
transformation matrix. Makes rebuilding full atom structure easy (based only on simplified information - basic info on motif, must know to rebuild
full atom structure).
2. extended class (Motif) - full information on motif, i.e. its type, id, transformation matrix, joint nucleotides, motif nucleotides centroids and
nucleotides radiuses. Methods enabling motifs superposition, reading/writing motifs as json objects and clash test. This class is used to perform most
operations on motifs.
3. Branch class - consists of Motif objects, encapsulates an information about graph topology and possible operations on graph structure, i.e. mostly
predefined methods of Motif class - dump, calculate clashes.
'''


class BasicMotif(object):
    '''
    Class representing simplified rna3db motif - stores information enabling to later rebuild full atom structure, i.e. fragment id, transformation
    matrix, but don't have Motif object functionality - superpose_1, calculate clashes, etc.
    NOT USED DIRECTLY AT THE MOMENT (used as this is baseclass for Motif class)
    '''
    def __init__(self, project_dir, local_db, **args):
        self.project_dir = project_dir
        self.local_db = local_db

        if 'frag' in args:
            # Copy constructor
            fields_and_values = args['frag'].__dict__
            [setattr(self, field, copy.deepcopy(fields_and_values[field])) for field in fields_and_values]

        elif 'jsonized_fragment' in args:
            # Constructs basic fragment object from jsonized basic fragment object
            jsonized_fragment = args['jsonized_fragment'] # fragment (motif) as json object
            r = matrix.sqr( flex.double( jsonized_fragment['rtmx'][0] ) ) # matrix as cctbx object
            t = matrix.col( flex.double( jsonized_fragment['rtmx'][1] ) ) # matrix as cctbx object

            self.rtmx = matrix.rt( (r,t) )
            self.fragment_id = jsonized_fragment['fragment_id']
            self.frg_as_consecutive_seqs = jsonized_fragment['frg_as_consecutive_seqs']
        else:
            # Constructs basic fragment object from input data
            self.fragment_id = copy.deepcopy(args['fragment_id'])
            self.frg_as_consecutive_seqs = args['frg_as_consecutive_seqs']

            if 'rtmx' in args:
                self.rtmx = args['rtmx']
            else:
                rotation_matrix = matrix.identity(3)
                translation_vector = matrix.col([0,0,0])
                self.rtmx = matrix.rt( (rotation_matrix, translation_vector) ) # rt matrix - identity matrix when instantiating object

    def dump_fragment(self):
        # dumps fragment object as json string
        r_matrix = self.rtmx.r.as_list_of_lists() # rotation matrix as pythonic list of lists object
        t_vector = self.rtmx.t.as_list_of_lists() # translation vector as pythonic list of lists object

        return json.dumps({'rtmx' : (r_matrix, t_vector),\
                            'fragment_id' : self.fragment_id,\
                            'frg_as_consecutive_seqs' : self.frg_as_consecutive_seqs})

    def get_hierarchy(self, cut_nucleotides=None):
        """
            loads fragment hierarchy from local drive
        """

        if self.fragment_id.startswith('m'):
            with open(os.path.join(self.project_dir, \
                                        'fake_fragments', \
                                        'structure_'+str(self.fragment_id)+'.pdb'), 'r') as ifile:
                ifile_read = ifile.read()


        elif self.fragment_id.startswith('i'):
            with open(os.path.join(self.project_dir, \
                                        'input_fragments', \
                                        'structure_'+str(self.fragment_id)+'.pdb'), 'r') as ifile:
                ifile_read = ifile.read()
        else:
            fname = os.path.join(self.local_db, \
                                self.fragment_type, \
                                'structure_'+str(self.fragment_id)+'.pdb')


            try:
                with open(fname, 'r') as ifile:
                    ifile_read = ifile.read()
            except KeyboardInterrupt:
                raise
            except IOError:
                ifile_read = get_fobj_from_database(fname, self.local_db).read()
            except Exception as e:
                print("DB broken!", e, fname)
                raise

        hierarchy = pdb_input(source_info = None, lines = ifile_read).construct_hierarchy()

        # transform fragment using appropriate transformation matrix
        #[atom.set_xyz(self.rtmx * atom.xyz) for atom in hierarchy.atoms()]
        hierarchy.atoms().set_xyz(self.rtmx * hierarchy.atoms().extract_xyz())

        resseqs = sorted( [rg.resseq_as_int() for rg in hierarchy.residue_groups()] )
        cyclic_permutation = list(map(len,self.frg_as_consecutive_seqs))
        consecutive_resseqs = [list(map(itemgetter(1), group)) for _, group in groupby(enumerate(resseqs), lambda i_x1:i_x1[0]-i_x1[1])]

        #XXX gchojnowski: added a crude fuse
        if not len(consecutive_resseqs) == len(cyclic_permutation):
            raise Exception('-- Skipping fragment (fuse #1, %s)'%str(self.fragment_id))

        if not set(map(len, consecutive_resseqs)) == set(cyclic_permutation):
            raise Exception("-- Skipping fragment (fuse #2, %s)"%str(self.fragment_id))


        # provide correct cyclic permutation of fragment nucleotides (if a fragment has cyclic nucleotides premutation i.e. [m,n], [m,n,k], etc.)
        if len(cyclic_permutation) > 1:
            # construct list of consecutive resseqs lists (list of consecutive nucleotides sequences)
            consecutive_resseqs = deque([list(map(itemgetter(1), group)) for _, group in groupby(enumerate(resseqs), lambda i_x:i_x[0]-i_x[1])])
            # check cyclic permutation (i.e. just list of consecutive resseqs lists lengths)
            nrotated=0
            while list(map(len, consecutive_resseqs)) != cyclic_permutation:
                if nrotated>len(consecutive_resseqs):
                    raise Exception("-- Skipping fragment (fuse #3, %s)"%str(self.fragment_id))

                # if current cyclic permutation is not compliant with required one then move last sequence of consecutive nucleotides to the begining
                consecutive_resseqs.rotate(1)
                nrotated+=1
            resseqs = list(chain(*consecutive_resseqs))
            #if not resseqs==sorted(resseqs): print "CP: ", resseqs

        residue_groups = [rg for i in resseqs for rg in hierarchy.residue_groups() if rg.resseq_as_int() == i]
        as_list = list(chain(*self.frg_as_consecutive_seqs)) # chain method from itertools package

        if len(residue_groups) != len(as_list):
            return None
            print("FRAG_ID = %s" % self.fragment_id)

        #Assigns correct numbers to this motif residue groups based on given list of residue groups numbers (frg_as_consecutive_seq).
        for i,rg in enumerate(residue_groups):
            rg.resseq = as_list[i]

        # cut mutual nucleotides or don't cut (depending on desired condition)
        if cut_nucleotides:
            rgs_to_delete = []
            for resseq_to_delete in cut_nucleotides:
                rgs_to_delete.append( next((rg for rg in residue_groups if rg.resseq_as_int() == resseq_to_delete), None) )

            for rg in rgs_to_delete:
                residue_groups.remove(rg)

        return residue_groups

# ==========================================================================================

class IncorrectInputDataError(Exception):
    pass

class NoCorrespondingJointException(Exception):
    pass

class IncorrectArgumentError(Exception):
    pass

class AtomsAssertionError(Exception):
    pass

# ==========================================================================================

class Motif(BasicMotif):
    # Class representing rna3db motif (fragment) with full functionality
    def __select_atoms(self, residue_group):
        # Select atoms of a given residue group for superposition
        sel_string = r"resseq " + residue_group.resseq + " and "+\
                        "(((resname G or resname A) and (name N9 or name O4' or name C[1-9]')) or"+\
                        " ((resname U or resname C) and (name N1 or name O4' or name C[1-9]')))"

        hierarchy = pdb.hierarchy.root()
        model = pdb.hierarchy.model()
        chain = pdb.hierarchy.chain()

        chain.append_residue_group(residue_group.detached_copy())
        model.append_chain(chain)
        hierarchy.append_model(model)

        sel_cache = hierarchy.atom_selection_cache()
        isel = sel_cache.iselection
        selected_atoms = hierarchy.atoms().select(isel(sel_string)).extract_xyz()

        return selected_atoms

    def __get_phosphorus_atoms(self, residue_groups):
        '''
        selects phosphorus atoms for centroids superposition. If there is no P atom in certain residue group then such residue group will be later
        excluded from superposition.
        '''
        P_atoms = []
        for rg in residue_groups:
            P_atom = next((matrix.rec( atom.xyz, (3,1) ) for atom in rg.atoms() if ' P ' in atom.name), None)
            P_atoms.append( (rg.resseq_as_int(),P_atom) )

        return P_atoms

    def __get_centroids_and_radiuses(self, residue_groups):
        '''
        Calculates centroids and radiuses for each nucleotide in fragment (approximates centroid with N1 or N9 atom and sets radius of
        arbitrary length). This information will be later used to calculate steric clash.
        '''
        # gchojnowski@19.11.2019
        # first option would use C1' if base (and N1/N9 atoms) is missing. after fixing the parser,
        # which removes incompelete/baseless residues now, it should not happen
        if 0:
            sel_atoms = [( [( rg.resseq_as_int(), matrix.rec( atom.xyz, (3,1) ) )\
                            for atom in rg.atoms() if ' N1' in atom.name or ' N9' in atom.name or " C1'" in atom.name] ) \
                            for rg in residue_groups]

            centroids = [_[-1] for _ in sel_atoms]
        else:
            centroids = [itemgetter(-1)( [( rg.resseq_as_int(), matrix.rec( atom.xyz, (3,1) ) )\
                            for atom in rg.atoms() if ' N1' in atom.name or ' N9' in atom.name or " C1'" in atom.name] ) \
                            for rg in residue_groups]

        radius = 6.0 # arbitrary radius for each nucleotide
        assert len(residue_groups) == len(centroids)

        return centroids, radius

    def __init__(self, project_dir, local_db, **args):
        # constructor
        super(Motif, self).__init__(project_dir, local_db, **args) # call base class constructor

        if 'jsonized_fragment' in args:
            '''
            constructs fragment object from json string (extends base fragment object)
            read object attributes from dict (json string) and transform them from primitive types to cctbx/numpy objects (uses local databases)
            '''
            jsonized_fragment = args['jsonized_fragment']
            if 'strand' in jsonized_fragment['fragment_type']:
                self.fragment_type = 'strand'
            elif 'stem' in jsonized_fragment['fragment_type']:
                self.fragment_type = 'stem'
            else:
                self.fragment_type = 'loop'

            self.centroids = [(centroid[0], matrix.rec( flex.double( centroid[1] ), (3,1) ) ) for centroid in jsonized_fragment['centroids']]
            self.P_atoms = [(P_atom[0], matrix.rec( flex.double( P_atom[1] ), (3,1) ) if P_atom[1] is not None else None )\
                                                                                    for P_atom in jsonized_fragment['P_atoms']]
            self.radiuses = jsonized_fragment['radiuses']
            self.joint_nucs_as_int = jsonized_fragment['joint_nucs_as_int']
            self.joint_nucleotides = {}
            joint_nucleotides_as_dict = jsonized_fragment['joint_nucleotides']
            for resseq, j_nuc_as_list in list(joint_nucleotides_as_dict.items()):
                j_nuc_as_vec3_double = flex.vec3_double()
                [j_nuc_as_vec3_double.append(tuple(atm_coord)) for atm_coord in j_nuc_as_list]
                self.joint_nucleotides[int(resseq)] = j_nuc_as_vec3_double
        elif 'frag' not in args:
            '''
            Downloads fragment from rna3db based on fragments id and creates corresponding fragment Motif object. Most important atribute of
            this class are:
            joint_nucs_as_int - a list of numbers (in case of strand Motif) or list of tuples numbers indicating which residue groups (nucleotides)
            may participate as joints in a superposition with another Motif.
            joint_nucleotides - a dictionary which for each joint residue group contain scitbx matrix object indicating its atoms coordinates.
            '''
            self.fragment_type = args['fragment_type']
            self.fragment_id = args['fragment_id']
            residue_groups = self.get_hierarchy()
            if not residue_groups: raise Exception("Broken frag")
            if self.fragment_type == 'strand':
                self.joint_nucs_as_int = [self.frg_as_consecutive_seqs[0][0],\
                                            self.frg_as_consecutive_seqs[0][-1]]
                self.joint_nucleotides = dict([ ( rg.resseq_as_int(), self.__select_atoms(rg) )\
                                                            for rg in residue_groups\
                                                            if rg.resseq_as_int() in self.joint_nucs_as_int])
            else:
                self.joint_nucs_as_int = [(self.frg_as_consecutive_seqs[0][0],\
                                            self.frg_as_consecutive_seqs[-1][-1])]
                l = len(self.frg_as_consecutive_seqs)
                if l > 1:
                    for i in range(1,l):
                        self.joint_nucs_as_int.append( (self.frg_as_consecutive_seqs[i-1][-1],\
                                                        self.frg_as_consecutive_seqs[i][0]) )
                self.joint_nucleotides = dict([ ( rg.resseq_as_int(), self.__select_atoms(rg) )\
                                                            for rg in residue_groups\
                                                            if rg.resseq_as_int() in list(chain(*self.joint_nucs_as_int))])

            self.centroids, self.radiuses = self.__get_centroids_and_radiuses(residue_groups)
            self.P_atoms = self.__get_phosphorus_atoms(residue_groups)


    def superpose_fragment(self, fragment, method=["kearsley", "kabsch"][0]):
        '''
        superpose_1 given fragment object on this fragment object.
        fragment - motif to superimpose
        ith_basepair - index of a basepair of a rigid fragment which is going to be used for superposition
        strand_conection - a tuple (i,j), i - same as ith_basepair, j - 5' or 3' nucleotide of i basepair of a non strand motif
        NOTE THAT THIS METHOD DO NOT TRANSFORM SUPERIMPOSED FRAGMENT. FOR THIS PURPOSE move_fragment(transformation) METHOD HAS TO BE INVOKED
        '''
        if self.fragment_type == 'strand':
            joint_resseqs = [self.joint_nucs_as_int[-1]]
        elif fragment.fragment_type == 'strand':
            joint_resseqs = [fragment.joint_nucs_as_int[0]]
        else:
            joint_resseqs = fragment.joint_nucs_as_int[0]

        try:
            rig_atoms_as_list = [self.joint_nucleotides[resseq] for resseq in joint_resseqs]
            sup_atoms_as_list = [fragment.joint_nucleotides[resseq] for resseq in joint_resseqs]
        except KeyError:
            raise NoCorrespondingJointException()

        rig_atoms = vec3_double()
        sup_atoms = vec3_double()
        for i in range(len(joint_resseqs)):
            ra = rig_atoms_as_list[i]
            sa = sup_atoms_as_list[i]

            assert ra.size() == sa.size(), self.fragment_id
            rig_atoms.extend(ra)
            sup_atoms.extend(sa)

        superposition = superpose.least_squares_fit(rig_atoms, sup_atoms, method)
        transformation = matrix.rt((superposition.r, superposition.t))

        x = rig_atoms
        y = transformation * sup_atoms
        d_sq = (x-y).dot()

        return flex.mean(d_sq)**0.5, transformation

    def move_fragment(self, transformation_matrix):
        # Moves fragment using given transformation matrix
        self.centroids = [(centroid[0], transformation_matrix * centroid[1]) for centroid in self.centroids]
        self.P_atoms = [(P_atom[0], transformation_matrix * P_atom[1] if P_atom[1] is not None else None) for P_atom in self.P_atoms]
        for resseq, atoms in list(self.joint_nucleotides.items()):
            self.joint_nucleotides[resseq] = transformation_matrix * atoms
        self.rtmx = transformation_matrix * self.rtmx

    def calculate_clash_with_other_fragment(self, fragment, if_motif_graph=True):
        '''
        Calculates clash between two fragments using one of two approximations - approximates fragment by sphere with certain radius and centroid
        (default) or approximates fragment by set of spheres with certain radiuses (of arbitrary selected lengths) and centroids (if_motif_graph=False).
        Each sphere approximates nucleotide (centroid is N1 or N9 atom).
        '''
        def calculate_clash(dist_bet_centr, radius1, radius2):
            '''
            Calculates clashes between two spheres with certain radiuses. Clash is a ratio of intersection length of two radiuses (where
            lengths sum is bigger then distance between their centers) divided by length of a shorter radius expressed as percentage value
            '''
            if dist_bet_centr < radius1 + radius2:
                x = radius1 + radius2 - dist_bet_centr
                shorter_radius = radius1 if radius1 < radius2 else radius2

                return (x / shorter_radius) * 100
            else:
                return 0

        # --------------------------------------------------

        # second approximation, i.e. motif as a list of spheres with certain centroid and radius each of which corresponds to nucleotide
        target = flex.double([]).as_1d()
        query = flex.double([]).as_1d()

        [target.extend( centroid[1].as_flex_double_matrix().as_1d() ) for centroid in self.centroids]
        [query.extend( centroid[1].as_flex_double_matrix().as_1d() ) for centroid in fragment.centroids if centroid[0] not in [x[0] for x in self.centroids]]

        dimension = 3

        # Build kd tree and find distances between each pair of points
        kd_tree = AnnAdaptor(target.as_1d(), dimension, int(len(target)/dimension))
        kd_tree.query(query.as_1d())

        distances = [d for d in kd_tree.distances]
        clash_sum = 0

        # calculates distances between each pair of spheres from rigid fragment and superimposed fragment and seeks for clashes
        for distance in distances:

            if distance ** 0.5 < self.radiuses + fragment.radiuses:
                clash_percentage = calculate_clash(distance ** 0.5, self.radiuses, fragment.radiuses)
                clash_sum += clash_percentage

        # returns average clash percentage
        return clash_sum / len(distances)

    def dump_fragment(self):
        # saves fragment as a json string
        jsonized_base_object = super(Motif, self).dump_fragment()
        json_dict = json.loads(jsonized_base_object)

        # centroid vector as pythonic list of lists object:
        centroids_as_list = [(centroid[0], centroid[1].as_list_of_lists() ) for centroid in self.centroids]
        P_atoms_as_list = [(P_atom[0], P_atom[1].as_list_of_lists() if P_atom[1] is not None else None ) for P_atom in self.P_atoms]

        # joint_nucleotides as pythonic list of list of tuples (tuple - atom coordinates):

        joint_nucleotides_as_dict = dict( [(resseq,[atm_coord for atm_coord in joint_nuc_atoms])\
                                        for resseq, joint_nuc_atoms in list(self.joint_nucleotides.items())] )

        json_dict.update( {'fragment_type' : self.fragment_type,\
                            'joint_nucleotides' : joint_nucleotides_as_dict,\
                            'centroids' : centroids_as_list,\
                            'radiuses' : self.radiuses,\
                            'joint_nucs_as_int' : self.joint_nucs_as_int,\
                            'P_atoms' : P_atoms_as_list} )

        return json.dumps(json_dict)

# ==========================================================================================

def renumber_residue_groups(func):
    '''
    Wrapper to predefine get_hierarchy method in Branch class. Constructs cctbx hierarchy and return it.
    '''
    def wrapper(*args, **kwargs):
        graph = func(*args, **kwargs)
        root = pdb.hierarchy.root()
        model = pdb.hierarchy.model()

        chain_number = 1 # use numbers as chain ids (in case a molecule consists of many chains)
        for n in sorted(graph.nodes()):
            chain = pdb.hierarchy.chain(id=str(chain_number))
            [chain.append_residue_group(rg) for rg in graph.nodes[n]['motif_res_groups']]
            model.append_chain(chain)
            chain_number += 1
        root.append_model(model)

        return root

    return wrapper

def get_subnodes(graph, root):
    # Simple method to retrieve subnodes of given node
    l=[]
    def dfs(g,r):
        l.append(r)
        for n in sorted( [s for s in g.neighbors(r) if s > r] ):
            dfs(g,n)
    dfs(graph,root)
    return l

#def erf(x):
#    # constants
#    a1 =  0.254829592
#    a2 = -0.284496736
#    a3 =  1.421413741
#    a4 = -1.453152027
#    a5 =  1.061405429
#    p  =  0.3275911
#
#    # Save the sign of x
#    sign = 1
#    if x < 0:
#        sign = -1
#    x = abs(x)
#
#    # A&S formula 7.1.26
#    t = 1.0/(1.0 + p*x)
#    y = 1.0 - (((((a5*t + a4)*t) + a3)*t + a2)*t + a1)*t*math.exp(-x*x)
#
#    return sign*y

class Branch(object):
    '''
    Class to represent branch object - a tree structure of RNA motifs. Good solution would be to inherit after NetworkX Graph and predefine its
    methods, however there is plenty of them and therefore it was easier to make this class a container with Motif objects.
    Encapsulates the same methods as the Motif class (predefined). Allows for instantiation from Networkx graph of Motif objects as well as
    using json string and single Motif object.
    '''
    def __init__(self, project_dir, local_db, **args):
        self.project_dir = project_dir
        self.local_db = local_db

        if 'branch_graph' in args:
            # Initiate branch with other branch - networkx graph
            self.branch = args['branch_graph']
            self.root = self.branch.nodes[0]['fragment']
        elif 'topology' in args:
            def superimpose(branch):
                def supimpose(node=0):
                    sons = [s for s in sorted(branch.neighbors(node)) if s > node]
                    rig_frag = branch.nodes[node]['fragment']
                    for son in sons:
                        # gchojnowski 09112015 - skip incomplete frags
                        try:
                            sup_frag = branch.nodes[son]['fragment']
                            _,transformation = rig_frag.superpose_fragment(sup_frag)
                        except KeyboardInterrupt:
                            raise
                        except:
                            continue


                        if rig_frag.fragment_type == 'strand':
                            joint_nuc = [rig_frag.joint_nucs_as_int[-1]]
                        elif sup_frag.fragment_type == 'strand':
                            joint_nuc = [sup_frag.joint_nucs_as_int[0]]
                        else:
                            joint_nuc = sup_frag.joint_nucs_as_int[0]
                        branch.nodes[son]['occupied_joints'].extend(joint_nuc)

                        sup_frag.move_fragment(transformation)
                        supimpose(node=son)
                supimpose()
            '''
            Initiate branch with certain topology and motifs which comprise this branch, i.e. take topology and provide
            correct superposition of motifs
            '''
            self.branch = args['topology']
            for n in self.branch.nodes():
                self.branch.nodes[n]['occupied_joints'] = []

            # propagate superposition
            mapping = dict([(n,i) for i,n in enumerate(sorted(self.branch.nodes()))])
            self.branch = nx.relabel_nodes(self.branch,mapping,copy=True)
            self.root = self.branch.nodes[0]['fragment']
            superimpose(self.branch)
        elif 'branch' in args:
            # copy constructor
            branch = args['branch']
            self.branch = nx.Graph()
            self.branch.add_nodes_from( branch.nodes() )
            self.branch.add_edges_from( branch.edges() )

            for n in branch.nodes():
                self.branch.nodes[n]['fragment'] = Motif(project_dir, frag=branch.nodes[n]['fragment'])
                self.branch.nodes[n]['occupied_joints'] = copy.deepcopy( branch.nodes[n]['occupied_joints'] )
            self.root = self.branch.nodes[0]['fragment']
        elif 'jsonized_branch' in args:
            # string-list with jsonized motifs where motifs are separated with '\n' character
            data = args['jsonized_branch']
            topology = data.pop()

            self.branch = nx.Graph()
            self.branch.add_nodes_from( topology[0] )
            self.branch.add_edges_from( topology[1] )
            for n, dict_frag in enumerate(data):
                occupied_joints = dict_frag.pop('occupied_joints')
                self.branch.nodes[n]['occupied_joints'] = occupied_joints
                self.branch.nodes[n]['fragment'] = Motif(self.project_dir, \
                                                        self.local_db, \
                                                        jsonized_fragment=dict_frag)

            self.root = self.branch.nodes[0]['fragment']
        else:
            # Initiate branch as single node - root of the branch
            self.branch = nx.Graph()
            self.branch.add_node(0)
            self.root = Motif(self.project_dir, self.local_db, **args)
            self.fragment_id = args['fragment_id']

            #if args['fragment_id'].startswith('i'):
            #    raise Exception("Input structure seems to have an indexing break or missing residue!")
            #else:
            #    raise Exception("Found broken fragment in the database, that should not have happened")


            self.branch.nodes[0]['fragment'] = self.root
            self.branch.nodes[0]['fragment_id'] = args['fragment_id']
            self.branch.nodes[0]['occupied_joints'] = []


    @renumber_residue_groups
    def get_hierarchy(self, cut_joint_nucleotides=False):
        # get cctbx hierarchy for this structure
        graph = nx.Graph()
        graph.add_nodes_from( self.branch.nodes() )
        graph.add_edges_from( self.branch.edges() )

        for n in self.branch.nodes():
            motif = self.branch.nodes[n]['fragment']
            if cut_joint_nucleotides and n != 0:
                joint_to_remove = self.branch.nodes[n]['occupied_joints']
                graph.nodes[n]['motif_res_groups'] = motif.get_hierarchy(cut_nucleotides=joint_to_remove)
            else:
                graph.nodes[n]['motif_res_groups'] = motif.get_hierarchy()

        return graph

    def move_fragment(self, transformation_matrix):
        # Moves location of this branch
        for n in self.branch.nodes():
            self.branch.nodes[n]['fragment'].move_fragment(transformation_matrix)

    def calculate_clash(self, branch=None, which_motif=None):
        '''
        Calculate clashes in this branch or this branch with another, specified branch.
        '''
        self_fragments = [self.branch.nodes[n]['fragment'] for n in self.branch.nodes()]
        if which_motif is not None:
            # Calculate clashes of each motif (except which_motif) belonging to this branch with motif in node number which_motif
            nodes = self.branch.nodes()
            assert which_motif in nodes
            clashes = [self_fragments[i].calculate_clash_with_other_fragment(self_fragments[which_motif], if_motif_graph=False)\
                                                                                                for i in nodes if i != which_motif]
        elif branch is not None:
            # Calculate clashes between this branch and given branch
            branch_fragments = [branch.branch.nodes[n]['fragment'] for n in branch.branch.nodes()]
            clashes = [self_frag.calculate_clash_with_other_fragment(branch_frag, if_motif_graph=False)\
                                             for self_frag in self_fragments for branch_frag in branch_fragments]
        else:
            # Calculate clashes within this branch
            clashes = []
            for i in range(len(self_fragments)):
                for j in range(i+1,len(self_fragments)):
                    clashes.append( self_fragments[i].calculate_clash_with_other_fragment(self_fragments[j], if_motif_graph=False) )

        return sum(clashes) / len(clashes) if clashes else 0

    def dump_fragment(self):
        jsonized_motifs = []
        for n in sorted( self.branch.nodes() ):
            # dump fragment and information about available joints
            base_json = self.branch.nodes[n]['fragment'].dump_fragment()
            base_dict = json.loads( base_json )
            base_dict.update( {'occupied_joints' : self.branch.nodes[n]['occupied_joints']} )

            jsonized_motifs.append( base_dict )
        # gchojnowski@20.06.2023 - not jsoonable any more and seems not needed too...
        #jsonized_motifs.append( ( self.branch.nodes(), self.branch.edges() ) )

        return json.dumps( jsonized_motifs )

    def superimpose_phosphorus(self, branch, p_value=0.01, threshold_rmsd=1.0):
        '''
        Superimpose given branch phosphorus atoms on this branch phosphorus atoms and calculate p value corresponding to this superposition.
        P value stands here for the probability of how likely such superposition is as good as superposition of this branch and a random one.
        In other words this method checks that similarity between branches is more significant than a chance.
        For precise details see: Nikolay V. Dokholyan et al., RNA, 2010, 16, 1340-1349 *
        '''
        P_atoms_1 = [self.branch.nodes[n]['fragment'].P_atoms for n in self.branch.nodes()]
        P_atoms_2 = [branch.branch.nodes[n]['fragment'].P_atoms for n in branch.branch.nodes()]

        n1 = sum([len(P_atoms) for P_atoms in P_atoms_1])
        n2 = sum([len(P_atoms) for P_atoms in P_atoms_2])

        assert len(P_atoms_1) == len(P_atoms_2)
        assert n1 == n2

        N = n1
        #rmsd1 = 5.1 * (N ** 0.41) - 15.8
        #mean_rmsd = threshold_rmsd if rmsd1 < threshold_rmsd else rmsd1 # taken from *
        #std_dev = 1.8 # taken from *

        shared_atoms_1 = pdb.hierarchy.ext.af_shared_atom()
        shared_atoms_2 = pdb.hierarchy.ext.af_shared_atom()
        for i in range(len(P_atoms_1)):
            motif_P_atoms_1 = P_atoms_1[i]
            motif_P_atoms_2 = P_atoms_2[i]
            for j in range(len(motif_P_atoms_1)):
                coord_atom_1 = motif_P_atoms_1[j][1]
                coord_atom_2 = motif_P_atoms_2[j][1]
                if coord_atom_1 is not None and coord_atom_2 is not None:
                    a1 = pdb.hierarchy.ext.atom()
                    a2 = pdb.hierarchy.ext.atom()

                    a1.set_xyz(coord_atom_1)
                    a2.set_xyz(coord_atom_2)

                    shared_atoms_1.append(a1)
                    shared_atoms_2.append(a2)

        s1 = shared_atoms_1.extract_xyz()
        s2 = shared_atoms_2.extract_xyz()

        superposition = superpose.least_squares_fit(s1, s2,["kearsley","kabsch"][0])
        transformation = matrix.rt((superposition.r, superposition.t))

        X = s1
        Y = transformation * s2
        d_sq = (X-Y).dot() ### d_sq = [r1, r2, ..., rn], where n = |X| = |Y| and ri = [(Xix - Yix)^2 + (Xiy - Yiy)^2 + (Xiz - Yiz)^2]
        rmsd = flex.mean(d_sq)**0.5 ### rmsd = sqrt( mean( d_sq ) )

        #z_score = (rmsd - mean_rmsd) / std_dev
        #p = ( 1 + erf(z_score/(2**0.5)) )/2

        # XXX gchojnowski 18.06.2015
        p = rmsd_stats(n1).pval(rmsd)
        belong_to_cluster = False if p > p_value else True

        return belong_to_cluster
