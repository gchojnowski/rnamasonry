#!/usr/bin/env python

# Setup script for ModeRNA

from distutils.core import setup
import sys, time

import os
if os.sep!='/' and sys.argv[1]=='py2exe':
    import py2exe

if sys.argv[1]=='install':
    sys.stderr.write("\nCAUTION:\n'python setup.py install' copies 45 MB PDB files to your site-packages folder.\n")
    sys.stderr.write("[press ENTER to confirm, x+ENTER to abort]\n")
    result = raw_input()
    if result.strip() == 'x':
        sys.stderr.write("\ninstallation cancelled.\n")
        sys.exit(1)
    
packages = ['moderna', 'moderna.data','moderna.minimize', \
    'moderna.examples','moderna.analyze', 'moderna.builder']

package_data ={
    'moderna': ['LICENSE_GPL.TXT', ],
    'moderna.data':['LIR_fragments.lib', 'modification_names_table','modifications',
                    'modification_topologies.txt', 
                    'helix.pdb','phosphate_group.pdb','single_strand.pdb', 
                    'IsostericityMatrices.txt','suite_clusters.txt',
                    'rnaDB05_list.txt', 'pair_fragment.pdb', 
                    'modification_fragments/*.pdb', 'rnaDB05/*.pdb',
                    'standard_bases/*.ent','base_pairs/*.pdb' ], 
    'moderna.minimize':['*.sh', '*.pdb'], 
    }

# add tests to source distro
if sys.argv[1]=='build':
    packages.append('test')
    packages.append('test.test_analyze')
    packages.append('test.test_builder')
    package_data['test'] = ['test_data/*.pdb',\
        'test_data/lir_chains.txt', \
        'test_data/modification_sample_table.txt', 'test_data/clash/*', \
        'test_data/gaps/*.pdb','test_data/gaps/*.fasta','test_data/gaps/*pml',\
        'test_data/geometry/*', 'test_data/lir_test_files/*', \
        'test_data/loop_candidates/*', 
        'test_data/nucleotides/*.pdb', \
        'test_data/nucleotides/*.ent', \
        'test_data/nucleotides/border_cases/*', \
        'test_data/topology_data/*', \
        'test_data/check_pdb/*', \
        'test_data/other/*', 'test_data/rna_structures/*']
    package_data['moderna'] = ['../README.TXT', '../RELEASE_NOTES.TXT', '../LICENSE_GPL.TXT', '../setup.py']

setup(
        name = "ModeRNA",
        #console=['moderna.py'],
        version = "1.7.1",
        author="Magdalena Musielak, Kristian Rother, Tomasz Puton, Pawel Skiba, Janusz M. Bujnicki",
        author_email="mmusiel@genesilico.pl",
        url="http://iimcb.genesilico.pl/moderna", 
        packages=packages, 
        package_data=package_data, 
        )
        


