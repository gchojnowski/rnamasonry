"""
    gchojnowski@genesilico.pl

    Extract motifs and motif association graph from RNA secondary strcuture
    input:
        RNA sequence and secondary strcucture in dot-bracket notation
    output:
        - list of motifs
        - motifs assoctation graph - simplified representation of RNA structure with motifs represented as nodes (jsonized networkx graph)
"""
import networkx as nx
import numpy as np
#import matplotlib.pyplot as plt

from optparse import OptionParser
from networkx.readwrite import json_graph
from itertools import *
from operator import *

import sys,os
import os




SS_LETTERS = ['(', '.', ')','[',']']
SEQ_LETTERS = ['A', 'G', 'U', 'C']

class rnagraphs:


    def __init__(self, basepairs_list=None, dotbracket_text=None, sequence_text=None):

        assert [basepairs_list, dotbracket_text].count(None)==1, Exception("ERROR: Either base-pairs list or dotbracket 2D structure required on input")
        assert sequence_text, Exception("ERROR: input sequence missing")

        self.sequence = sequence_text

        if dotbracket_text:
            if set("".join(dotbracket_text)) - set(SS_LETTERS):
                    raise ValueError('Input should be in dot-bracket notation')

            self.input_dotbracket = dotbracket_text
            self.basepairs = self.get_basepairs_from_dotbracket()
        else:
            self.basepairs = basepairs_list
            self.input_dotbracket = None
            self.get_nested()

        # test input sequence letters
        for letter in self.sequence:
            if letter.upper() not in SEQ_LETTERS:
                self.sequence = 'X'*len(self.input_dotbracket)
                break

    # -------------------------------------------------------------------------


    def get_basepairs_from_dotbracket(self):
        stack = []
        basepairs = []
        for i, letter in  enumerate(self.input_dotbracket[0]):
            if letter=='(':
                stack.append(i)
            elif letter==')':
                if not stack:
                    return None
                basepairs.append(tuple([stack.pop(-1), i]))

        if stack:
            return None

        return basepairs


    # -------------------------------------------------------------------------


    def chain2graph(self):

        """
            This function creates a RNA graph from input secondary strcutre
            backbone edges have weight <2 (to be distinguishable from the WC
            and weak base pair edges)

            edges weights:
                1.0 - backbone
                3.0 - WC basepair
                2.0 - remaining pairing interactions (not used here)
        """

        #nested_structure = self.get_nested()
        #nested_db = ["."]*len(self.sequence)
        #if not self.input_dotbracket:
        #    self.input_dotbracket = ["".join(nested_db)]
        #    rejected_bps = ['.']*len(self.sequence)
        #    for bp in self.basepairs:
        #        l,r = sorted(bp)
        #        rejected_bps[l] = '('
        #        rejected_bps[r] = ')'
        #else:
        #    rejected_bps = list(self.input_dotbracket[0].replace('[', '(').replace(']', ')'))

        RNA_graph=nx.Graph()

        #XXX gchojnowski - replace base ss with a nested one
        for bp in self.get_basepairs_from_dotbracket():
            RNA_graph.add_edge(bp[0], bp[1], weight=3.0)
            #nested_db[sorted(bp)[0]] = '('
            #nested_db[sorted(bp)[1]] = ')'
            #rejected_bps[sorted(bp)[0]] = '.'
            #rejected_bps[sorted(bp)[1]] = '.'

        #self.input_dotbracket[0] = "".join(nested_db)
        #if rejected_bps.count('('): self.input_dotbracket.append("".join(rejected_bps))

        for i,_ in enumerate(self.sequence[:-1]):
            RNA_graph.add_edge(i, i+1, weight=1.0)
        #self.plot_graph(RNA_graph)
        return RNA_graph

    # -------------------------------------------------------------------------

    def plot_graph(self, graph, filename=None, agraph=False, draw_labels=False):

        # prog is one of: 
        # twopi, gvcolor, wc, ccomps, tred, sccmap, fdp, circo, neato, acyclic, nop, gvpr, dot
        #-Gmodel="circuit"
        pos=nx.graphviz_layout(graph, prog='neato', args='-Gmodel="circuit" -Elen=10')

        dist = []
        for u,v in graph.edges():
            dist.append( np.linalg.norm(np.array(pos[u])-np.array(pos[v])) )
        node_size = 0.1 * min(dist)

        if agraph:

            loop_nodes = [n for (n,d) in graph.nodes(data=True) if d['fragment_info'][1] == 'loop']
            stem_nodes = [n for (n,d) in graph.nodes(data=True) if d['fragment_info'][1] == 'stem']
            strand_nodes = [n for (n,d) in graph.nodes(data=True) if d['fragment_info'][1] == 'strand']

            labels = dict([(n, "%s_%i"%(d['fragment_info'][1],n)) for (n,d) in graph.nodes(data=True)])

            offset=100
            pos_labels = {}
            for key in list(pos.keys()):
                x, y = pos[key]
                pos_labels[key] = (x+offset, y+offset)


            nx.draw_networkx_nodes(graph,pos,nodelist=loop_nodes,node_size=node_size, node_color='0.5', linewidths=0.0)
            nx.draw_networkx_nodes(graph,pos,nodelist=stem_nodes,node_size=node_size, node_color='k')
            nx.draw_networkx_nodes(graph,pos,nodelist=strand_nodes,node_size=node_size, node_color='w', linewidths=2.0)


            nx.draw_networkx_edges(graph,pos,edgelist=graph.edges(), width=2)
            if draw_labels: nx.draw_networkx_labels(graph,pos_labels,labels,font_size=12,font_family='sans-serif', label_pos=1)

        else:

            wc_edges=[(u,v) for (u,v,d) in graph.edges(data=True) if d['weight'] >2.0]
            bb_edges=[(u,v) for (u,v,d) in graph.edges(data=True) if d['weight'] <2.0 ]
            other_edges=[(u,v) for (u,v,d) in graph.edges(data=True) if d['weight'] == 2.0]

            nx.draw_networkx_nodes(graph,pos,node_size=200, node_color='w')

            # non-WC bp edges (thin)
            nx.draw_networkx_edges(graph,pos,edgelist=other_edges, width=1, style='dotted')
            # W-C edges (thin)
            nx.draw_networkx_edges(graph,pos,edgelist=wc_edges, width=1, style='dashed')
            # Backbone edges (thick)
            nx.draw_networkx_edges(graph,pos,edgelist=bb_edges, width=2)

            nx.draw_networkx_labels(graph,pos,font_size=8,font_family='sans-serif')

        return 1

    # -------------------------------------------------------------------------

    def extract_loops_stems_and_strands_from_rna_graph(self, input_graph):

        """
            Function extracting simple secondary structure motifs from a
            graph representing RNA 2D strcuture. Input graph nodes represent nucleotides,
            edges represent canonical/wobble basepairs and backbone
            (weights 3.0 and 1.0 respectively).
            Output motifs share basepairs (loops and stems) or single nucleotides (strands)

            Cycle_basis algorithm (Paton, K. An algorithm for finding a fundamental set
            of cycles of a graph. Comm. ACM 12, 9 (Sept 1969), 514-518.)
        """


        stems_graph =  nx.Graph()
        loops_graph =  nx.Graph()
        strands_graph = input_graph.copy()


        for cycle in nx.cycle_basis(input_graph):
            # identify tandems and add to the stems_graph
            if len(cycle)==4 and len( [(u,v) for (u,v,d) in input_graph.subgraph(cycle).edges(data=True) if d['weight'] > 2.0] )==2: 
                stems_graph.add_edges_from(input_graph.subgraph(cycle).edges(data=True))
            else:
                # remaining cycles (length>4, any number of pairings) are loops
                loops_graph.add_edges_from(input_graph.subgraph(cycle).edges(data=True)) 


        # prepare strand graphs
        stemloops_graph = nx.Graph()
        stemloops_graph.add_edges_from(loops_graph.edges(data=True))
        stemloops_graph.add_edges_from(stems_graph.edges(data=True))

        for edge in stemloops_graph.edges(): 
            strands_graph.remove_edge(edge[0], edge[1])

        # remove all WC/GU-edges
        wc_edges = [(u,v) for (u,v,d) in strands_graph.edges(data=True) if d['weight'] > 2.0]
        for edge in wc_edges:
            strands_graph.remove_edge(edge[0], edge[1])

        # remove short rubish
        cc_list = list(nx.connected_components(strands_graph))
        for cc_item in cc_list:
            if len(cc_item)<2:
                strands_graph.remove_nodes_from(cc_item)

        return loops_graph, stems_graph, strands_graph


    # -------------------------------------------------------------------------


    def create_association_graph(self, RNA_graph, motif_dict, verbose=False, plot=False):
        association_graph =  nx.Graph()

        association_graph.add_nodes_from(list(motif_dict.keys()))
        keys = list(motif_dict.keys())
        n_frags = len(keys)

        # find frags sharing w-c edge ..
        for i in range(n_frags):
            for j in range(i+1, n_frags):
                common_subset = [ item for item in motif_dict[keys[i]][0] if item in motif_dict[keys[j]][0] ]
                # if the two share an wc-edge
                if len( [(u,v) for (u,v,d) in RNA_graph.subgraph(common_subset).edges(data=True) if d['weight'] > 2.0] )>0:
                    association_graph.add_edge(keys[i], keys[j])
                elif motif_dict[keys[j]][1] == 'strand' and\
                        len( set( motif_dict[keys[i]][0] )&set( motif_dict[keys[j]][0] ) ) == 1:
                    association_graph.add_edge(keys[i], keys[j])

        # .. and frags sharing one node and not connected by any path
        for i in range(n_frags):
            for j in range(i+1, n_frags):
                common_subset = [ item for item in motif_dict[keys[i]][0] if item in motif_dict[keys[j]][0] ]
                if len( set( motif_dict[keys[i]][0] )&set( motif_dict[keys[j]][0] ) ) == 1 and\
                    not nx.has_path(association_graph, keys[i], keys[j]):
                    association_graph.add_edge(keys[i], keys[j])

        #nx.draw(association_graph)
        #plt.show()

        keys = list(motif_dict.keys())
        keys.sort()

        for inode in keys:
            # Setting nodes atributes

            frg = sorted( motif_dict[inode][0] )
            frg_type = motif_dict[inode][1]
            frg_as_consecutive_seqs = [list(map(itemgetter(1), group)) for key, group in groupby(enumerate(frg), lambda i_x:i_x[0]-i_x[1])]

            # fragment info - [cyclic permutation, motif type, motif residue groups numbers]

            association_graph.nodes[inode]['fragment_info'] = { 'fragment_type' : frg_type,\
                                                                'frg_as_consecutive_seqs' : frg_as_consecutive_seqs}
        return association_graph

    # -------------------------------------------------------------------------

    def extract_motifs(self, output_fname=None, plot_graph=False, verbose=False):

        print(" --> Extracting RNA motifs")

        rna_graph = self.chain2graph()

        loops_graph, stems_graph, strands_graph = self.extract_loops_stems_and_strands_from_rna_graph( rna_graph.copy() )

        self.motifs_dict = {}

        for motif_graph, motif_type in [(loops_graph, 'loop'), (stems_graph, 'stem'), (strands_graph, 'strand')]:

            if motif_type=='loop':
                ccomponents = nx.cycle_basis(motif_graph)
            else:
                ccomponents = nx.connected_components(motif_graph)

            for frag in ccomponents:
                self.motifs_dict[min(frag)] = frag, motif_type

        motifs = [item[1] for item in list(self.motifs_dict.values())]
        print("      Found %i loops, %i stems and %i single stranded fragments" % (motifs.count('loop'),\
                                                                                    motifs.count('stem'),\
                                                                                    motifs.count('strand')))

        self.motifs_dict = dict([(i, self.motifs_dict.pop(key)) for i, key in enumerate( sorted(self.motifs_dict.keys()) )])
        self.association_graph = self.create_association_graph(rna_graph, self.motifs_dict)

        if plot_graph: self.plot_graph(self.association_graph, agraph=True)
        if output_fname:
            print(" --> Writing output to a file: %s" % output_fname)
            open(output_fname, 'w').write(json_graph.dumps(self.association_graph))

    # -------------------------------------------------------------------------
def parse_args():
    parser=OptionParser()

    parser.add_option("-f", "--input_file", dest="input_file", action="store", type="string",\
                        help="Input secondary structure id dot-bracket notation (file)", metavar="FILENAME")


    parser.add_option("-i", "--ss_input", dest="ss_input", action="store", type="string",\
                        help="Input secondary structure (text)", metavar="TEXT")


    parser.add_option("--seq", dest="seq_input", action="store", type="string",\
                        help="Input sequence", metavar="SEQ_INPUT", default='')


    parser.add_option("-o", "--graph_output", dest="output_fname", action="store", type="string",\
                        help="Output file name", metavar="FNAME")


    parser.add_option("-p", "--plot_graph", dest="plot_graph", action="store_true",\
                        help="Plot RNA motifs association graph", default=False)


    parser.add_option("-v", "--verbose", dest="verbose", action="store_true",\
                        help="Verbose output", default=False)

    (options, _args)  = parser.parse_args()
    return (parser, options)


def main():
    (parser, options) = parse_args()

    if options.ss_input:
        try:
            rnagraphs_object = rnagraphs(options.ss_input, options.seq_input)
        except Exception as e:
            print(e)
            exit(1)
    elif options.input_file:
        try:
            rnagraphs_object = rnagraphs(options.input_file)
        except Exception as e:
            print(e)
            exit(1)
    else:
        parser.print_help()
        exit(1)

    if options.verbose:
        print(rnagraphs_object.sequence)
        print("\n".join(rnagraphs_object.input_dotbracket))

    rnagraphs_object.extract_motifs(plot_graph=options.plot_graph, output_fname=options.output_fname, verbose=options.verbose)


if __name__=="__main__":
    main()


