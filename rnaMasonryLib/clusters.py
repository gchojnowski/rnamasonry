#! /usr/bin/env python
# -*- coding: utf-8 -*-
# =============================================================================
# Created at:
#  International Institute of Molecular and Cell Biology in Warsaw, Poland
# Author:
#  Grzegorz Chojnowski - gchojnowski@genesilico.pl, gchojnowski@gmail.com
# =============================================================================

import os,sys
import numpy as np

# -------------- CCTBX
from iotbx.pdb import input as pdb_input
from scitbx.math import superpose
from scitbx import matrix
from scitbx.array_family import flex
import iotbx
# --------------------

try:
    from rnamasonry_config import *
except:
    sys.path.append("..")
    from rnamasonry_config import *

sys.path.append(os.path.join(ROOT, "lib"))
from progressbar import ProgressBar, Percentage, Bar, ETA
from pdb_preprocessor import iPDB


# ------------------------------------------------------------------------------


class clusters:

    def __init__(self, mc):
        self.all_clusters = []
        self.decoys = []
        self.mc = mc

    # -------------------------------------------------------------------------


    def get_ref_atoms(self, _ph, sel_string = "name P"):
        sel_cache = _ph.atom_selection_cache()

        assert sel_cache.n_seq == _ph.atoms_size()
        isel = sel_cache.iselection
        return _ph.select(isel(sel_string)).atoms().extract_xyz()


    # -------------------------------------------------------------------------


    def rmsd(self, ph_ref, ph_mov):


        ref_xyz = self.get_ref_atoms(ph_ref)
        mov_xyz = self.get_ref_atoms(ph_mov)

        superposition = superpose.least_squares_fit(ref_xyz, mov_xyz, method=["kearsley", "kabsch"][0])

        rtmx = matrix.rt((superposition.r, superposition.t))

        x = ref_xyz
        y = rtmx * mov_xyz

        d_sq = (x-y).dot()

        return flex.mean(d_sq)**0.5



        pass


    # -------------------------------------------------------------------------


    def add_decoy(self, ib, ie):

        self.decoys.append( (ib, ie) )

    # -------------------------------------------------------------------------


    def cluster_decoys(self, fraction=0.1):

        best_decoys = max( 1000, int(fraction*len(self.decoys)) )
        best_decoys = min( best_decoys, len(self.decoys) )


        print(' --> Clustering best %i decoys '%best_decoys)
        widgets = ['     ', Percentage(), ' ', Bar(), ' ', ETA()]
        pbar = ProgressBar(widgets=widgets, maxval=best_decoys, fd=sys.stdout).start()



        self.all_clusters = []

        for idecoy, decoy in enumerate(sorted(self.decoys, key=lambda x: x[1])[:best_decoys]):

            pbar.update(idecoy+1)

            _ph = self.mc.get_ph(decoy[0])
            _e = decoy[1]

            redundant = False
            for clust in self.all_clusters:
                if self.rmsd(clust[0][0], _ph)<10.0:
                    clust.append( (_ph, _e) )
                    redundant = True
                    break
            if not redundant:
                self.all_clusters.append( [(_ph, _e)] )


    # -------------------------------------------------------------------------


    def show_clusters(self):
        print()
        print(" --> Clustering stats")
        print("       idx      size    e_mean     e_std")
        for ic, clust in enumerate(self.all_clusters):
            _a =  np.array([_c[1] for _c in clust])
            print("%10i%10i%10.2f%10.2f" % (ic+1, len(clust), _a.mean(), _a.std()))
        print()


    # -------------------------------------------------------------------------

    def dump_clusters(self, project_dir):

        print(" --> Saving PDB files with cluster representatives:")

        for ic, clust in enumerate(self.all_clusters):
            best = sorted(clust, key=lambda x:x[1])[0]
            fname = os.path.join(project_dir, "clust_%05i.pdb" % (ic+1))
            best[0].write_pdb_file( fname )
            print(("     %s" % fname))
        print()

    # -------------------------------------------------------------------------

    def dump_top_decoys(self, project_dir, fraction):


        n_best_decoys = int( fraction*len(self.decoys) )

        for idecoy, decoy in enumerate(sorted(self.decoys, key=lambda x: x[1])[:n_best_decoys]):
            _ph = self.mc.get_ph(decoy[0])
            _e = decoy[1]
            with open( os.path.join(project_dir, "decoy_%05i.pdb" % (idecoy+1)), 'w' ) as _of:
                _of.write('REMARK E=%f\n' % _e)
                _of.write( _ph.as_pdb_string() )




    # -------------------------------------------------------------------------

    def compare_with_reference(self, ref_fname, project_dir):

        ref_ipdb = iPDB(ref_fname, verbose=False)
        ofile = open(os.path.join(project_dir, 'energy_rmsd.dat'), 'w')

        widgets = [' --> Comparing decoys with reference structure ', Percentage(), ' ', Bar(), ' ', ETA()]
        pbar = ProgressBar(widgets=widgets, maxval=len(self.decoys), fd=sys.stdout).start()


        for idecoy, decoy in enumerate(self.decoys):
            pbar.update(idecoy+1)
            _ph = self.mc.get_ph(decoy[0])
            _e = decoy[1]
            try:
                ofile.write( "%.5f %.5f\n" % (self.rmsd(ref_ipdb.rna_ph, _ph), _e) )
            except:
                raise Exception('ERROR: Refefrence model and target sequence do not match.')

        ofile.close()

# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------






