#! /usr/bin/env libtbx.python
# -*- coding: utf-8 -*-
# =============================================================================
# Created at:
#  International Institute of Molecular and Cell Biology in Warsaw, Poland
# Author:
#  Grzegorz Chojnowski - gchojnowski@genesilico.pl, gchojnowski@gmail.com
# =============================================================================

import os, sys
from itertools import *
from operator import *
import numpy as np
from collections import deque
import gzip
from Bio import PDB
import io
from progressbar import ProgressBar, Percentage, Bar, ETA

# a simple class for adding base-pairs at the end of RNA/DNA helices
import iotbx.pdb
from grow import growth

# --------------- MODERNA imports -------------------
from moderna.ModernaStructure import ModernaStructure
from moderna import *
# ---------------------------------------------------

from dbutils import get_fobj_from_database

# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------

class builder():


    def __init__(self, local_db='local_db', tmp_dir=None, nfrags=20, max_gap_size=10):

        self.local_db = local_db

        try:
            with open(os.path.join(self.local_db, "db_info"), 'r') as f:
                self.structures = [line.split() for line in f.read().split('\n') if line and not line.startswith('#')]
        except KeyboardInterrupt:
            raise
        except:
            raise Exception("ERROR: db_info file not found")

        for _t in self.structures:
            _t.append( int( (_t[1].count('_')-1)/2 ) )



        self.tmp_dir = tmp_dir
        self.nfrags = nfrags
        self.max_gap_size = max_gap_size
        if self.tmp_dir:
            directory = os.path.join(self.tmp_dir, 'fake_fragments')
            if not os.path.exists(directory): os.mkdir(directory)


    # -------------------------------------------------------------------------


    def __get_chain_id(self, fname):
        """
            a simple helper function. it opens an PDB file to find the model chain id
            it is assumed that all the reisues in the PDB file belong to the same chain
        """

        try:
            ifile = open(fname, 'r')
        except:
            ifile = get_fobj_from_database(fname, self.local_db)

        for line in ifile.readlines():
            if line.startswith("ATOM") or line.startswith("HETATM"): break


        ifile.close()

        return line[21]


    # -------------------------------------------------------------------------


    def __get_resi_ranges(self, model):
        return [[_[1].number for _ in group] for key, group in groupby(enumerate(model), lambda i_x:i_x[0]-i_x[1].number)]


    # -------------------------------------------------------------------------


    def __renumber_residues(self, model):
        """
            remove alt confs introduced by moderna
            keeping index-breaks at flanking WC-pairs
        """
        index = list(model)[-1].number+1
        for resi, next_resi in zip(list(model), list(model)[1:]):
            model.renumber_residue(resi.identifier, str(index))
            if next_resi.number-resi.number>1:
                index+=10
            index+=1
        model.renumber_residue(next_resi.identifier, str(index))


    # -------------------------------------------------------------------------


    def __insert_fragment(self, model, frag_residues, length):
        """
            Finds a fragment of specified length and inserts it into input fragment
        """

        resi_start = frag_residues[0]
        resi_end = frag_residues[1]

        lf = FragmentFinder(model[str(resi_start)], \
                            model[str(resi_end)], \
                            Sequence('U'*length), \
                            model, 1)
        candidates = lf.find_fragment_candidates()
        insert_fragment(model, candidates[0])

    # -------------------------------------------------------------------------


    def get_matches(self, cyclic_permutation, fragment_type):
        """
            Returns fragment ids with desired toplogy (any cyclic permutation)
        """
        clen = cyclic_permutation.strip('_').count('_')+1

        if 1:

            matching_structure_ids = []

            for _idx,t in enumerate(self.structures[:-1]):

                if t[3] != clen: continue

                if t[0] != fragment_type: continue

                if cyclic_permutation in t[1]:
                    matching_structure_ids.append(t[2])


        else:

            matching_structure_ids = [t[2] for t in self.structures[:-1] \
                                                  if t[0] == fragment_type and \
                                                    int(len(t[1].split('_')[1:-1])/2) == clen and \
                                                    cyclic_permutation in t[1]]

        return matching_structure_ids


    # -------------------------------------------------------------------------


    def find_similar_frags(self, cyclic_permutation, fragment_type):
        """
            Finds frags that could be used to build a target structure
        """
        lengths = list(map(int, cyclic_permutation.strip('_').split('_')))
        results = []
        maxd = 4
        while True:
            for dvec in list(product(list(range(maxd)), repeat=len(lengths))):
                l = np.array(lengths)-np.array(dvec)

                # a chain frag in a motif should be at least 2 nucs long
                if list(l>=2).count(False):
                    continue

                matches = self.get_matches("_%s_" % "_".join(map(str,l)), fragment_type)
                if matches:
                    results.append( [tuple(dvec), matches])

            if len(results)>self.nfrags: break
            if not results and maxd<self.max_gap_size:
                maxd+=1
            else:
                break

        return results


    # -------------------------------------------------------------------------


    def build(self, cyclic_permutation, fragment_type, debug=False, verbose=False):
        """
            Function that builds complete fragments from smaller ones
        """



        widgets = ['     ', Percentage(), ' ', Bar(), ' ', ETA()]
        if not verbose: pbar = ProgressBar(widgets=widgets, maxval=self.nfrags, term_width=50, fd=sys.stdout).start()


        lengths = list(map(int, cyclic_permutation.strip('_').split('_')))
        if verbose: print("       ", lengths)
        results = []

        similar_frags = self.find_similar_frags(cyclic_permutation, fragment_type)


        if not similar_frags:
            raise Exception("cannot find template for a motif")

        for mi, (dvec, matches) in enumerate(similar_frags):
            for _fid in matches:
                dvec_str = "_"+"_".join(map(str,dvec))

                fname = os.path.join(self.local_db, \
                                        fragment_type, \
                                        'structure_' + _fid + '.pdb')

                # stems must be treated separately, modeRNA cannot extend them properly
                if fragment_type=='stem':

                    try:
                        with open(fname, 'r') as ifile:
                            stem = iotbx.pdb.hierarchy.input(pdb_string=ifile.read()).hierarchy
                    except:
                        ifile = get_fobj_from_database(fname, self.local_db)
                        stem = iotbx.pdb.hierarchy.input(pdb_string=ifile.read()).hierarchy

                    grower = growth(dna=False)
                    assert len(dvec)==2 and dvec[0]==dvec[1]

                    #XXX it may fail in 1001 cases, why should i care?
                    try:
                        for _i in range(dvec[0]):
                            stem = grower.grow_stem(stem, append_to_leading_chain = True if _i%2 else False)
                        stem.write_pdb_file(self.tmp_dir+'/fake_fragments/structure_m' + str(_fid) + dvec_str + '.pdb')
                        results.append("m%s%s" % ( str(_fid), dvec_str ))
                    except KeyboardInterrupt:
                        raise
                    except:
                        pass


                else:

                    chid = self.__get_chain_id( fname )


                    # gchojnowski@04.2020: obsolete
                    #try:
                    #    m = load_model(fname, chid)
                    #except:

                    try:
                        ifile = open(fname, 'r')
                    except:
                        ifile = get_fobj_from_database(fname, self.local_db)

                    parser = PDB.PDBParser()
                    _h = parser.get_structure("c",ifile)
                    m = load_model(_h, chain_name=chid, data_type='structure')


                    if debug: m.write_pdb_file('mod_before_%i.pdb' % mi)
                    mfrags = self.__get_resi_ranges(m)
                    mfrags_deque = deque(mfrags)
                    mlen_deque = deque( list(map(len, mfrags)) )
                    for _i in range(len(dvec)):
                        if list(np.array(mlen_deque) + np.array(dvec)) == lengths:
                            break
                        mlen_deque.rotate()
                        mfrags_deque.rotate()

                    if verbose: print("     - ", list(mlen_deque), dvec, _fid)

                    mfrags = list(mfrags_deque)
                    mlen = list(mlen_deque)

                    broken = False

                    for di,d in enumerate(dvec):
                        try:
                            if d>0: self.__insert_fragment(m, mfrags[di], d)
                        except:
                            broken = True
                            break

                    self.__renumber_residues(m)

                    if debug: m.write_pdb_file('mod_after_%i.pdb' % mi)
                    if self.tmp_dir and not broken:
                        m.write_pdb_file(self.tmp_dir+'/fake_fragments/structure_m' + str(_fid) + dvec_str + '.pdb')
                        results.append("m%s%s" % ( str(_fid), dvec_str ))

                if not verbose: pbar.update( min(len(results), self.nfrags-1) )

                if len(results)>self.nfrags: return results

        if not verbose: pbar.update( self.nfrags )

        return results

# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------

def test(cyclic_permutation = "_9_3_", fragment_type="loop"):
    bo = builder(local_db="../local_db/", tmp_dir=None)
    bo.build(cyclic_permutation, fragment_type, debug=True)
    pass



if __name__=="__main__":
    test()
