# -*- coding: utf-8 -*-
# =============================================================================
# Created at:
#  International Institute of Molecular and Cell Biology in Warsaw, Poland
# Author:
#  Grzegorz Chojnowski - gchojnowski@genesilico.pl, gchojnowski@gmail.com
# =============================================================================


import os

ROOT = os.path.join( os.path.dirname(os.path.realpath(__file__)), '..' )

RNABRICKS2_URL = "http://iimcb.genesilico.pl/rnabricks2/"




MIN_MODERNA_NFRAGS = 5
FRAG_TYPES = ('internal_loop', 'terminal_loop', 'stem', 'strand')

FOXS_PATH = "%s/rnaMasonryLib/foxs" % ROOT
AVAILABLE_RMSDS = [5.0, 2.0, 1.0]
AVAILABLE_NRLISTS = ['1.5', '2.0', '2.5', '3.0', '3.5', '4.0', 'all', 'ignore']

