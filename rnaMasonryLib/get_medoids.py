#! /usr/bin/env python

import urllib2, urllib
import string
import os
from StringIO import StringIO
import gzip
import json
import tarfile
from progressbar import ProgressBar, Percentage, Bar, ETA
from motifs import Branch


RNABRICKS_URL = "http://iimcb.genesilico.pl/rnabricks"


def get_pdb(name):
    url = urllib2.urlopen(url="%s/site_media/data/fragments/%s" % \
                                    (RNABRICKS_URL, name) )
    url_f = StringIO(url.read())
    gzipfile = gzip.GzipFile(fileobj=url_f)
    return gzipfile.read()



def parse_list_if_medoids(fname):
    data = []
    with open(fname, 'r') as f:
        for line in f.readlines():
            d = line.split()
            name = d[1].split('_')[1]
            filename = d[1].split('/')[-1]
            fid = d[0]
            strings  = '_%s_'%'_'.join([str(len(_)) for _ in filter(None, d[2].split('_'))])

            data.append( (name, strings, fid, filename) )

    return sorted(data, key=lambda x:x[1])



def create_db_local(fname="medoids_20052015.txt"):
        db_dir = 'local_db'
        os.mkdir(db_dir)

        db_info = []
        data = parse_list_if_medoids(fname)

        widgets = [' --> Downloading fragments: ', \
                                Percentage(), ' ', Bar(), ' ', ETA()]

        pbar = ProgressBar(widgets=widgets, maxval=len(data), fd=sys.stdout).start()




        for idx,item in enumerate(data):
            pbar.update(idx+1)
            ftype = item[0]
            fid = item[2]
            fname = item[-1]
            path = os.path.join(db_dir, ftype)
            if not os.path.exists(path):
                os.mkdir(path)

            s=get_pdb(fname)
            with open(path+'/structure_'+str(fid)+'.pdb', 'w') as f:
                f.write(s)


            db_info.append( " ".join(item[:-1]) )


        with open(db_dir+'/db_info','w') as handle:
            handle.write("\n".join(db_info))

        with tarfile.open(db_dir+'.tar.gz', 'w:gz') as handle:
            handle.add(db_dir, arcname=db_dir)


def main():
    create_db_local()


if __name__=="__main__":
    main()
