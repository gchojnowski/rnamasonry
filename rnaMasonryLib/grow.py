#! /usr/bin/env libtbx.python
# -*- coding: utf-8 -*-
# =============================================================================
# Created at:
#  International Institute of Molecular and Cell Biology in Warsaw, Poland
# Author:
#  Grzegorz Chojnowski - gchojnowski@genesilico.pl, gchojnowski@gmail.com
# =============================================================================


import iotbx.pdb
from scitbx import matrix
from scitbx.math import r3_rotation_axis_and_angle_as_matrix
import math
from libtbx.test_utils import approx_equal
from cctbx.array_family import flex
from scitbx.math import superpose
import numpy as np
import string
from cctbx import maptbx



from itertools import *

model_stem_DNA="""\
REMARK    3DNA v2.1 (c) 2012 Dr. Xiang-Jun Lu (http://x3dna.org)
ATOM      1  P     G A   1      -0.356   9.218   1.848  1.00  0.00           P
ATOM      2  O1P   G A   1      -0.311  10.489   2.605  1.00  0.00           O
ATOM      3  O2P   G A   1      -1.334   9.156   0.740  1.00  0.00           O
ATOM      4  O5'   G A   1       1.105   8.869   1.295  1.00  0.00           O
ATOM      5  C5'   G A   1       2.021   8.156   2.146  1.00  0.00           C
ATOM      6  C4'   G A   1       2.726   7.072   1.355  1.00  0.00           C
ATOM      7  O4'   G A   1       1.986   5.817   1.352  1.00  0.00           O
ATOM      8  C3'   G A   1       2.952   7.370  -0.127  1.00  0.00           C
ATOM      9  O3'   G A   1       4.210   6.832  -0.518  1.00  0.00           O
ATOM     10  C2'   G A   1       1.848   6.598  -0.850  1.00  0.00           C
ATOM     11  C1'   G A   1       1.913   5.344   0.016  1.00  0.00           C
ATOM     12  N9    G A   1       0.711   4.472  -0.101  1.00  0.00           N
ATOM     13  C8    G A   1      -0.606   4.826  -0.294  1.00  0.00           C
ATOM     14  N7    G A   1      -1.430   3.807  -0.354  1.00  0.00           N
ATOM     15  C5    G A   1      -0.599   2.700  -0.190  1.00  0.00           C
ATOM     16  C6    G A   1      -0.914   1.317  -0.165  1.00  0.00           C
ATOM     17  O6    G A   1      -2.010   0.775  -0.284  1.00  0.00           O
ATOM     18  N1    G A   1       0.233   0.533   0.025  1.00  0.00           N
ATOM     19  C2    G A   1       1.516   1.023   0.172  1.00  0.00           C
ATOM     20  N2    G A   1       2.476   0.111   0.344  1.00  0.00           N
ATOM     21  N3    G A   1       1.811   2.321   0.149  1.00  0.00           N
ATOM     22  C4    G A   1       0.709   3.095  -0.035  1.00  0.00           C
ATOM     23  P     G A   2       5.130   7.667  -1.527  1.00  0.00           P
ATOM     24  O1P   G A   2       5.914   8.669  -0.770  1.00  0.00           O
ATOM     25  O2P   G A   2       4.303   8.192  -2.635  1.00  0.00           O
ATOM     26  O5'   G A   2       6.107   6.526  -2.080  1.00  0.00           O
ATOM     27  C5'   G A   2       6.430   5.410  -1.229  1.00  0.00           C
ATOM     28  C4'   G A   2       6.362   4.119  -2.020  1.00  0.00           C
ATOM     29  O4'   G A   2       5.026   3.539  -2.023  1.00  0.00           O
ATOM     30  C3'   G A   2       6.720   4.227  -3.502  1.00  0.00           C
ATOM     31  O3'   G A   2       7.422   3.053  -3.893  1.00  0.00           O
ATOM     32  C2'   G A   2       5.374   4.251  -4.225  1.00  0.00           C
ATOM     33  C1'   G A   2       4.689   3.199  -3.359  1.00  0.00           C
ATOM     34  N9    G A   2       3.204   3.200  -3.476  1.00  0.00           N
ATOM     35  C8    G A   2       2.346   4.261  -3.669  1.00  0.00           C
ATOM     36  N7    G A   2       1.081   3.921  -3.729  1.00  0.00           N
ATOM     37  C5    G A   2       1.103   2.537  -3.565  1.00  0.00           C
ATOM     38  C6    G A   2       0.034   1.603  -3.540  1.00  0.00           C
ATOM     39  O6    G A   2      -1.170   1.808  -3.659  1.00  0.00           O
ATOM     40  N1    G A   2       0.502   0.295  -3.350  1.00  0.00           N
ATOM     41  C2    G A   2       1.828  -0.064  -3.203  1.00  0.00           C
ATOM     42  N2    G A   2       2.068  -1.366  -3.031  1.00  0.00           N
ATOM     43  N3    G A   2       2.829   0.813  -3.226  1.00  0.00           N
ATOM     44  C4    G A   2       2.393   2.087  -3.410  1.00  0.00           C
ATOM     45  P     G A   3       8.657   3.187  -4.902  1.00  0.00           P
ATOM     46  O1P   G A   3       9.880   3.537  -4.145  1.00  0.00           O
ATOM     47  O2P   G A   3       8.296   4.098  -6.010  1.00  0.00           O
ATOM     48  O5'   G A   3       8.777   1.690  -5.455  1.00  0.00           O
ATOM     49  C5'   G A   3       8.382   0.598  -4.604  1.00  0.00           C
ATOM     50  C4'   G A   3       7.568  -0.407  -5.395  1.00  0.00           C
ATOM     51  O4'   G A   3       6.146  -0.091  -5.398  1.00  0.00           O
ATOM     52  C3'   G A   3       7.921  -0.530  -6.877  1.00  0.00           C
ATOM     53  O3'   G A   3       7.799  -1.892  -7.268  1.00  0.00           O
ATOM     54  C2'   G A   3       6.846   0.281  -7.600  1.00  0.00           C
ATOM     55  C1'   G A   3       5.674  -0.168  -6.734  1.00  0.00           C
ATOM     56  N9    G A   3       4.473   0.705  -6.851  1.00  0.00           N
ATOM     57  C8    G A   3       4.403   2.068  -7.044  1.00  0.00           C
ATOM     58  N7    G A   3       3.179   2.536  -7.104  1.00  0.00           N
ATOM     59  C5    G A   3       2.383   1.404  -6.940  1.00  0.00           C
ATOM     60  C6    G A   3       0.970   1.276  -6.915  1.00  0.00           C
ATOM     61  O6    G A   3       0.116   2.151  -7.034  1.00  0.00           O
ATOM     62  N1    G A   3       0.579  -0.056  -6.725  1.00  0.00           N
ATOM     63  C2    G A   3       1.441  -1.126  -6.578  1.00  0.00           C
ATOM     64  N2    G A   3       0.870  -2.320  -6.406  1.00  0.00           N
ATOM     65  N3    G A   3       2.767  -1.005  -6.601  1.00  0.00           N
ATOM     66  C4    G A   3       3.162   0.282  -6.785  1.00  0.00           C
ATOM     67  P     C B   4      -8.877  -2.510  -8.598  1.00  0.00           P
ATOM     68  O1P   C B   4     -10.072  -2.945  -9.355  1.00  0.00           O
ATOM     69  O2P   C B   4      -9.120  -1.561  -7.490  1.00  0.00           O
ATOM     70  O5'   C B   4      -8.094  -3.791  -8.045  1.00  0.00           O
ATOM     71  C5'   C B   4      -7.132  -4.443  -8.896  1.00  0.00           C
ATOM     72  C4'   C B   4      -5.883  -4.778  -8.105  1.00  0.00           C
ATOM     73  O4'   C B   4      -4.919  -3.686  -8.102  1.00  0.00           O
ATOM     74  C3'   C B   4      -6.097  -5.085  -6.623  1.00  0.00           C
ATOM     75  O3'   C B   4      -5.197  -6.115  -6.232  1.00  0.00           O
ATOM     76  C2'   C B   4      -5.704  -3.797  -5.900  1.00  0.00           C
ATOM     77  C1'   C B   4      -4.491  -3.471  -6.766  1.00  0.00           C
ATOM     78  N1    C B   4      -4.033  -2.058  -6.649  1.00  0.00           N
ATOM     79  C2    C B   4      -2.666  -1.809  -6.740  1.00  0.00           C
ATOM     80  O2    C B   4      -1.896  -2.761  -6.913  1.00  0.00           O
ATOM     81  N3    C B   4      -2.228  -0.528  -6.635  1.00  0.00           N
ATOM     82  C4    C B   4      -3.095   0.476  -6.448  1.00  0.00           C
ATOM     83  N4    C B   4      -2.618   1.708  -6.352  1.00  0.00           N
ATOM     84  C5    C B   4      -4.505   0.241  -6.351  1.00  0.00           C
ATOM     85  C6    C B   4      -4.922  -1.046  -6.458  1.00  0.00           C
ATOM     86  P     C B   5      -5.706  -7.249  -5.223  1.00  0.00           P
ATOM     87  O1P   C B   5      -6.417  -8.303  -5.980  1.00  0.00           O
ATOM     88  O2P   C B   5      -6.461  -6.623  -4.115  1.00  0.00           O
ATOM     89  O5'   C B   5      -4.320  -7.825  -4.670  1.00  0.00           O
ATOM     90  C5'   C B   5      -3.159  -7.787  -5.521  1.00  0.00           C
ATOM     91  C4'   C B   5      -1.951  -7.323  -4.730  1.00  0.00           C
ATOM     92  O4'   C B   5      -1.813  -5.874  -4.727  1.00  0.00           O
ATOM     93  C3'   C B   5      -1.943  -7.697  -3.248  1.00  0.00           C
ATOM     94  O3'   C B   5      -0.610  -8.002  -2.857  1.00  0.00           O
ATOM     95  C2'   C B   5      -2.383  -6.424  -2.525  1.00  0.00           C
ATOM     96  C1'   C B   5      -1.593  -5.448  -3.391  1.00  0.00           C
ATOM     97  N1    C B   5      -2.053  -4.036  -3.274  1.00  0.00           N
ATOM     98  C2    C B   5      -1.094  -3.031  -3.365  1.00  0.00           C
ATOM     99  O2    C B   5       0.089  -3.349  -3.538  1.00  0.00           O
ATOM    100  N3    C B   5      -1.492  -1.737  -3.260  1.00  0.00           N
ATOM    101  C4    C B   5      -2.783  -1.434  -3.073  1.00  0.00           C
ATOM    102  N4    C B   5      -3.122  -0.158  -2.977  1.00  0.00           N
ATOM    103  C5    C B   5      -3.786  -2.453  -2.976  1.00  0.00           C
ATOM    104  C6    C B   5      -3.367  -3.740  -3.083  1.00  0.00           C
ATOM    105  P     C B   6      -0.356  -9.218  -1.848  1.00  0.00           P
ATOM    106  O1P   C B   6      -0.311 -10.489  -2.605  1.00  0.00           O
ATOM    107  O2P   C B   6      -1.334  -9.156  -0.740  1.00  0.00           O
ATOM    108  O5'   C B   6       1.105  -8.869  -1.295  1.00  0.00           O
ATOM    109  C5'   C B   6       2.021  -8.156  -2.146  1.00  0.00           C
ATOM    110  C4'   C B   6       2.726  -7.072  -1.355  1.00  0.00           C
ATOM    111  O4'   C B   6       1.986  -5.817  -1.352  1.00  0.00           O
ATOM    112  C3'   C B   6       2.952  -7.370   0.127  1.00  0.00           C
ATOM    113  O3'   C B   6       4.210  -6.832   0.518  1.00  0.00           O
ATOM    114  C2'   C B   6       1.848  -6.598   0.850  1.00  0.00           C
ATOM    115  C1'   C B   6       1.913  -5.344  -0.016  1.00  0.00           C
ATOM    116  N1    C B   6       0.711  -4.472   0.101  1.00  0.00           N
ATOM    117  C2    C B   6       0.897  -3.095   0.010  1.00  0.00           C
ATOM    118  O2    C B   6       2.040  -2.657  -0.163  1.00  0.00           O
ATOM    119  N3    C B   6      -0.186  -2.282   0.115  1.00  0.00           N
ATOM    120  C4    C B   6      -1.409  -2.796   0.302  1.00  0.00           C
ATOM    121  N4    C B   6      -2.433  -1.963   0.398  1.00  0.00           N
ATOM    122  C5    C B   6      -1.621  -4.210   0.399  1.00  0.00           C
ATOM    123  C6    C B   6      -0.526  -5.004   0.292  1.00  0.00           C
END
"""

model_stem_RNA = """\
REMARK    3DNA v2.1 (c) 2012 Dr. Xiang-Jun Lu (http://x3dna.org)
ATOM      1  P     A A   1       3.063   8.025  -4.135  1.00  0.00           P
ATOM      2  O1P   A A   1       3.223   8.856  -5.350  1.00  0.00           O
ATOM      3  O2P   A A   1       1.891   7.121  -4.118  1.00  0.00           O
ATOM      4  O5'   A A   1       4.396   7.181  -3.881  1.00  0.00           O
ATOM      5  C5'   A A   1       5.621   7.881  -3.587  1.00  0.00           C
ATOM      6  C4'   A A   1       6.719   6.889  -3.258  1.00  0.00           C
ATOM      7  O4'   A A   1       6.486   6.364  -1.919  1.00  0.00           O
ATOM      8  C3'   A A   1       6.776   5.642  -4.140  1.00  0.00           C
ATOM      9  O3'   A A   1       7.397   5.908  -5.390  1.00  0.00           O
ATOM     10  C2'   A A   1       7.567   4.725  -3.208  1.00  0.00           C
ATOM     11  O2'   A A   1       8.962   4.911  -3.068  1.00  0.00           O
ATOM     12  C1'   A A   1       6.874   4.999  -1.877  1.00  0.00           C
ATOM     13  N9    A A   1       5.666   4.158  -1.647  1.00  0.00           N
ATOM     14  C8    A A   1       4.345   4.449  -1.900  1.00  0.00           C
ATOM     15  N7    A A   1       3.524   3.496  -1.584  1.00  0.00           N
ATOM     16  C5    A A   1       4.348   2.496  -1.086  1.00  0.00           C
ATOM     17  C6    A A   1       4.082   1.215  -0.578  1.00  0.00           C
ATOM     18  N6    A A   1       2.849   0.697  -0.485  1.00  0.00           N
ATOM     19  N1    A A   1       5.134   0.481  -0.167  1.00  0.00           N
ATOM     20  C2    A A   1       6.356   1.001  -0.262  1.00  0.00           C
ATOM     21  N3    A A   1       6.725   2.179  -0.716  1.00  0.00           N
ATOM     22  C4    A A   1       5.655   2.893  -1.121  1.00  0.00           C
ATOM     23  P     U A   2       6.913   5.099  -6.683  1.00  0.00           P
ATOM     24  O1P   U A   2       7.496   5.711  -7.898  1.00  0.00           O
ATOM     25  O2P   U A   2       5.439   4.971  -6.666  1.00  0.00           O
ATOM     26  O5'   U A   2       7.579   3.667  -6.429  1.00  0.00           O
ATOM     27  C5'   U A   2       8.988   3.595  -6.135  1.00  0.00           C
ATOM     28  C4'   U A   2       9.376   2.167  -5.806  1.00  0.00           C
ATOM     29  O4'   U A   2       8.896   1.851  -4.467  1.00  0.00           O
ATOM     30  C3'   U A   2       8.750   1.087  -6.688  1.00  0.00           C
ATOM     31  O3'   U A   2       9.417   0.975  -7.938  1.00  0.00           O
ATOM     32  C2'   U A   2       8.920  -0.112  -5.756  1.00  0.00           C
ATOM     33  O2'   U A   2      10.194  -0.710  -5.616  1.00  0.00           O
ATOM     34  C1'   U A   2       8.486   0.493  -4.425  1.00  0.00           C
ATOM     35  N1    U A   2       7.014   0.438  -4.195  1.00  0.00           N
ATOM     36  C2    U A   2       6.509  -0.720  -3.655  1.00  0.00           C
ATOM     37  O2    U A   2       7.207  -1.677  -3.367  1.00  0.00           O
ATOM     38  N3    U A   2       5.143  -0.735  -3.456  1.00  0.00           N
ATOM     39  C4    U A   2       4.261   0.286  -3.745  1.00  0.00           C
ATOM     40  O4    U A   2       3.056   0.157  -3.522  1.00  0.00           O
ATOM     41  C5    U A   2       4.885   1.461  -4.308  1.00  0.00           C
ATOM     42  C6    U A   2       6.213   1.503  -4.512  1.00  0.00           C
ATOM     43  P     A A   3       8.572   0.556  -9.231  1.00  0.00           P
ATOM     44  O1P   A A   3       9.394   0.756 -10.446  1.00  0.00           O
ATOM     45  O2P   A A   3       7.262   1.245  -9.214  1.00  0.00           O
ATOM     46  O5'   A A   3       8.359  -1.008  -8.977  1.00  0.00           O
ATOM     47  C5'   A A   3       9.505  -1.830  -8.683  1.00  0.00           C
ATOM     48  C4'   A A   3       9.061  -3.241  -8.354  1.00  0.00           C
ATOM     49  O4'   A A   3       8.487  -3.248  -7.015  1.00  0.00           O
ATOM     50  C3'   A A   3       7.950  -3.812  -9.236  1.00  0.00           C
ATOM     51  O3'   A A   3       8.451  -4.266 -10.486  1.00  0.00           O
ATOM     52  C2'   A A   3       7.446  -4.913  -8.304  1.00  0.00           C
ATOM     53  O2'   A A   3       8.196  -6.104  -8.164  1.00  0.00           O
ATOM     54  C1'   A A   3       7.407  -4.169  -6.973  1.00  0.00           C
ATOM     55  N9    A A   3       6.139  -3.421  -6.743  1.00  0.00           N
ATOM     56  C8    A A   3       5.854  -2.099  -6.996  1.00  0.00           C
ATOM     57  N7    A A   3       4.646  -1.749  -6.680  1.00  0.00           N
ATOM     58  C5    A A   3       4.079  -2.914  -6.182  1.00  0.00           C
ATOM     59  C6    A A   3       2.804  -3.206  -5.674  1.00  0.00           C
ATOM     60  N6    A A   3       1.820  -2.300  -5.581  1.00  0.00           N
ATOM     61  N1    A A   3       2.574  -4.467  -5.263  1.00  0.00           N
ATOM     62  C2    A A   3       3.556  -5.362  -5.358  1.00  0.00           C
ATOM     63  N3    A A   3       4.781  -5.207  -5.812  1.00  0.00           N
ATOM     64  C4    A A   3       4.984  -3.937  -6.217  1.00  0.00           C
ATOM     65  P     U B   4      -6.022  -6.126  -0.961  1.00  0.00           P
ATOM     66  O1P   U B   4      -6.710  -6.617   0.254  1.00  0.00           O
ATOM     67  O2P   U B   4      -5.688  -4.684  -0.978  1.00  0.00           O
ATOM     68  O5'   U B   4      -4.699  -6.987  -1.215  1.00  0.00           O
ATOM     69  C5'   U B   4      -4.826  -8.391  -1.509  1.00  0.00           C
ATOM     70  C4'   U B   4      -3.467  -8.977  -1.838  1.00  0.00           C
ATOM     71  O4'   U B   4      -3.086  -8.547  -3.177  1.00  0.00           O
ATOM     72  C3'   U B   4      -2.309  -8.509  -0.956  1.00  0.00           C
ATOM     73  O3'   U B   4      -2.293  -9.185   0.294  1.00  0.00           O
ATOM     74  C2'   U B   4      -1.146  -8.847  -1.888  1.00  0.00           C
ATOM     75  O2'   U B   4      -0.734 -10.193  -2.028  1.00  0.00           O
ATOM     76  C1'   U B   4      -1.684  -8.332  -3.219  1.00  0.00           C
ATOM     77  N1    U B   4      -1.422  -6.883  -3.449  1.00  0.00           N
ATOM     78  C2    U B   4      -0.204  -6.546  -3.989  1.00  0.00           C
ATOM     79  O2    U B   4       0.645  -7.372  -4.277  1.00  0.00           O
ATOM     80  N3    U B   4       0.003  -5.195  -4.188  1.00  0.00           N
ATOM     81  C4    U B   4      -0.884  -4.179  -3.899  1.00  0.00           C
ATOM     82  O4    U B   4      -0.586  -3.003  -4.122  1.00  0.00           O
ATOM     83  C5    U B   4      -2.135  -4.631  -3.336  1.00  0.00           C
ATOM     84  C6    U B   4      -2.363  -5.939  -3.132  1.00  0.00           C
ATOM     85  P     A B   5      -1.758  -8.408   1.587  1.00  0.00           P
ATOM     86  O1P   A B   5      -2.072  -9.193   2.802  1.00  0.00           O
ATOM     87  O2P   A B   5      -2.256  -7.014   1.570  1.00  0.00           O
ATOM     88  O5'   A B   5      -0.180  -8.418   1.333  1.00  0.00           O
ATOM     89  C5'   A B   5       0.473  -9.668   1.039  1.00  0.00           C
ATOM     90  C4'   A B   5       1.932  -9.427   0.710  1.00  0.00           C
ATOM     91  O4'   A B   5       2.020  -8.860  -0.629  1.00  0.00           O
ATOM     92  C3'   A B   5       2.654  -8.408   1.592  1.00  0.00           C
ATOM     93  O3'   A B   5       3.033  -8.968   2.842  1.00  0.00           O
ATOM     94  C2'   A B   5       3.815  -8.064   0.660  1.00  0.00           C
ATOM     95  O2'   A B   5       4.888  -8.974   0.520  1.00  0.00           O
ATOM     96  C1'   A B   5       3.084  -7.921  -0.671  1.00  0.00           C
ATOM     97  N9    A B   5       2.522  -6.560  -0.901  1.00  0.00           N
ATOM     98  C8    A B   5       1.253  -6.091  -0.648  1.00  0.00           C
ATOM     99  N7    A B   5       1.077  -4.846  -0.964  1.00  0.00           N
ATOM    100  C5    A B   5       2.310  -4.449  -1.462  1.00  0.00           C
ATOM    101  C6    A B   5       2.779  -3.228  -1.970  1.00  0.00           C
ATOM    102  N6    A B   5       2.021  -2.126  -2.063  1.00  0.00           N
ATOM    103  N1    A B   5       4.060  -3.178  -2.381  1.00  0.00           N
ATOM    104  C2    A B   5       4.808  -4.276  -2.286  1.00  0.00           C
ATOM    105  N3    A B   5       4.482  -5.467  -1.832  1.00  0.00           N
ATOM    106  C4    A B   5       3.196  -5.489  -1.427  1.00  0.00           C
ATOM    107  P     U B   6       3.063  -8.025   4.135  1.00  0.00           P
ATOM    108  O1P   U B   6       3.223  -8.856   5.350  1.00  0.00           O
ATOM    109  O2P   U B   6       1.891  -7.121   4.118  1.00  0.00           O
ATOM    110  O5'   U B   6       4.397  -7.181   3.881  1.00  0.00           O
ATOM    111  C5'   U B   6       5.621  -7.881   3.587  1.00  0.00           C
ATOM    112  C4'   U B   6       6.719  -6.889   3.258  1.00  0.00           C
ATOM    113  O4'   U B   6       6.486  -6.364   1.919  1.00  0.00           O
ATOM    114  C3'   U B   6       6.776  -5.642   4.140  1.00  0.00           C
ATOM    115  O3'   U B   6       7.397  -5.908   5.390  1.00  0.00           O
ATOM    116  C2'   U B   6       7.567  -4.725   3.208  1.00  0.00           C
ATOM    117  O2'   U B   6       8.962  -4.910   3.068  1.00  0.00           O
ATOM    118  C1'   U B   6       6.874  -4.999   1.877  1.00  0.00           C
ATOM    119  N1    U B   6       5.666  -4.158   1.647  1.00  0.00           N
ATOM    120  C2    U B   6       5.867  -2.911   1.107  1.00  0.00           C
ATOM    121  O2    U B   6       6.971  -2.482   0.819  1.00  0.00           O
ATOM    122  N3    U B   6       4.725  -2.160   0.908  1.00  0.00           N
ATOM    123  C4    U B   6       3.431  -2.543   1.197  1.00  0.00           C
ATOM    124  O4    U B   6       2.487  -1.783   0.974  1.00  0.00           O
ATOM    125  C5    U B   6       3.322  -3.869   1.760  1.00  0.00           C
ATOM    126  C6    U B   6       4.416  -4.621   1.964  1.00  0.00           C
END
"""



# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------

class growth:

    # adds base pairs to a helix at selected end
    # 1. match four terminal phosphate atoms of input model and 3bps model helix
    # 2. copy sticking base pair from a model


    # -------------------------------------------------------------------------


    def __init__(self, debug=False, dna=False):

        if dna:
            self.stem_model = iotbx.pdb.hierarchy.input(pdb_string=model_stem_DNA).hierarchy
        else:
            self.stem_model = iotbx.pdb.hierarchy.input(pdb_string=model_stem_RNA).hierarchy

        if debug: self.stem_model.show()


    # -------------------------------------------------------------------------

    def list_resids(self, chain):
        return [string.atoi(rg.resseq) for rg in chain.residue_groups()]

    # -------------------------------------------------------------------------

    def select_coords(self, model, resids):
        sel_cache = model.atom_selection_cache()
        assert sel_cache.n_seq == model.atoms_size()
        isel = sel_cache.iselection
        vec  = []
        for resi in resids:
            vec.append(model.select(isel(r"name P and resid %i"%resi)).only_atom().xyz)
        return flex.vec3_double(vec)

    # -------------------------------------------------------------------------

    def grow_stem(self, input_stem, append_to_leading_chain=True):

        if input_stem.overall_counts().n_chains==2:

            target_stem = input_stem.deep_copy()

        else:
            resi_groups = [[_[1] for _ in group] for key, group in groupby(enumerate(input_stem.residue_groups()), \
                                                                            lambda i_x:i_x[0]-i_x[1].resseq_as_int())]

            target_stem = iotbx.pdb.hierarchy.root()
            target_stem.append_model(iotbx.pdb.hierarchy.model(id="0"))
            target_stem.models()[0].append_chain(iotbx.pdb.hierarchy.chain(id="A"))
            target_stem.models()[0].append_chain(iotbx.pdb.hierarchy.chain(id="B"))

            chidx = 0
            for _i in range(2):
                for rg in resi_groups[chidx]:
                    target_stem.models()[0].chains()[chidx].append_residue_group(rg.detached_copy())
                chidx = 1-chidx

        assert target_stem.overall_counts().n_chains == 2

        pivot_bases = []

        chains = list(target_stem.chains())
        if not append_to_leading_chain: chains.reverse()


        for idx,chain in enumerate(chains):
            if idx==0:
                pivot_bases.extend(self.list_resids(chain)[-2:])
            else:
                pivot_bases.extend(self.list_resids(chain)[:2])


        model_pivot_bases = [4, 5, 2, 3]

        new_basepair = (1, 6)

        input_coords = self.select_coords(target_stem, pivot_bases)
        model_coords = self.select_coords(self.stem_model, model_pivot_bases)

        superposition = superpose.least_squares_fit(input_coords, model_coords, \
                                                        method=["kearsley", "kabsch"][1])
        rtmx = matrix.rt((superposition.r, superposition.t))


        new_basepair_model = self.stem_model
        new_basepair_model.atoms().set_xyz(new_xyz=(rtmx * new_basepair_model.atoms().extract_xyz()))

        # append fitted residues to the chains
        sel_cache = self.stem_model.atom_selection_cache()
        assert sel_cache.n_seq == self.stem_model.atoms_size()
        isel = sel_cache.iselection

        rg = self.stem_model.select(isel(r"resid 6")).only_residue_group().detached_copy()
        rg.resseq = 99
        rg.only_atom_group().altloc = "X"
        chains[0].append_residue_group(rg)

        rg = self.stem_model.select(isel(r"resid 1")).only_residue_group().detached_copy()
        rg.resseq = 0
        rg.only_atom_group().altloc = "X"
        chains[1].insert_residue_group(0,rg)

        assert target_stem.overall_counts().n_residues % 2 == 0


        # renumber residues (continuous indexing in ONE chain)

        output_stem = iotbx.pdb.hierarchy.root()
        output_stem.append_model(iotbx.pdb.hierarchy.model(id="0"))
        output_stem.models()[0].append_chain(iotbx.pdb.hierarchy.chain(id="A"))

        resseq=1
        for chain in target_stem.chains():
            first=True
            for rg in chain.residue_groups():
                rg.resseq = resseq
                if first:
                    rg.link_to_previous = False
                    first = False
                else:
                    rg.link_to_previous = True
                output_stem.only_chain().append_residue_group(rg.detached_copy())

                resseq += 1

            # there must be indexing gap between the two strands
            # thsi silly functions uses it
            resseq+=10

        return output_stem




# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------




def tests():

    ph = iotbx.pdb.hierarchy.input(pdb_string=model_stem_RNA).hierarchy
    grower = growth(dna=False)

    ph.write_pdb_file("small_stem.pdb")

    stem = grower.grow_stem(ph)
    stem = grower.grow_stem(stem, append_to_leading_chain=False)
    stem = grower.grow_stem(stem, append_to_leading_chain=True)
    stem = grower.grow_stem(stem, append_to_leading_chain=False)
    stem = grower.grow_stem(stem, append_to_leading_chain=True)
    stem = grower.grow_stem(stem, append_to_leading_chain=False)

    stem.write_pdb_file("bigger_stem.pdb")

if __name__=="__main__":
    tests()


