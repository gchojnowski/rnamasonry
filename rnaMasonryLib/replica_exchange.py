'''
Created on May 19, 2015

@author: gchojnowski
'''


import sys,os
import time
from random import randint as ri
import numpy as np
import copy
import multiprocessing as mp


try:
    from rnamasonry_config import *
except:
    sys.path.append("..")
    from rnamasonry_config import *

sys.path.append(os.path.join(ROOT, "lib"))



from metropolis_hastings import MCMC
from ph_parser import ph_parser

from clusters import clusters


def worker(mc, rq, b, n, T, fs):
    x = mc.refine(b, max_steps=n, inpkT=T, fix_stems=fs, verbose=False)
    rq.put((T, x))

class ReplicaExchange(object):



    def __init__(self, treeTraverser, \
                        branch_dict, \
                        threads=7, \
                        dT=2, \
                        kTmin = 8.0, \
                        kTmax = 30.0, \
                        max_steps=100, \
                        mc_steps = 100, \
                        save_decoys_fraction=0.0, \
                        rg = None, \
                        blobfit_obj = None, \
                        blobfit_weight = 1.0, \
                        saxs_data_str = None, \
                        foxs_path = None, \
                        cryson_opts = None,
                        fix_stems = False):


        # blobfit
        self.blobfit_obj = blobfit_obj
        self.blobfit_weight = blobfit_weight

        # do not try to replace stems during the simulation
        self.fix_stems = fix_stems


        self.tv_obj = treeTraverser
        self.branch_dict = branch_dict
        self.threads = threads
        self.sequence = treeTraverser.sequence
        self.dotbracket = treeTraverser.input_dotbracket
        self.project_dir = treeTraverser.project_dir
        self.local_db = treeTraverser.local_db
        self.save_decoys_fraction = save_decoys_fraction

        self.max_steps = max_steps
        self.mc_steps = mc_steps

        self.rg = rg
        self.saxs_data_str = saxs_data_str
        self.foxs_path = foxs_path
        self.cryson_opts = cryson_opts

        if 0:
            self.T = [1]*self.threads
            self.T = np.array(list(range(0,dT*self.threads,dT))) + 2#int(dT/2)
        else:
            _qT=np.power( kTmax/kTmin, 1.0/float(self.threads-1) )
            self.T = [kTmin]
            for _ in range(self.threads-1):
                self.T.append( self.T[-1]*_qT )
        print(" --> Defined shelf temperatures: ", ", ".join(["%.2f"%_ for _ in self.T]))



    def run(self, reference_model_fname=None):

        print(" --> [SimRNA3] Input sequence and secondary structure:\n")
        print(self.sequence)
        print("\n".join(self.dotbracket[1:]))
        print()




        mc = MCMC(self.tv_obj.association_graph, \
                    cut_joint_nuc=True, \
                    one_chain_only=True, \
                    rg=self.rg, \
                    ss=self.dotbracket, \
                    seq=self.sequence, \
                    project_dir=self.project_dir, \
                    local_db = self.local_db, \
                    blobfit_obj = self.blobfit_obj, \
                    blobfit_weight = self.blobfit_weight, \
                    saxs_data_str = self.saxs_data_str, \
                    foxs_path = self.foxs_path,
                    cryson_opts = self.cryson_opts)


        clusters_obj = clusters(mc)



        efile = open(os.path.join(self.project_dir, 'energy_evol.dat'), 'w')
        self.representative_decoys = []

        rq = mp.Queue()

        shelfs_dict = {}
        shelfs_decoys = {}

        for i in range(self.threads):
            shelfs_dict[self.T[i]] = (self.branch_dict, 666.6)

        global_best = None

        step=1

        print(" ==> Starting %i folding steps" % self.max_steps)
        print()
        for re_step in range(self.max_steps):

            print(" --> STEP: %i" % (re_step+1))


            jobs = []
            for i in range(self.threads):
                jobs.append( mp.Process(target=worker, args=(mc, \
                                                             rq, \
                                                             shelfs_dict[self.T[i]][0], \
                                                             self.mc_steps, \
                                                             self.T[i], \
                                                             self.fix_stems) ) )

            for job in jobs: job.start()

            # gchojnowski: moved join after results collector. otherwise for many
            #               jobs ald large result chunks the queue pipe get chocked
            # more info: https://docs.python.org/2/library/multiprocessing.html#multiprocessing-programming
            n_results = 0
            #while not rq.empty():
            while n_results<self.threads:
                r = rq.get()
                shelfs_dict[r[0]] = r[1][-1]
                shelfs_decoys[r[0]] = r[1]

                # XXX gchojnowski: store results for clustring
                clusters_obj.decoys.extend(r[1])
                n_results+=1

            for job in jobs: job.join()

            print()

            replaced = []
            for i in range(step, self.threads-1, 2):
                dB = 1.0/self.T[i] - 1.0/self.T[i+1]
                dE = shelfs_dict[self.T[i+1]][1] - shelfs_dict[self.T[i]][1]
                diff = dB*dE
                if diff<0 or np.random.random(1)[0]<np.exp(-diff):
                    shelfs_dict[self.T[i]], shelfs_dict[self.T[i+1]] = \
                                                shelfs_dict[self.T[i+1]], shelfs_dict[self.T[i]]

                    replaced.append(i)

            if len(clusters_obj.decoys):

                self.min_e_decoy = sorted( clusters_obj.decoys, key=lambda x:x[1] )[0]
                branch = mc.dump_branch( self.min_e_decoy[0], \
                                        "./%s/%i_min.pdb" % (self.project_dir, re_step+1), \
                                        energy = self.min_e_decoy[1] )


            # XXX gchojnowski: store results for clustring
            #for _v in shelfs_dict.values():
            #    for _b,_e in zip(_v[2], _v[3]):
            #        clusters_obj.add_decoy(_b, _e)

            for _i in range(self.mc_steps):
                e_str = " ".join(map(str, [shelfs_decoys[_iT][_i][1] for _iT in self.T]))
                efile.write("%i %s" % (self.mc_steps*re_step+_i, e_str))
                efile.write("\n")
            efile.flush()


            print()
            for i in reversed(list(range(self.threads))):
                if i-1 in replaced:
                    s = "|"
                elif i in replaced:
                    s = "|"
                else:
                    s = ""

                print("     %5.2f: %10.2f %s" % (self.T[i], shelfs_dict[self.T[i]][1], s))

            print()
            print()


            step=1-step

            print()

        efile.close()

        clusters_obj.cluster_decoys(fraction=0.1)
        clusters_obj.show_clusters()
        clusters_obj.dump_clusters(self.project_dir)

        if self.save_decoys_fraction>0:
            clusters_obj.dump_top_decoys(self.project_dir, self.save_decoys_fraction)

        if reference_model_fname:
            clusters_obj.compare_with_reference(reference_model_fname, self.project_dir)


# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------


def test():
    pass

def main():
    pass

if __name__=="main":
    main()



