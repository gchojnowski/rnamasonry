#! /usr/bin/env python
# -*- coding: utf-8 -*-
# =============================================================================
# Author:
#  Grzegorz Chojnowski - gchojnowski@gmail.com
# =============================================================================


import os,sys
import re
import glob
import tarfile

from io import StringIO
from rnamasonry_config import ROOT
from collections import Counter, defaultdict

import io as strIO


QUANTA=1e3
DBDIR=os.path.join(ROOT, 'local_db')

# -----------------------------------------------------------------------------


def ls_database():

    pdb_fnames = []

    for fname in glob.glob( os.path.join(DBDIR, "*.dat") ):

        with tarfile.open(fname, 'r|gz') as tf:
            for ifile in tf:
                pdb_fnames.append(ifile.name)

    return pdb_fnames


# -----------------------------------------------------------------------------

def get_fobj_from_database(fid, local_db, dump=False):
    """
        fid can be either a fileneme (structure_99999.pdb), or just a number
    """
    try:
        idx = int(fid)
    except:
        idx = int(os.path.basename(fid).split('_')[-1].split('.')[0])

    db_file = os.path.join(local_db, "data%04i.dat"%(int(idx)//QUANTA))
    with tarfile.open(db_file, 'r|gz') as tf:
        for ifile in tf:
            if re.match(r'.*structure_%i\.pdb'%idx, ifile.name):
                s = tf.extractfile(ifile).read()
                if dump:
                    with open( os.path.join(local_db, ifile.name), 'w') as of:
                        of.write(s.decode('utf-8'))
                return StringIO(s.decode('utf-8'))

    return None

# -----------------------------------------------------------------------------

def build_db(path_to_pdbfiles=os.path.join(ROOT, 'local_db')):

    idxs = []
    fnames_dict = {}
    for fname in glob.iglob( os.path.join( path_to_pdbfiles, '**', '*.pdb')):
        idx = int(os.path.basename(fname).split('_')[-1].split('.')[0])
        idxs.append(int(idx//QUANTA))
        fnames_dict[idx] = fname


    for idx,c in list(Counter(idxs).items()):
        print(idx, c)


    tarfile_obj = tarfile.open(os.path.join(DBDIR, "data%04i.dat"%0), "w:gz")
    for i,idx in enumerate(sorted(fnames_dict)):
        tarfname = "data%04i.dat"%(idx//QUANTA)
        name_in_tar = os.path.join(*fnames_dict[idx].split('/')[-2:])

        if os.path.basename(tarfile_obj.name)==tarfname:
            tarfile_obj.add( fnames_dict[idx], arcname=name_in_tar)
            continue

        tarfile_obj.close()
        tarfile_obj = tarfile.open(os.path.join(DBDIR, "data%04i.dat"%(idx//QUANTA)), "w:gz")
        tarfile_obj.add( fnames_dict[idx], arcname=name_in_tar)

        print('newfile %i/%i: '%(i,len(fnames_dict)), tarfname)

# -----------------------------------------------------------------------------

def update_gzip(new_data={}):
    # new_data is dictionary where keys are file paths inside archive and
    # values are pdb strings for example: {'loop/structure_3968.pdb': pdbstr}
    # 1. assign strucutres to appropriate .dat files
    file_keys = defaultdict(list)
    for k in list(new_data.keys()):
        fid = int(k.split('_')[-1].split('.')[0])
        file_keys["data%04i.dat"%(fid // QUANTA)].append(k)
    # 2. update gzip files
    for fname, lk in list(file_keys.items()):
        gzip_fpath = os.path.join(DBDIR, fname)
        ufpath = "{0}_updated.dat".format(os.path.splitext(gzip_fpath)[0]) # remove extension, i.e. ".dat" and make new name
        with tarfile.open(ufpath, 'w:gz') as dst_handle:
            # 2a. copy the content of old gzip file to new gzip file
            if os.path.exists(gzip_fpath):
                with tarfile.open(gzip_fpath, 'r:gz') as src_handle:
                    for member in src_handle:
                        dst_handle.addfile(member, src_handle.extractfile(member))
            # 2b. write new data to new gzip file
            for fpath in lk:
                pdbstr = new_data[fpath]
                # string to bytes
                pdb_bytes = strIO.StringIO()
                pdb_bytes.write(pdbstr)
                pdb_bytes.seek(0)
                # create tar info object
                tarinfo = tarfile.TarInfo(name=fpath)
                tarinfo.size=len(pdb_bytes.buf)
                # save to new gzip file
                dst_handle.addfile(tarinfo=tarinfo, fileobj=pdb_bytes)
        # remove old gzip file and rename new one
        try:
            os.remove(gzip_fpath)
        except:
            pass
        os.rename(ufpath, gzip_fpath)
        print(("    --> updated file {0}".format(gzip_fpath)))

# -----------------------------------------------------------------------------
def main():

    if 1:
        build_db()
        return

    f = get_fobj_from_database(47206)
    f = get_fobj_from_database("/Users/fake-user/rnamasonry/lib/../local_db/loop/structure_70024.pdb")
    print(f.read())

if __name__=='__main__':
    main()
