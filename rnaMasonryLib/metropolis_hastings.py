'''
Created on May 15, 2015

@author: gchojnowski
'''


import sys,os
import time
from random import randint
import numpy as np
import networkx as nx
import copy
from io import StringIO

# ----------------- CCTBX
from scitbx.array_family import flex
# -----------------------

from rnamasonry_config import *
sys.path.append(os.path.join(ROOT, "rnaMasonryLib"))
sys.path.append(os.path.join(ROOT, "rnaMasonryLib", "moderna_source_1.7.1_py3"))
from progressbar import ProgressBar, Percentage, Bar, ETA
from mutate import mutants
from motifs import Branch
from motifs import Motif
from ph_parser import ph_parser
from rnamasonry_utils import stderr_redirected

# ============================== pySimRNA ==========================================================
sys.path.append(os.path.join(ROOT, "rnaMasonryLib", "pysimrna"))
simrna_path = os.path.join(ROOT, "rnaMasonryLib", "pysimrna", "")
import pysimrna
# ==================================================================================================

from pysaxs import pysaxs

import moderna


# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------

class MCMC(object):


    def __init__(self, association_graph, \
                       cut_joint_nuc, \
                       one_chain_only, \
                       rg=None, \
                       ss=None, \
                       seq=None, \
                       project_dir=None, \
                       local_db=None, \
                       saxs_data_str = None, \
                       blobfit_obj = None, \
                       blobfit_weight = 1.0, \
                       cryson_opts = None,
                       foxs_path = None):



        assert not [project_dir, local_db].count(None), Exception("Either local_db or project_dir is None")

        self.project_dir = project_dir
        self.local_db = local_db

        # SAXS
        self.saxs_data_str = saxs_data_str
        self.foxs_path = foxs_path
        self.cryson_opts = cryson_opts

        # blobfit
        self.blobfit_obj = blobfit_obj
        self.blobfit_weight = blobfit_weight


        self.association_graph = association_graph
        self.cut_joint_nuc = cut_joint_nuc
        self.one_chain_only = one_chain_only
        self.sequence = seq
        self.mutate = mutants(debug=False)

        with stderr_redirected():
            self.so = pysimrna.pysimrna(simrna_path)

        self.rg = rg
        self.rg_scale = 1.0
        #if rg: self.so.set_rg(rg, 0.05)
        if ss:
            self.so.set_ss("\n".join(ss[1:]), 20.0)
        else:
            self.so.set_ss("", 1.0)


    # -------------------------------------------------------------------------


    def __build_branch_from_dictionary(self, branch_as_dict, graph):
        # Builds Branch object based on branch as a dictionary.
        shallow_copy = graph.subgraph(list(branch_as_dict.keys()))
        for n in shallow_copy.nodes():
            which_structure = branch_as_dict[n]
            fragments = graph.nodes[n]['fragments']
            fragment = fragments[which_structure].root
            shallow_copy.nodes[n]['fragment'] = fragment

        topology = nx.Graph()
        topology.add_nodes_from( shallow_copy.nodes() )
        topology.add_edges_from( shallow_copy.edges() )
        for n in topology.nodes():
            topology.nodes[n]['fragment'] = Motif(self.project_dir, self.local_db, frag=shallow_copy.nodes[n]['fragment'])

        branch = Branch(self.project_dir, self.local_db, topology=topology)
        root_number = min(branch_as_dict.keys())

        return branch


    # -------------------------------------------------------------------------


    def __branch2ph(branch, cut_joints=True, one_chain_only=True):
        """
            Multiprocessing method to create branch object based on
            information which fragments to use and save branch hierarchy to disk.
        """

        hierarchy = branch.get_hierarchy(cut_joint_nucleotides=cut_joints)
        if one_chain_only:
            hierarchy = ph_parser().parse(hierarchy, clean=False)
        return hierarchy


    # -------------------------------------------------------------------------


    def refine(self, branch_dict, max_steps=1000, inpkT=None, fix_stems=False, verbose=False):
        dir_name = os.path.join(self.project_dir, 'results')

        start_time = time.time()

        decoys = []

        # XXX
        n_nodes = len(list(branch_dict.keys()))
        branch_current = copy.deepcopy(branch_dict)

        # calc energy for the input model
        branch_current = copy.deepcopy(branch_dict)
        energy_current = self.get_score(branch_dict)

        # energy calcs failed - accept ANY model processed by SimRNA ff
        if energy_current is None:
            energy_current=0

        if verbose:
            print("\n")
            print(" --> Annealing the final model")
            widgets = ['     ', Percentage(), ' ', Bar(), ' ', ETA()]
            pbar = ProgressBar(widgets=widgets, maxval=max_steps, fd=sys.stdout).start()


        kT = 20
        for i in range(max_steps):

            while True:
                ni = randint(0,n_nodes-1)
                if fix_stems and self.association_graph.nodes[ni]['motif_type'] == 'stem': continue

                if len(self.association_graph.nodes[ni]['fragments'])>1: break





            if inpkT:
                kT = inpkT
            else:
                kT = 50.0*(1.0-float(i)/float(max_steps))+1

            # generate a step
            branch_new = copy.deepcopy(branch_current)
            branch_new[ni]=randint(0,len(self.association_graph.nodes[ni]['fragments'])-1)
            e_new = self.get_score(branch_new)


            if e_new is None:
                pass

            elif energy_current>e_new:
                branch_current = copy.deepcopy(branch_new)
                energy_current = e_new

            elif np.exp((energy_current - e_new)/kT)>np.random.random(1)[0]:
                branch_current = copy.deepcopy(branch_new)
                energy_current = e_new

            else:
                pass

            if verbose: pbar.update(i+1)

            decoys.append( (copy.deepcopy(branch_current), energy_current) )

        elapsed_time = time.time() - start_time


        return decoys


    # -------------------------------------------------------------------------


    def get_ph(self, branch_dict):
        branch = self.__build_branch_from_dictionary(branch_dict, self.association_graph)
        #gchojnowski: prints frag id - useful for debugging
        #print([branch.branch.nodes[n]['fragment'].fragment_id for n in branch.branch.nodes()])
        hierarchy = branch.get_hierarchy(cut_joint_nucleotides=self.cut_joint_nuc)
        if self.one_chain_only: hierarchy = ph_parser().parse(hierarchy, clean=False)

        # XXX here mutate hierarchy to the target sequence
        self.mutate.mutate2seq(hierarchy.only_chain(), target_seq = self.sequence)

        hierarchy.atoms().set_b( flex.double(hierarchy.atoms().size(), 20.0) )
        hierarchy.atoms().set_occ( flex.double(hierarchy.atoms().size(), 1.0) )
        hierarchy.atoms_reset_serial()

        return hierarchy


    # -------------------------------------------------------------------------


    def dump_branch(self, branch_dict, fname, moderna_fix=True, energy=None, verbose=False, renumber_resseqs_from_one=False):

        hierarchy = self.get_ph(branch_dict)


        if self.saxs_data_str:
            co = pysaxs(foxs_path=self.foxs_path, cryson_opts=self.cryson_opts)
            chi2 = co.run( hierarchy.as_pdb_string(), self.saxs_data_str )
            print("     current best model Chi^2: %.2f" % chi2)
        else:
            chi2 = None


        if self.blobfit_obj:
            ph,score=self.blobfit_obj.fit_model(pdb_model=hierarchy)
            hierarchy.atoms().set_xyz( ph.atoms().extract_xyz() )
        else:
            score = None


        if renumber_resseqs_from_one:
            for rg in hierarchy.residue_groups(): rg.resseq = rg.resseq_as_int()+1

        output_pdb_string = hierarchy.as_pdb_string()


        # gchojnowski: run moderna to fix backbone issues at the interfaces between motifs
        # ugly solution, but moderna cannot handle file objects nor strings.

        if moderna_fix:

            with open(fname, 'w') as f:
                f.write(hierarchy.as_pdb_string())

            m = moderna.load_model(fname, "A")
            try:
                moderna.fix_backbone(m)
            except:
                pass
            m.write_pdb_file(fname)

            with open(fname, 'r') as f:
                output_pdb_string = f.read()


        with open(fname, 'w') as f:
            f.write("REMARK E=%s CHI2=%s BFIT=%s\n" % (str(energy), \
                                                       "%.2f"%chi2 if chi2 else "NONE", \
                                                       "%.2f"%score if score else "NONE" )  )

            shallow_copy = self.association_graph.subgraph(list(branch_dict.keys()))
            for n in shallow_copy.nodes():
                which_structure = branch_dict[n]
                fragments = self.association_graph.nodes[n]['fragments']
                f.write( "REMARK NODE %5i %s\n" % (n, fragments[which_structure].fragment_id) )

            f.write(output_pdb_string)


    # -------------------------------------------------------------------------


    def calc_rg(self, ph):

        xyz = ph.atoms().extract_xyz()
        cm = flex.vec3_double(xyz.size()*[xyz.mean()])

        return xyz.rms_difference(cm)


    # -------------------------------------------------------------------------

    def get_score(self, branch_dict):
        hierarchy = self.get_ph(branch_dict)

        #XXX gchojnowski 26.08.2015 - test that
        hierarchy.atoms().reset_i_seq()

        # gchojnowski@27.06.2022 check if all atoms SimRNA needs are in the model (VERY roughly)
        sel_cache = hierarchy.atom_selection_cache()
        isel = sel_cache.iselection
        phsel = hierarchy.select(isel("chain A and (name P or name N1 or name C2 or name C4)"))
        if not phsel.overall_counts().n_atoms==4.0*hierarchy.overall_counts().n_residues:
            return None

        self.so.process_pdbstring("dummyX", hierarchy.as_pdb_string())

        score = self.so.get_energy()

        if self.rg:
            _b_rg = self.calc_rg(hierarchy)
            _f = min(1, self.rg_scale*self.rg_scale*np.abs(self.rg-_b_rg)/_b_rg)
            if score<0:
                score *= (1.0-_f)
            else:
                 score *= _f

        if self.saxs_data_str:

            co = pysaxs(foxs_path=self.foxs_path, cryson_opts=self.cryson_opts)
            # if things go wrong chi2<0.0
            chi2 = co.run( hierarchy.as_pdb_string(), self.saxs_data_str )
            chi2_f = (.4*np.exp(-2.0*(chi2-1.0)**2) + .6*np.exp(-.05*(chi2-1.0)**2))
            score *= (1.0 +  self.blobfit_weight*chi2_f)


        # the bscore is a fraction of phosphorus atom that are within expected
        # volume of the target and ranges from 0 to 1
        # this simple linear scoring may need to be replaced with somethin less aggressive
        if self.blobfit_obj:
            ph,bscore=self.blobfit_obj.fit_model(pdb_model=hierarchy)

            #score *= (1 + self.blobfit_weight*5.0*(bscore-0.5))
            score *= (1 + self.blobfit_weight*(bscore-0.5))


        return score


# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------


def test():
    pass

def main():
    pass

if __name__=="main":
    main()
