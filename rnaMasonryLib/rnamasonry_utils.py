#! /usr/bin/env python
# -*- coding: utf-8 -*-
# =============================================================================
# Created at:
#  International Institute of Molecular and Cell Biology in Warsaw, Poland
# Author:
#  Grzegorz Chojnowski - gchojnowski@genesilico.pl, gchojnowski@gmail.com
# =============================================================================




import os,sys
import math



# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------


# gets rid of stderr crap produced by C-level SimRNA module
#https://stackoverflow.com/a/17954769/2965879

from contextlib import contextmanager
@contextmanager
def stderr_redirected(to=os.devnull):

    '''
    import os

    with stderr_redirected(to=filename):
        print("from Python")
        os.system("echo non-Python applications are also supported")
    '''
    fd = sys.stderr.fileno()

    ##### assert that Python and C stdio write using the same file descriptor
    ####assert libc.fileno(ctypes.c_void_p.in_dll(libc, "stderr")) == fd == 1

    def _redirect_stderr(to):
        sys.stderr.close() # + implicit flush()
        os.dup2(to.fileno(), fd) # fd writes to 'to' file
        sys.stderr = os.fdopen(fd, 'w') # Python writes to fd

    with os.fdopen(os.dup(fd), 'w') as old_stderr:
        with open(to, 'w') as file:
            _redirect_stderr(to=file)
        try:
            yield # allow code to be run with the redirected stderr
        finally:
            _redirect_stderr(to=old_stderr) # restore stderr.
                                            # buffering and flags such as
                                            # CLOEXEC may be different


# -----------------------------------------------------------------------------

def print_stats(graph, nodes):
    print("\n --> Node statistics")
    for node in nodes:
        n = graph.nodes[node]
        assert len(n['fragments']) > 0 , "FATAL ERROR: failed to parse motifs - check the database!"
        print('      Node %i - %8s %15s [%5i]' % (node, n['motif_type'],\
                                                "_".join(map(str,n['cyclic_permutation'])),\
                                                len(n['fragments'])))
    print("\n")


# -----------------------------------------------------------------------------

class rmsd_stats:
    '''
        [1]  For details see: Nikolay V. Dokholyan et al., RNA, 2010, 16, 1340-1349
    '''


    def __init__(self, N):

        self.threshold_rmsd=1.0
        self.N = N
        rmsd_1 = 5.1 * (self.N ** 0.41) - 15.8
        self.mean_rmsd_chance = max(self.threshold_rmsd, rmsd_1)
        self.std_dev = 1.8


        # p_value < 0.01
        self.mean_rmsd_001 = max(1.0, 5.1 * (self.N ** 0.41) - 19.8)

        pass


    # -------------------------------------------------------------------------

    def erf(self, x):
        # constants
        a1 =  0.254829592
        a2 = -0.284496736
        a3 =  1.421413741
        a4 = -1.453152027
        a5 =  1.061405429
        p  =  0.3275911

        # Save the sign of x
        sign = 1
        if x < 0:
            sign = -1
        x = abs(x)

        # A&S formula 7.1.26
        t = 1.0/(1.0 + p*x)
        y = 1.0 - (((((a5*t + a4)*t) + a3)*t + a2)*t + a1)*t*math.exp(-x*x)

        return sign*y


    # -------------------------------------------------------------------------


    def pval(self, rmsd):
        #belong_to_cluster = False if p > p_value else True

        z_score = (rmsd - self.mean_rmsd_chance) / self.std_dev
        return ( 1 + self.erf(z_score/(2**0.5)) )/2





