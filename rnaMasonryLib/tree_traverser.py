#! /usr/bin/env libtbx.python
# -*- coding: utf-8 -*-
# =============================================================================
# Created at:
#
# Author:
#  Grzegorz Chojnowski - gchojnowski@gmail.com
#  Rafał Zaborowski
# =============================================================================

## @file rnamasonry.py
#
#  A program matching recurrent RNA motifs from RNABricks database to input RNA models.
#  to validate plausibility of base-pairs and genberate geometry restraints for resinement
#  with Refmac5, phenix, and for display in Pymol.
#  The package is based on a smatch_cpp project


__author__ = "Grzegorz Chojnowski, Rafał Zaborowski"
__author_email__ = "gchojnowski@gmail.com"
__date__ = "2013-12-30"
__updated__ = "2020-04-16"
__version__ = "0.1"

import sys, os
_fpath = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.join(_fpath, "lib"))

from rnamasonry_config import ROOT, RNABRICKS2_URL, MIN_MODERNA_NFRAGS, AVAILABLE_RMSDS, AVAILABLE_NRLISTS, FOXS_PATH

import copy
import tarfile
from progressbar import ProgressBar, Percentage, Bar, ETA

from multiprocessing import Process, Lock, Manager

from itertools import product

from rnamasonry_utils import print_stats
from pdb_preprocessor import iPDB
from ct_preprocessor import iCT

from rnagraphs import rnagraphs

import networkx as nx
from ph_parser import ph_parser
from motifs import Motif
from motifs import Branch

from build_fragments import builder

class TreeTraverser(object):
    """
        class building and traversing 2D motifs graph (data structure). 
        Combines motifs and builds 3D structures.
    """

    # -------------------------------------------------------------------------

    def __init__(self, input_file, threads_number=4, project_dir=None, debug=False, **args):
        """
            Builds association graph (motifs graph) and graph
            used for building tertiary structures, clashes assesment, etc.
        """

        self.debug = debug
        self.threads_number = threads_number
        self.project_dir = project_dir
        self.local_db = args['local_db']

        assert not [self.local_db, self.project_dir].count(None), Exception("ERROR: local_db or project_dir missing")



        self.clash_percentage = args['max_clash_percentage']
        self.rmsd_for_frags = args['frag_types_rmsd']
        self.max_rmsd = args['rmsd_distance'] if args['rmsd_distance'] in AVAILABLE_RMSDS else AVAILABLE_RMSDS[2]
        self.nr_list = args['nr_list'] if args['nr_list'] in AVAILABLE_NRLISTS else AVAILABLE_NRLISTS[2]
        self.p_value = args['p_value']
        self.threshold_rmsd = args['threshold_rmsd']
        self.cut_joint_nuc = args['cut_joint_nuc']
        self.one_chain_only = args['one_chain_only']
        self.exclude_pdb_id = args['exclude_pdb_id']
        self.max_frag_counts = args['max_frag_counts']
        self.verbose = args['verbose']


        self.lock = Lock()
        self.save_lock = Lock()
        self.manager = Manager()
        self.queue = self.manager.Queue()
        self.shared_value = self.manager.Value('i',0)

        self.minimum_clash_str = self.manager.dict()
        self.medoids_number = self.manager.dict()
        self.medoids = self.manager.dict()

        self.ipdb = None
        # PDB or a standard 2D+seq?
        if os.path.splitext(input_file)[-1].lower() in ['.pdb', '.ent', '.cif']:
            self.ipdb = iPDB(input_file, verbose=False)
            self.ipdb.run_clarna()
            sequence = self.ipdb.sequence
            dotbracket = self.ipdb.dotbracket

            input_frags_directory = os.path.join(self.project_dir, 'input_fragments')
            if not os.path.exists(input_frags_directory):
                os.mkdir(input_frags_directory)

        elif os.path.splitext(input_file)[-1].lower() in ['.ct']:
            ict = iCT(input_file)
            sequence = ict.sequence
            dotbracket = ict.dotbracket

        else:
            with open(input_file) as f:
                input_lines = []
                for line in f.readlines():
                    if not line.startswith('>'):
                        input_lines.append(line.strip())
                sequence = input_lines[0]
                dotbracket = input_lines[1:]


        # add a single dot-only line, in case there are no pseudo-knots defined (dotbracket[1:] is passed to simrna scoring function)
        dotbracket.append('.'*len(dotbracket[0]))

        # define frozen resi ranges (input is in the original numbering)
        frozen_resi = None
        if args['freeze'] and self.ipdb:
            frozen_resi = set([])
            query=args['freeze']
            print(" --> The following residues will be kept frozen: %s" % query)

            selected_resi = []
            for r in query.split(','):
                try:
                    _s, _e = r.split(':')
                    _s = self.ipdb.orig_rna_resseqs.index(str(_s))
                    _e = self.ipdb.orig_rna_resseqs.index(str(_e))

                    assert _s<_e, Exception("check resids range order")
                    frozen_resi.update( list(range(_s, _e+1)) )
                except KeyboardInterrupt:
                    raise
                except Exception as e:
                    raise Exception("ERROR: Wrong selection of frozen residues (%s)" % e)


        rnagraphs_object = rnagraphs(dotbracket_text=dotbracket, sequence_text=sequence)

        rnagraphs_object.extract_motifs()
        self.association_graph = rnagraphs_object.association_graph

        #XXX gchojnowski
        self.sequence = rnagraphs_object.sequence.upper()
        self.input_dotbracket = rnagraphs_object.input_dotbracket

        nodes = sorted(self.association_graph.nodes())
        for node in nodes:
            print(" --> Preparing data for node [%3i]" % (node))
            fragment_info = self.association_graph.nodes[node]['fragment_info']
            self.association_graph.nodes[node]['motif_type'] = fragment_info['fragment_type']
            self.association_graph.nodes[node]['cyclic_permutation'] = \
                                    list(map(len,fragment_info['frg_as_consecutive_seqs']))

            self.association_graph.nodes[node]['fragments'] = []

            # extract and save a frag to the disk
            if self.ipdb:
                all_resids = []
                for ids_list in fragment_info['frg_as_consecutive_seqs']:
                    all_resids.extend( ids_list )

                self.ipdb.extract_residues( all_resids ).write_pdb_file( \
                    os.path.join(self.project_dir, 'input_fragments', ('structure_i%i.pdb' % node)))

                frag = Branch(self.project_dir, \
                              self.local_db, \
                              fragment_type=fragment_info['fragment_type'], \
                              fragment_id='i%i' % node, \
                              frg_as_consecutive_seqs=fragment_info['frg_as_consecutive_seqs'])

                self.association_graph.nodes[node]['fragments'].append( frag )

                if frozen_resi and frozen_resi.intersection(set(all_resids)): continue




            self.association_graph.nodes[node]['fragments']. \
                            extend( self.__get_fragments( node, fragment_info ) )

        print_stats(self.association_graph, nodes)
        self.copy_graph = copy.deepcopy(self.association_graph)

        self.working_tree = {}

    # -------------------------------------------------------------------------


    def __get_matching_fragments(self, fragment_info, local_db_path):
        # downloads motifs from rna3db
        fragment_type = fragment_info['fragment_type']

        cp = "_".join(map(str,list(map(len,fragment_info['frg_as_consecutive_seqs']))))
        cyclic_permutation = "_%s_" % (cp)

        clen = int(len(cyclic_permutation.split('_')[1:-1]))

        # XXX structure - list of tuples: (structure type, cyclic permutation, structure id)
        with open(os.path.join(local_db_path, 'db_info'), 'r') as f:
            structures = [line.split() for line in f.read().split('\n') if line and not line.startswith('#')]

        matching_structure_ids = [t[2] for t in structures[:-1] \
                                        if t[0] == fragment_type and \
                                            int(len(t[1].split('_')[1:-1])/2) == clen and \
                                            cyclic_permutation in t[1]]

        return matching_structure_ids


    # -------------------------------------------------------------------------


    def __dump_fragments(self, node, directory, fragments):
        """
            saves fragment structures assigned to each of the nodes
            into jsonized motif lists
        """

        fragments_as_jsonized_strings = [fragment.dump_fragment() for fragment in fragments]
        #ofile = file(os.path.join(directory, str(node)), 'w')
        with open(os.path.join(directory, str(node)), 'w') as ofile:
            ofile.write("\n".join(fragments_as_jsonized_strings))
        #ofile.close()


    # -------------------------------------------------------------------------


    def __get_fragments(self, node, fragment_info):


        # feeds nodes with sets of motifs of certain type
        fragment_type = fragment_info['fragment_type']
        cyclic_permutation = list(map(len,fragment_info['frg_as_consecutive_seqs']))
        frag_objs_directory = os.path.join(self.project_dir, 'fragment_objects')
        if not os.path.exists(frag_objs_directory):
            os.mkdir(frag_objs_directory)
        else:
            # file with required motifs exists --> load file, create motifs and return
            try:
                #ifile = file(os.path.join(frag_objs_directory, str(node)), 'r')
                with open(os.path.join(frag_objs_directory, str(node)), 'r') as ifile:
                    fragments_as_strings = ifile.read().split('\n')
                return [Branch(self.project_dir, self.local_db, \
                                    jsonized_branch=json.loads(fragment_as_string)) \
                                        for fragment_as_string in fragments_as_strings]
            except KeyboardInterrupt:
                raise
            except IOError as e:
                if self.debug: print(e)
                pass
            except Exception as e:
                if self.debug: print(e)
                pass

        # file with required motifs don't exist --> download motifs from database,
        # save to file (local db of motif objects) and return
        fragments_ids = self.__get_matching_fragments(fragment_info, \
                                                      local_db_path=self.local_db)


        # build frags with moderna with a margin of 10 frags that may be rejected due to errors
        if len(fragments_ids)<MIN_MODERNA_NFRAGS+10:
            print(" --> Not enough frags found (%i), building more with modeRNA" % len(fragments_ids))
            fragment_type = fragment_info['fragment_type']
            cp = "_".join(map(str,list(map(len,fragment_info['frg_as_consecutive_seqs']))))
            cyclic_permutation = "_%s_" % (cp)

            bo = builder(local_db=self.local_db, tmp_dir=self.project_dir, nfrags=MIN_MODERNA_NFRAGS+10)
            fragments_ids = bo.build(cyclic_permutation, fragment_type, verbose=self.verbose)

        fragments = []

        assert len(fragments_ids) > 0 , f"FATAL ERROR: failed to find any fragment for the motiff {cp}. Check input secondary structure."

        widgets = ['     ', Percentage(), ' ', Bar(), ' ', ETA()]
        pbar = ProgressBar(widgets=widgets, maxval=len(fragments_ids), term_width=50, fd=sys.stdout).start()


        j = 0
        for i, fragment_id in enumerate(fragments_ids):
            pbar.update(i+1)
            if fragment_type == 'loop':
                if len(cyclic_permutation) == 1:
                    n = self.max_frag_counts['terminal_loop']
                else:
                    n = self.max_frag_counts['internal_loop']
            else:
                n = self.max_frag_counts[fragment_type]

            if len(fragments) < n:
                try:

                    # XXX gchojnowski: check if a frag was initialized correclty skip if not
                    frag = Branch(self.project_dir, \
                                       self.local_db, \
                                       fragment_type=fragment_type, \
                                       fragment_id=fragment_id, \
                                       frg_as_consecutive_seqs=fragment_info['frg_as_consecutive_seqs'])

                except KeyboardInterrupt:
                    raise
                except Exception as e:
                    if self.debug: print(e)
                    continue
                except:# (IndexError, ZeroDivisionError, IncorrectInputDataError):
                    """
                        #If a downloaded fragment has a residue group with
                        #no N1 neither N9 atom or in case of ZeroDivisionError
                        #an empty hierarchy --> reject this fragment
                        no matter what went wrong - skip the frag
                    """
                    continue

                fragments.append( frag )

        self.__dump_fragments(node, frag_objs_directory, fragments)

        return fragments
    # -------------------------------------------------------------------------



    def build(self):
        ### start
        for _ in range( self.threads_number ):
            assoc_graph_copy = copy.deepcopy(self.association_graph)
            p = Process(target=self.worker, args=(assoc_graph_copy,))
            p.start()

        ### build structures
        self.assembly()

        ### save result
        #dir_name = TMP_DIR + 'results/'
        #if os.path.exists(dir_name):
        #    shutil.rmtree(dir_name)
        #os.mkdir(dir_name)
        branches = self.working_tree[0]
        #for i,b in enumerate(branches):
        #    branch = self.build_branch_from_dictionary(b, self.association_graph, force_build=True)
        #    hierarchy = save_branch_to_disk(branch, \
        #                                    dir_name + 'structure_' + str(i+1) + '.pdb', \
        #                                    self.cut_joint_nuc, \
        #                                    self.one_chain_only)

        return branches

    # -------------------------------------------------------------------------



    def assembly(self, limit_number_of_frags=True):

        for n in sorted(self.association_graph.nodes(),reverse=True):
            print(" --> Processing node: %i" % n)
            sons = [i for i in self.association_graph.neighbors(n) if i > n]

            #XXX stem limit!
            if limit_number_of_frags and self.association_graph.nodes[n]['fragment_info']['fragment_type']=="stem":
                current_node = [{n : i} for i,_ in enumerate(self.association_graph.nodes[n]['fragments'][:2])]
            elif limit_number_of_frags:
                current_node = [{n : i} for i,_ in enumerate(self.association_graph.nodes[n]['fragments'][:50])]
            else:
                current_node = [{n : i} for i,_ in enumerate(self.association_graph.nodes[n]['fragments'])]


            if not sons:
                ### if node has no sons then go to its father, i.e. no nodes to superimpose
                self.working_tree[n] = current_node
            else:
                self.minimum_clash_str[n] = None
                self.medoids_number[n] = 0
                self.medoids[n] = []

                tree_combinations = [current_node]
                for son in sons:
                    tree_combinations.append(self.working_tree[son])

                combined = list(product(*tree_combinations)) ### these are all structural permutations of these node with its sons

                for l in combined:
                    d1 = {}
                    for d2 in l:
                        d1.update(d2)
                    self.queue.put(d1)
                self.queue.join() ### construct structures and cluster them

                if not self.medoids[n]:
                    self.working_tree[n] = [self.minimum_clash_str[n][1]]
                else:
                    self.working_tree[n] = self.medoids[n]

            for son in sons:
                del self.working_tree[son]


    # -------------------------------------------------------------------------


    def worker(self, assoc_graph):
        while True:
            task = self.queue.get()
            if task is None:
                self.queue.task_done()
                break

            self.build_and_cluster(task, assoc_graph)
            self.queue.task_done()


    # -------------------------------------------------------------------------


    def build_and_cluster(self, branch_as_dict, graph):
        # Compare given branch (passed as a dictionary) with medoids of existing clusters and assign it to one of them or construct new cluster.
        branch = self.build_branch_from_dictionary(branch_as_dict, graph)
        root_number = min(branch_as_dict.keys())
        if branch is not None:
            self.lock.acquire()
            if self.medoids_number[root_number] == 0:
                l = self.medoids[root_number]
                l.append(branch_as_dict)
                self.medoids[root_number] = l
                self.medoids_number[root_number] += 1
                self.lock.release()
                return
            until = self.medoids_number[root_number]
            self.lock.release()

            begin = 0
            while(True):
                for i in range(begin, until):
                    medoid_as_dict = self.medoids[root_number][i]
                    medoid = self.build_branch_from_dictionary(medoid_as_dict, graph)
                    belong_to_cluster = medoid.superimpose_phosphorus(branch, p_value=self.p_value, threshold_rmsd=self.threshold_rmsd)
                    if belong_to_cluster:
                        return

                self.lock.acquire()
                if not self.medoids_number[root_number] - until:
                    l = self.medoids[root_number]
                    l.append(branch_as_dict)
                    self.medoids[root_number] = l
                    self.medoids_number[root_number] += 1
                    self.lock.release()
                    return
                begin = until
                until = self.medoids_number[root_number]
                self.lock.release()


    # -------------------------------------------------------------------------


    def build_branch_from_dictionary(self, branch_as_dict, graph, force_build=False):
        """
            Builds Branch object based on branch as a dictionary.
        """

        shallow_copy = graph.subgraph(list(branch_as_dict.keys()))
        for n in shallow_copy.nodes():
            which_structure = branch_as_dict[n]
            fragments = graph.nodes[n]['fragments']
            fragment = fragments[which_structure].root
            shallow_copy.nodes[n]['fragment'] = fragment

        topology = nx.Graph()
        topology.add_nodes_from( shallow_copy.nodes() )
        topology.add_edges_from( shallow_copy.edges() )
        for n in topology.nodes():
            topology.nodes[n]['fragment'] = Motif(self.project_dir, \
                                                    self.local_db, \
                                                    frag=shallow_copy.nodes[n]['fragment'])

        branch = Branch(self.project_dir, self.local_db, topology=topology)
        root_number = min(branch_as_dict.keys())
        if not force_build:
            clash = branch.calculate_clash(which_motif=0)
            if clash > self.clash_percentage:
                self.lock.acquire()
                if not self.minimum_clash_str[root_number]:
                    self.minimum_clash_str[root_number] = (clash,branch_as_dict)
                elif clash < self.minimum_clash_str[root_number][0]:
                    self.minimum_clash_str[root_number] = (clash,branch_as_dict)
                self.lock.release()
                return None

        return branch



