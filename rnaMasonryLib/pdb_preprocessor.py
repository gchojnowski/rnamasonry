#! /usr/bin/env python
# -*- coding: utf-8 -*-
# =============================================================================
# Created at:
#  International Institute of Molecular and Cell Biology in Warsaw, Poland
# Author:
#  Grzegorz Chojnowski - gchojnowski@genesilico.pl, gchojnowski@gmail.com
# =============================================================================


test_pdb_str = """\
"""






import os,sys
import string
from iotbx.pdb import input as pdb_input
from iotbx.pdb.rna_dna_detection import residue_analysis
import iotbx
from io import StringIO
import locale

from wieheisster import wieheisster

try:
    from rnamasonry_config import *
except:
    ROOT = os.path.dirname(os.path.realpath(__file__))
    sys.path.append(ROOT+"/..")
    from rnamasonry_config import *

sys.path.append(os.path.join(ROOT, "lib"))
from clarna_utils import Clarna_utils
from nested import nested_ss

from ph_parser import ph_parser

class iPDB:


    def __init__(self, ipdb_fname, verbose=False):

        try:
            with open(os.path.expanduser(ipdb_fname), 'r') as ifile:
                pdb_str = ifile.read()
        except:
            raise Exception('Input PDB file broken')

        self.verbose = verbose
        self.pdb_str = pdb_str

        self.ph = ph_parser().string2model(self.pdb_str)
        if self.ph is None:
            self.ph = pdb_input(source_info = None, lines = self.pdb_str.splitlines()).construct_hierarchy()

        self.rna_ph = None
        self.rna_chains = []
        self.protein_chains = []
        self.alien_chains = []

        self.rna_chains, self.protein_chains, self.alien_chains = self.find_rna_chains(self.ph)

        #[rg.resseq for rg in ph.residue_groups()]


        if True:
            print(" --> Input PDB file contents")
            print("       RNA chains: %s" % (",".join([ch.id for ch in self.rna_chains])
                                                        if self.rna_chains else "None"))
            print("       Protein chains: %s" % (",".join([ch.id for ch in self.protein_chains]) \
                                                        if self.protein_chains else "None"))
            print("       Aliens detected in chains: %s" % (",".join(set([ch.id for ch in self.alien_chains])) \
                                                        if self.alien_chains else "None"))

        assert len(self.rna_chains) == 1, Exception("There should be exactly one RNA chain in"+\
                                          "the input file. Otherwise I have no idea what to do.")




        self.rna_ph = iotbx.pdb.hierarchy.root()
        self.rna_ph.append_model(iotbx.pdb.hierarchy.model(id="0"))
        self.rna_ph.models()[0].append_chain( self.rna_chains[0].detached_copy() )

        self.remove_alt_confs(self.rna_ph)


        self.orig_rna_resseqs = [rg.resseq.strip() for rg in self.rna_ph.residue_groups()]
        self.orig_rna_seq = [rg.only_atom_group().resname for rg in self.rna_ph.residue_groups()]
        # reindex the stuff

        self.reindex_rna_residues()
        self.sequence = "".join([rg.only_atom_group().resname for rg in self.rna_ph.residue_groups()])

        self.sel_cache = self.rna_ph.atom_selection_cache()
        assert self.sel_cache.n_seq==self.rna_ph.atoms_size()

        if self.verbose:
            print("       Before processing:")
            print("            ", ",".join(self.orig_rna_resseqs))
            print("            ", "".join(map(string.strip,self.orig_rna_seq)))

            print("       After processing:")
            print("            ", ",".join(map(string.strip, [rg.resseq for rg in self.rna_ph.residue_groups()])))
            print("            ", self.sequence)

    # -------------------------------------------------------------------------

    def extract_residues(self, ids_list):
        ids = list(map(str, ids_list))
        selected_residues = self.rna_ph.select(self.sel_cache.selection("resseq "+" or resseq ".join(ids)))
        return selected_residues.deep_copy()


    # -------------------------------------------------------------------------


    def run_clarna(self):

        pdb_str = StringIO(self.rna_ph.as_pdb_string())
        self.dotbracket = ['.']*len(self.sequence)
        self.basepairs = []
        _tmp_bps = []


        cutils = Clarna_utils()
        res_graph = cutils.start(pdb_str)
        for (u,v,d) in res_graph.edges(data=True):
            if d['type']=='contact' and d['desc']=='WW_cis' and d['weight']>0.6:
                if self.verbose: print("%8s%8s%4s%8s%10.3f" % (u,v, d['n_type'], d['desc'], d['weight']))
                l,r = sorted(map(locale.atoi, [u[1:],v[1:]]))
                #self.basepairs.append((l,r))
                _tmp_bps.append( (l,r,d['weight']) )



        # use unly pairs with highest scores. in case there are bifurcated ones
        paired = []
        for l,r,v in sorted(_tmp_bps, key=lambda x: x[2]):
            if l in paired or r in paired: continue
            self.basepairs.append((l,r))
            paired.extend((l,r))


        self.dotbracket = nested_ss().get_nested_dotbracket(self.basepairs, self.sequence)

        print(" --> Parsed sequence and secondary structure from the input PDB file: \n      ", \
                    "\n%s\n" % self.sequence, \
                    "\n".join(self.dotbracket))
        print()
        sys.stdout.flush()

    # -------------------------------------------------------------------------

    def reindex_rna_residues(self):
        wr = wieheisster()

        for iseq, ag in enumerate(self.rna_ph.only_chain().atom_groups()):
            resname = wr.identify_uncommon_resi(ag, debug=True).upper()

            ag.resname = resname
            ag.parent().resseq = iseq
            ag.parent().icode=''


    #--------------------------------------------------------------------------
    # may not remove partially ambigous residues (e.g. phosphate only)

    def remove_alt_confs(self, pdb_hierarchy):
        for model in pdb_hierarchy.models() :
            for chain in model.chains() :
                for residue_group in chain.residue_groups() :
                    atom_groups = residue_group.atom_groups()
                    assert (len(atom_groups) > 0)
                    for atom_group in atom_groups :
                        if (not atom_group.altloc in ["", " ", "A"]) :
                            residue_group.remove_atom_group(atom_group=atom_group)
                            try:
                                self.na_atom_groups.remove(atom_group)
                            except:
                                pass
                        else:
                            atom_group.altloc = " "
                    if (len(residue_group.atom_groups()) == 0) :
                        chain.remove_residue_group(residue_group=residue_group)
                if (len(chain.residue_groups()) == 0) :
                    model.remove_chain(chain=chain)

        # find and merge divided residues (two objects of the same resseq, e.g. created from partial altlocs)  
        for rg in pdb_hierarchy.residue_groups():
            if len(rg.atom_groups())>1:
                primary_ag = rg.atom_groups()[0]
                for sec_ag in rg.atom_groups()[1:]:
                    rg.merge_atom_groups(primary=primary_ag, secondary=sec_ag)

        for rg in pdb_hierarchy.residue_groups():
            for ag in rg.atom_groups():
                if len(ag.atoms()) == 0:
                    rg.remove_atom_group(atom_group=ag)


        pdb_hierarchy.atoms_reset_serial()

    # -------------------------------------------------------------------------

    def find_rna_chains(self, ph):

        def rna_residues(residues, min_content=0.6):
            fail = 0.0
            for residue in residues:
                ra = residue_analysis(residue_atoms=residue.atoms())#, distance_tolerance=0.1)
                # iotbx/pdb/rna_dna_detection.py
                # problems: long_distance_P_OP%d, long_distance_P_O5', long_distance_C2'_O2'
                if (not ra.is_rna):
                    fail += 1.0
                    if (fail/len(residues))>(1.0-min_content):
                        return False
            return True

        # ---------------------------------------

        rna_chains = []
        protein_chains = []
        alien_chains = []

        for model in ph.models() :
            for chain in model.chains() :
                chain_is_rna=False
                chain_is_prot=False
                for conformer in chain.conformers():
                    if conformer.is_na(min_content=0.2):
                        chain_is_rna = rna_residues( conformer.residues() )
                    elif conformer.is_protein(min_content=0.5):
                        chain_is_prot=True
                if chain_is_rna and len(list(chain.atom_groups()))>1:
                    rna_chains.append(chain)
                elif chain_is_prot:
                    protein_chains.append(chain)
                else:
                    alien_chains.append(chain)


        return rna_chains, protein_chains, alien_chains

    # -------------------------------------------------------------------------
    # -------------------------------------------------------------------------

