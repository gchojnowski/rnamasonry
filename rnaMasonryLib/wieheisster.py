#! /usr/bin/env libtbx.python
# -*- coding: utf-8 -*-
# =============================================================================
# Created at:
#  International Institute of Molecular and Cell Biology in Warsaw, Poland
# Author:
#  Grzegorz Chojnowski - gchojnowski@genesilico.pl, gchojnowski@gmail.com
# =============================================================================

import iotbx.pdb
import numpy as np


NORMAL_SUPPORT = {
        'C':['N1','C2','N3','C4','C5','C6'],
        'U':['N1','C2','N3','C4','C5','C6'],
        'T':['N1','C2','N3','C4','C5','C6'],
        'G':['N1','C2','C4','N3','C5','C6'],
        'A':['N1','C2','C4','N3','C5','C6'],
        }


pdb_test = """\
HETATM  195  P   2MG 1   9      22.791 -57.026 -74.401  0.87 62.11           P
HETATM  196  OP1 2MG 1   9      23.085 -56.339 -73.099  1.00 61.54           O
HETATM  197  OP2 2MG 1   9      21.407 -57.075 -74.951  0.99 55.61           O
HETATM  198  O5' 2MG 1   9      23.310 -58.504 -74.123  0.83 67.22           O
HETATM  199  C5' 2MG 1   9      22.416 -59.546 -73.688  1.00 70.01           C
HETATM  200  C4' 2MG 1   9      22.933 -60.844 -74.245  0.97 74.67           C
HETATM  201  O4' 2MG 1   9      22.394 -61.053 -75.582  0.92 78.15           O
HETATM  202  C3' 2MG 1   9      24.440 -60.871 -74.452  0.99 75.79           C
HETATM  203  O3' 2MG 1   9      25.144 -61.205 -73.263  0.99 78.94           O
HETATM  204  C2' 2MG 1   9      24.575 -61.958 -75.506  0.81 74.72           C
HETATM  205  O2' 2MG 1   9      24.265 -63.179 -74.865  0.73 70.76           O
HETATM  206  C1' 2MG 1   9      23.382 -61.646 -76.412  0.68 78.55           C
HETATM  207  N9  2MG 1   9      23.809 -60.674 -77.416  1.00 82.77           N
HETATM  208  C8  2MG 1   9      23.479 -59.340 -77.439  1.00 83.08           C
HETATM  209  N7  2MG 1   9      24.027 -58.692 -78.429  0.72 86.14           N
HETATM  210  C5  2MG 1   9      24.778 -59.652 -79.094  0.96 89.24           C
HETATM  211  C6  2MG 1   9      25.579 -59.548 -80.264  0.96 89.65           C
HETATM  212  O6  2MG 1   9      25.826 -58.545 -80.944  1.00 90.29           O
HETATM  213  N1  2MG 1   9      26.183 -60.759 -80.578  1.00 87.04           N
HETATM  214  C2  2MG 1   9      25.989 -61.936 -79.899  1.00 86.53           C
HETATM  215  N2  2MG 1   9      26.642 -62.999 -80.378  1.00 87.25           N
HETATM  216  CM2 2MG 1   9      26.493 -64.270 -79.690  0.50 90.08           C
HETATM  217  N3  2MG 1   9      25.230 -62.054 -78.822  0.75 88.76           N
HETATM  218  C4  2MG 1   9      24.659 -60.880 -78.477  1.00 87.93           C
"""




# -----------------------------------------------------------------------------

class wieheisster():

    # fpair_sub.c: residue_ident

    # identify uncommon residues in a way similar to RNAVIEW
    # identifying a residue as follows:
    #  R-base  Y-base  amino-acid, others [default]
    # (puRine)
    #   +1        0        -1        -2   [default]
    #

    def get_atom_by_name(self, resi, name):
        for atom in resi.atoms():
            if atom.name.strip()==name:
                return atom
        return None




    def get_base_type(self, resi, dcrt=2.0, dcrt2=3.0):

        base_id = -2

        N9 = self.get_atom_by_name(resi, 'N9')
        N1 = self.get_atom_by_name(resi, 'N1')
        C2 = self.get_atom_by_name(resi, 'C2')
        C6 = self.get_atom_by_name(resi, 'C6')

        if N1 and C2 and C6:
            d1 = np.linalg.norm(np.array(N1.xyz, float) - np.array(C2.xyz, float))
            d2 = np.linalg.norm(np.array(N1.xyz, float) - np.array(C6.xyz, float))
            d3 = np.linalg.norm(np.array(C2.xyz, float) - np.array(C6.xyz, float))

            if d1<=dcrt and d2<=dcrt and d3<=dcrt2:
                base_id = 0
                if N9:
                    d3 = np.linalg.norm(np.array(N1.xyz, float) - np.array(N9.xyz, float))
                    if d3 >= 3.5 and d3 <= 4.5:
                        base_id = 1
            return base_id

        # try to detect an amino-acid
        CA = self.get_atom_by_name(resi, 'CA')
        C  = self.get_atom_by_name(resi, 'C')
        if not C:
            C  = self.get_atom_by_name(resi, 'N')
        if C and CA:
            d = np.linalg.norm(np.array(CA.xyz, float) - np.array(C.xyz, float))
            if d <= dcrt:
                base_id = -1
            return base_id
        return base_id





    def identify_uncommon_resi(self, resi, RY_only=False, debug=False):

        base_type = self.get_base_type(resi)

        base_name=resi.resname.strip()
        if base_name in list(NORMAL_SUPPORT.keys()):
            return base_name

        if not RY_only:
            if base_type>=0 and base_name in ['P', 'PSU']:
                # gchojnowski clearly wrong, but works. this is a coarse-grained method anyway
                #base_name = 'P'
                base_name = 'u'
                if debug: print("     WARNING: modified residue %s/%s/%s assigned to: %s" % (resi.resname,\
                    resi.parent().resseq.strip(), resi.parent().parent().id, base_name ))
                return base_name



        if base_type==1:
            if self.get_atom_by_name(resi, 'N2'):
                base_name='g'
            else:
                base_name='a'
        elif base_type==0:
            C5M = self.get_atom_by_name(resi, 'C5M')
            N4  = self.get_atom_by_name(resi, 'N4')
            #O4  = get_atom_by_name(resseq, 'O4')
            O2p = self.get_atom_by_name(resi, "O2'")
            #if not O2p and ( C5M or (C5M and O4)):
            if not O2p and C5M:
                base_name=='t'
            else:
                if N4:
                    base_name='c'
                else:
                    base_name='u'



        if base_name != resi.resname.strip() and debug:
            print("     WARNING: modified residue %s/%s/%s assigned to: %s" % \
                                                        (resi.resname, \
                                                        resi.parent().resseq.strip(), \
                                                        resi.parent().parent().id, \
                                                        base_name ))

        return base_name


# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------



def test():
    ph = iotbx.pdb.hierarchy.input(pdb_string=pdb_test).hierarchy


    wr = wieheisster()
    assert wr.get_base_type(ph.only_atom_group()) == 1, "get_base_type failed"
    assert wr.identify_uncommon_resi(ph.only_atom_group(), debug=True) == 'g', "identify_uncommon_resi failed"
    print("OK")
    pass


if __name__=="__main__":
    test()



