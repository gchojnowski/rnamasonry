#! /usr/bin/env libtbx.python
# -*- coding: utf-8 -*-
# =============================================================================
# Created at:
#  International Institute of Molecular and Cell Biology in Warsaw, Poland
# Author:
#  Grzegorz Chojnowski - gchojnowski@genesilico.pl, gchojnowski@gmail.com
# =============================================================================

import re
from mmtbx.refinement.real_space import individual_sites
import mmtbx.command_line.real_space_refine as rs
import mmtbx.monomer_library.pdb_interpretation
from mmtbx import monomer_library
import time
from cctbx.array_family import flex
import cctbx, scitbx

from BasePairsConst import base_pairs_families
from pdb_preprocessor import iPDB
import sys

import math

from cctbx import crystal

import moderna

# -----------------------------------------------------------------------------


class refinery:

    # -------------------------------------------------------------------------

    @staticmethod
    def create_hbond_proxies(pdb_hierarchy, basepairs, uc=None):
        import numpy as np
        from mmtbx.geometry_restraints import hbond
        from mmtbx.secondary_structure import utils as ss_utils


        build_proxies = hbond.build_simple_hbond_proxies()
        #build_proxies = hbond.build_lennard_jones_proxies
        # Fabiola et al. (2002) Protein Sci. 11:1415-23
        # http://www.ncbi.nlm.nih.gov/pubmed/12021440
        #build_proxies = hbond.build_implicit_hbond_proxies


        selection_cache = pdb_hierarchy.atom_selection_cache()
        atoms = pdb_hierarchy.atoms()

        for bp in basepairs:

            resname1 = ss_utils.get_residue_name_from_selection(
                resi_sele="resseq %i"%bp[0],
                selection_cache=selection_cache,
                atoms=atoms).upper()

            resname2 = ss_utils.get_residue_name_from_selection(
                resi_sele="resseq %i"%bp[1],
                selection_cache=selection_cache,
                atoms=atoms).upper()

            bpname = "%s%s%s"%(resname1.strip(), resname2.strip(), bp[2])
            inv_bpname = bpname[1]+bpname[0]+bpname[3]+bpname[2]+bpname[4:]

            atom_pairs = None
            distance_values = None
            for item in base_pairs_families:
                if item[0] == bpname or item[0] == inv_bpname:
                    atom_pairs = item[1]
                    break

            if not atom_pairs: continue

            distance_values = []
            for item in atom_pairs:
                distance_values.append((3.0 if item[0][0]==item[1][0]=='N' else 2.8, None, None))

            assert (len(atom_pairs) == len(distance_values))
            n_proxies = 0
            hbond_counts = [1]*atoms.size()
            use_db_values=True
            sigma = 0.05
            slack = 0.0
            remove_outliers = False

            for i, (name1, name2) in enumerate(atom_pairs):
                # atom name order in BasePairConst is broken...
                try:
                    sele1 = "name %s and resseq %i" % (name1, bp[0])
                    sele2 = "name %s and resseq %i" % (name2, bp[1])
                    (i_seq,j_seq) = ss_utils.hydrogen_bond_from_selection_pair(sele1, sele2, selection_cache)
                except:
                    sele1 = """name %s and resseq %i""" % (name2, bp[0])
                    sele2 = """name %s and resseq %i""" % (name1, bp[1])
                    try:
                        (i_seq,j_seq) = ss_utils.hydrogen_bond_from_selection_pair(sele1, sele2, selection_cache)
                    except:
                        continue

                if (hbond_counts[i_seq] > 2) or (hbond_counts[j_seq] > 2):
                    #print >> log, "One or more atoms already bonded:"
                    #print >> log, "  %s" % atoms[i_seq].fetch_labels().id_str()
                    #print >> log, "  %s" % atoms[j_seq].fetch_labels().id_str()
                    continue
                hbond_counts[i_seq] += 1
                hbond_counts[j_seq] += 1
                if (use_db_values) :
                    bp_distance_cut = -1
                    if (distance_values[i][0] is not None) :
                        bp_distance = float(distance_values[i][0])
                    else:
                        bp_distance = distance_ideal
                    if (distance_values[i][1] is not None) :
                        bp_sigma = float(distance_values[i][1])
                    else:
                        bp_sigma = sigma
                    if (distance_values[i][2] is not None) :
                        bp_slack = float(distance_values[i][2])
                    else:
                        bp_slack = slack
                else:
                    bp_distance = distance_ideal
                    bp_distance_cut = distance_cut
                    bp_sigma = sigma
                    bp_slack = slack

                if (remove_outliers) and (distance_cut > 0) :
                    dist = atoms[i_seq].distance(atoms[j_seq])
                    if (dist > distance_cut) :
                        continue

                build_proxies.add_proxy(
                    i_seqs=[i_seq,j_seq],
                    distance_ideal=bp_distance,
                    distance_cut=bp_distance_cut,
                    weight=1/(bp_sigma ** 2),
                    slack=bp_slack)
                build_proxies.add_nonbonded_exclusion(i_seq, j_seq)
                n_proxies += 1
        return build_proxies.proxies

    # -------------------------------------------------------------------------


    def __init__(self, pdb_hierarchy, \
                    inp_map=None, \
                    symm=None, \
                    basepairs=None, \
                    d_min=1.5, \
                    max_iteration=150, \
                    optimize_weights=False,
                    buffer_cart=5.0):


        assert pdb_hierarchy is not None


        self.optimize_weights = optimize_weights

        xrs_poor = pdb_hierarchy.extract_xray_structure()

        if 1:
            sel_cache = pdb_hierarchy.atom_selection_cache()
            assert sel_cache.n_seq == pdb_hierarchy.atoms_size()
            isel = sel_cache.iselection

            fixed_bases = []
            for bp in basepairs: fixed_bases.extend(bp[:2])

            print fixed_bases



            bb_selstr = "name OP1 or name OP2 or name P or name O5? or name C5? or name O3?"

            selstr = "resseq " + " or resseq ".join( map(str, fixed_bases) )
            selection  = isel(r"%s" % bb_selstr)



        else:
            selection = flex.bool(xrs_poor.scatterers().size(), True)



        if inp_map is None:
            # generate fake map
            inp_map = self.calc_map(xrs_poor, d_min, ofname_base="dupa")

            sites_cart = pdb_hierarchy.atoms().extract_xyz()
            sites_cart_min = sites_cart.min()
            sites_cart_max = sites_cart.max()

            uc_params = [0,0,0,90,90,90]

            for _i in range(3):
                uc_params[_i] = math.ceil(sites_cart_max[_i] - sites_cart_min[_i] + 2*buffer_cart)


            symm=crystal_symmetry=crystal.symmetry(
                                    unit_cell=uc_params,
                                    space_group_symbol="P1")



        processed_pdb_file = monomer_library.pdb_interpretation.process(
                        mon_lib_srv = monomer_library.server.server(),
                        ener_lib    = monomer_library.server.ener_lib(),
                        raw_records = pdb_hierarchy.as_pdb_string(crystal_symmetry=symm),
                        strict_conflict_handling = False,
                        force_symmetry           = True,
                        log                      = None)

        geometry_restraints_manager = rs.get_geometry_restraints_manager(
                                        processed_pdb_file = processed_pdb_file,
                                        xray_structure     = xrs_poor)

        if basepairs:

            hbond_params = self.create_hbond_proxies(pdb_hierarchy, basepairs, uc=symm.unit_cell())

            sctr_keys = xrs_poor.scattering_type_registry().type_count_dict().keys()
            has_hd = "H" in sctr_keys or "D" in sctr_keys

            geometry = processed_pdb_file.geometry_restraints_manager(
                    show_energies                = False,
                    plain_pairs_radius           = 5,
                    hydrogen_bond_proxies        = hbond_params,
                    assume_hydrogens_all_missing = not has_hd)
            geometry_restraints_manager = mmtbx.restraints.manager(
                    geometry      = geometry,
                    normalization = True)

            geometry_restraints_manager.crystal_symmetry = xrs_poor.crystal_symmetry()

        else:
            geometry_restraints_manager = rs.get_geometry_restraints_manager(
                                            processed_pdb_file = processed_pdb_file,
                                            xray_structure     = xrs_poor)



        self.rsr_simple_refiner = individual_sites.simple(
                                target_map                  = inp_map,
                                selection                   = selection,
                                real_space_gradients_delta  = d_min/3,
                                max_iterations              = max_iteration,
                                geometry_restraints_manager = geometry_restraints_manager.geometry)


    # -------------------------------------------------------------------------


    def calc_map(self, xrs, d_min, ofname_base=None):


        resolution_factor = min(0.5, d_min/3.0)

        f_calc = xrs.structure_factors(d_min = d_min).f_calc()
        #XXX resolution_factor=0.1 at resolution of 20\AA should yield roughly 2\AA grid
        ref_map = f_calc.fft_map(resolution_factor=resolution_factor)
        ref_map.apply_sigma_scaling()
        #ref_map_data = ref_map.real_map_unpadded()
        #self.n_real = self.ref_map._n_real

        if ofname_base:
            ref_map.as_ccp4_map(file_name="%s_map.ccp4"%ofname_base)

        return ref_map.real_map_unpadded()



    # -------------------------------------------------------------------------


    def process(self, xrs_poor, verbose=False):

        t0 = time.time()

        if self.optimize_weights:
            p = (0.08, 8.0)
            for start_value in [1.0,]:

                refined = individual_sites.refinery(
                                            refiner                  = self.rsr_simple_refiner,
                                            xray_structure           = xrs_poor,
                                            start_trial_weight_value = start_value,
                                            rms_bonds_limit          = p[0],
                                            rms_angles_limit         = p[1],
                                            optimize_weight=False)


                xrs_refined = xrs_poor.replace_sites_cart(refined.sites_cart_result)

        else:
            self.rsr_simple_refiner.refine( xray_structure = xrs_poor, weight = 0.8)
            xrs_refined = xrs_poor.replace_sites_cart(self.rsr_simple_refiner.sites_cart())

        if verbose: dist = flex.mean(flex.sqrt((xrs_poor.sites_cart() - xrs_refined.sites_cart()).dot()))


        t1 = time.time()-t0
        if verbose: print "     Done (time = %.2f, dist = %.2f)" % (t1, dist)

        return xrs_refined

# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------




def test(input_fname):


    m = moderna.load_model(input_fname, "1")
    moderna.fix_backbone(m)
    m.write_pdb_file("moderna.pdb")
    input_fname = "moderna.pdb"


    ipdb = iPDB(input_fname, verbose=False)
    ipdb.run_clarna()

    wc_bps = [[bp[0], bp[1], "WW_cis"] for bp in ipdb.basepairs]
    db_before = ipdb.dotbracket

    ipdb.ph.atoms().set_b( flex.double(ipdb.ph.atoms().size(), 20.0) )
    ipdb.ph.atoms().set_occ( flex.double(ipdb.ph.atoms().size(), 1.0) )

    ro = refinery(ipdb.ph, d_min=2.5, basepairs=wc_bps, max_iteration=500)
    new_xrs = ro.process( ipdb.ph.extract_xray_structure(), verbose=True )


    ipdb.ph.adopt_xray_structure(new_xrs)

    ipdb.ph.write_pdb_file("output.pdb")

    ipdb = iPDB("output.pdb", verbose=False)
    ipdb.run_clarna()

    db_after = ipdb.dotbracket

    print db_before[0]
    print db_after[0]


def main():
    test(sys.argv[1])


if __name__=="__main__":
    main()
