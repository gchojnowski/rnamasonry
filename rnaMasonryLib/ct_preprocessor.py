#! /usr/bin/env python
# -*- coding: utf-8 -*-
# =============================================================================
# Created at:
#  International Institute of Molecular and Cell Biology in Warsaw, Poland
# Author:
#  Grzegorz Chojnowski - gchojnowski@genesilico.pl, gchojnowski@gmail.com
# =============================================================================

import os,sys
import string
import locale

try:
    from rnamasonry_config import ROOT
except:
    sys.path.append("..")
    from rnamasonry_config import ROOT

sys.path.append(os.path.join(ROOT, "lib"))

from nested import nested_ss



class iCT:


    def __init__(self, ict_filename):

        self.sequence = []
        self.basepairs = []
        self.dotbracket = []

        with open(ict_filename, 'r') as f:
            for line in f.readlines()[1:]:
                idx, seq, _, _, bp_idx, _ = line.split()
                self.sequence.append(seq)
                if locale.atoi(bp_idx)<1: continue
                self.basepairs.append( tuple(sorted( (locale.atoi(idx)-1, \
                                                      locale.atoi(bp_idx)-1) )) )

        self.sequence = "".join(self.sequence)

        self.dotbracket = nested_ss().get_nested_dotbracket(self.basepairs, self.sequence)


        print(" --> Parsed sequence and secondary structure from the input CT file:")
        print()
        print(self.sequence)
        print("\n".join(self.dotbracket))
        print()
        sys.stdout.flush()

