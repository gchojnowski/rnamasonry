#! /usr/bin/env python
# -*- coding: utf-8 -*-
# =============================================================================
# Created at:
#  International Institute of Molecular and Cell Biology in Warsaw, Poland
# Author:
#  Grzegorz Chojnowski - gchojnowski@genesilico.pl, gchojnowski@gmail.com
# =============================================================================

import sys, os
import urllib.request, urllib.error, urllib.parse, urllib.request, urllib.parse, urllib.error
import string
from Bio import SeqIO
from io import StringIO
import gzip
import json
import re
import networkx as nx
import random
from optparse import OptionParser
import itertools

try:
    from rnamasonry_config import *
except:
    sys.path.append("..")
    from rnamasonry_config import *

CLARNA_PATH = os.path.join(ROOT, "rnaMasonryLib", "ClaRNA")
sys.path.append(CLARNA_PATH)

from Bio import PDB
from structure_ciach import StructureCiachCiach
from utils import simplify_residue, compute_close_doublets, bench_start, bench_stop
from distances import Residue
from clarna import MAX_RES_DIST, _find_contacts_using_library, ContactProgram

class AttributeDict(dict): 
    __getattr__ = dict.__getitem__
    __setattr__ = dict.__setitem__


# -------------------------------------------------------------------------

class Clarna_utils:

    def __init__(self):

        clarna_libs = ['lib/classifier.bp.json.gz', \
                       'lib/classifier.stacking.json.gz', \
                       'lib/classifier.base-phosphate.json.gz', \
                       'lib/classifier.base-ribose.json.gz', \
                       'lib/classifier.other.json.gz', \
                       'lib/classifier.other2.json.gz', \
                       'lib/classifier.other3.json.gz']


        init_opts = {'input_doublet_id': None, 'pdb_id': None, \
                        'ignore_bad_doublets': None, 'compare_with': None, \
                        'descriptions_dict': '../ClaRNA/descriptions-dict.json', \
                        'disable_align_to': True, \
                        'input': None, \
                        'show_scores_for': None, \
                        'use_old_params': False, \
                        'normalize_graph': False, \
                        'save_graph': None, \
                        'input_doublet_ids': None, \
                        'threads': 1, \
                        'lib': ",".join(clarna_libs), \
                        'disable_compare_distances': True, \
                        'data_dir': 'gc-data/pdb_files', \
                        'save_scores': None, \
                        'show_pair': None}

        # reduce classifier database (WC and non-WC)
        init_opts['lib'] = 'lib/classifier.bp.json.gz'
        # update ClaRNA paths
        init_opts['lib'] = ",".join([os.path.join(CLARNA_PATH,item) for item in init_opts['lib'].split(',')])

        self.options = AttributeDict()
        for key,val in list(init_opts.items()):
            self.options[key] = val

        bench_start(" ==> Loading ClaRNA libraries")
        self.libs = []
        for lib_fn in self.options['lib'].split(","):
            if not os.path.isfile(lib_fn):
                print("library %s does not exists" % lib_fn)
                exit(1)
            lib = ContactProgram()
            lib.load_from_json(lib_fn)
            self.libs.append(lib)
        bench_stop()


    # -------------------------------------------------------------------------


    def load_pdb_fobject(self, fobject):
        parser = PDB.PDBParser()
        res = parser.get_structure("c",fobject)
        for a in res.get_atoms():
            if re.match(r'^[A-Z]{1,2}[0-9]?\*$',a.id):
                a.id = a.id.replace("*","'")
        return res

    # -------------------------------------------------------------------------

    def extract_close_doublets_from_fobject(self, fobject,ids=None,use_old_params=False):
        # LETITSTAY: code duplications with StructureCiachCiach
        res_num = {}

        structure = self.load_pdb_fobject(fobject)
        ciach = StructureCiachCiach(structure,dont_normalize=True)
        close_doublets = []
        s = []
        for model in structure:        
            s_begin = len(s)
            residues = []
            for chain in model:
                for r in chain:
                    r.resname = r.resname.upper()
                    rr = simplify_residue(r)
                    if rr is not None:
                        next_o3p = ciach._locate_backbone(r,"P")
                        if next_o3p is not None:
                            for a in next_o3p:
                                rr['NEXT:%s'%a.id] = a.get_coord().tolist() 
                        _id = chain.id+str(r.get_id()[1])
                        _num = r.get_id()[1]
                        _resname = r.get_resname().strip()
                        if ids is not None:
                            res_num[_id] = len(s) 
                        if not _resname in ['A','C','G','U']:
                            print("ignoring %s (bad resname: %s)" % (_id,_resname))
                            residues.append(None)
                        else:
                            residues.append(r)
                        if not use_old_params:
                            s.append([chain.id, _resname, _num, Residue(_id,_resname,rr), _id])
                        else:
                            s.append([chain.id, _resname, _num, rr, _id])
                    else:
                        print("ignoring %s (missing atoms)" % _id)
            s_end = len(s)

            if ids is None:
                for (i,j) in compute_close_doublets(residues, max_distance=MAX_RES_DIST):
                    close_doublets.append((i,j,0))
                    close_doublets.append((j,i,0))

        if ids is not None:
            d_ids = [re.sub("^[^:]+:","",tmp) for tmp in ids]
            for id in d_ids:
                (id1,id2) = id.split(":")
                i = res_num.get(id1)
                j = res_num.get(id2)
                if i is None or j is None:
                    print("UNKNOWN doublet %s" % id)
                    continue
                close_doublets.append((i,j,0))
        return ("dummy_id", s, close_doublets)

    # -------------------------------------------------------------------------

    def clean_results(self, graph):
        pass


    def start(self, fobject):
        bench_start(" --> Running ClaRNA")
        (pdb_id, s, close_doublets) = self.extract_close_doublets_from_fobject(fobject)
        res = _find_contacts_using_library(pdb_id, s, close_doublets, self.libs, self.options)
        bench_stop()
        return res

# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------

def test_clarna(fname="2cky_loop_18_mcannotate", min_score=0.5):

    url = urllib.request.urlopen(url="http://iimcb.genesilico.pl/rnabricks/site_media/data/fragments/%s.pdb.gz"%fname)
    url_f = StringIO(url.read())
    gzipfile = gzip.GzipFile(fileobj=url_f)

    cutils = Clarna_utils()
    res_graph = cutils.start(gzipfile)
    for (u,v,d) in res_graph.edges(data=True):
        if d['type']=='contact' and d['weight']>min_score: print(u,v, d['n_type'], d['desc'], d['weight'])



if __name__=="__main__":

    test_clarna()

