#! /usr/bin/env python

'''
Created on Dec 30, 2013

@author: rafal

Rewritten on Apr-Jun 2015
@author: gchojnowski
'''

import sys

# ========================== CCTBX ====================================
from iotbx import pdb
from iotbx.pdb import input as pdb_input

from scitbx.math import superpose

from scitbx import matrix
from cctbx.array_family import flex
# ========================== CCTBX ====================================

import numpy as np
import re

#XXX gchojnowski
try:
    from rnamasonry_config import *
except:
    sys.path.append("..")
    from rnamasonry_config import *

sys.path.append(os.path.join(ROOT, "lib"))

from wieheisster import wieheisster



PO4_pdb_string="""\
ATOM      1  P     U A   7     269.128 -41.423 413.575  1.00 20.00           P
ATOM      2  C5'   U A   7     271.122 -41.021 415.247  1.00 20.00           C
ATOM      3  O5'   U A   7     269.800 -41.497 415.024  1.00 20.00           O
ATOM      4  C4'   U A   7     271.330 -40.621 416.683  1.00 20.00           C
ATOM     19  OP1   U A   7     269.545 -42.557 412.706  1.00 20.00           O
ATOM     20  OP2   U A   7     267.683 -41.233 413.833  1.00 20.00           O
"""


UMP_pdb_string="""\
ATOM      1  P     U A   7     269.128 -41.423 413.575  1.00 20.00           P
ATOM      2  C5'   U A   7     271.122 -41.021 415.247  1.00 20.00           C
ATOM      3  O5'   U A   7     269.800 -41.497 415.024  1.00 20.00           O
ATOM      4  C4'   U A   7     271.330 -40.621 416.683  1.00 20.00           C
ATOM      5  O4'   U A   7     270.641 -39.380 416.984  1.00 20.00           O
ATOM      6  C3'   U A   7     270.789 -41.594 417.713  1.00 20.00           C
ATOM      7  O3'   U A   7     271.632 -42.709 417.896  1.00 20.00           O
ATOM      8  C2'   U A   7     270.645 -40.724 418.956  1.00 20.00           C
ATOM      9  O2'   U A   7     271.894 -40.560 419.621  1.00 20.00           O
ATOM     10  C1'   U A   7     270.229 -39.384 418.343  1.00 20.00           C
ATOM     11  N1    U A   7     268.764 -39.168 418.428  1.00 20.00           N
ATOM     12  C2    U A   7     268.259 -38.729 419.645  1.00 20.00           C
ATOM     13  O2    U A   7     268.967 -38.521 420.612  1.00 20.00           O
ATOM     14  N3    U A   7     266.899 -38.526 419.692  1.00 20.00           N
ATOM     15  C4    U A   7     266.006 -38.720 418.649  1.00 20.00           C
ATOM     16  O4    U A   7     264.800 -38.507 418.819  1.00 20.00           O
ATOM     17  C5    U A   7     266.605 -39.177 417.428  1.00 20.00           C
ATOM     18  C6    U A   7     267.929 -39.379 417.355  1.00 20.00           C
ATOM     19  OP1   U A   7     269.545 -42.557 412.706  1.00 20.00           O
ATOM     20  OP2   U A   7     267.683 -41.233 413.833  1.00 20.00           O
END
"""


#NUCLEOTIDES_ATOMS = {'C' : [(' P  ', ' P'), (' OP1', ' O'), (' OP2', ' O'), (" O5'", ' O'), (" C5'", ' C'), (" C4'", ' C'),\
#                              (" O4'", ' O'), (" C3'", ' C'), (" O3'", ' O'), (" C2'", ' C'), (" O2'", ' O'), (" C1'", ' C'),\
#                              (' N1 ', ' N'), (' C2 ', ' C'), (' O2 ', ' O'), (' N3 ', ' N'), (' C4 ', ' C'), (' N4 ', ' N'),\
#                              (' C5 ', ' C'), (' C6 ', ' C'), (" H5'", ' H'), ("H5''", ' H'), (" H4'", ' H'), (" H3'", ' H'),\
#                              (" H2'", ' H'), ("HO2'", ' H'), (" H1'", ' H'), (' H41', ' H'), (' H42', ' H'), (' H5 ', ' H'), (' H6 ', ' H')],\
#                     'A' : [(' P  ', ' P'), (' OP1', ' O'), (' OP2', ' O'), (" O5'", ' O'), (" C5'", ' C'), (" C4'", ' C'),\
#                              (" O4'", ' O'), (" C3'", ' C'), (" O3'", ' O'), (" C2'", ' C'), (" O2'", ' O'), (" C1'", ' C'),\
#                              (' N9 ', ' N'), (' C8 ', ' C'), (' N7 ', ' N'), (' C5 ', ' C'), (' C6 ', ' C'), (' N6 ', ' N'),\
#                              (' N1 ', ' N'), (' C2 ', ' C'), (' N3 ', ' N'), (' C4 ', ' C'), (" H5'", ' H'), ("H5''", ' H'),\
#                              (" H4'", ' H'), (" H3'", ' H'), (" H2'", ' H'), ("HO2'", ' H'), (" H1'", ' H'), (' H8 ', ' H'),\
#                              (' H61', ' H'), (' H62', ' H'), (' H2 ', ' H')],\
#                     'G' : [(' P  ', ' P'), (' OP1', ' O'), (' OP2', ' O'), (" O5'", ' O'), (" C5'", ' C'), (" C4'", ' C'),\
#                              (" O4'", ' O'), (" C3'", ' C'), (" O3'", ' O'), (" C2'", ' C'), (" O2'", ' O'), (" C1'", ' C'),\
#                              (' N9 ', ' N'), (' C8 ', ' C'), (' N7 ', ' N'), (' C5 ', ' C'), (' C6 ', ' C'), (' O6 ', ' O'),\
#                              (' N1 ', ' N'), (' C2 ', ' C'), (' N2 ', ' N'), (' N3 ', ' N'), (' C4 ', ' C'), (" H5'", ' H'),\
#                              ("H5''", ' H'), (" H4'", ' H'), (" H3'", ' H'), (" H2'", ' H'), ("HO2'", ' H'), (" H1'", ' H'),\
#                              (' H8 ', ' H'), (' H1 ', ' H'), (' H21', ' H'), (' H22', ' H')],\
#                     'U' : [(' P  ', ' P'), (' OP1', ' O'), (' OP2', ' O'), (" O5'", ' O'), (" C5'", ' C'), (" C4'", ' C'),\
#                              (" O4'", ' O'), (" C3'", ' C'), (" O3'", ' O'), (" C2'", ' C'), (" O2'", ' O'), (" C1'", ' C'),\
#                              (' N1 ', ' N'), (' C2 ', ' C'), (' O2 ', ' O'), (' N3 ', ' N'), (' C4 ', ' C'), (' O4 ', ' O'),\
#                              (' C5 ', ' C'), (' C6 ', ' C'), (" H5'", ' H'), ("H5''", ' H'), (" H4'", ' H'), (" H3'", ' H'),\
#                              (" H2'", ' H'), ("HO2'", ' H'), (" H1'", ' H'), (' H3 ', ' H'), (' H5 ', ' H'), (' H6 ', ' H')]}



NUCLEOTIDES_ATOMS = {'C' : [(' P  ', ' P'), (' OP1', ' O'), (' OP2', ' O'), (" O5'", ' O'), (" C5'", ' C'), (" C4'", ' C'),\
                              (" O4'", ' O'), (" C3'", ' C'), (" O3'", ' O'), (" C2'", ' C'), (" O2'", ' O'), (" C1'", ' C'),\
                              (' N1 ', ' N'), (' C2 ', ' C'), (' O2 ', ' O'), (' N3 ', ' N'), (' C4 ', ' C'), (' N4 ', ' N'),\
                              (' C5 ', ' C'), (' C6 ', ' C')], \
                     'A' : [(' P  ', ' P'), (' OP1', ' O'), (' OP2', ' O'), (" O5'", ' O'), (" C5'", ' C'), (" C4'", ' C'),\
                              (" O4'", ' O'), (" C3'", ' C'), (" O3'", ' O'), (" C2'", ' C'), (" O2'", ' O'), (" C1'", ' C'),\
                              (' N9 ', ' N'), (' C8 ', ' C'), (' N7 ', ' N'), (' C5 ', ' C'), (' C6 ', ' C'), (' N6 ', ' N'),\
                              (' N1 ', ' N'), (' C2 ', ' C'), (' N3 ', ' N'), (' C4 ', ' C')], \
                     'G' : [(' P  ', ' P'), (' OP1', ' O'), (' OP2', ' O'), (" O5'", ' O'), (" C5'", ' C'), (" C4'", ' C'),\
                              (" O4'", ' O'), (" C3'", ' C'), (" O3'", ' O'), (" C2'", ' C'), (" O2'", ' O'), (" C1'", ' C'),\
                              (' N9 ', ' N'), (' C8 ', ' C'), (' N7 ', ' N'), (' C5 ', ' C'), (' C6 ', ' C'), (' O6 ', ' O'),\
                              (' N1 ', ' N'), (' C2 ', ' C'), (' N2 ', ' N'), (' N3 ', ' N'), (' C4 ', ' C')], \
                     'U' : [(' P  ', ' P'), (' OP1', ' O'), (' OP2', ' O'), (" O5'", ' O'), (" C5'", ' C'), (" C4'", ' C'),\
                              (" O4'", ' O'), (" C3'", ' C'), (" O3'", ' O'), (" C2'", ' C'), (" O2'", ' O'), (" C1'", ' C'),\
                              (' N1 ', ' N'), (' C2 ', ' C'), (' O2 ', ' O'), (' N3 ', ' N'), (' C4 ', ' C'), (' O4 ', ' O'),\
                              (' C5 ', ' C'), (' C6 ', ' C')]}

NUCLEOBASE_ATOMS = {'C' : [ 'N1', 'C2', 'O2', 'N3', 'C4', 'N4', 'C5', 'C6'], \
                    'A' : [ 'N9', 'C8', 'N7', 'C5', 'C6', 'N6', 'N1', 'C2', 'N3', 'C4'], \
                    'G' : [ 'N9', 'C8', 'N7', 'C5', 'C6', 'O6', 'N1', 'C2', 'N2', 'N3', 'C4'], \
                    'U' : [ 'N1', 'C2', 'O2', 'N3', 'C4', 'O4', 'C5', 'C6']}




class ph_parser:

    def __init__(self):

        self.ump_ph = pdb.hierarchy.input(pdb_string=UMP_pdb_string).hierarchy

        self.hinge_atoms = np.array(["C4'", "C5'", "O5'"])

        self.po4_atoms = ["OP1", "OP2", "P"]


        self.mov_hinge = self.get_o5p_hinge_coords(self.ump_ph.only_residue())




    def parse(self, hierarchy, clean=True):
        root = pdb.hierarchy.root()
        model = pdb.hierarchy.model()
        chain = pdb.hierarchy.chain('A')

        atom_number = 1
        for rg in sorted(hierarchy.residue_groups(),key=lambda x: x.resseq_as_int()):
            rg.link_to_previous=True
            for ag in rg.atom_groups():
                ag.resname = ag.resname.upper()
                for atom in sorted(ag.atoms(),key=lambda x: x.serial_as_int()):
                    atom.serial = str(atom_number)
                    atom_number += 1
            chain.append_residue_group(rg.detached_copy())

        model.append_chain(chain)
        root.append_model(model)

        if clean:
            # it should not be used during production
            return self.string2model( root.as_pdb_string() )
        else:
            return root


    # -------------------------------------------------------------------------

    def get_o5p_hinge_coords(self, rg):


        _tmp_hinge = [None]*3

        for atm in rg.atoms():
            ii = np.where(self.hinge_atoms == atm.name.strip())
            if len(ii[0]):
                _tmp_hinge[ii[0][0]] = atm.xyz


        assert _tmp_hinge.count(None)==0

        return flex.vec3_double(_tmp_hinge)




    # -------------------------------------------------------------------------


    def add_missing_o5p_phosphate( self, rg ):

        # clean remaining PO4 atoms
        for atom in rg.atoms():
            if atom.name.strip() in self.po4_atoms:
                rg.only_atom_group().remove_atom(atom)


        ref_hinge = self.get_o5p_hinge_coords(rg)
        superposition = superpose.least_squares_fit(flex.vec3_double(ref_hinge), flex.vec3_double(self.mov_hinge), method=["kearsley", "kabsch"][0])


        rtmx = matrix.rt((superposition.r, superposition.t))
        _mov = self.ump_ph.deep_copy()

        _mov.atoms().set_xyz(new_xyz = rtmx * _mov.atoms().extract_xyz())


        for atom in _mov.atoms():
            if atom.name.strip() in self.po4_atoms:
                rg.only_atom_group().append_atom(atom.detached_copy())


    # -------------------------------------------------------------------------


    def add_base( self, rg, missing_names_set ):

        for base,atom_names in list(NUCLEOBASE_ATOMS.items()):

            if missing_names_set == set(atom_names):
                #print "   -> Missing %s" % base
                break


        return False




    # -------------------------------------------------------------------------


    def string2model( self, pdb_string, reject_broken=False ):

        lines = pdb_string.split('\n')
        new_lines = []
        for line in lines:

            line = re.sub("O2P", "OP2", line)
            line = re.sub("O1P", "OP1", line)
            line = re.sub("\*", "\'", line)

            if line.startswith('HETATM'):
                new_lines.append('ATOM  '+line[6:])
            elif line.startswith('ANISOU'):
                continue
            else:
                new_lines.append(line)

        as_pdb_string = "\n".join(new_lines)
        res_hier = pdb_input(source_info = None, lines = as_pdb_string.splitlines()).construct_hierarchy()

        wr = wieheisster()
        for rg in res_hier.residue_groups():
            ag = rg.atom_groups()[0]

            #XXX gchojnowski, fixes uncommon resnames issue
            res_name = ag.resname.strip()
            if not res_name in list(NUCLEOTIDES_ATOMS.keys()):
                res_name = wr.identify_uncommon_resi(ag).upper()
            atoms = rg.atoms()
            atoms_names = [a.name for a in atoms]

            try:
                missing_atoms = [(i,a) for i,a in enumerate(NUCLEOTIDES_ATOMS[res_name]) if a[0] not in atoms_names]
            except:
                missing_atoms = []



            missing_names_set = set(_[1][0].strip() for _ in missing_atoms)

            if len( missing_names_set.intersection( set(self.po4_atoms) ) ):
                #print "  Adding missing phosphate group"
                try:
                    self.add_missing_o5p_phosphate(rg)
                except:
                    if reject_broken: return None
                    pass

            elif len(missing_names_set)==1 and missing_names_set == set(["O2'"]):
                # hydroxyl, not that much needed :), 4192
                #print "  Missing hydroxyl, ignoring"
                pass


            elif len(missing_atoms)>0:
                #try to add a base here: 45342
                if reject_broken: return None
                print("     WARNING: Incomplete residue removed: %s/%s" % (rg.resseq.strip(), rg.parent().id))
                rg.parent().remove_residue_group(rg)
                pass
                #return None
                #if not self.add_base(rg, missing_names_set): return None


        a_serial = 1
        for rg in sorted(res_hier.residue_groups(),key=lambda x: x.resseq_as_int()):
            for ag in rg.atom_groups():
                res_name = ag.resname

                #XXX gchojnowski, fixes uncommon resnames issue
                res_name = ag.resname.strip()
                if not res_name in list(NUCLEOTIDES_ATOMS.keys()):
                    res_name = wr.identify_uncommon_resi(ag).upper()


                if not res_name in list(NUCLEOTIDES_ATOMS.keys()):
                    if reject_broken: return None
                    print("     WARNING: Unknown residue removed: %s/%s/%s" % (ag.resname, rg.resseq.strip(), rg.parent().id))
                    rg.parent().remove_residue_group(rg)
                    continue

                mapping = {}
                for a_name in NUCLEOTIDES_ATOMS[res_name]:
                    mapping[a_name[0]] = a_serial
                    a_serial += 1

                for atom in ag.atoms():
                    try:
                        atom.serial = str(mapping[atom.name])
                    except KeyError:
                        ag.remove_atom(atom)

        return res_hier
