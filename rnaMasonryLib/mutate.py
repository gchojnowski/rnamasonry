#! /usr/bin/env libtbx.python
# -*- coding: utf-8 -*-
# =============================================================================
# Created at:
#  International Institute of Molecular and Cell Biology in Warsaw, Poland
# Author:
#  Grzegorz Chojnowski - gchojnowski@genesilico.pl, gchojnowski@gmail.com
# =============================================================================

import os
import iotbx.pdb
from scitbx import matrix
from scitbx.math import r3_rotation_axis_and_angle_as_matrix
import math
from libtbx.test_utils import approx_equal
from cctbx.array_family import flex
from scitbx.math import superpose
import numpy as np
import string
from cctbx import maptbx

#XXX gchojnowski
from wieheisster import wieheisster


pdb_test = """\
REMARK    3DNA v2.1 (c) 2012 Dr. Xiang-Jun Lu (http://x3dna.org)
CRYST1   25.000   25.000   25.000  90.00  90.00  90.00 P 1
SCALE1      0.040000  0.000000  0.000000        0.00000
SCALE2      0.000000  0.040000  0.000000        0.00000
SCALE3      0.000000  0.000000  0.040000        0.00000
ATOM      1  P   A   A   1      17.552  11.721   4.374  1.00  0.00           P  
ATOM      2  OP1 A   A   1      17.394  11.350   2.949  1.00  0.00           O  
ATOM      3  OP2 A   A   1      16.924  12.997   4.784  1.00  0.00           O1-
ATOM      4  C5' A   A   1      17.686   9.261   5.264  1.00  0.00           C  
ATOM      5  O5' A   A   1      17.015  10.536   5.301  1.00  0.00           O  
ATOM      6  C4' A   A   1      17.095   8.333   6.307  1.00  0.00           C  
ATOM      7  O4' A   A   1      17.569   8.748   7.621  1.00  0.00           O  
ATOM      8  C3' A   A   1      15.572   8.361   6.437  1.00  0.00           C  
ATOM      9  O3' A   A   1      14.941   7.605   5.413  1.00  0.00           O  
ATOM     10  C2' A   A   1      15.441   7.780   7.844  1.00  0.00           C  
ATOM     11  O2' A   A   1      15.613   6.390   8.042  1.00  0.00           O  
ATOM     12  C1' A   A   1      16.539   8.545   8.576  1.00  0.00           C  
ATOM     13  N1  A   A   1      14.274  10.997  12.478  1.00  0.00           N  
ATOM     14  C2  A   A   1      14.556   9.714  12.259  1.00  0.00           C  
ATOM     15  N3  A   A   1      15.145   9.158  11.223  1.00  0.00           N  
ATOM     16  C4  A   A   1      15.474  10.087  10.303  1.00  0.00           C  
ATOM     17  C5  A   A   1      15.250  11.432  10.385  1.00  0.00           C  
ATOM     18  C6  A   A   1      14.613  11.905  11.543  1.00  0.00           C  
ATOM     19  N6  A   A   1      14.331  13.198  11.757  1.00  0.00           N  
ATOM     20  N7  A   A   1      15.726  12.074   9.250  1.00  0.00           N  
ATOM     21  C8  A   A   1      16.214  11.112   8.529  1.00  0.00           C  
ATOM     22  N9  A   A   1      16.098   9.867   9.103  1.00  0.00           N  
ATOM     23  P   C   A   2      13.510   8.075   4.873  1.00  0.00           P  
ATOM     24  OP1 C   A   2      13.167   7.314   3.651  1.00  0.00           O  
ATOM     25  OP2 C   A   2      13.487   9.549   4.746  1.00  0.00           O1-
ATOM     26  C5' C   A   2      12.629   6.268   6.553  1.00  0.00           C  
ATOM     27  O5' C   A   2      12.554   7.624   6.074  1.00  0.00           O  
ATOM     28  C4' C   A   2      11.736   6.098   7.767  1.00  0.00           C  
ATOM     29  O4' C   A   2      12.380   6.729   8.911  1.00  0.00           O  
ATOM     30  C3' C   A   2      10.370   6.779   7.688  1.00  0.00           C  
ATOM     31  O3' C   A   2       9.448   6.025   6.911  1.00  0.00           O  
ATOM     32  C2' C   A   2      10.050   6.847   9.181  1.00  0.00           C  
ATOM     33  O2' C   A   2       9.629   5.678   9.857  1.00  0.00           O  
ATOM     34  C1' C   A   2      11.390   7.313   9.742  1.00  0.00           C  
ATOM     35  N1  C   A   2      11.559   8.795   9.731  1.00  0.00           N  
ATOM     36  C2  C   A   2      11.045   9.514  10.807  1.00  0.00           C  
ATOM     37  O2  C   A   2      10.476   8.901  11.717  1.00  0.00           O  
ATOM     38  N3  C   A   2      11.189  10.865  10.816  1.00  0.00           N  
ATOM     39  C4  C   A   2      11.814  11.491   9.810  1.00  0.00           C  
ATOM     40  N4  C   A   2      11.929  12.808   9.867  1.00  0.00           N  
ATOM     41  C5  C   A   2      12.349  10.770   8.695  1.00  0.00           C  
ATOM     42  C6  C   A   2      12.194   9.421   8.705  1.00  0.00           C  
ATOM     43  P   G   A   3       8.926  19.107  18.412  1.00  0.00           P  
ATOM     44  OP1 G   A   3       9.125  19.625  19.784  1.00  0.00           O  
ATOM     45  OP2 G   A   3       9.999  19.417  17.440  1.00  0.00           O1-
ATOM     46  C5' G   A   3       7.516  17.019  19.129  1.00  0.00           C  
ATOM     47  O5' G   A   3       8.686  17.527  18.462  1.00  0.00           O  
ATOM     48  C4' G   A   3       7.424  15.518  18.945  1.00  0.00           C  
ATOM     49  O4' G   A   3       6.989  15.241  17.583  1.00  0.00           O  
ATOM     50  C3' G   A   3       8.739  14.751  19.076  1.00  0.00           C  
ATOM     51  O3' G   A   3       9.103  14.556  20.438  1.00  0.00           O  
ATOM     52  C2' G   A   3       8.349  13.477  18.326  1.00  0.00           C  
ATOM     53  O2' G   A   3       7.521  12.524  18.956  1.00  0.00           O  
ATOM     54  C1' G   A   3       7.631  14.065  17.116  1.00  0.00           C  
ATOM     55  N1  G   A   3      10.066  12.314  12.996  1.00  0.00           N  
ATOM     56  C2  G   A   3       9.234  11.655  13.880  1.00  0.00           C  
ATOM     57  N2  G   A   3       9.025  10.359  13.632  1.00  0.00           N  
ATOM     58  N3  G   A   3       8.661  12.247  14.925  1.00  0.00           N  
ATOM     59  C4  G   A   3       8.989  13.563  15.015  1.00  0.00           C  
ATOM     60  C5  G   A   3       9.802  14.300  14.188  1.00  0.00           C  
ATOM     61  C6  G   A   3      10.415  13.670  13.073  1.00  0.00           C  
ATOM     62  O6  G   A   3      11.166  14.156  12.231  1.00  0.00           O  
ATOM     63  N7  G   A   3       9.883  15.619  14.628  1.00  0.00           N  
ATOM     64  C8  G   A   3       9.121  15.640  15.695  1.00  0.00           C  
ATOM     65  N9  G   A   3       8.544  14.424  15.994  1.00  0.00           N  
ATOM     66  P   U   A   4      10.656  14.517  20.824  1.00  0.00           P  
ATOM     67  OP1 U   A   4      10.800  14.572  22.297  1.00  0.00           O  
ATOM     68  OP2 U   A   4      11.384  15.556  20.062  1.00  0.00           O1-
ATOM     69  C5' U   A   4      10.302  11.924  20.701  1.00  0.00           C  
ATOM     70  O5' U   A   4      11.078  13.068  20.296  1.00  0.00           O  
ATOM     71  C4' U   A   4      10.799  10.683  19.987  1.00  0.00           C  
ATOM     72  O4' U   A   4      10.344  10.728  18.603  1.00  0.00           O  
ATOM     73  C3' U   A   4      12.316  10.542  19.873  1.00  0.00           C  
ATOM     74  O3' U   A   4      12.894  10.072  21.083  1.00  0.00           O  
ATOM     75  C2' U   A   4      12.380   9.561  18.704  1.00  0.00           C  
ATOM     76  O2' U   A   4      12.088   8.195  18.924  1.00  0.00           O  
ATOM     77  C1' U   A   4      11.341  10.165  17.764  1.00  0.00           C  
ATOM     78  N1  U   A   4      11.886  11.229  16.875  1.00  0.00           N  
ATOM     79  C2  U   A   4      12.479  10.822  15.705  1.00  0.00           C  
ATOM     80  O2  U   A   4      12.573   9.651  15.380  1.00  0.00           O  
ATOM     81  N3  U   A   4      12.972  11.836  14.906  1.00  0.00           N  
ATOM     82  C4  U   A   4      12.922  13.189  15.175  1.00  0.00           C  
ATOM     83  O4  U   A   4      13.398  14.004  14.382  1.00  0.00           O  
ATOM     84  C5  U   A   4      12.280  13.517  16.426  1.00  0.00           C  
ATOM     85  C6  U   A   4      11.791  12.551  17.222  1.00  0.00           C  
END
"""

model_bases_pdb_string="""\
REMARK EXTRACTED FROM 3DNA v2.1 (c) 2012 Dr. Xiang-Jun Lu (http://x3dna.org)
ATOM      1  C1' A   A   1       6.874   4.999  -1.877  1.00  0.00           C
ATOM      2  N1  A   A   1       5.134   0.481  -0.167  1.00  0.00           N
ATOM      3  C2  A   A   1       6.356   1.001  -0.262  1.00  0.00           C
ATOM      4  N3  A   A   1       6.725   2.179  -0.716  1.00  0.00           N
ATOM      5  C4  A   A   1       5.655   2.893  -1.121  1.00  0.00           C
ATOM      6  C5  A   A   1       4.348   2.496  -1.086  1.00  0.00           C
ATOM      7  C6  A   A   1       4.082   1.215  -0.578  1.00  0.00           C
ATOM      8  N6  A   A   1       2.849   0.697  -0.485  1.00  0.00           N
ATOM      9  N7  A   A   1       3.524   3.496  -1.584  1.00  0.00           N
ATOM     10  C8  A   A   1       4.345   4.449  -1.900  1.00  0.00           C
ATOM     11  N9  A   A   1       5.666   4.158  -1.647  1.00  0.00           N
ATOM      1  C1' C   C   2       8.486   0.494  -4.425  1.00  0.00           C
ATOM      2  N1  C   C   2       7.014   0.438  -4.195  1.00  0.00           N
ATOM      3  C2  C   C   2       6.487  -0.729  -3.648  1.00  0.00           C
ATOM      4  O2  C   C   2       7.253  -1.661  -3.379  1.00  0.00           O
ATOM      5  N3  C   C   2       5.148  -0.800  -3.431  1.00  0.00           N
ATOM      6  C4  C   C   2       4.352   0.233  -3.736  1.00  0.00           C
ATOM      7  N4  C   C   2       3.054   0.114  -3.504  1.00  0.00           N
ATOM      8  C5  C   C   2       4.874   1.442  -4.299  1.00  0.00           C
ATOM      9  C6  C   C   2       6.214   1.492  -4.509  1.00  0.00           C
ATOM      1  C1' G   G   3       3.084  -7.921  -0.671  1.00  0.00           C
ATOM      2  N1  G   G   3       4.091  -3.226  -2.376  1.00  0.00           N
ATOM      3  C2  G   G   3       4.910  -4.335  -2.300  1.00  0.00           C
ATOM      4  N2  G   G   3       6.162  -4.170  -2.736  1.00  0.00           N
ATOM      5  N3  G   G   3       4.507  -5.512  -1.826  1.00  0.00           N
ATOM      6  C4  G   G   3       3.207  -5.489  -1.431  1.00  0.00           C
ATOM      7  C5  G   G   3       2.320  -4.440  -1.469  1.00  0.00           C
ATOM      8  C6  G   G   3       2.748  -3.184  -1.974  1.00  0.00           C
ATOM      9  O6  G   G   3       2.110  -2.141  -2.086  1.00  0.00           O
ATOM     10  N7  G   G   3       1.079  -4.828  -0.969  1.00  0.00           N
ATOM     11  C8  G   G   3       1.251  -6.088  -0.649  1.00  0.00           C
ATOM     12  N9  G   G   3       2.522  -6.560  -0.901  1.00  0.00           N
ATOM      1  C1' U   U   4       6.874  -4.999   1.877  1.00  0.00           C
ATOM      2  N1  U   U   4       5.666  -4.158   1.647  1.00  0.00           N
ATOM      3  C2  U   U   4       5.867  -2.911   1.107  1.00  0.00           C
ATOM      4  O2  U   U   4       6.971  -2.482   0.819  1.00  0.00           O
ATOM      5  N3  U   U   4       4.725  -2.160   0.908  1.00  0.00           N
ATOM      6  C4  U   U   4       3.431  -2.543   1.197  1.00  0.00           C
ATOM      7  O4  U   U   4       2.487  -1.783   0.974  1.00  0.00           O
ATOM      8  C5  U   U   4       3.322  -3.869   1.760  1.00  0.00           C
ATOM      9  C6  U   U   4       4.416  -4.621   1.964  1.00  0.00           C
END
"""



# between purines and pyrimidines, the normals are reversed, because
# the rings are reversed with respect to the helix axis.
NORMAL_SUPPORT = {
        'C':['N1','C2','N3','C4','C5','C6'],
        'U':['N1','C2','N3','C4','C5','C6'],
        'T':['N1','C2','N3','C4','C5','C6'],
        'G':['N1','C2','C4','N3','C5','C6'],
        'A':['N1','C2','C4','N3','C5','C6'],
        'TRP':['CD2','CE2','CZ2','CH2','CZ3','CE3'],
        'HIS':['CG','ND1','CE1','NE2','CD2'],
        'PHE':['CG','CD2','CE2','CZ','CE1','CD1'],
        'TYR':['CG','CD2','CE2','CZ','CE1','CD1'],
        }

MUT_TABLE = {"A":"U", "U":"A", "G":"C", "C":"G"}


BACKBONE_ATOM_NAMES = ["P", "OP1", "OP2", "O1P", "O2P", "O5'", "C5'", "C4'", "O4'", "C3'", "O3'", "C2'", "O2'",]

# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------

class mutants:

    def _get_atom_by_name(self, resi, name):
        for atom in resi.atoms():
            if atom.name.strip()==name:
                return atom
        return None


    def _get_glyco_vec(self, resi):

        resname = resi.resname.strip().upper()

        glyco_vec = matrix.col(self._get_atom_by_name(resi, "N1" if resname in ["U", "C"] else "N9").xyz) - \
                    matrix.col(self._get_atom_by_name(resi, "C1'").xyz)

        return glyco_vec.normalize()


    def _get_c1p_translation(self, resi1, resi2):
        trans_vec = matrix.col(self._get_atom_by_name(resi1, "C1'").xyz) - \
                    matrix.col(self._get_atom_by_name(resi2, "C1'").xyz)

        return trans_vec

    # -------------------------------------------------------------------------

    def _get_base_normal_vector(self, resi):

        resname = resi.resname.strip().upper()

        #XXX gchojnowski, fixes uncommon resnames issue
        if not resname in list(NORMAL_SUPPORT.keys()):
            wr = wieheisster()
            resname = wr.identify_uncommon_resi(resi).upper()

        normal_set = NORMAL_SUPPORT[resname]

        vec_1 = matrix.col( self._get_atom_by_name(resi, normal_set[1]).xyz ) - \
                matrix.col( self._get_atom_by_name(resi, normal_set[0]).xyz )


        vec_2 = matrix.col( self._get_atom_by_name(resi, normal_set[3]).xyz ) - \
                matrix.col( self._get_atom_by_name(resi, normal_set[2]).xyz )


        normal_vec = vec_1.cross(vec_2)
        return normal_vec.normalize()


    # -------------------------------------------------------------------------


    # replaces selected residue with purine/pyrimidine (repr. with Guanine and Cytosine)
    # 1. match model and atrget C1' atoms
    # 2. match glycosidic bonds vectors (rotation around the two vectors plane normal)
    # 3. match base normals (rotation around matched glycosidic bond vector)


    # -------------------------------------------------------------------------


    def __init__(self, debug=False):
        #path_to_script = os.path.dirname(os.path.realpath(__file__))
        #model_fname = "./data/model_bases.pdb"
        #model_fname = os.path.join(path_to_script, "..", model_fname)
        #self.model_bases = iotbx.pdb.input(source_info=None, lines=open(model_fname).read()).construct_hierarchy()

        self.model_bases = iotbx.pdb.hierarchy.input(pdb_string=model_bases_pdb_string).hierarchy

        if debug: self.model_bases.show()

        self.model_chains_dict = {}

        for ch in self.model_bases.chains():
            self.model_chains_dict[ch.id] = ch

        if debug: print(list(self.model_chains_dict.keys()))


    # -------------------------------------------------------------------------


    def mutate(self, ag, mutate2="U"):

        new_base = self.model_chains_dict[mutate2].only_residue_group().detached_copy()

        new_base_normal = self._get_base_normal_vector(new_base.only_atom_group())
        try:
            ag_normal = self._get_base_normal_vector(ag)
        except:
            return


        n19_xyz = self._get_atom_by_name(new_base, "N1" if mutate2 in ["U", "C"] else "N9").xyz

        new_base_ref = flex.vec3_double([
                            self._get_atom_by_name(new_base, "C1'").xyz, \
                            n19_xyz, \
                            matrix.col(n19_xyz) + \
                                new_base_normal ])


        resname = ag.resname.strip().upper()
        #XXX gchojnowski, fixes uncommon resnames issue
        if not resname in list(NORMAL_SUPPORT.keys()):
            wr = wieheisster()
            resname = wr.identify_uncommon_resi(ag).upper()


        n19_xyz = self._get_atom_by_name(ag, "N1" if resname in ["U", "C"] else "N9").xyz

        ag_ref = flex.vec3_double([
                            self._get_atom_by_name(ag, "C1'").xyz, \
                            n19_xyz, \
                            matrix.col(n19_xyz) + \
                                ag_normal ])


        superposition = superpose.least_squares_fit(ag_ref, new_base_ref, method=["kearsley", "kabsch"][0])
        rtmx = matrix.rt((superposition.r, superposition.t))
        new_base.atoms().set_xyz(new_xyz = rtmx * new_base.atoms().extract_xyz())

        for atm in ag.atoms():
            if atm.name.strip() in BACKBONE_ATOM_NAMES: continue
            ag.remove_atom(atm)


        for atm in new_base.atoms():
            ag.append_atom(atm.detached_copy())

        ag.resname = mutate2

    # -------------------------------------------------------------------------


    def mutate_selected(self, frag, selected_resids):
        frag_mut = frag.deep_copy()
        for ag in frag_mut.atom_groups():
            if ag.parent().resseq_as_int() in selected_resids:
                resname = ag.resname.strip().upper()
                #XXX gchojnowski, fixes uncommon resnames issue
                if not resname in list(MUT_TABLE.keys()):
                    wr = wieheisster()
                    resname = wr.identify_uncommon_resi(ag).upper()

                self.mutate(ag, mutate2=MUT_TABLE[resname])

        return frag_mut


    # -------------------------------------------------------------------------


    def mutate2seq(self, chain, target_seq):

        for ag,target_resname in zip(chain.atom_groups(), target_seq.upper()):
            if ag.resname.strip() == target_resname: continue
            self.mutate(ag, mutate2=target_resname)



    # -------------------------------------------------------------------------


    def test(self, debug=False):

        target_seq = "UGCA"

        # load test fragment
        ph = iotbx.pdb.hierarchy.input(pdb_string=pdb_test).hierarchy
        chain = ph.only_chain()

        if debug: ph.write_pdb_file("before.pdb")


        self.mutate2seq(chain, target_seq)

        assert "UGCA" == "".join([r.strip() for r in chain.get_residue_names_padded()]), "FAILED"
        print("OK")


        if debug: ph.write_pdb_file("after.pdb")


# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------




def tests():
    mutate = mutants(debug=False)
    mutate.test()




if __name__=="__main__":
    tests()


