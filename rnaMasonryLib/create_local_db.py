from sys import argv
from collections import defaultdict
from os import mkdir
from os.path import exists
import json
import urllib2, urllib
import tarfile

from iotbx.pdb import input as pdb_input

from sys import path
path.append('../k2n_standalone/')

from rnagraphs import rnagraphs

RNADB_URL = 'http://rna3db-mirror'
AVAILABLE_RMSDS = [5.0, 2.0, 1.0]
AVAILABLE_NRLISTS = ['1.5', '2.0', '2.5', '3.0', '3.5', '4.0', 'all', 'ignore']

def get_motifs(dir_with_ss_files,ss_files):
    """This parse file with secondary structure and gets ss motifs info."""
    motifs = []

    for ss_file in ss_files:
        rg = rnagraphs(dir_with_ss_files+'/'+ss_file)
        rg.extract_motifs()
        rnagraph = rg.association_graph

        for n in rnagraph.nodes():
            ftype = rnagraph.nodes[n]['fragment_info']['fragment_type']
            cp = "_".join(map(str,(map(len,(rnagraph.nodes[n]['fragment_info']['frg_as_consecutive_seqs'])))))
            cyclic_permutation = "_%s_%s_" % (cp,cp)

            continue_outer = False
            for m in motifs:
                cyc_permut = "_%s_" % cp

                len1 = len(cyc_permut.split('_')[1:-1])
                len2 = int(len(m[1].split('_')[1:-1]) / 2)

                if ftype == m[0] and len1 == len2 and cyc_permut in m[1]:
                    continue_outer = True
                    break
            if continue_outer:
                continue
            motifs.append( (ftype,cyclic_permutation) )

    return motifs

def download_motifs_from_Bricks(ss_motifs,rmsd,nr_list,max_number_of_motifs=None):
    """This downloads 3D structures for each motif from RNA Bricks."""
    results = defaultdict(list) ### this keeps result as {(fragment_type,cyclic_permutation) : list_of_structures}

    for ss_motif in ss_motifs:
        print "parsing motif: %s, %s" % (ss_motif[0],ss_motif[1])
        fragment_type = ss_motif[0]
        cp = ss_motif[1].split('_')[1:-1]
        cyclic_permutation = cp[:int(len(cp)/2)]

        query = {'chain_frags':json.dumps([int(i) for i in cyclic_permutation]),\
                 'fragment_type':fragment_type,\
                 'max_rmsd':rmsd,\
                 'nr_list' : nr_list}

        request = urllib2.Request(url=RNADB_URL+"/api/serve_class_members",\
                                  data=urllib.urlencode( query ),\
                                  headers={'Content-Type': 'application/json'})

        fragments_ids = json.loads(urllib2.urlopen(request).read())

        for i, fragment_id in enumerate(fragments_ids):
            if max_number_of_motifs != None and i+1 > max_number_of_motifs:
                break
            print "    downloading motif: %i" % fragment_id
            pdb_object = urllib2.urlopen(RNADB_URL+"/fragments/get_fragment_by_id/frag_id=%i"%fragment_id).read()
            hierarchy = pdb_input(source_info = None, lines = pdb_object).construct_hierarchy()

            results[(fragment_type,ss_motif[1])].append( (fragment_id,hierarchy) )

    return results

def create_local_db(fragments):
    db_dir = 'local_db'
    mkdir(db_dir)

    db_info = []
    for k,v in fragments.iteritems():
        fragment_type = k[0]
        cyclic_permutation = k[1]

        p = db_dir + '/' + fragment_type
        if not exists(p):
            mkdir(p)

        for fragment in v:
            fragment_id = fragment[0]
            hierarchy = fragment[1]

            hierarchy.write_pdb_file(p+'/structure_'+str(fragment_id)+'.pdb')
            db_info.append("%s %s %i" % (fragment_type,cyclic_permutation,fragment_id))

    with open(db_dir+'/db_info','w') as handle:
        handle.write("\n".join(db_info))

    with tarfile.open(db_dir+'.tar.gz', 'w:gz') as handle:
        handle.add(db_dir, arcname=db_dir)

if __name__=="__main__":
    ###
    ### This script takes file with pdb ids (one pdb id per each row) and examines all secondary structure motifs
    ### that are present in this structures. Then for each motif it downloads from RNA Bricks db all 3D structures
    ### which corresponds to these motifs. After that it creates a local database of motifs (tar compressed).
    ###
    ### argv[1] - file with pdb ids
    ### argv[2] - path to directory with files containing secondary structures
    ### argv[3] - max number of fragments
    ###

    rmsd = AVAILABLE_RMSDS[2]
    nr_list = AVAILABLE_NRLISTS[2]
    dir_with_ss_files = argv[2]

    with open(argv[1],'r') as handle:
        pdb_ids = handle.read().split()

    ss_motifs = get_motifs(dir_with_ss_files,pdb_ids)
    print "#############################################################################"
    print "Motifs to download:"
    print ss_motifs
    print "#############################################################################"
    fragments = download_motifs_from_Bricks(ss_motifs,rmsd,nr_list,int(argv[3]))

    ### save results to local db
    create_local_db(fragments)
