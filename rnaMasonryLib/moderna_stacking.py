# *************************************************************************************************
# *************************************************************************************************
# *************************************************************************************************
# *************************************************************************************************
# Moderna stacking detector


#__author__ = "Magdalena Rother, Tomasz Puton, Kristian Rother"
#__copyright__ = "Copyright 2008, The Moderna Project"
#__credits__ = ["Janusz Bujnicki"]
#__license__ = "GPL"
#__version__ = "1.5.0"
#__maintainer__ = "Magdalena Rother"
#__email__ = "mmusiel@genesilico.pl"
#__status__ = "Production"
# ADAPTED for CCTBX objects from: StackingCalculator.py 
# 27.06.2011 gchojnowski@genesilico.pl

#from numpy import array, add, cross, sqrt, arccos
#import math

# between purines and pyrimidines, the normals are reversed, because 
# the rings are reversed with respect to the helix axis. 
NORMAL_SUPPORT = {
        'C':['N1','C2','N3','C4','C5','C6'],
        'U':['N1','C2','N3','C4','C5','C6'],
        'T':['N1','C2','N3','C4','C5','C6'],
        'G':['N1','C2','C4','N3','C5','C6'],
        'A':['N1','C2','C4','N3','C5','C6'],
        'TRP':['CD2','CE2','CZ2','CH2','CZ3','CE3'],
        'HIS':['CG','ND1','CE1','NE2','CD2'],
        'PHE':['CG','CD2','CE2','CZ','CE1','CD1'],
        'TYR':['CG','CD2','CE2','CZ','CE1','CD1'],
        }

STACKED_AA = ['TRP','HIS','PHE','TYR']


STACKINGS = {
        (True, True): '>>',
        (True, False): '<<',
        (False, False): '<>',
        (False, True): '><',
        }

ARCPI = 180.0/np.pi



"""
A procedure for calculating stacking of RNA nucleotides.

The definition of base stacking from Major & Lemieux 
MC-Annotate paper (JMB 2001, 308, p.919ff):
"Stacking between two nitrogen bases is considered
if the distance between their rings is less
than 5.5 Ang., the angle between the two normals to
the base planes is inferior to 30 deg., and the angle
between the normal of one base plane and the vector
between the center of the rings from the two
bases is less than 40 deg."

There are two classes defined here:
- ResidueVector
- StackingCalculator

The latter class should be used for calculating stacking. There are two 
public methods inside StackingCalculator class that can be used
for calculating stacking:

    - process_pdbfile(file_name, chain_id='A') - which runs StackingCalculator
    on the RNA from the 'file_name'.
    The second parameter is optional and has to be set, if the chain ID
    of RNA from PDB file is different than 'A'.

"""
# *************************************************************************************************


class ResidueVector:

    def __init__(self, residue, rt_mx, unit_cell, xray_structure):
        """Creates a dictionary of vectors for each atom from a ModernaResidue."""
        self.residue = residue
        self.atoms = {}

        for atom in residue.atoms():
            atom_name = atom.name.strip().upper()
            #if not 'H' in atom_name: # skip hydrogens
            #site_orth_A = self.unit_cell.orthogonalize(rt_mx_ji * self.xray_structure.sites_frac()[A_seq])

            #print unit_cell.orthogonalize(rt_mx * unit_cell.fractionalize(atom.xyz))
            #self.atoms[atom_name] = np.array(atom.xyz)
            self.atoms[atom_name] = np.array(unit_cell.orthogonalize(rt_mx * unit_cell.fractionalize(atom.xyz)))


        self.resn = residue.resname.strip()
        self.normal_set = NORMAL_SUPPORT.get(self.resn.upper())
        self.normal = None
        self.center = None

    # vector placeholder functions
    # code snatched from Scientific.Geometry
    def angle(self, vec_a, vec_b):
        cosa = np.add.reduce(vec_a*vec_b) / \
            np.sqrt(np.add.reduce(vec_a*vec_a) * \
            np.add.reduce(vec_b*vec_b))
        cosa = max(-1., min(1., cosa))
        return np.arccos(cosa) * ARCPI


    def is_valid(self):
        """Checks if all necessary atoms are present."""
        if self.normal_set:
            for name in self.normal_set:
                if not self.atoms.has_key(name):
                    return False
            return True


    def calculate_vectors(self):
        """
        Constructs the normal vectors for nucleotide bases.
        Returns a tuple of vectors, the first pointing
        from O to the center of the six-ring of the according base, 
        and the second being the normal 
        vector according to the definition of Major & Thibault 2006.
        Assumes the residue has a complete set of atoms.
        """
        # sum all six atom vectors up to get center point.
        asum = np.array([0.0, 0.0, 0.0])
        for atomname in self.normal_set:
            asum += self.atoms[atomname]
        self.center = asum / float(len(self.normal_set))#6.0

        # get two pairs of atoms spanning a plane
        # and calculate the normal vector
        atoma = self.atoms[self.normal_set[1]] - self.atoms[self.normal_set[0]]
        atomb = self.atoms[self.normal_set[3]] - self.atoms[self.normal_set[2]]
        self.normal = np.cross(atoma, atomb)
        self.normal = self.normal/np.sqrt(np.add.reduce(self.normal*self.normal))



    def calc_angles(self, rvec):
        """
        Calculates whether the distance and angles between the vectors are OK.
        Returns a tuple of (dist,nn_angle,n1cc_angle,n2cc_angle) or None.
        """
        # calculate the distance between the two ring centers
        ccvec = rvec.center - self.center
        dist = np.sqrt(np.add.reduce(ccvec*ccvec)) # vector length
        # check whether the distance is small enough to allow stacking
        if 0.0 < dist < 5.5:
            # check whether the angles are in the allowed range
            nn_angle = self.angle(self.normal, rvec.normal)
            if (nn_angle < 30 or nn_angle > 150):
                n1cc_angle = self.angle(self.normal, ccvec)
                n2cc_angle = self.angle(rvec.normal, ccvec)
                return (dist, nn_angle, n1cc_angle, n2cc_angle)
        return (None, None, None, None)

    def get_stacking(self, rvec):
        """
        Returns dictionary with one of the types
        (<<, >>, <>, ><) for the two residues.
        Or None, if they are not stacked.
        """
        distance, nn_ang, n1cc_ang, n2cc_ang = self.calc_angles(rvec)
        if distance and (n1cc_ang < 40 or n1cc_ang > 140 \
                          or n2cc_ang < 40 or n2cc_ang > 140):
            # find out whether the normals are opposed or straight 
            # (pointing in the same direction).
            if nn_ang < 30:
                straight = True
            elif nn_ang > 150:
                straight = False
            else:
                return None # invalid normal angle
            # find out whether base2 is on top of base1
            # calculate whether the normal on base1 brings one closer to base2
            n1c2 = rvec.center - self.center - self.normal
            n1c2dist = np.sqrt(np.add.reduce(n1c2*n1c2)) # vector length
            is_up = n1c2dist < distance

            stacktype = STACKINGS[(straight, is_up)]
            return stacktype#self.residue, rvec.residue, stacktype




# *************************************************************************************************
# *************************************************************************************************
# *************************************************************************************************
# *************************************************************************************************


