#! /usr/bin/env libtbx.python
# -*- coding: utf-8 -*-
# =============================================================================
# Created at:
#  International Institute of Molecular and Cell Biology in Warsaw, Poland
# Author:
#  Grzegorz Chojnowski - gchojnowski@genesilico.pl, gchojnowski@gmail.com
# =============================================================================


import sys
import gzip
import io
import base64
import itertools
import math

import numpy as np
from optparse import OptionParser

from progressbar import ProgressBar, Percentage, Bar, ETA
#from scipy.stats import spearmanrpearsonr

#b64/gzip compressed RF01753 rnamasonry model
pdb_string_gz = """\
H4sIAFJ5WFcC/519zZImt67c/j6Fdt51EPzn8saEwytrTjjs938VEwkWAVYX+yu2FkfSHHU2WawCCCCR+O//+/d//4O/6J9//oN/+F/8j/84+VOfvjzFf/7J9JVS/Iec+0qx/zt9OfdP819U/tG//vNf/z3h/D///P0PfYMLX40c4Bqlf/5p7asFN+Bq/crNwP01cIHh/MPqaun/V3ZfvhSsjiJdcO6r0gau7+Bv+h/f4PKXD7xZ/1UyAS64a7OVvmLawPU///MAx6tjuPjlmjy7HMOAK/mLvIH7Y+By/9f4Ha58OcCl/vMCV1K74MKXrxu4fkR/H+Ear6avLuXQ4eirBgMXy2az/df8Cd/h6pejIKtLFSebclY45zar67/yb3haXfIFcKUxXO1Pv15wvr89z6vjl+uP/w7X+gvSAJf7f8LPzrvrvctt++yoY/x9hCPHcPmrdVj5KtIFF7/i7r3rf/6HHlfX5Chq/yr4KGK7NvuP2z+7/gL9S9++2RD6M5PXODl5jZvbwf27fmR//BNc4q8gh/6CFKyO5lfx0+r6f/PvA1x/35LHZnOOApfci9X15/tv+A7nv3xOgAtOnp2P8QUcPrLvcPQVQsBmAzVZXaEXmy1sAp7gkk9i74qXoyjtBRx/ZPlpszUKXCxysjW4F3D8keWnkxXj3u0cDQPFR/MId/vI/i3f4RyOgFfnm3wVLb84Cv7I/tR/Hr6K6AmvsU9OjoKN0qfNsln9tz3BtSInm5y8xr6+eO9CWh0jTQOVq4dFIT7R1k1BJWPc3bNjDHl1jDRdT20eFsV1P8twrlQ1nyE8H0Uoq2PU1cXgsdnCz6N130GkcHnjyUJdHSOpgeLn0Dcb+Df21eXi1Xxu4drqGBWu4iPrMFQBV9R89peyPZ9sdKtjJP1mKQMuZS9H0a7Npm7xN8adrfTfR7ic5WRTa3A9jq5vNtUv7583G/3qGGl+s8TGvK8Odq71W0G4TjbF9UWxqwurY1Q4X/mbLV/EJ9y6z/BF4fLmm2WbaB0jqXHnF6NvtlZ5dmn62ZS/wm51aXWMNI078bPi1bnxVcyT7UeRXjpGXZ1zsjpqGa9xDfncMV5w/aPKYqCIH2+Hi/EXjlHhAi4VvlvjCrjs8rljvODYooi9K+xyGC63c8d4waWvmMXeRf5wGK79wjGak20Fq2swUAU3qWPHSOaOIoFATrK6mOK5Y9TVFb5tdk+WvZxsyL9wjAqX+UdSd0Ekm/XenTtG/Wajk82mcbKRwrljVDjHRr0fReX4q8Pp7fPAMZqPjL9L+IokJ1teOEZ+N61j9HN1uSbYu8JWubHzuuBSWGMy4xj5V1rHqHAJYUoPV2Dv+rOs10eW/Paqzc/ZOkY/jXtLEldE3kCHa/MW0M2n23gy9nbWMfr5ouC22TcrL0r/2OhaXexRQt3AhdUx+vnNEluSvlmxKHxXuVYX+zu9iRg5LLSO0U8Dhdc4j7sJw+XLbUd2vhu4tDpGhZOP7Lqj9K9k3o1TD4F2m82rY/TT3rlSxDHWgqMI87oYOSrdrK6sjlHhcAS8WbZzrd/vpgmI/d3e3KD4OVvHeMF1R5/E9cjJ9hclmqPwu2fXVsd4wbWvAOOe+6o8jsK1rM/Ou3eOUTdb+Ec42ubT6kcRgj93jLrZWCUQcPAV8Yeo5wfHaDaLKw/N1VFO545xwEU3LIqTTTOc+4Vj1NWJvevvG+dPOpwnf+4YzVG4ING2l836V5u9OUbzkbEHY1+R5X7nSz53jOYj47twf3bUBpzP545RVxfYbHbHiPzJj3A/OEZv7igOz474IobXOJ87RoUrNK48QQxUyP7cMZpnB1/Rjcf4KoL/7BiJ3wZxjP8PcGHCkRfHGNmM1tbvedctIHZfnh4dI3It4hjvcG5ctXPqP1rrV52phbh1jMQeThzjCpeRVYS943teX12N01f0SD5s4PLlGFc4zteJgUKkWNkkFIXLcQNXLse4wvVn5cVXeA6eOlyel4oOV8vjyRKnvcQxLnD9JyRDlvprLEcR53sX+/+GDVy7HOM3uEDIQXm+iLXuC+eL0v1sec5qE5KV4QkuxywpLTZQ/WSpmtWlzeqQrHyAG3kT5I15s/3FCUGfXXGb1fnLMd7hAm7uQTIUfXVpJo26n3W0WV24HOMK58dR9LCfV9nh9G7c4ah9coz31blCI0xpOAoXw4ljvL93jXPDHc4h8dE99asgajrG+8lSE0+W+RkyXC7vHOPfBzgaCbcOM1bn0oswRR3jfbM49G7cKxsofo3dUcR4N1CVJCbznIJptl7xwTH+fYDr9zovrgffaofDTf7jZuvlZ++rywzT/WzhV5ZXl+s7xyh+9m7v8njvkh/PLn4OQCmENYiKagLG/S5wDqqmH1ILxleEuAZRcb4ovsnNPfD1uUaJzT48OwppDaKivihJMmSRk0Z9dXV7IbNweQ2idLPIE3cT0Pg31mRynz/BlTWIitOiBH5WnOjlS0XlDFl+cRR1DaIMXJOTJRK4kN7AtTWI0qNoWd4772WzkV48u+jWIErhYpOUVsFmu8Wsn2+fqJL+DU+bdUj09s3yx1ZJou6Pq/NrEKWrC2EYKP5P+upCbC9WF9YgSleH9y1xbbH/aO2Xpm2B4YcgyqzOiYHCJYtflPKLICpOi+L5TpL6PQ+r699sa+dBVDQ3qP7PKctFrMNR/EXZTeEyXxA4Ccu3TjYBqZ4HUWoCIn8wiQPRBLhWy3kQpXAIh1PtXwfJV/Eq0XsLovQoGjbbb51s1GuWPPJpEBWn66l8on11EV9F7idcz4OoqGEKn2iHy7yqDpe3+bsfgiizWdQrODz2gAv+F0GUMe4jSe5qxVG0N5nZexClcG3UGF0WTxbd52ib+LsWx/jfgEt6v6tyISuVX2P3g+sxjjGnyzHe4Rp/BVzu5a+i9Dgjv7B3OV+OcYXrP1ElAM3sipkz0uILuHI5xjtc8JJHSfw19M1SCS/g6uUYvz27IEeRkmw2hs9fBfGxiWO8r474AsYf2Xh2rr0w7kj0fofjEI9/nL8KfLNOqnqfNsu3LnGMK1wPh9ld99VltiyFzekLt41E7yNcI7l9Nr53lB5EZXqxunA5xhWuf7OcGWOLwledvrrqyovVxcsx3lfnq1jjwH/vq2uJThzjHQ6Xir46YnddnYTJ7x3jChf7V0FYXS3DbdNREHWHE0/W/Sy/IP0jq+mtYwzf4bo1zuK2fZJv1vuX2UXxZCtc7j6i4tmBSNLhcn2TDsyXJ7vDOX5W/b1D+a0/u/w2iBJPdn92iHLYz/Ilq1/IfPLvHOO/j3A+yFdRkFrgVOqbZ9cuT3bfrAQC3bLA9fx0v/v3Rvqq/zx9ZBweczzbBM75F0VBfrnEk93hMpvN/uwKgiiSVP4nT8bHZkO8PD8yx+4aSfLGVZJuRv1nT8Y7sSGewqURCAR2dqUHUW9u7mxybYiXNXhPwiFDPNEtyv7mrnBICtsQL5tK1GAacaGBCTI5vYCjNcRTuJhGGpq9U9+sb5/Np+eLhw3x8jQBSJKz6+ErdoeLPryAC2uIl+dXIb6im09+/0oGnfTzZuMa4uVZn21ONpuqrM6/CKI8Er2PcJkXzl8Fm4AS+tfhXqwuryFenvXZy23XLJt1zr9YXVlDPIXzSTwZfEQZ/OPTEE83m9hnMm2Hr9yFY7NfhHh5Mtxks4PcWkcQfxriKVwiWV0ucpn1ROchXp60KricDufSgGt0HuJlpVXFMKKeiPudcmYPQjw92dgkJiPObjNcLechnsLVLIUtVPN4symch3i62YbKO986K072XUH1FuLp6pA/QRCV4Hp8aechnloU8rLZxK6nMJfxFyGewsUieRSwUfn2mX8R4pmPLAhbhnnuDAcj/2F13t8cY1F6C0hfJIzy0q+PWpy51Sv+Y+BujrHoyXpJuKHgUrwkjwDHOZvnShTugNYx6upw+0SGTFZXZxAVmGv1DBdujrGoCYgjeIef7ZfbCYdc4Qbu5hjLNAHFyckWrqZ0OIqm1rMQNY01DjfHWCapP0a5fSK73eFaMbUeqhu4m2NUOLlqMzHGy5XHmaKgr5vN3hxjmSTXcmUq3OUYk64u+c3qbo6xTApuS3Ihi+NkaRZnYvflabO6m2M0cCS9KZl9RcmG4RbjCmdXd3OMZTaTYLMIQOXZuRlXxLIrqH5zjPrsahFrXJglWIqUkE4do4EjSct4/sgqSUBw6hj12VV/8dwlDe3qLwgk+uzyeFEQHvPqYjh3jOZkq1jjWoc1duHcMepX4Qfjw9UGOKzy1DGqCcCdmCsCTfxse9UjcHOMxbS6SCCAuLY/O2p07hgVrpEQ05Hw5eDd/8IxqvkMZZQ/8shB1V+0HJjV5UFc4gpJ92Qu/6LlQE8WqSx22xyeFC7kf76Q+UwrgaTO966MYnTDixLEwkz+3TOBBEGqJZAoHJHQW5BEY8eoxn3bi+dzWAkkVRvA8M32+AJX7WjuxkzU3MHFlUBSzTcrmdnIq+R4dsLF9hU3YUpOK4HkgouSpGTOLH8NDDfzKDGttwBjjXNeCSR19jFik8xawHuXDS+gG/e8gysrgcSsLpZB/XawxnFGjH11GxopyuqWQKKrS0xqzVGy2X11KSQ92biJyXJbCSQXnJeSERejizy7og0RtHXbxa0EEl0dkpXodpPbp5uup3uyDb3FF1oJJBcclyib9DFW+WaDHgWnad8RSPQoyHtN4RdLbzkgkOhrHEe3G27wbKAonRNIDFyTllJwKTjqCXROINHNJidMozYKDM2HcwKJMVAxaLtGZRZDPSeQVO1Q9VdLqTy75ss5gUQNVKsXayFKgaHmcwKJrg5pQGZpZTHuOYdzAok+O2QqQILwgHsTz/qWVl/R5jebSfrJwIPilFYwPPcYnn1Fy6uvaPMjC4i2+zWxiHHPLWibUH5uFkZSzvqKpm3gXkj9AQm3aG4BccvC962uvkI3iwxFh0ucyur2Ds1Mk6u9g2urr9DNooTGLPwstZ40O7Zid77PcUVwbvUVBg4EkioFfI625w2Kiel1A0err9DN0jiKiM32G/wkfUW/s8bB+dVX6Ookb9wkri1c6CqGhb9bXVh9xQXHnDGuQDkhubL5zOZS0fxmdXH1FbpZx7SWvrpyfbMzb9xXF9NmdWn1FbpZIbm2Uf4YBLprdT6+8xW6usKeK48wGfaOzn1FU9eDo6hCH+XN+nLuK9r0Fc7JybpRjA70C1+hq5NLBbflShXvnfm8+Yo27V1IAueaRIzuLIi6wzX+Rvuzy1WSldX7c1/Rpq8grhZ3uIJMRZOe0FNfoatzTl4USqPcm+jcV+hRwPp2ONdkdeQ/5+/AChFf8Yfhrs5TP7LapcdiTWo9tSS934XnhBveTfEVCxw1oY1yPMF1C77MTl5AyjtiepD8nf+2Oi7vBqwOjRFs3IOJK9wOrl2+4g6H62EZXZZ8IcumY8ttDBQ/hD8PcE7aIEvQNPT8yJjSsDkK5O/i0+pAm+9wxY0aYynqKypt4PzlK1a4fgS4YnPFnaQYXUxPVNv4Cs7fia+4w9Hw/o3fO74uOpOs3G42Xr7ivlk/3DWNzTpn83dus7p0+YoVLsiNiUO7KPaulWhyn2Wzunz5ivtmCzwZFwOzlMq9gUsfien31eVxsjCjXIxORxofK1wc2Z0evCfxZN4dxRUrXBaKBuu+jMJWju7EV9zhPMesnLovUnkvLp34ihWOe92rfGRV4or6pvyhvuK+uobkuB+J3iZ5lTe+4t8HONYHkMIC9HnYovg3ZbfpK1a41APPIHDDuPuzuOL+oqQm2Z1YxUC5FycbUln4d0Tzq0DWge/EIEH0O8qluEQcu+RnX5Hqwr+bcCQ/3h0iOmg6XLpyAYTUz7MJSG3h35nV5avMm+Sq3a5aD8P5DVx2C/9uwgXpJodDFANFV3hM3MZVNnC08O8WOLkTo6bdv4p01RgJV9Lno8h+4d9NuG6Fr3gCJqAb+SvxQex82wYuLPw7InNHybLZJKublDRiBZKNY8xx4d9NuAQBHQTtWVxPo3rBxV3pCLf7v+F5dcNHkBRU01VQJSZYbHxFzgv/zsCBuMQfVxSyYQtVN9s2viKXhX9HKrciVLTB/+RbwMU0YrhdG/iNf2dWB/oevyj1enbn/DtSQRNKQgkqbXyzvh7z7yacE+4iX8SKrM61cMy/IyMGg291KEBAZ+2cfzfh+MYudKrqxsm+agBb+XcG7uKhgMzPFNyYjvl35tmJj+BGfzfClHTMvzNwfpR53ahEuRSP+XfmNU5eblCBPzJe3RvhsBv/zrzGcsXu97sm8Wx8w8K/8e/M6uJIjqOTiK0xfb4FoHnH0Awu1b2+OmTEOI4dRM1yEYQJWZBnT9ZWZUMDh+RZvyZGfv+YqHnloBhud5ltq7LhhOO+2Syr8/LexTxXV7ZZnrYqG064IjIXHQ49sZWU4UZ8/2kbuFXZcML1x9PaoLXIKuN1qSC2B5sMWVuVDS+4q4rHdm50HdVL/454yXkDtyobGrjoJTxJ+DuZSwXD7Ta7KhsauJov6q3cUXKccGEtu9nVrcqGpAokeKz9J9G/yAbqihippt2LAgEZQzMwcNI0R5ITYAM1XU8NO8kQfIh/H+DYWUk6pjr5Zks1cMm/ohnYzcbhcoJY41jjMc3AwEHDqLCDlMgxt3BMMyAjVUNiUeBnSzGCJu9pBubZlSLltuzlFooS0iHNwKwujIIWjerxXnFpTzMwzw76Y2WQcLghorZjmoH5KqjIqmKTa2NK6ZhmsHyzchTIfXLJMoVjmsE0UK1b3zACASlZ5hiOaQYGziVxiGBUllH7OaQZGDgIh/F1kXOefIP37ZhmYJ5djpLKKkFWGeiz245+LR1R0I+MrQa39A0CSW7TBPR3+7l0hCSJKR1NOLbfEW1p/mKlXnljhvPPQVT0a+lowvElQtrS5EiqSGJecBsDBe6KKR2Z1ZUivZ+oQaARZ7oebm7cwK2lownXL2JZGl3buJCFOn1F3UkR4iJiSkcGrsIah0ENqkLcFLiOnTZwa+nIwEHpBqSbUWNUT9a+0nP5A0qSpnQ04S71jCBCidwTVbLC1Y3rCWvpiLTBP5C8d3HATZmffn/8CrRZ3Vo6MquD1+cbFInPCBdrAasrm9WtpSOzuisHUEd8Ea9evG9w+9KROYrsxW37YT6dq8elowkX5D6HFKqQDamcl44MXPIjeG9iPv2rrqO1dGQ2mwblG9lWjn5cOS4dmW82DMOEymeJUHQ9LR0Ze3exFWASGa7F49KRMVAsVzsvYtxfkc9pBnZ1Vb7ZWCSITzUfl47MswMvh6OdIH72Dc8dEbktHWlnNPjG3RqHeKUYqvlmn+XhIfpkS0faygz6XmXtQPlmfTRwmzAFXClbOtJGa2SxuXW+yotSruoxw6WN68l+LR1pCz168Woc18Yk18YLLm8MVA5r6Ujh0oCrYHwE6UUWuK0IUcxxLR1po7VHPBFGIZ8DUadwu0Agp7V0pKsLVbKKMUmbUIpmdRs9KASptnSkq0N0zavjF4Sz29P1sFTfbnVlLR2pdAMkMNkx4spDJtHbjyLuXpS6lo6MOMLorkQAWri1L+rq0sZX5LaWjkwLfZNnh+Rt4d538+zCxyamb0cxqncVVx4meaTz0pF+s230zULLjY+i0nnpyJiAEdoRYjN6pY/yrXSkJqAEKcpkVI+dTIo4LR2pCRCzed2NOQpq56WjqJ0zXi6xYZTMc4rnpaM4/aynMBjkFXDxjf7dvXSkq0OHIOcCnJxsdL8oHemzuyoBwr/zHf4z/y62Vf+OtMEfl3poGXnh9LR5v0tr+cP4irbq35HKD4D0xRFJFopQuhgf3+CMCWir/h2ZZuEqvkLopP1Kd5Grf4Rb9e/MZqHpUccLw0SS5F7Arfp3ZrMtjaRRFLiY0gbOHsWqf2fgapWTLYPplmbU8xPcqn9n4PzI2+FrYJpLLh83C2lbG1cYFY0iNR4cySX692F1ya36d3azcTjGKIqufpYsf1rdqn9nTrZ4+cjSEIitWzi7ulX/zqyuYLNOqnfMnU314+q+xRW6ukRuwBUhbNK5/h2pEoQbmQoQllhs0ofzuEI/ssCXCNzcRbvS+3NKmoGr3o0KqJBdU6rncYXRWvBXXOFls9TO4woVlghVnl3xwv8M+Zy+TKq1gN5/Tj87+cj2fOMf4grdLCqgyAUIca5Efx5X6FGk0R4kbblVLhcf4JKPa3Emq7YMP3q+ajtR1pzKN7h9tkdfAQFeW5zJ87pIQ8sIZCRo4pcNnDEBPq/FmTw/MrBSGS4LG79qXPEDXFmLM9k4RtFuS1U6aCi9WV1dizP67PwokVcnWm4+uw2cPYq2Fmd0dfhmUREoIkbk42e44NbijOpUXEXoOlSY45tnF2gtzigc+Tqu2qI0p/m7n1bn1+KMwknDoRcHyY3+vr1YXViLM6qi4aNE21B65F7Q+ubZxbU4o3BpWJQwunzTJSL+w+q+FWcMXCijLivdR8658+KMtoFTcNpNzmPPwrn+nYGrSQoMIF9y11s7178zcMI0KuL9+b2jcl6cMZIhrmj6mXU/83kPqIGD+AsHArGMvrJfFGe0wb+Q+IoUR48K1fPijD67OKhoKPty91E5178jIxlyKXy1oRHdzqdrmGcnBE2e23N1vZ2LI5DRlil1FFTlKFoN58UZhUskEWOuokrq4ufVoZZmHWOZ3+xVnHFDdqU5p7SqpV5hHGNax05NuDpUSKOkVBPT+nZwxt6ldeyUWZ1YlDHHgpvVKb+AW8dOTTjOYhtOBXRSygu4dezUhEvyrLiQz5EiNps2cMYa53Xs1ITjLqpBgoDsSoe7mjZ/hFvHThk4Gr4CWS+WmUrt82bzOnZqwvEVRxS+Al8q0uiN+ri6deyUgYMOVIdD+SNlk/v8aXXr2ClSFQ3vBjHYixQmKVvmh9WtY6dINT7iRSApcrI+pI+r++YYdXV1cCpQ5sW0g1+wFrRJHcxxvt/l0XDdyrljNBofWa6LjobbpvOxU2Q1Pkii7SAyBNo8d+AYjaDJCN5Rp0ArfTt3jHqy0FjgF2WIc4RXbOibY9STRTWTyfxYnZcG2FPHqKsTMV2eEDEkp8MvHKPZbBZ5bpQ9eEQmnYsjmG+2+iHyd80p+8V0DWOgrrRMiTIS+NWgk7tj1KPAaDi+ueNS4TBh+KNjrG0lplc1UJdyMFwPB/NFC/l+EzE2txLTqzEBQ3id37/ERUG/gTP2rtFKTK/qK5wkK10WUed3cH4lplclfQ2pX5R7k2l4/REurMR0hYN6Rv9J0MxYWTPunp2xxi2uxPSqdD4Y99JjMRHqdDND9hNcWonpVQnC2OzoI0vpq0xewE+bzSsxXeFQhOYxGMMxtvxms2Ulphs4jrLBFcniK8Kb1dWVmG5UNMaz8ykNTxZfrK6txHRtUheyYZUSeRoc2g+r+0ZMr8ZXyLMjyNcmYfieEtNVzUBK5VHU0lJ4OQr4RkxXOHR9cFoG3+wQ/zslpmtHPtKBrPaNjywqW+aEmK5HAdV9Xh1/h2xRXmmS34jp5mRrNIFAlHmgp8R03axQcMPQdbeD7A6I6QoXi7ht8NyTl47BU2K6vndI3aN6LJtNr0K8GzFdv1kwi1gDP8prTG9elDsxXVcnzLYkhgpwn18UDPiwdbJmDJSI1tchDZdm0oh5kfXRk6Hz3NbJDFwaBmoMZStK/c67qX2IkGydrBmiprzGYC1kJqb7F3BhrZM1JaYj6sljGKCTdrWPcHGtk2mjNQabMIFkSHQpH+UO98fApbVOpi30mKwJDfww1CHzC7i81skUrlyXiuRH8F5ebLasdTKFC+O6iItCdqoK+ePq6lonM13lJNY4D4l9E5P9sLq21skMnJdvtg4F69ra59Uhb/wAF0YqlScMD1Hn2D6u7ludTJ8d1EQ48RFF6jyHc5kf894hQwaGvMyx8K+CqFudzIgjgN7C04TkBvUu6rnVycxmBxUSs6MxUuR8TpT5ZiViJJnBwPe7fD4nilRrQThkzOEZeZTf8O/U3uUwWufHhJK92/6hTmZW1+Q1dmNoR4rtvE6mzw6XV44Yi7wo9QWzEmV1E/V47e7Ng28cx1A2P0tHkDh49hVpHYfhtUkdwzpAM5AANGpP1A3OmIC0jsPwpqt8hMei4hKUj/Ij3DoOw6xO0jJe+FAcvGuy8ge4dRyG1wb/ONpwwXRDJWq3OnsU6zgMb3QqytCEpjomW7fPcHkdh2FX543KPFxP+bzZvI7DMHBI9HJL3xDqnH2MP65uHYdhNguSV7nG2gYdUfjj6tZxGGZ1YFZy210tQ1nzxVHkdRyGN6Ico8UFryaX3bab3Uc99iguJc0kIV5p5Tjq8drgj0AckquSSi0hHEc9xgRcHfmQIGTj/parbaIe85GlS/qSL1mpaM79IOoxqwuj5700MZ/x1WD2Neoxzy43P8puopweYz6OeoyBCkMnAClUdj3+POrxqlORr/ErWYL36M7bcc3qMGbv4rnzoJ03QdQt6jHPzo2ym3fXKJt6HPUsvkJamCFez3MZX1CDkE0zTHKvShAiVFcHOzBqGzg3vLrndlw8b8Mkn3BO2jW4UytIMXoGoN/gjIGqbWGSexWWSFcvlJNZqmFmyH6Aa25hkntVgiAn3W5Ssgz6zf4IRwuT3Kyu4L3LQ/TPzCr/BmfMZ/MLk9yrsATcNUtMtyHqXOoLuLAwyQ1cGSoa0IZmx+jfbDYuTHKvwhJIeHAjBI0rD73ZbFqY5GZ1oO+xJEUYs6Ore7G6vDDJvfa84zLBjRCDfDPH2f64urIwyQ1c4ksF17BIRJ0jfX7v7kxyA4cZluWabE0y2fqQSW7gMEOVO1RHgWGfcNszyb02+LeWRpfbuJC1dMwk90YJYuiPwV1js+dMcgMXq5gAhHoYWHwuQmQ2m8s1W0sYbu6tJ/s3Pq1OevFI6rQQiC3HTHJzsvjk+cpDAkeRjpnk5psFKwvSl/KRhRecCtAmTYbMqziCyJoFYX7YzXKTenkWwi5+FcKecFH029EIIa6nth3cXwO3CmGb1WFKaYkil5yTysP/CLcKYXuVbkDgWYLc4HPU8Y4/wq1C2AYOpJviRVySX2NtKb3B2aNYhbC9KkGA71mGAsTien6CW4WwvXZGU5LmJVwXYQL8i82uQtgGDsM5uGOGhAflanuxulUI23uj3lLQk1LHvPfgXqwurELYXjvy0S9b+JmJRUktfF5dWIWwDVz1sln0M2YWw2ofV3fPkJnNxjRGiCQ5iv2cqH2GzMBBU4y1ZbxEjNWdd6iazeKm3uFabkNf+zxD5q0ShLhtpEdZ/budZ8j8TeOD/ezIo7Qzwbr7s/NTHEFaDpJPxxmyBe6SbijSX1HOO1TNRwbPxSpcmMWQvijE4wyZNQFXK6n34yP77BjLjTDntR23QkVjCHNyu8YU0uFa3MZX3AhzPphvVjR6MdONuYszhV+5If7ZBNwIc2Z1Igs/MhS5SMZM4OKu4bXcCHPedEbjRXEioMOuZ/ZEceZ8B7cS5ry2gYuvGKotLMQegwrpbKYJlRthzpued+hpk9xVWIjdZYVzbQO3EuYMHGqM3d6haADV+agiRJsuy3IjzHnVCxBrTGOGLw99brq6zYSIciPMGThoerCun5NmEpqUNDByNqtbCXMWbjRroqWU4cgoLtXds1sJcxNuNN6wY4TqfBay6wVXPsYV99UR8ndhrC6LhTkkzJmTxahGHjqJMIVbm8+nCZnVXUMnWxidM/Vc5serEgR6sKEW5AHXYjsmzJnVXY39NUkvXnP+mDBnn10RP4vJVB2uxHPCnDEBbrjtVGWzvpxPUjcGCj3u7CvG10HuXObHWOM2FPqpSUtpfjFp806YM6sTdb4oL0h/djXXY8KcgZPERxgtVvzNnjPJvdWWaXIhc9IW+YZTUWpegygzhd5L3yyGwLLqfDUGakMzKLWsQZS2gUeSTdYqyumxmUBgo+Raal2DKG1lhmoLf/pRXpQZHjNc3Rj32tYgSleHmmJh+UZ5UZJaY78bs1eaW4MoowSBasq4mOUmnJ7pyZ6lG1BItEGUUYIo0pweovhb7VCFBPgGzq9BlFldlLgijwEAZRa2WCFmt9mwBlEKh3wxrw4JX5LmkgtuM8iutLgGUSqOIN3kTqrIHDlOUj93hfnN6tIaRBlxhFiHnrtsmlTmh8nHm9XlNYjS1UG2kUVbcxr+1sLRuyBK1Qwg4FSuDhruRc7nQZT5yK6ZvVmmbLQzx3iHo+xUj6eMcXunQZRRbxnyobXK67zf7A9BlJoAlCohwE6Ae0X6ugdRqrUgw7CYlyKvMdXzCRETjj+uMUiMZHXB1fMgSjcLXQq+QQ0TkF/pzN6CKCN7AbcdhE5a3CvWQqVV9dtrG7gUtpyMjGO9WUr6zW58BY7P0gxMk3oZsz/GpM2kl1kOiR6fHaaFWZqB9h7DQPFVu4iA0xyHAXHT56s2WsctzUBXR7wC/hqi+NtSzN3YtQ3cqvrtjU5FkfkVmF5VouHfcbPPMw8KTEJLM1A4yVQEcdt8MfNuc3O3cKvqt9lsi1JgwMAhnsSUjOvZ+NnqV9VvC1ckfyeK/VlnHRGmT2xWt6p+m83iN7GPaHLPqxrP0o6+XP2q+m1WhwIfm4ARX3hnVtd2z25V/TarK+2aPi9VleLNpSJ+7Dq6rw4y+WyNxxyLSufkaq/yAy7JJjHQCVpa8ZxmoHoBwcdR6xkzBWM9pxkYJYgqq0OJnOX0Xs2vuNEMFE4GduYhqNOfXfHnNAN9drjnMoy7ao10TjNQ6QbwPhH1XAMU/TnNIGlrX6YxUXg4yN/QDJLJVIhjFOnVoft5SjMwR3FZYWilMu0gnNMMjMZHTUMx3X/Qv9PVVX43bTrQiCNMpaVreEI0t8/nWUegmtp0oApLyKc/5lYwH8oZX7HzZMmt6UCVHxDZ0CGOAIFOr6srYQNHazpQpRvKGHSCoTkYT0AbmW4L59d0oD47dLuV0RvAjj+bhJt7DgQwnMKmA83q3CXfOEqXGpPRTisVx/X3Ec45eUFoiCS4KXvBHL+42Wxa04F6FFAewTAIIR06u7oSN6vLazrQrC5LhqwMlb6Q7C0gbVZX1nSgkW4YhskNlmBzRjHd7VZX13SgwuVBIKE42nLt6nJ4lw40cFk+fSEKc6jnz9OBqhfQxtAE5+tQrj7vn/VWzUDg2pjXE157Mv+0OrBzkV0sQ2munqcDjXTD8P6Qh2e4V+p8t3SgEUdIktJCqwErWJdzYQkDJ4mPIIUFPgrvz9OBehQi0x1EdI3vKqmdpwONAsmQXJWBJ3y/OxeW8CrdIBLTYehCVfk6TtOB+lVgmPjMBVRpMT1NB+rqEJbwZXaofr/pn62lrY5RlSCkdOTHjK0g1hlwTGl45t8hu2EdYzGqQSMHgN6oKE3DAhd2xRmU6qxjVGEJtBpwwmNQwNscYcP8jU0gUP3qGBXOVUka1eJGF8hcXd7Jh4KT/OcRzrfxGoMCnpSShumKz+lAKAxZx2h0KspwjE1kptLkGzOFy2/g0uoYFU4kppOUyFmScCrfMDOnbjabV8eobeCYFV2CNDFxe+Q0UKxWvAmialkdo66O8pX4iGhTSzPxwSmgTfBe6+oYtec9jywP7sbokL5un1wL3gSgta2O0cA5scbg9HS4NjfLJKn8zjGajvwqR+HQgxxFqPPUMaoSBLjZfBSj29KVeu4YFQ4NhwiPh+p88+eOUYUlJOpJQpSDigudO0YVR0DOCRkK6VTNuZw7RpUfcHRVUwTuHcPt5hh1dW0QlzA7mhtfw7niklcVjeIkOY6RrCw26X9RJ1M4XMA4Oe78GFR8Pg7DvMYSRHlJpaL+7M8do8KJynyYbWolngtLGDhRmY9yC+Ba1gsdskZhdYwq3VBGghdzemqVBkSxKNzU/egY0UdlHWNVa1ykmoIKSW06RwCFoGdrDBamdYwKJ6/xNaSIhyZENZ+bZCVUmaxjNCoaXsoeuPpU1ofKZuhp2sCV1TGqikYa3EWHJjpO+DadVb7pAcWgQOsYVafCjTmghKinag/oPzHvJM4btdUxqk4FmoXZfDoRmEizPhvbLn/XvFsdo5G9oDxO1mF1bbaUxrb6CrM6T6tjVDjpESChkTYnfT7X3OPNlQfXcusYtecdakHXWFE+WZ3dW3b0lubD6hi1553G0FMUDXizV6M1w1F85xh1dYnEojS8KOOufOoYVRwh05hCXyXadukXEaOqGbQsSSMMaK+jQeLUMapORR5a5NLSPCaBnTpGs9nxolQnt08Xf+EY9WTR+wlpc7kbt/ILAomebK5S0AJRk59dcOeO0bzGTjJk+NhQu4/njlHhcCcBceTK8tRzx2gMVJTSkQjERhmkeOoYDVwT11Oy6AZE94uI0QjpjPkBVIaEQ/kstwJBSusYVQkCwyah4x5goCYVEtIGG8cY6+oYtW9b1L6bTD5s3owV5ca856Nosa2Osc1nJ/08TWh9PKJnqlVBzekZLrnVMWobOJpXmemBIGpIYV7WeKMwhzvhnwc4bqEX8g2+2Wbrsx1uM1OwJb86RtULwEQIZssgYmyqNs+uJ29cTwqrYzTyA/XSwBdfEdS4110qtaW4OkazuouK1uTZaaYiua1jTGl1jM30gNJgVDZ572abUKIdlwdECesYjYoGJPWLMD94s3rlaTspQljsvw9wI1/MPCiouFhhieRfjI2/r66S8I1xxeY7isvnjlGPAhVQJrfSGHgS0rlj1NVh2gMfxYgYU07njlElQ6BVmbP0M0KF+ReO0ayO5CguT1ZKOneM5tnFwQps4meDq+eOUU0AOlSxOpLVvcp93hyjEdIZxGCXh5BT/kUqVe0dxDj4KMoYLu5/kUpVOGRschU1SFbACe7cMSocbk7MWYRM/JgBcuoY9WRBp8JUDUkapRel8lbq0socjOxFLqM3wMNAaUG1u576rNHbSltamYP2bYcxGQIyU81rxxb72fBcnGnVLa3MQZvUUxjsexK3XWfSiGVdd3C0tDLb1eXRg4eRwE1U5y9fkTeOsfqllTmYrvIkXR8FBqqZDtW41b+DQrNpZTZwCOm48WtEPXGmFiJzDjZwcWllDqpmgAIqdwhGUekLya5uEx7XtLQy280OFdLL9UzdRaxu43pqXlqZDRxUb7nBuorrqVPvk/3s5spTy9LKbOBQieLVufGR6eriji3Tal1amYOqaMilwsvshZrNFPrIx/6qldkcBWo7fLJOvllP8biV2bzGeXRXgs5Xk9BJD1uZDVwb3eRhRIyvHePfRzjnRw9ekXi2ER23MhsTgKwO2nBFQIzi+VCsCcfizTJVAy6IB9rVetzKPOE489IEzksQFcN5K7PZLLL7+cpqZ6UZHLQyG+MOgbocRRGCfcXnbrd+O1oHKAaVH5C+CjeqKc1YlDvcfxSO1gGKgcw3K2MwQAFvY6blzy8K4ZW3vkLbwEHQZGW5YY1TCy/g1gGKZrPQCmQBHcyLqj+kZSzcOkDRbBZj9lgXBbcAI17/01HQOkDRwOE+Bym4YVE+T2UmFCX+PsJhDAEP6RizZ3x48+zWAYrm2UESDpHweI3ji/eO1gGKZnWV3yF2+EWs8X6QnV3dOkDRrA6ia6x4k8aIzM9DAAmp67/+aXXoXGBppCYCYu1NWubuK8xHxhlZTkoEsSi+hXNfoWoGUK7mk00jy5N+4Sv0I8OkOX6NvRj3d6u7+QqFwzhpfo39UEtL7txXqLAEaKSst1jHPIFA577CqGh4UfvOQdKBobhzX0GaXYyiMk/zKNq5r1BRjsTPCvpjI+FW4rmvMGIw6IlyI2mU3ojqEnpsDQs/qE4F0oBpFPQhiE2ffUWMCwt/wgXR9kiD+s23z5o/m4CYFhZ+UNmLMiwK2r+5wFDaC7i8sPANXIY17t8uP0++LpYX5jOWhYVvnh04FSkINY2PIvkXR1EXFr5ZHcj8aeje8XtX35xsW1j4E27okOEoxASkLX3ZbDa5hYUf/Hq/43RTCeNC9sJXJFpY+AYOlKDEY3pl2LOvL44i+YWFH1QyBF1Hyc85FrnVF6sLCwvfrA71wsRqpFIRKI5OfMUdDhky1tMe8rWU/TELP6iwBJo22ZMlsca5nIv9BSN7gWeXxoAnL+HKIQs/GI0PEosi4bGXGYOHLPyg0g1QMkxpynW/G++4svDN6tDHk64h4xwut2MWvnl2Yajy+XqNonbHLHwDhxaDNOaTsQnIdMzCN89OvgpmGIkna2+qeDcWvvnIMDcgDfF6zi6+0Te+sfANHPjGSbk8uX18jQlaFjbqCfrNMq0qOamqcBD1JuopcY16FA4p+8ikQ8lUNP8irihpjXrM6thzsUUZWZ70uaucUGayUU+YJ4sW0uSG3mz6YaCYhStr1KNwcIxMUBiTwNrnaeAEYpiNeoygCV+1Y5PRNRhY/MKTlbZGPQqHMSyJWynGZJzgPm+2ujXq0RZ6BOtYneTc42c1A0KS7u8jXORTilUyZVwBbW9W59eoR8URoLbMubWhTZ7bi6OoYY16gmEHenmNB3GplHIe9djNJnHbVYoztdF51GPgEICOwU7gLtbzqEc3ixx7CjJ5jjNklc6jHhVHAG00BeF7sq8ov8iQmdW5MfZnBAL7XMAPUY95dhjYGcWyoPL+iwyZPYoixn1QcOOr6Rq3qEe/Wai18JVn3I0b1fOox8BVuX1iGBs/u/zZoqAD3dAMgvZtIwEUeTKOZHnCtij4HwO30gyC9m0jeI/scoQwl19EPZgXYmgGBg4hXl8dmkkQ9byAo5VmELQNHI4w1qH+XUQG4yPcSjMIpg2cTzQWUSPFi/L5qo32Z0MzCNG0HLSxOoF7MeSZIHry9xEuDXuHVgPOubs3z26lGQSVH0DFM9aRNGJ9nvRidSvNIKiwBLLa8VLsr2/mCHS4lWZg4JAk6pttfM/j4UufiUuEdvu/D3BNCMF8Cxhp6OzCia/49uyKuG2o73PuM9AxzcDAJUxgSkJzqVycpmOagXl2UG9JQ5eHEx90LuBknh0ImnyZzcOiuPOOLbM6KN/wyCQveePWzvl35tmBiA5PJpulek5MNx8Zrn6c+Kiy2ULnEw8NHIb+pdEIxkmjdD7x0BqoKC9KGfXZnM4nHhrzCTpL8qIzy5v9Bc3ArC4gq838u/HNunNiunl2OY+4ItEY8PTxRSG8DdYxqigH+nliHYPF+I5s+MZ14xijXx2jwiW+mHbHiJt7c8K0vIhLcWPvYlgdY1rb05gCMApbKmsWduRqQk3DOkYVR8AVOzbJUDDcJAjz1SBs4NLqGK2KRsLqpD2t6SQmWPz6bI1jXh1jmm3goBmkQYnkZzfLvRy7pA1cWR2jwlGQhJsbmyVvNlt3R1FXx6hwkDTvqwPDqJHMe5/swLJZXVsd4wXnZRoub5ajneaFZnXBPbccECb7WceocMiy9lcMrQeNtG+bn0Bsz6tLtDrGCy6I8k3iDgapgE4xZ/5D/24UsH12JHABJqC9TBrdHGNa2yI5yzO4i8mfd2zZ1xg5KJaJJyn3hvNRwPY1zjLzI43XONVfOEaFc0Xm4QUvPChP563MZrNuhCmY08MfWY3njtFqGklMVvjPOtw7T3ZzjArnkyTcQP5qTsbanjpGlb2gUU1Jg0MW2y8co8KJYxwjMdnevVHqvztG3Sw6BRNLTAtcaefKhuZkweTlagrJR/bKMZa4ZhdVlAMUtOTGe8fkomuzPK+wPTvGktbsYp4GCtRHtuO8qhbNDYqHCqRne1fyml1UFQ3wjZMTNcgWpQH2MlC7uKKUNbuoq0PknEiE2DucJwMX6waurtnFrOYTq+NRcRVwYWYXU9hIERLGP9jsosLBgqQgV+5my70prFxtA1fdml3UzWKcI9fJ+Ef7yc55jMgklefNVlqzi7o6uWqP5FHjRuumnqxtYrLq1+ziBTdU0bA6fnbZEtPTRtmww4U1u6irQ1o0JRFFaCykY+5322cX1+yirg5izikLRaMfRVViOr8977KLehRxVqIcjiK3dp5d1K8CrfP8mKJ8s+UXo0QMnLzGRW5OHa5Gf55dNDI/JLUex4Eow4V2nl00YjBNVoeWPjZQ5M+ziypVc03+orG6Rv48u6jCEmCYpiw1bYar7jy7qKuD+BwTn/lO/PoobtlFPVkqAocmYbwon7M8iNusryhq74KUe+FfWxX1IDHuaZddhH659RVlfmRpFPId5zxbE0Xh6XqeTYB3ZfUVRe/Gw3xCCahVFYbFhxw3cHX1FbpZT3L7dHzVadWwA/l3lA1cW32FbtaPFwXtkc1qQ99bXcxRkFt9ha4Ow8S5oMq30A7nFtcTNnC0+gp9dhDOYTj2na2Y3mMe0bw5CvKrr9DVuTg4ZLDGzbLweajKZnVh9RUKh9ZanprWX2NyTnJR14viabO6uPoK3Sw8OvM+g7womcxm8251afUVCtfQE8UNrwKnGTI+Cv/OVxS9kNEYYIevgnn8v/AVRY17lqPAbLdWTMfWga8wUjVjvB5UmAHnzn1FMZnZqqOnW/mBzveDr1BdHmT3U5NGf15djOe+QqVqkIlNQ/GL4d529/77AJeFVsB+tsnJhpbOfYU+O8zn4RmWbsBVd+4r9EVBcSYNsUmGe8FKRbOi9RVVL7NhvMaBv1lSNVLwweuzrwhl9RV1ms/qrrGOrsN5E23nm1aqMQGhrr6izm82FaEvs3diODcFnHjW48aihLb6CoWDNFeWigDD0cx9ch/Cc1yBJN2fB7gg7Rl9X6w3i2c32dCY4vl8FJFWX6GrAy8gB4j+AS4ZuLLxFdGvvqJqws1LbwpfYtka12xbrDZ+NobVVyhcGAOdWMmVXDBcbZ6mmDari6uv0M26MaqLZ2uRi8LpEbh+C9i8KDGtvqJqHiWmMYGp4WSzerK2uh67urz6CoWDz+T2tH7CeI0nqZ9t6kvWQp0moI5mEv4O+WR9See+ourt00mLFYfFDEeunfuKqnWyKmMdeaw04F4RhG++QuFQBMT8T4EL+XxE4YTr70JtY9JhAZzz5yMKDRwuZHxvTVG+Cv8LrrZK1WAMQZIZqgyXX+lB3XxFNeUPIaYznYrhSvqFr9D3DpNIuNXFVcDtx7CYryLfWAtNrzxJTADfPrvplKQR6zohjNzEFfnGWmh6ISPpsuQMWf/8ha0lcGmjQ0a4UtrijK6uVLHGtQRZXTFwz03qPZK+sRaa+tkqo1gbXA8PjnUXXNy09hGChT+PcG50qAaXsLqmzy6s8yvMUZQba6FpYSvlMTWtYnXINgrcTeLcwt1YC21+s6WKvWOGM68OjKMLrmz8bLmxFprJzEqHKs+LIlfEd1xwm9IRbOLf8LRZRH+8WeKT7ZvPc7P9nDYvSrmxFppalCiDEyvgeqinL0r/ROpmdTfWQlMTUER+gA0Tb9bri7IdEvOtOKOrgyw3t/YVOYr4G9ZCU6ZRkK+CcL/jyerhvDjTJjWoYrYWJ2ED4NLbuOLfBzgemxzHhM0CuJh+UZxphmwoPaDcTMJwlfJ5cUZPFslKDJ2U1dXgz4szzVTe5Sj4Bo9n185VgyZcFKIme7LY5GRLOi/OGDg2n3xZivLsXDgfO2VeY4yTziyhLfe78mY05r04oxYFFQBcecS4uzfpwHtxRlfnWxoXMoGjz6sjtBQZx3jRB2A+ZXAi92/3y5g05nyg82HOqnGME65KEMVaC3xHoW6T4mfCXHAra8GsLo67MZfKsbqcX8CtrIUJNxT6Ye8KVkctvYBbWQsWronGR+wejeF8+aiSRhifaRzjhBsFBRbl4I+M4T7rLhI6lP4+wEXpTGV9FN5sdz2N3jy7lbVg4JCvy+wQebP0pj7b4VbWgoETPSiZJ0Dkdc77T6ujlbUw4bzM+sgyxpufXfw82KnDrawFA4exP1lU+hguvwoEVsc44YJ0zmRuFk5yFO1cZzZqRz7UC1iUIxTcArKPx47RwiGuYGpak0tFOqfzRZUfwGD2IVWDu3E7n8c4n50bHfkZ9FG+o6RX5Y/VMU64foVrMhWXtbQYrqZy7BgNHKR+Mw8/dbK6ci7AbjYLUQQOQMO43zl/7BgNXBwaH5I0yj/IXuwdo/kqIGqNxIe8xq/i2ZtjNF+FmM8IiS65G8djx2hNQJb3jqLYu+TcZ8fI1yLT3XvdeXk+GcndmKspRCzAXj87xhCW7t4JNzQqIWtGzOYSQadP9i7EpbvXwKHnmP0sDFTol9n2Ai4t3b0TzktGjHXICglcLi/g8tLdO+FI2r7Zk4UIuJpfOEbOzJru3qhd5Y0uT+YAFz/f3DtcXbp7DRwmUrE15ktF92StvTmKtnT3GjiUedlAUcbJOv9is/ze/n2AqzJmj987TgdSv3qX+nl1yMz6JzjUyTgvOV7jvRqpXZ1funsjmd5j8bP87TKcd/m4u9esrqTLuHscRQn5uLvXrC4USQeynYOfLe24uzeqXgDmQ2URsefbJxrBDrt7zepAWM1xmAAnvciH3b3m2cUgt4DkxpXH1+PuXvMa+yGRxFQN3I3pvLvXrA6N/RxtI2L8SUhn391rVoeaIgLQPE72fMbWNFBu3I37yZKEKfmNTsWtu9fAiYpGxDQhfo39G7nkW3evPYoRRAV8sySztj55spxXT2aahdl2sa/AN5t/6Iw2niyX1ZN5896NoeIIU4qRhPvBQOW6ejJttJbV8RzQjNXRC4kkNDpZT6Z6AXhtefp8jjCfzb3wZMWtnswIS3DFne/GnKmgLH0Wn8xnodWTWckQN8T+vBj39gbOr57spqLBwXsWa9yyf7HZsHoyhZtKXzBQzJF7s7q4ejJvFJcuSTjCi7KfX2FXl1ZPpnDQ5M0BuXb2s/nz6PMOl1dPdsF50UPhFH4UuOLauSfzRg8qDmXDLDco9wtPZl4UtLo0qBrAQNEvPJk5CkdT5gdpmeTOPZnCoUTJhS0Se1dcOPdk5jUeRUEmbHIugPK5TkVU9RbQqdhX+CT3O/cLT2ZMQBICCTEvoMO52s49mVFvcaJDxmOU+SjKK4mkmycz9i4INYjV+fjZpVdDsW6eTJ+dzCj3yNvxa0zpF55Mnx2aNJkXkDNW1z5LnFPkPzeMjxj0NR7FaMnfDaalwPl1ROF/DBwtjA8DJzKOMkCx+43+zRq4TZ0sSu7Tf4NLMi8lezhEXl20cBteQETuUykay+pEPpTp82w+jdif38z+IPzKPw9wJIOKGS6INQ7qtv06lfmPgUsL48PAURFprgzjPlpe5lGkDVxeGB8GDvYuO3SqsuvxGkRtafPomzKMDwOHnBMqAgWbJQ2PmSq9WV1dGB8Tjs1lGGRDee+CdpX7zTxGwnAAw/gwq5MG/wp6Ae4o3pxs2BwFmtS/w/XXFtydDse9eAyXA31a3TfGhz2K69l5D8dYX+lqr4wP8xqDisbPjssfxB0M5zoVBg5k/lTQFgk/W86Z5BPOyzB7ENOTxLPhnPEx4aII6KSR6GXzWc51KoxFQccCc8jyuFTkfMz4mHBZppImrnFfxj0eMz7M6kSEqI3LbPxhZvSe8WGenR9XHglToqjOf7jfYWywLWxFtXd8XWRyB1ePiTvzkzGfz1EPQmpb2IrTBNSr5YCvPDxGkIyBKhsTEPJa2Ipq3PnaDKpdENejm/Xr6CQLV9bClm4WvcYpQ+KcDVRpxrhvCHMx1LWwpZsF84VbXYrYu+SNJ0u0OYq2FrYUTnRmo7C0iL9dY6DCxhpHtxa2dLOQO+IOBsBlGSB7wdHGV0RaC1sKF5poB8rqquj0PMLZ1fm1sGVfFFE2pPHe5WJWFzZuO4a1sGVOto7WPq6msNt2xvW4sFldXAtb5iiSqPMxQZOfnSmo+g3P/XthS1eHcQR4dnILoBzOC1vRXLVHszCNnHs6H4cx4aKM/k2EKBvhcYjnhS2FQwzGnYJFrHF07rywpSaggDBHGPDE9i61cF7Y0tU5I4HJt4BXc97vhS09CvTocBtKFbfdXsl03wpbcboeX6WllG/sDOfDLwpbcUY9uPpxH2MSOEf5vLClm4XGTpKRrPzsUj4foGhOFnIXDBdG0ij+orClcEj/cX9mk9WV+iKIynV1jEkdI9rTuF4W4cmMGIxfW0qNY8xtdYxJ7R1uAR7dR+S9jBR59BXG3hW3OsY0L2SYbsCbZd4n35WtvfNuA0erY1S4xGmZJIoQvNlQ28Z8Wji/OkZ9drVIKzO347Jxb/ZSQRvHWMLqGHV1tQyhRORRmrgigaOV1G/h4uoY03xRkL9jsREvASgpgYRzNJvNptUxpmlRwOhlDQnOffKtQIOoG5xdXV4do64OdxIeWMWJj/4so6qR9s1u7iilrI5RV9eGvFRgoiY163povd/Z1dXVMSrcJbfSkL+rNuqhF0HUHa6Q6EGBhd/ddovx3DGmtTjDGh9VIsZ3SaObY0xaEXCiu9hI4LRd48Ax6uqg7RELBpwgeG+/cIxJq3h822RlFYQpWVTTTh1j0qgH6ssOBCZceco5FdKsTvp6umEKcvtsvpw7Rj2Ket1RRvljL9P9g2NMKzWI9VHcqKaEc50K8+xAG4VQi5ccVKFzx5imny3oduNLraS02pt+srtjNHCxDb3PDLicPtfJILlnHWPW+50XiSQeLs6erKigCe0EnNCnbB1jnvYOSSOWL+L3jm8F2bqeZ8eIuqR1jLq6NkR1OVnEqzOpVO7o3MDdHGNer9qR+SgFcKEaa5zKBu7mGC2ch/lk3j/DUbTm83F0Uoe7OcY8PzJ8BZF1yAp8RarGk23a05C9/fsIJ+p8LFpP4smsr9hE28ndHGNW8xllszzRGs8uG7cddqu7OUaFEwVhFl9zeFGyEoRplzeGIKp1jNnQ+ZI8u+rkBmWfnd+t7uYYFa5Amkv0KnBdTOYWEF9SIXWzjZMYPE6L2zW8E6L6qWPMk7iEGIxls4JkKsj7c8eocFAJYkHikeiFusipY8zKcOM3nz+ykYbO8RdUyDxpVVD26nApSqbilZze3TFmw1oQwboQ5Abl6XyAooEDH4zlQ7kniuFcOHeMulko3bBMd5bNZvqFYzTProqfhb3jlBaFc8eoXwWG2XMuAHeU9kOS/AfHaCxKFhHxK7tIv6FC6up8FZ1ZVrDG6sLH1VFSKuT/A1xR8g1UcIX8QOyKyNi7lB8dY1Iq5B0OslIxgRVNXK+1nmwj4JSUCnmHE8V01pvl1SXb/UG75rmkVMgFLjhpeI1JsotMiih2s2EDN6mQKxxhVVAigTWOIuEwHeOzEkRSKuQKx/PdkRuATDL7itjMs8tuAzepkHe4EmWzIMyxJyNr3DeOUamQK9wY69jhWH2fN+ttALpbnVIhV7j+glR5dsz35M2m9DEm63CTCrnCBUl0sNQ0LhVRhrI9+lm7ukmF/La6JB8ZzCfqF/mT27aO8b46po9ideMGleho0MkdDrKNsUgPKF/I0pHk77dnx6sZYs64kMUjefj7s3ND+J+NDu4on6lB1jHeVxdQYHAQ5QCcP4oY718FUqfseq5MRXpZdvv7AMcuZ+SgOEzh0rmPLzY7y253OHxULDWNr8LLsOf3Zbc7HKdQceXBs+OK6OdGa0SZlqKhXeWwrOwrOLXAid9ZTeGsSHz2FXkVcIra886ZMVgUXBeLvDgCx0SfZxOQVwGnCRdlbgWL6nJWm3MDMy3D1da8gVsFnMzqMAyLk0Z8v+MJ11MrlUt8dQO3CjhFlR9ABz6HKU1cT50mwG87ozFk0VI0tCMfniuKSAJcz7wF+H1MVlYBJ7s6T6JvzLlPHm873zsEGc+bLauAU9QGf0ykYhNQo7htbza7kQzBAK2/3+FY9qIJHKt3EQbIXiaAY+W4Wd0q4GRW50e0zVds+Iqpj8LPrm5Wtwo4mdWBDMJhihO37WcQ5WkjHPadoqEni8mPYwwLwxGdD1A0r3EdE3FCvfysO6doKBzEdDGvRxIfwZ0PUDTvXYsyn4zjCaRlfiH2ZwwUWqpYEI3rsxxtt3MBJ2NR6pA4h/IN80DduYDThGPlEdks3/PoIpScUjR0dWWIm6ZhUd7NPb5RNIxkSJNou46vgj7Pead0T7ipsATDIA3NMj/I1BqLEvyzr7gn3FRFA1OsYAIIvkLHxqP68GgC8j3h1qYJwKPn4gyTIDzn8aZFcTvV73xPuOnqcKlH6Ug2G2ZrH7/TaQN3S7jps5OhpyOVymNGySmce44r8j3hpoImUJdnmkEQx6gNEd7vZC/yPeGmcFD5ZhFxxBXFjBLhC4vfbPaWcDNyK0F0F+V+l4x0AzvG3epuCTeFQ68Eq0L68ZHNioC/iera1d0Sbhcc18myiDkjrshm5gy/xnWzulvC7YK7TECEoWK4Mudtd7j2Ma74tromFiU3+Srib3qPL7gqokNpKEH4BMma44SbwmGwCVtjZrj59EOT+g8JN4VrZehqcwcDLmTlPOF2wWXp0+ZSeRnfbPHnCTeFE+4ih67jZGs6T7iZ1Y2pfSGFAUfnCTdzFCQG6krL+HYuymFfFM7YcEoLJ5t/UJv/IeF2wSUhKnHUk8Si+PqLhJt+FS1IUZD1T+Er6rkoh1kdmpYQ4omvCJ8bIgg0XdOxddWsWdzUi4HiEdS4Lk5PxonQZ8fIL5Xt2JpwXqZrMPkmSmohzDQ032yfo54cwtKxNeFY6UYMFFM1kIMKxpO5uIGLS8eWWR3KvSyLmiVTUWa0zXfHuoFLS8fWhKPBg0qSc2dvo+aTlf+erXHIS8fWhHPSoZUKVFsYznuzug1FA9PX/z7CQXkqiTCn1HqcOQrabLYuHVtmszQIwjTyKKmaeNbnzera0rFlVlcHMZ2nlaJORiba3ui55+iWji0Dh5NkRc2R0gqTZvDD6iItHVsXHF/IvBCE5bpIhuSKXt1XHVv2ZIdAZ4FjDC8nMa0dW+YocLmCzI9YlBDzcceWWR2sQ+YblJjPGMpxx5bdLPSgSErlfIMK/rhjy2wWBYYsEjWItl087thaTMBosWLFJd5sKccdWwYOSfEhl4yjaOW4Y8tYY9DL+NkhYqxfgdxxx5Y17pdy9bhU+Dck11vHlt3sIKZHMNySUf1+37FlTjbAkzVpiPCvdMgIvd7Wk5G+xlV6BAIJJS0F95FsyO32iyejubriRemLyQ/oPXamcyZuQrx882Q0LQpzxwDnhPSVNA3NjmMDd/NkZrNOuj8K+nrql0umR8BtzGe+eTKzWXQdZUwURvV4YZI/kw3ZiS6eTOHAWechgE0awIzGx20Sk4W7eTLdbBkDJ1yQZ1eKOQq3O4qbJ1O4NKYJtTZI/dGQINomns03T6abxdwAXh3fmSjBjOrJblZXbp5MV0fX1D4nLHzn7Oo2R1FunkxXJ+Rqj27LG9yO5Prdk+lrzL2f4MyC4VZlrPKpJyNN4Tu5zNYmnFly6dyT0UwHIvbniBHaMu2NGMx3T0YzHVjdoM1nqbzHV3725skULl3E9FEq9680Pm6eTDcbadCXkxCEY0rnnkyPIgw4T7K65vK5J1PVIOG5B5R9cbLxXEXDbDYV+chYcQ8U3JTPPZk5WRpwTSga5U2j9d2T0UySy2sc8bGB0etfxGSNlsJW8uYGFYaBkua5Uownc8+9x9zKZgtbE46k/ZYNFF9maYzumsZ9Y6BaWApbFxx7MiezPziPh1YX1z6Sq5H5NoUtAyeuJ0DRVRpxDKei7eDSUtgyzy6NrDaHegyXydLmN/m7lpfC1rI6oZHybDdsNlrX4zZwZSlsGTgMJUoEIw/6csofmZW51aWwNeGq8O94vCN0eVjGtm6Owq6uLYUtszrUxThTcUkkLZt9vqNggrMpbE24/pE1ScuIQGeQRMgHPgrUI/76x81eLPwmTUxu4czWV4UtszopHTnMnMFRVDoubE24PO4o/WSzyK0UT8eFrQnH7biDF1CESe5bPS5sGbgyUlp+NM/tZc32ha0JF0fnjFx9GG4/vntf2DJwOFnOzJLApXTeezzhBlGJ29NwN2YW/rnavD2KlEdKS6Rq2i8KW/ZF8ddmrwawj1U8Qv+e9RVhfhU0iOnJiXqLjmRl8/nsK4oPq69QOChXR5kSyVpapkndr4xeYwJ8XH1FmB8ZRsTxDQptQjyl1H30FdBXtb4iaPW4tTGPUeDK4npoA5dXX6GrcxfNgOTZlWzgWtocRVl9RdCPzNFgB4r5pFg2pH4LV1dfEcyFTMiGBRJJXpjlkx1YN5ttq68I8yNDaoGJmkUkf0MMH1uswPr9+wgHFUjeLMRNq4yOm8/uuexWAq2+Iiitqgoc0+Uha2ZJ/SluVudXX2HgwJZJIq3PcM1cKsq7KVYGrtbRXxHlBpV+McXKwKH3HxOTxTG6/AtfESaHzI/yBw1FzZLPdSom3JCt5a8ihKH0Vc99hcKBrgzGR4HbpnY+8dBsViYxCclaLmT13FconCs0xtm2IRnSzn2FnmzJcr9jOweND9/OfYV+ZHT1gDrRNHqhj0JoZDfk6uvj4dc4CgnCjxkMxTbitPDsK1JeyNUTji9g0nKQvIg5x5Y3WR5jAlJZyNUGDgOucF0UfePmjUXZcCpKqgu5esJ5+eR5QvzQ6KXsXsC1hVw94Vjjo6ISyjcpwIW4CVPMUWS3kKsnnBMGL7MDh2J6rvbm7jdwtJCrLRwN80lD79NSvzctVjAThlx9wTGpvyTtsmT15WJyUBsBJ8z4+fsAV6V/LEpvFL8oxZlLhdu8KDku5GoDR0F6QAvJHcUkjWhlVtrVpYVcnUyDfwyjTUjkQ01DxJvpuN82m0eHahx+toZjcrU5ChoXMsxgIB5q74/J1fbZRYFDtxuRiLAdkqvNs3Mtjd5jkQ+tvh6Tq83qQHrgaHtceVw+J1ebZ5dI0oHXLeBdOnAlVxu4PFKpUhHwUr84JFebbxbdCBxEJRH7o1fiVyu52sCFwdVuIxDYd6iar6LmhTCXtFkYgibdfIoWfpKQ70MOqtSyEOYMXBquh4WIZQzLTofMmIBaF8Jc0t7jOnrec5XxP0bmh9YB2RauLYS5ZNrAx4XMjzkCrtnL7GZ1zS2EuaStzBTE9cQmWvjmZLfSDVwatoS5ZOQHhgkITqZY5WzTgRvX0/xCmEvayoypffzsmsyJcvaqvXOMLSyEObO6K1MBXR4e2WUlkjYju0qLC2HOwGEkMuJZmSaUU9hUouzq0kKYS0YJAnUywvxjvlQ4KyyxmWJVWl4Ic0lVNDA2mVfH6cAOZybi7PSgvhHmzGYhTpIc4lo+ikDnU6ySVYLII9ErQRS1c00j8+xA8+GsdhMl11rKMWHOPrskWloiL8VSSfmYMGdPlkZaZqQWyJ0T5uxHVkWqpuGbZUrueYeqNQGjmlK8wLVwTpgzcGg0ZB2yEbyXVxmylTCXrE6F1GfzdD3umDBnzKf3QRNunDR6Iy91I8yZ1SEnwoxeL6mFXM4Jc8beQXKbc1BJLrN7BRItzlQqaxBlxBFGoheCJo45tPmjxDkGLdggyihBjAxZxsgujzlln5SrUb2yQVSezy4MGqkbw9j2qpAGzrs1iNLV+VHF82MuXvg8g6HD0RpEZRUhggRmgCw8P7tWPsezGLVsgyiF83FYlDEdlxVdP8OFNYgycFU+MhmdlCHn+HmzcQ2iVJQDroaVDTlvzM8uv9lsWoMoXR3EERITliKenXsxrAPjR2wQpXBQnkrM5JXZbulzXw+Bs/f3AY4vsQ18FI9Jm1zYiicS599WR1KyhL4xX3kSnQdRehQ5DZFJzDrikO+8Q9XCeamA1iLBewz1PIjSzYJ0yUquY9ZRrr8IovSbxV04yVUH8Wxs50GUkVsp8t5xShWJ3ldHcQui1EBhGFHiGo8E7zmF8yDKrM6NQn6WpFF9FW3fgih9dtC7Yzm9Ji8KfZ4WSRzoL8WZMl+UCiWI/qJEGZDtyudxGDW2tTijcBC74pPF0FNn2jV+MAHJrcWZop4syldR4cl+0sK3cLQWZ4q5zI4p9D6MofZvVufX4oxuFgQ56Gp7sXefBYkJrt0WZ3R1IYuIOOvLMlxo9AIursWZopcKTGV2wuXhkaw5vthsWoszCgcqEM8BrfLs/AtBYlw8/obHo0jCSi15DBf3b062rMWZopdZDLVnJSlx2+TePLu6Fmd0sxBZyyw2WQCXzsZh3FcXSTizNFILIfjz4kyZl1mZxOSGn+XGw3JenCkz8YGmJWb0JhkY+2KW5ffijMIhLZplhiXyKPVcRDypAonDFKsevEd5jZ07FxE3cATyTUMci7txSefFGdULAK+dafNZTEAJ5x2q5tmxxDToyywvxaur/rw4Y16UetGXw4dRwH/+6//+z//zX/8fZc3YH3OBAQA="""



pdb_test = """\
REMARK    3DNA v2.1 (c) 2012 Dr. Xiang-Jun Lu (http://x3dna.org)
CRYST1   25.000   25.000   25.000  90.00  90.00  90.00 P 1
SCALE1      0.040000  0.000000  0.000000        0.00000
SCALE2      0.000000  0.040000  0.000000        0.00000
SCALE3      0.000000  0.000000  0.040000        0.00000
ATOM      1  P   A   A   1      17.552  11.721   4.374  1.00  0.00           P  
ATOM      2  O1P A   A   1      17.394  11.350   2.949  1.00  0.00           O  
ATOM      3  O2P A   A   1      16.924  12.997   4.784  1.00  0.00           O1-
ATOM      4  C5' A   A   1      17.686   9.261   5.264  1.00  0.00           C  
ATOM      5  O5' A   A   1      17.015  10.536   5.301  1.00  0.00           O  
ATOM      6  C4' A   A   1      17.095   8.333   6.307  1.00  0.00           C  
ATOM      7  O4' A   A   1      17.569   8.748   7.621  1.00  0.00           O  
ATOM      8  C3' A   A   1      15.572   8.361   6.437  1.00  0.00           C  
ATOM      9  O3' A   A   1      14.941   7.605   5.413  1.00  0.00           O  
ATOM     10  C2' A   A   1      15.441   7.780   7.844  1.00  0.00           C  
ATOM     11  O2' A   A   1      15.613   6.390   8.042  1.00  0.00           O  
ATOM     12  C1' A   A   1      16.539   8.545   8.576  1.00  0.00           C  
ATOM     13  N1  A   A   1      14.274  10.997  12.478  1.00  0.00           N  
ATOM     14  C2  A   A   1      14.556   9.714  12.259  1.00  0.00           C  
ATOM     15  N3  A   A   1      15.145   9.158  11.223  1.00  0.00           N  
ATOM     16  C4  A   A   1      15.474  10.087  10.303  1.00  0.00           C  
ATOM     17  C5  A   A   1      15.250  11.432  10.385  1.00  0.00           C  
ATOM     18  C6  A   A   1      14.613  11.905  11.543  1.00  0.00           C  
ATOM     19  N6  A   A   1      14.331  13.198  11.757  1.00  0.00           N  
ATOM     20  N7  A   A   1      15.726  12.074   9.250  1.00  0.00           N  
ATOM     21  C8  A   A   1      16.214  11.112   8.529  1.00  0.00           C  
ATOM     22  N9  A   A   1      16.098   9.867   9.103  1.00  0.00           N  
ATOM     23  P   C   A   2      13.510   8.075   4.873  1.00  0.00           P  
ATOM     24  O1P C   A   2      13.167   7.314   3.651  1.00  0.00           O  
ATOM     25  O2P C   A   2      13.487   9.549   4.746  1.00  0.00           O1-
ATOM     26  C5' C   A   2      12.629   6.268   6.553  1.00  0.00           C  
ATOM     27  O5' C   A   2      12.554   7.624   6.074  1.00  0.00           O  
ATOM     28  C4' C   A   2      11.736   6.098   7.767  1.00  0.00           C  
ATOM     29  O4' C   A   2      12.380   6.729   8.911  1.00  0.00           O  
ATOM     30  C3' C   A   2      10.370   6.779   7.688  1.00  0.00           C  
ATOM     31  O3' C   A   2       9.448   6.025   6.911  1.00  0.00           O  
ATOM     32  C2' C   A   2      10.050   6.847   9.181  1.00  0.00           C  
ATOM     33  O2' C   A   2       9.629   5.678   9.857  1.00  0.00           O  
ATOM     34  C1' C   A   2      11.390   7.313   9.742  1.00  0.00           C  
ATOM     35  N1  C   A   2      11.559   8.795   9.731  1.00  0.00           N  
ATOM     36  C2  C   A   2      11.045   9.514  10.807  1.00  0.00           C  
ATOM     37  O2  C   A   2      10.476   8.901  11.717  1.00  0.00           O  
ATOM     38  N3  C   A   2      11.189  10.865  10.816  1.00  0.00           N  
ATOM     39  C4  C   A   2      11.814  11.491   9.810  1.00  0.00           C  
ATOM     40  N4  C   A   2      11.929  12.808   9.867  1.00  0.00           N  
ATOM     41  C5  C   A   2      12.349  10.770   8.695  1.00  0.00           C  
ATOM     42  C6  C   A   2      12.194   9.421   8.705  1.00  0.00           C  
ATOM     43  P   G   A   3       8.926  19.107  18.412  1.00  0.00           P  
ATOM     44  O1P G   A   3       9.125  19.625  19.784  1.00  0.00           O  
ATOM     45  O2P G   A   3       9.999  19.417  17.440  1.00  0.00           O1-
ATOM     46  C5' G   A   3       7.516  17.019  19.129  1.00  0.00           C  
ATOM     47  O5' G   A   3       8.686  17.527  18.462  1.00  0.00           O  
ATOM     48  C4' G   A   3       7.424  15.518  18.945  1.00  0.00           C  
ATOM     49  O4' G   A   3       6.989  15.241  17.583  1.00  0.00           O  
ATOM     50  C3' G   A   3       8.739  14.751  19.076  1.00  0.00           C  
ATOM     51  O3' G   A   3       9.103  14.556  20.438  1.00  0.00           O  
ATOM     52  C2' G   A   3       8.349  13.477  18.326  1.00  0.00           C  
ATOM     53  O2' G   A   3       7.521  12.524  18.956  1.00  0.00           O  
ATOM     54  C1' G   A   3       7.631  14.065  17.116  1.00  0.00           C  
ATOM     55  N1  G   A   3      10.066  12.314  12.996  1.00  0.00           N  
ATOM     56  C2  G   A   3       9.234  11.655  13.880  1.00  0.00           C  
ATOM     57  N2  G   A   3       9.025  10.359  13.632  1.00  0.00           N  
ATOM     58  N3  G   A   3       8.661  12.247  14.925  1.00  0.00           N  
ATOM     59  C4  G   A   3       8.989  13.563  15.015  1.00  0.00           C  
ATOM     60  C5  G   A   3       9.802  14.300  14.188  1.00  0.00           C  
ATOM     61  C6  G   A   3      10.415  13.670  13.073  1.00  0.00           C  
ATOM     62  O6  G   A   3      11.166  14.156  12.231  1.00  0.00           O  
ATOM     63  N7  G   A   3       9.883  15.619  14.628  1.00  0.00           N  
ATOM     64  C8  G   A   3       9.121  15.640  15.695  1.00  0.00           C  
ATOM     65  N9  G   A   3       8.544  14.424  15.994  1.00  0.00           N  
ATOM     66  P   U   A   4      10.656  14.517  20.824  1.00  0.00           P  
ATOM     67  O1P U   A   4      10.800  14.572  22.297  1.00  0.00           O  
ATOM     68  O2P U   A   4      11.384  15.556  20.062  1.00  0.00           O1-
ATOM     69  C5' U   A   4      10.302  11.924  20.701  1.00  0.00           C  
ATOM     70  O5' U   A   4      11.078  13.068  20.296  1.00  0.00           O  
ATOM     71  C4' U   A   4      10.799  10.683  19.987  1.00  0.00           C  
ATOM     72  O4' U   A   4      10.344  10.728  18.603  1.00  0.00           O  
ATOM     73  C3' U   A   4      12.316  10.542  19.873  1.00  0.00           C  
ATOM     74  O3' U   A   4      12.894  10.072  21.083  1.00  0.00           O  
ATOM     75  C2' U   A   4      12.380   9.561  18.704  1.00  0.00           C  
ATOM     76  O2' U   A   4      12.088   8.195  18.924  1.00  0.00           O  
ATOM     77  C1' U   A   4      11.341  10.165  17.764  1.00  0.00           C  
ATOM     78  N1  U   A   4      11.886  11.229  16.875  1.00  0.00           N  
ATOM     79  C2  U   A   4      12.479  10.822  15.705  1.00  0.00           C  
ATOM     80  O2  U   A   4      12.573   9.651  15.380  1.00  0.00           O  
ATOM     81  N3  U   A   4      12.972  11.836  14.906  1.00  0.00           N  
ATOM     82  C4  U   A   4      12.922  13.189  15.175  1.00  0.00           C  
ATOM     83  O4  U   A   4      13.398  14.004  14.382  1.00  0.00           O  
ATOM     84  C5  U   A   4      12.280  13.517  16.426  1.00  0.00           C  
ATOM     85  C6  U   A   4      11.791  12.551  17.222  1.00  0.00           C  
END
"""

import numpy


# ----------- CCTBX
from cctbx import miller
import mmtbx.bulk_solvent.bulk_solvent_and_scaling as bss
import iotbx
import mmtbx.utils
import mmtbx
from cctbx import xray

import scitbx, math
from cctbx import crystal
from scitbx import matrix
from cctbx.array_family import flex
from cctbx import maptbx
from iotbx.pdb import input as pdb_input
from libtbx.math_utils import ifloor, iceil

from libtbx.utils import n_dim_index_from_one_dim

from annlib_ext import AnnAdaptor

from libtbx.utils import n_dim_index_from_one_dim

import mmtbx.refinement.rigid_body
import mmtbx.refinement.real_space.rigid_body

from iotbx import ccp4_map
from cctbx import maptbx

# -----------------



class blobfit:




    def __init__(self, pdb_string=None, map_fname=None, d_min=20.0, debug=False, debug_ofname_base=None):


        self.d_min = d_min
        self.resolution_factor = 3.0/d_min
        self.dummy_dist_sq = 50

        self.dummy_model = None
        self.pdb_model = None

        assert [pdb_string, map_fname].count(None)==1, Exception("Cannot process both map and pdb on input")


        if pdb_string:
            try:
                self.parse_pdbstring(pdb_string)

                self.pdb_model = self.parse_pdbstring(pdb_string)
                self.model2xs(ofname_base=debug_ofname_base)



            except:
                raise ValueError('Failed to interpret input string as PDB')

            try:
                self.calc_map(ofname_base=debug_ofname_base, d_min=d_min)
            except:
                raise ValueError("Map calcs failed, check d_min value")


        else:

            try:
                m = iotbx.ccp4_map.map_reader(file_name=map_fname)
                if 0: m.show_summary()

                assert m.space_group_number==1, Exception("Failed to parse map. Currently only P1 is supported")
                assert not m.data.is_padded()

                if debug: m.show_summary()

                self.n_real = m.unit_cell_grid
                self.unit_cell = m.unit_cell()
                if m.data.is_0_based():
                    self.ref_map_data = m.data
                else:
                    self.ref_map_data = m.data.shift_origin()

            except:
               raise ValueError('Failed to interpret input map. Expected CCP4 format.')



        self.debug = debug




    # -------------------------------------------------------------------------


    def model2xs(self, buffer_cart=10, ofname_base=None):


        atoms = self.pdb_model.atoms()
        atoms.set_b(new_b=flex.double(atoms.size(), 20.0))
        atoms.set_occ(new_occ=flex.double(atoms.size(), 1.0))


        sites_cart = self.pdb_model.atoms().extract_xyz()
        sites_cart_min = sites_cart.min()
        sites_cart_max = sites_cart.max()

        self.sites_cart_shift = [0,0,0]
        self.uc_params = [0,0,0,90,90,90]

        for _i in range(3):
            self.sites_cart_shift[_i] = sites_cart_min[_i] - buffer_cart

            self.uc_params[_i] = math.ceil(sites_cart_max[_i] - sites_cart_min[_i] + 2*buffer_cart)

        sites_cart_new = sites_cart# - self.sites_cart_shift


        self.xrs = xray.structure(
                         crystal_symmetry=crystal.symmetry(
                                unit_cell=self.uc_params,
                                space_group_symbol="P1"),
                         scatterers=self.pdb_model.extract_xray_structure().scatterers())

        self.unit_cell = self.xrs.unit_cell()


        self.xrs.set_sites_cart(sites_cart=sites_cart_new)

        if ofname_base:
            _ph_shifted = self.pdb_model.deep_copy()
            _ph_shifted.atoms().set_xyz(new_xyz = sites_cart_new)
            _ph_shifted.write_pdb_file("%s.pdb"%ofname_base, crystal_symmetry=crystal.symmetry(
                                                                            unit_cell=self.unit_cell,
                                                                            space_group_symbol="P1"))


    # -------------------------------------------------------------------------



    def parse_pdbstring(self, pdb_string):
        return iotbx.pdb.input(source_info=None, lines=pdb_string).construct_hierarchy()


    # -------------------------------------------------------------------------


    def calc_map(self, d_min=20, ofname_base=None):

        f_calc = self.xrs.structure_factors(d_min = d_min).f_calc()
        #XXX resolution_factor=0.1 at resolution of 20\AA should yield roughly 2\AA grid
        self.ref_map = f_calc.fft_map(resolution_factor=self.resolution_factor)
        self.ref_map.apply_sigma_scaling()
        self.ref_map_data = self.ref_map.real_map_unpadded()
        self.n_real = self.ref_map._n_real

        if ofname_base:
            self.ref_map.as_ccp4_map(file_name="%s_map.ccp4"%ofname_base)

    # -------------------------------------------------------------------------

    def _create_tmp_ph(self):

        tmp_structure = iotbx.pdb.hierarchy.root()
        tmp_structure.append_model(iotbx.pdb.hierarchy.model(id = "0"))
        chain = iotbx.pdb.hierarchy.chain(id="X")
        tmp_structure.models()[0].append_chain(chain)

        return tmp_structure


    # -------------------------------------------------------------------------


    def scorch(self, na_number=None, average_density=450.0, seed_xyz=None, debug=False):
        """
            builds a dummy model for a map, given expected RNA density
            TODO: density should pobably vary with estimated resolution
        """

        def _build_ph(_model_coords):


            tmp_structure = self._create_tmp_ph()
            chain = tmp_structure.only_chain()

            idx=0
            for xyz in _model_coords:
                rg = iotbx.pdb.hierarchy.residue_group(resseq = "%i"%(idx%9999))
                ag = iotbx.pdb.hierarchy.atom_group(resname = "XXX", altloc = " ")
                atom = iotbx.pdb.hierarchy.atom()

                ag.append_atom(atom)
                rg.append_atom_group(atom_group = ag)
                chain.append_residue_group(rg)

                atom.name = "P"
                atom.set_element('P')
                atom.set_xyz(xyz)
                atom.set_occ(1.0)
                atom.set_b( 0.00 )

                idx += 1

            return tmp_structure



        def _model_neighbour( xyz, model_coords, n_real_cart, n_shift_cart):
            """
                check if any of xyz basic shifts is close to the model
                    yes: update model, return True
                     no: return False
            """

            model_ann = AnnAdaptor(np.array(model_coords).flatten(),3)

            query = []
            for _in in itertools.product(list(range(-1,2)),repeat=3):

                _dr = np.array(n_real_cart)*np.array(_in, float)+n_shift_cart
                #print _dr, n_real_cart, _in, n_shift_cart
                query.append( ( xyz + _dr ) )

            model_ann.query(np.array(query).flatten())

            for _i in range(len(model_ann.nn)):
                if model_ann.distances[_i]<self.dummy_dist_sq:
                    model_coords.append( query[_i] )
                    return True


            return False

        # ---------------------------------------


        assert na_number is not None

        map_3d = self.ref_map_data.as_numpy_array()#.real_map_unpadded().as_numpy_array()

        #n_real = self.ref_map._n_real

        grid2cart = maptbx.grid2cart( self.n_real, self.unit_cell.orthogonalization_matrix() )
        print(grid2cart((1,1,1)))
        cubicle_volume = numpy.prod( grid2cart((1,1,1)) )
        requested_number_of_points = int(average_density*na_number/cubicle_volume)

        sigma_cutoff = 0.9*map_3d.max()
        sel_g = numpy.where( map_3d>sigma_cutoff )
        d_sigma = 0.01 * sigma_cutoff

        # shift of the UC that contains initial model point in grid points
        # it ensures that dummy-model is exactly where input model
        n_shift_cart = np.array( [0,0,0], float )

        model_coords = []
        cart2grid = maptbx.cart2grid( self.unit_cell.fractionalization_matrix(), self.n_real )

        if self.pdb_model:

            _vals = []
            for _atm in self.pdb_model.atoms():
                g = cart2grid( _atm.xyz )
                _vals.append( map_3d[ g[0]%self.n_real[0], g[1]%self.n_real[1], g[2]%self.n_real[2] ] )

            _imax = np.argmax(_vals)

            _atm = self.pdb_model.atoms()[_imax]
            model_coords.append( _atm.xyz )
            g = cart2grid( _atm.xyz )
            map_3d[ g[0]%self.n_real[0], g[1]%self.n_real[1], g[2]%self.n_real[2] ] = -999.9
            n_shift_cart = np.floor(np.divide( cart2grid(_atm.xyz), self.n_real))*grid2cart(self.n_real)

        else:
            if seed_xyz:
                uvw = cart2grid(seed_xyz)
                map_3d[uvw[0]%self.n_real[0], \
                           uvw[1]%self.n_real[1], \
                           uvw[2]%self.n_real[2]] = -999.9
                model_coords.append( seed_xyz )
            else:
                map_3d[sel_g[0][0], sel_g[1][0], sel_g[2][0]] = -999.9
                model_coords.append( grid2cart( (sel_g[0][0], sel_g[1][0], sel_g[2][0]) ) )

            n_shift_cart = np.array( [0,0,0], float )

        _info = ' --> Analysing the input density %s' % "[%s]" % ",".join(map(str,seed_xyz)) if seed_xyz else ''

        widgets = [_info, Percentage(), ' ', Bar(), ' ', ETA()]
        pbar = ProgressBar(widgets=widgets, maxval=requested_number_of_points).start()

        idx=1

        while True:
            pbar.update(len(model_coords))

            updated = False
            done = False

            if not updated:
                sigma_cutoff-=d_sigma
                sel_g = numpy.where( map_3d>sigma_cutoff )

            #print sigma_cutoff, len(model_coords)
            if 1:

                grid_indices = maptbx.grid_indices_around_sites(
                        unit_cell=self.unit_cell,
                        fft_n_real=self.n_real,
                        fft_m_real=self.n_real,
                        sites_cart=flex.vec3_double(model_coords),
                        site_radii=flex.double(len(model_coords), 25.0))
                updated=False

                for i_seq in grid_indices:
                    uvw = n_dim_index_from_one_dim(i_seq, self.n_real)
                    if  map_3d[uvw[0], uvw[1], uvw[2]] > sigma_cutoff:

                        xyz = grid2cart(uvw)
                        if _model_neighbour(xyz, model_coords, grid2cart(self.n_real), n_shift_cart):
                            updated = True
                            map_3d[uvw[0], uvw[1], uvw[2]]=-999.9


                if len(model_coords) > requested_number_of_points:
                    pbar.update(requested_number_of_points)
                    print()
                    break

                if sigma_cutoff<0.0:
                    pbar.update(requested_number_of_points)
                    print(" --> Blobfit warning: could not define dummy model with positive density threshold")
                    print()
                    break


                continue


            for (_gi,_gj,_gk) in zip( sel_g[0], sel_g[1], sel_g[2] ):
                if len(model_coords) > requested_number_of_points:
                    done=True
                    break

                if _model_neighbour(grid2cart((_gi,_gj,_gk)), model_coords, grid2cart(self.n_real), n_shift_cart):
                    map_3d[_gi,_gj,_gk]=-999.9
                    updated=True



            if done: break

            if debug:
                print(sigma_cutoff, requested_number_of_points, len(model_coords), len(sel_g[0]))


            if len(model_coords) > requested_number_of_points:
                break

            if sigma_cutoff<0.0:
                print(" --> Blobfit warning: could not define dummy model with positive density threshold")
                break


        print(" --> Blobfit: selected %i points (%i requested) at sigma_cutoff=%.2f" % (len(model_coords), \
                                                                        requested_number_of_points, \
                                                                        sigma_cutoff))


        #sel_g = numpy.where( map_3d<-999 )

        self.dummy_model = _build_ph(model_coords)


    # -------------------------------------------------------------------------


    def _model_from_coords(self, np_coords, cm=None):

        _ph = self._create_tmp_ph()

        import numpy as np

        if cm is None:
            cm_xyz = np.array([0,0,0])
        else:
            cm_xyz = cm

        rg = iotbx.pdb.hierarchy.residue_group()
        ag = iotbx.pdb.hierarchy.atom_group()

        atom = iotbx.pdb.hierarchy.atom()

        ag.append_atom(atom)
        rg.append_atom_group(atom_group = ag)
        _ph.only_chain().append_residue_group(rg)

        atom.name = "P"
        atom.set_element('P')
        atom.set_xyz(cm_xyz)
        atom.set_occ(1.0)
        atom.set_b( 0.00 )

        for xyz in np_coords:
            rg = iotbx.pdb.hierarchy.residue_group()
            ag = iotbx.pdb.hierarchy.atom_group()

            atom = iotbx.pdb.hierarchy.atom()

            ag.append_atom(atom)
            rg.append_atom_group(atom_group = ag)
            _ph.only_chain().append_residue_group(rg)

            atom.name = "N"
            atom.set_element('N')
            atom.set_xyz(10*xyz+cm_xyz)
            atom.set_occ(1.0)
            atom.set_b( 0.00 )


        return _ph


    # -------------------------------------------------------------------------

    def _com_and_moments_of_inertia(self, atoms):
        """
            get center-of-mass and principal axes of inertia for a given set of atoms
            both are converted to numpy arrays.
            axes are encoded as rows of a symetric matrix
        """


        axes = scitbx.math.principal_axes_of_inertia(points=atoms.extract_xyz()).\
                                eigensystem().vectors().as_numpy_array()

        com = np.array(atoms.extract_xyz().sum())/float(atoms.size())

        return com, axes



    # -------------------------------------------------------------------------


    def align_and_refine_inplace(self, reference_ph, other_ph, verbose=True):


        com_reference, axes_reference = self._com_and_moments_of_inertia(reference_ph.atoms())
        com_other, axes_other = self._com_and_moments_of_inertia(other_ph.atoms())

        R = np.dot(axes_reference.transpose(), np.linalg.inv(axes_other.transpose()))


        f_min = 9999.9
        atoms_xyz_min = other_ph.atoms().extract_xyz()


        for perm in itertools.permutations([0,1,2]):


            _R = R[perm,:]
            if np.linalg.det(_R)<0: continue


            _ph = other_ph.deep_copy()


            for atom in _ph.atoms():
                atom.set_xyz( np.dot(_R,atom.xyz-com_other)+com_reference )



            minimized = mmtbx.refinement.real_space.rigid_body.refine(
                  residue                         = _ph,
                  density_map                     = self.ref_map_data.as_double(),
                  geometry_restraints_manager     = None,
                  real_space_target_weight        = 1,
                  real_space_gradients_delta      = self.d_min*self.resolution_factor,
                  lbfgs_termination_params        = scitbx.lbfgs.termination_parameters(max_iterations = 200),
                  unit_cell                       = self.unit_cell,
                  cctbx_geometry_restraints_flags = None,
                  states_collector                = None)


            if minimized.f_final < f_min:
                atoms_xyz_min = minimized.sites_cart_residue
                f_min = minimized.f_final



        other_ph.atoms().set_xyz(atoms_xyz_min)


    # -------------------------------------------------------------------------

    def map_score(self, ph, d_min=6, atom_radius=10):

        xrs = xray.structure(crystal_symmetry=crystal.symmetry(
                                    unit_cell=self.unit_cell,
                                    space_group_symbol="P1"),
                             scatterers=ph.extract_xray_structure().scatterers())

        xrs.set_sites_cart(ph.atoms().extract_xyz())
        xrs_fc = xrs.structure_factors(d_min=d_min).f_calc()


        small_gridding = maptbx.crystal_gridding(unit_cell=xrs_fc.unit_cell(),
                                      space_group_info=xrs_fc.space_group_info(),
                                      pre_determined_n_real= self.n_real)

        xrs_fft_map = miller.fft_map(crystal_gridding=small_gridding,
                                    fourier_coefficients=xrs_fc)
        xrs_fft_map.apply_sigma_scaling()
        xrs_map = xrs_fft_map.real_map_unpadded()


        grid_indices = maptbx.grid_indices_around_sites(
                    unit_cell=self.unit_cell,
                    fft_n_real=self.n_real,
                    fft_m_real=self.n_real,
                    sites_cart=flex.vec3_double(ph.atoms().extract_xyz()),
                    site_radii=flex.double(ph.atoms().size(), atom_radius))


        x=xrs_map.select(grid_indices)
        y=self.ref_map_data.as_double().select(grid_indices)


        corr = flex.linear_correlation( x, y ).coefficient()


        #print spearmanr( x.as_numpy_array(), y.as_numpy_array() ).correlation
        #print pearsonr( x.as_numpy_array(), y.as_numpy_array() )


        #xrs_fft_map.as_ccp4_map("map.ccp4")


        return corr



    # -------------------------------------------------------------------------


    def dummy_score(self, ph, selstr=r"name P"):

        sel_cache = ph.atom_selection_cache()
        assert sel_cache.n_seq == ph.atoms_size()
        isel = sel_cache.iselection

        ph_atoms = ph.select(isel(selstr)).atoms()

        data = []
        query = []
        for atm in self.dummy_model.atoms():
            data.extend( atm.xyz )

        for patm in ph_atoms:
            query.extend(patm.xyz)


        # ---

        AA = AnnAdaptor(data,3)
        AA.query(query)

        matched = 0.0

        for ii in range(len(AA.nn)):
            if AA.distances[ii]<self.dummy_dist_sq:
                matched+=1.0

        # model atoms covered with dummy atoms
        ph_score = 3.0*matched/float(len(query))

        # ---

        AA = AnnAdaptor(query,3)
        AA.query(data)

        matched = 0.0

        for ii in range(len(AA.nn)):
            #print AA.distances[ii]
            if AA.distances[ii]<100:#self.dummy_dist_sq:
                matched+=1.0

        # frac of dummy atoms that are covered with a model
        dummy_score =  3.0*matched/float(len(data))

        #print ph_score, dummy_score

        return ph_score*dummy_score#matched/float(ph_atoms.size())


    # -------------------------------------------------------------------------



    def fit_model(self, pdb_string=None, pdb_model=None):


        assert [pdb_string, pdb_model].count(None)==1


        if pdb_model is None:

            _ph = iotbx.pdb.input(source_info=None, lines=pdb_string).construct_hierarchy()

        else:
            _ph = pdb_model.deep_copy()


        if self.dummy_model is None:
            self.scorch( na_number = _ph.overall_counts().n_residues )


        self.align_and_refine_inplace(self.dummy_model, _ph)


        #_ph.atoms().set_xyz( _ph_atoms().extract_xyz() + self.sites_cart_shift )


        return _ph, self.map_score(_ph)



    # -------------------------------------------------------------------------



    def tst_dummy_model_fit(self, ph, ofname_base=None):



        if self.dummy_model is None:

            self.scorch( na_number = ph.overall_counts().n_residues )
            if ofname_base: self.dummy_model.write_pdb_file("%s_dummy.pdb" % ofname_base)



        rot_obj = mmtbx.refinement.rigid_body.rb_mat_zyz(phi = 90, psi = 180, the = 41)

        sel_cache = ph.atom_selection_cache()
        assert sel_cache.n_seq == ph.atoms_size()
        isel = sel_cache.iselection



        xrs_poor = ph.select(isel(r"all")).extract_xray_structure().deep_copy_scatterers()
        xrs_poor.apply_rigid_body_shift( rot = rot_obj.rot_mat().as_mat3(), trans = [100,40,20] )

        _ph = ph.select(isel(r"all")).deep_copy()
        _ph.adopt_xray_structure(xrs_poor)
        if ofname_base: _ph.write_pdb_file(file_name = "%s_dummy_poor.pdb" % ofname_base)

        print("Dummy score before: %.2f" % self.dummy_score(_ph.select(isel(r"name P"))))


        self.align_and_refine_inplace(self.dummy_model, _ph)
        if ofname_base: _ph.write_pdb_file(file_name = "%s_dummy_refined.pdb" % ofname_base)


        print("Dummy score after: %.2f" % self.dummy_score(_ph))



    # -------------------------------------------------------------------------



    def tst_rigid_body_fit(self, ph, ofname_base=None):


        # pymol
        # isomesh map, xyz_map, 2.0, (xyz_tst_target)


        rot_obj = mmtbx.refinement.rigid_body.rb_mat_zyz(phi = 90, psi = 180, the = 41)

        sel_cache = ph.atom_selection_cache()
        assert sel_cache.n_seq == ph.atoms_size()
        isel = sel_cache.iselection



        xrs_poor = ph.select(isel(r"all")).extract_xray_structure().deep_copy_scatterers()
        xrs_poor.apply_rigid_body_shift( rot = rot_obj.rot_mat().as_mat3(), trans = [100,40,20] )

        _ph = ph.select(isel(r"all")).deep_copy()
        _ph.adopt_xray_structure(xrs_poor)
        if ofname_base: _ph.write_pdb_file(file_name = "%s_rb_poor.pdb" % ofname_base)

        rmsd_before = math.sqrt((_ph.atoms().extract_xyz() - ph.atoms().extract_xyz()).sum_sq()/ph.atoms().size())
        self.align_and_refine_inplace(ph, _ph)
        rmsd_after = math.sqrt((_ph.atoms().extract_xyz() - ph.atoms().extract_xyz()).sum_sq()/ph.atoms().size())

        print("RMSD before/after : %.2f/%.2f" % ( rmsd_before, rmsd_after))

        if ofname_base:
            _ph.write_pdb_file("%s_rb_refined.pdb" % ofname_base)

    # -------------------------------------------------------------------------


    def calc_density(self, sigma_cutoff=1.5, ofname_base=None):


        map_3d =self.ref_map.real_map_unpadded().as_numpy_array()

        grids = numpy.where(map_3d>sigma_cutoff)

        grid2cart = maptbx.grid2cart(self.ref_map._n_real,self.unit_cell.orthogonalization_matrix())



        cubicle_volume = numpy.prod( grid2cart((1,1,1)) )


        tmp_structure = self._create_tmp_ph()
        chain = tmp_structure.only_chain()

        idx=0
        for (i,j,k) in zip(grids[0], grids[1], grids[2]):
            xyz = grid2cart((i,j,k))#, map_3d[i,j,k]

            rg = iotbx.pdb.hierarchy.residue_group(resseq = "%i"%(idx%10000))
            ag = iotbx.pdb.hierarchy.atom_group(resname = "XXX", altloc = " ")
            atom = iotbx.pdb.hierarchy.atom()

            ag.append_atom(atom)
            rg.append_atom_group(atom_group = ag)
            chain.append_residue_group(rg)

            atom.name = "P"
            atom.set_element('P')
            atom.set_xyz(xyz)
            atom.set_occ(1.0)
            atom.set_b( 0.00 )

            idx += 1

        vol = cubicle_volume * idx

        nres = self.pdb_model.overall_counts().n_residues

        print("%10i %10.2f %10.5f"  % (nres, vol, vol/float(nres)))


        if ofname_base:
            tmp_structure.write_pdb_file("%s_dummy.pdb"%ofname_base)

# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------


def extract_test_model(simple=False):

    if simple:
        #return iotbx.pdb.input(source_info=None, lines=pdb_test).construct_hierarchy()
        return pdb_test
    with gzip.GzipFile(mode='r', fileobj=io.StringIO(base64.b64decode(pdb_string_gz))) as pdbgz:
        #return iotbx.pdb.input(source_info=None, lines= pdbgz.read()).construct_hierarchy()
        return pdbgz.read()



# -----------------------------------------------------------------------------


def calc_density(ifname, d_min):

    with open(ifname, 'r') as ifile:
        pdb_string = ifile.readlines()


    bfit = blobfit(pdb_string=pdb_string, d_min=d_min, debug=False, debug_ofname_base=None)

    bfit.calc_density(sigma_cutoff=2.0, ofname_base=None)


# -----------------------------------------------------------------------------


def _test_map(ofname=None):

    with open("blobfit_test.pdb", 'r') as ifile:
        pdb_model = iotbx.pdb.input(source_info=None, lines=ifile.readlines()).construct_hierarchy()

    bfit = blobfit(map_fname="blobfit_test.ccp4")

    print(" -> Testing rigid body fit for dummy-atom target")
    bfit.tst_dummy_model_fit(pdb_model, ofname)
    print()


# -----------------------------------------------------------------------------



def _test_model(ofname=None):


    pdb_string = extract_test_model(False)
    bfit = blobfit(pdb_string=pdb_string, debug=False, debug_ofname_base=ofname)


    if ofname: bfit.pdb_model.write_pdb_file("%s_input.pdb" % ofname)




    print(" -> Testig rigid body for RNA target")
    bfit.tst_rigid_body_fit(bfit.pdb_model, ofname)
    print()

    print(" -> Testing rigid body fit for dummy-atom target")
    bfit.tst_dummy_model_fit(bfit.pdb_model, ofname)
    print()



# -----------------------------------------------------------------------------


def run_tests():


    print(" -> Running map-based tests\n")

    _test_map()


    print(" -> Running model-based tests\n")


    _test_model()


# -----------------------------------------------------------------------------


def calc_dummy(mapin=None, debug=False):
    bfit = blobfit(map_fname=mapin)

    print("Map ready...")

    #bfit.scorch( na_number=190, seed_xyz=(290.71,172.17,273.14), debug=debug )
    #bfit.scorch( na_number=60, seed_xyz=(31.65,12.55,71.11), debug=debug )
    #bfit.scorch( na_number=260, seed_xyz=(131.4,178,138.8), debug=debug )
    bfit.scorch( na_number=70, seed_xyz=(45,45,45), debug=debug )

    print(bfit.dummy_model)

    bfit.dummy_model.write_pdb_file("xyz.pdb")


    pass


# -----------------------------------------------------------------------------



def fit_model(pdbin, mapin=None, refin=None, pdbout=None, debug=False, seed=None):


    assert [mapin,refin].count(None) == 1, Exception("Cannot process mapin and pdbin simultaneously")

    with open(pdbin, 'r') as ifile:
        pdb_model = iotbx.pdb.input(source_info=None, lines=ifile.readlines()).construct_hierarchy()

    if mapin:
        bfit = blobfit(map_fname=mapin, debug=debug)

    else:
        with open(refin, 'r') as ifile:
            bfit = blobfit(pdb_string=ifile.readlines(), debug=False, debug_ofname_base=None)


    with open(pdbin, 'r') as ifile:
        pdb_model = iotbx.pdb.input(source_info=None, lines=ifile.readlines()).construct_hierarchy()


    if seed is not None:
        seed = list(map(float, seed.split(',')))

    bfit.scorch(na_number=pdb_model.overall_counts().n_residues, debug=debug, seed_xyz=seed)
    ph,score = bfit.fit_model(pdb_model=pdb_model)
    print("Dummy score: %.2f" % score)
    print("Map score:   %.2f" % bfit.map_score(ph))

    if pdbout:
        print("Writing output to: %s" % pdbout)
        ph.write_pdb_file(pdbout)
        bfit.dummy_model.write_pdb_file("%s_dummy.pdb"%pdbout.split('.')[0])

    pass


# -----------------------------------------------------------------------------


def parse_args():
    """setup program options parsing"""
    parser = OptionParser()


    parser.add_option("-o", "--pdbout", action="store", \
                            dest="pdbout", type="string", metavar="FILENAME", \
                  help="input model superposed onto reference (model or map)", default=None)

    parser.add_option("-i", "--pdbin", action="store", dest="pdbin", type="string", metavar="FILENAME", \
                  help="model to move", default=None)

    parser.add_option("-m", "--mapin", action="store", dest="mapin", type="string", metavar="FILENAME", \
                  help="refernce map in CCP4 format", default=None)

    parser.add_option("-r", "--refin", action="store", dest="refin", type="string", metavar="FILENAME", \
                  help="reference model in PDB format", default=None)

    parser.add_option("-t", "--tests", action="store_true", dest="tests", default=False, \
                  help="run basic tests and exit")

    parser.add_option("--seed", action="store", dest="seed", type="string", metavar="xyz", \
                  help="X,Y,Z", default=None)


    parser.add_option("--dmin", action="store", type="float", \
                            dest="dmin_user", default=20.0, metavar="FLOAT",\
                            help="Resolution of the map calculated for reference pdb model"
                                    " [default: %default]")

    parser.add_option("--debug", action="store_true", dest="debug", default=False, \
                  help="debugging output...")

    (options, _args)  = parser.parse_args()
    return (parser, options)


# -----------------------------------------------------------------------------


def main():



    (parser, options) = parse_args()
    if [options.mapin, options.pdbin, options.refin].count(None)>2 and not options.tests:
        parser.print_help()
        exit(1)


    if not options.debug: sys.tracebacklimit = 0

    if options.tests:
        run_tests()


    elif options.pdbin and not (options.mapin or options.refin):
        calc_density(options.pdbin, d_min=options.dmin_user)

    elif options.pdbin and options.mapin:
        fit_model(pdbin=options.pdbin, mapin=options.mapin, pdbout=options.pdbout, debug=options.debug, seed=options.seed)

    elif options.pdbin and options.refin:
        fit_model(pdbin=options.pdbin, refin=options.refin, pdbout=options.pdbout, debug=options.debug)

    elif options.mapin and not (options.pdbin or options.refin):
        calc_dummy(options.mapin, options.debug)

    else:
        print("Unrecognized options")





# -----------------------------------------------


if __name__=="__main__":

    main()



