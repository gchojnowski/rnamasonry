#! /usr/bin/env libtbx.python
# -*- coding: utf-8 -*-
# =============================================================================
# Created at:
#  International Institute of Molecular and Cell Biology in Warsaw, Poland
# Author:
#  Grzegorz Chojnowski - gchojnowski@genesilico.pl, gchojnowski@gmail.com
# =============================================================================


import sys,os,re
import subprocess
import tempfile
from mutate import pdb_test
import glob

import base64, zlib
import string

CRYSOL_BIN=os.environ.get('CRYSOL_BIN', 'crysol')
CRYSON_BIN=os.environ.get('CRYSON_BIN', 'cryson')

# base64.b64encode(zlib.compress(s))
test_saxs_data = """eJxll8mOXDcMRff5itoHaYuz6F0C5wccIHsHzrQI2jC8yOfnvqFIqVOrUr/XRxR5ecV6fPj7j3c/fnv9590vf3368vu7n16/fn78+/Ll82+P+/Ph6+v7x2O8DBmPj5/eP+hFp8rj45/463wZ4/Hrt9dv+P4gjnh51Oe785/Oz8/f4zWsiNVIsPJjpWLK+lyREXM8Vx5DfGKlJ4YuzA+DbgzrbIwGRWNGznrmHimN4TcYUYvGsA9bMBzWmOHZGHmDYcReGE6iWpHmyMbonEMKo28wQ5ZD4YjBjQn1URhhTS2M7RiaIyqpSpN0icZkdoqJ6SzNhfE3GBPvQ9GI2blRnlmRGs6kfah4g2GUvDCDeS7RjEGNcWzRKZ47ZszwfL4qyUG1IvEIL4yM6R1NvsFoRp1fJlMfg0S4y6/QEP9Pfk8Vj5G9v8SY3LnhFOkqurF5Y2jFpLueurETYzMW3bDx2igjuAtOvGEYcXth1LKzgTfnmjeLXKKRFYME21mNCyMc0ikmFy1NQRfjzNSN0Q0jQ0QLQ4kQGkMelRu02zyleWNsxcSUnJ2bYayNQS9SrZDv0CUa3zASOeKJQWcytd8MJKcSzs4jWzcUK8YT5ahDweXIKsUZzNbP5NbUjZkbRuclsetVF+oOT5a5RDpgI9mY3DDIRvSOmkujzkCJ8/mM0LTeGN5UbI7CzQ7cZucG2jet9JMZnbHdmE3FSIx3wZnZW7cxWYgLIxAgN2ZTsc4Q7tzAw7nkhyqK1xZwJjRcYzYVqwFbaaQM6wvGU/KM7XyWUy9NyYXZVKyU0j1F06KdGS1NWpgpsBtvzKZimbff39WA/dah4L48KjeBhhdqzKZiQcWlo/HD/8rvQnnUM4duziremE3F6EvrgsPfs1OMewEKeD4zXCLCjdlULOg/aYmpmlXBDTdBP8N3nQtmUzEjN9y5Eei2bTOQjYoG/s7nrSXr5fvE+MyWHyYQt778cE9ROyOReqdYNhXzoanODQ8a7b643bUw7CreBZdNxRgoxPpQBPVXh8tM9e5wXIuajdlUzIf8um+G+yy/E7cYfWDYm1tjNhUf90LviPPGYuIq4b2FU2Y3g2wqRjCktSNaOKzvCahGegtYMS3RbCrGJOL9ag6+0ngbvOZiFIQct25kUzHFcf1W+5nNxX0nJoGC5nQ6+50vzKZiOp5VNJOglKrbMV3E0uHHLNqYTcWQRnY0AaFqHYPRm+1FAdNILozuEwXOK20GYtbuw/i/hjph2BiN2VSMLhptokibZZk4nPgaIa8OV2RqwWwqRhcx944eTp1iXG/dDNjumkRvzKZiQhb7noJLSkNx9WbWSif8JhujO+ZouXY4DE3dYRjTBjVGrw1vzK7iYYsXW+DTJjaTZrvfyGvaujGbihG1XiP8+aoN6twct33Lz0z1nHZuzKZiWK+eoV4YFqGqDRQuUbpxKPUcU27MpuJ5TCb2xGC4ZetmdBldcIxMkgtmU/E8fjRQYYLmXC//EYWBauj0Yl5/ejynLQsedSicnrQPpbjFazX9+A1zYv4DQCG8Gg=="""

test_sans_data = """eJxdl8uO21YMhvd5Cu+LTni/zK5F+gIp0H2KpG0WxQzSeX+UkmzynPFOlvWJh/z5k759+v7Xx1/eXv79+Ps/X16/ffz15cfX2+vbt//enl6//nmrz6cfL883eKIkvH3+8nzDJwnh2+e/j28Bbn+8vbw91+8QTZ5uH27nt8fnt5/q7vWkgdWVHFcmBAmPK0Q1k77HquR1RScGL8zPgHcMQgymHuUFI0GDYSJuDL3DgBHMT6PeORhJnkip4snG8I6JQFsw7IQLBrhjM1Q4Y7sw8g4jrssbnTIGQx6N0Uw7X3Fh9B0GRHLemDhXWJ+BagDFpNh2jOt188JAsnZtEJKyr1RdYqLxHWPJjBN4wJQfwSHnHpdoJjfxDkMmNoEr4RKNGHbBFZh8Cp47RpWhD6VeB/HBYERXUSr7MNHgrmJxXgKvBEQnNSOJjoTziZE6pA4GNwxHjlKVU6hLnOp2RnNhIJhjMLRjAK11q1SpmWhIRPGB4aop2WB4w1RL0aQYqqn6iFGBnrW5MEK0VAplw1Tv66QxVCa2usCzwy4MZJyaumN0wwDhZEPMeDos0EtVD0w1hviSG1sxmKTUShGxlI7Go0rTh6JyMh8Vo2+YoBwVSwlsDuXChtEYgkrrYGLDOFcCGgPucygH09P9+IqbbekpzA1jAtK54Ygyp25Uq0pZYywpJje0qfjwSR+MRbnKamLQukFxkNENbSpG0RJrY6qKvhhVsHSlsMwgJze0qRi5ktPZKM/QiUaN01o31bQIkxvaVIyHpw8Gq1DTGtX73AUvX4I4XoEXRnZMGXc/SGlMbaKS4dHReCqcer9jNhVjvR87N+RpsWg6iLtS5iqZg9lVDCbZRlHVLv03ppzIOxp1irNt75hNxZnuPn0jYdSxVbeb9b1qW+LlUJuKk2tO9Rup3CAWMapzN0OZX56D+Y7ZVByuMUZZYnBdxIiC3Qxc4+WcWrgO3wemVgicQ0FNv27U8swr4de91Gsy3DGbiiujEa0NzApgUVH5RHtxVdhodMObip2opmFjoqbRoiKPsfSyt/DlUJuK7fjl9I3L1YwPFTlMMwTEIj/eVHxsNNZvLF9elqaaqEnzCmX10Q1vKjaoyTC50do9BiMuOgdm2FK8qbi2O12iEa9R2Zha06DFUGOSY4lmU7GW/GTBQHVDY6oY2bkpoZbEj+cuzKbi6loeM0DmZU05VITdmp5MZxPfMZuKa/Rcc/LhcLHsN9Xu05rmfvk0rLvfA1PL1uL+RzPMXhym7t3hYbhEI5uKpcZULGlUxInGHbjzpjUZzxXmjtlULMfr51DHnJ41qeaSdDTiZUYxmE3FHA7T4bWm5Zho2XtI50bUPJdDbSrm6gZaogGa3QurMWe/OSdRDmZTMUsuuklNmI0Gj82gCy7BxKMb2VRcojHrQ5X38ir/qnFDlUrxMJhNxXxsSd1+4WX/PphaGjsajdoZbDCbimv1Q+0Hg2ttWMZtLTQjP1XRpVKbikt8qm3b9bdAVxWFQR+xjnvtF7D+9XhgSuFTVDdfanP8gxlLr4Je/27gw/+TSrsz"""

# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------

class pysaxs:


    def __init__(self, foxs_path=None, cryson_opts=None):

        self.foxs_path = foxs_path
        self.cryson_opts = cryson_opts

        pass

    # -------------------------------------------------------------------------

    def check(self, with_dummy_data=False, debug=False):
        """
            try to run crysol/foxs with a dummy dataset
                or check versions (less reliable as the version listing changes fast)
        """

        if with_dummy_data:
            if self.cryson_opts:
                _chisq = self.run( pdb_test, \
                        zlib.decompress(base64.b64decode(test_sans_data)).decode("utf-8"), \
                        sans_opts = '-D2O 0.5 -per 1.0', \
                        verbose=debug)
            else:
                _chisq = self.run( pdb_test, \
                        zlib.decompress(base64.b64decode(test_saxs_data)).decode("utf-8"), \
                        verbose=debug)

            return _chisq>-3

        if self.foxs_path:
            # check if foxs works on that system
            return self.check_foxs_availability()

        else:
            return self.check_crysol_version()


    # -------------------------------------------------------------------------

    def check_foxs_availability(self):

        raw_output = []

        foxs_pipe = subprocess.Popen(self.foxs_path, \
                                             shell=True, \
                                             stdout=subprocess.PIPE, \
                                             stderr=subprocess.PIPE)

        raw_output.extend(foxs_pipe.communicate()[0].split('\n'))


        retcode = subprocess.Popen.wait(foxs_pipe)

        for _line in raw_output:
            if _line.startswith("Usage"): return True


        return False


    # -------------------------------------------------------------------------


    def check_crysol_version(self):

        raw_output = []

        crysol_pipe = subprocess.Popen("%s -v"%CRYSOL_BIN, \
                                                 shell=True, \
                                                 stdout=subprocess.PIPE, \
                                                 stderr=subprocess.PIPE)

        raw_output.extend(crysol_pipe.communicate()[0].split('\n'))


        retcode = subprocess.Popen.wait(crysol_pipe)

        for line in raw_output:
            if re.match(r"^crysol, ATSAS 2\.8\.[34] \([\S]+\)", line): return True
            if line.strip().startswith('crysol'):

                ver = line.split()[1].strip()
                if ver == "v2.8.3":
                    return True
                else:
                    return False

        return None


    # -------------------------------------------------------------------------

    def clean_tmp_files(self, pdb_fname, saxs_fname, cwd):

        if self.foxs_path:
            for _fn in [pdb_fname[:-4]+".plt", 
                        pdb_fname+".dat",
                        pdb_fname[:-4]+"_%s"%os.path.basename(saxs_fname[:-4])+".plt",
                        pdb_fname[:-4]+"_%s"%os.path.basename(saxs_fname[:-4])+".dat"]:
                try:
                    os.remove( _fn )
                except:
                    pass
        else:
            fn_base = cwd+'/'+os.path.basename(pdb_fname)[:-4]
            for suff in ['00.log', '00.fit']:
                try:
                    os.remove( fn_base+suff )
                except:
                    pass

    # -------------------------------------------------------------------------


    def test(self):

        raw_output = []


        tmp_file = tempfile.NamedTemporaryFile(delete=False, suffix=".pdb")
        tmp_file.write(pdb_test)
        tmp_file.close()


        crysol_pipe = subprocess.Popen("%s %s -lm 18" % (CRYSOL_BIN, tmp_file.name), \
                                                 shell=True, \
                                                 stdout=subprocess.PIPE, \
                                                 stderr=subprocess.PIPE)

        raw_output.extend(crysol_pipe.communicate()[0].split('\n'))


        retcode = subprocess.Popen.wait(crysol_pipe)
        os.unlink(tmp_file.name)


        self.clean_tmp_files(tmp_file.name)


        print("\n --> ".join(raw_output))


    # -------------------------------------------------------------------------


    def run(self, pdbstring, saxs_data, verbose=False, sans_opts=None):


        with tempfile.TemporaryDirectory() as tmpdirname:
            tmp_pdbfile=os.path.join(tmpdirname,"model.pdb")
            tmp_saxsfile=os.path.join(tmpdirname,"saxs.dat")


            with open(tmp_pdbfile, "w") as ofile:
                ofile.write(pdbstring)

            with open(tmp_saxsfile, "w") as ofile:
                ofile.write(saxs_data)


            if self.foxs_path:
                chisq = self.run_foxs(tmp_pdbfile, tmp_saxsfile, verbose)
            elif self.cryson_opts:
                chisq = self.run_cryson(tmp_pdbfile, tmp_saxsfile, verbose, opts=sans_opts)
            else:
                chisq = self.run_crysol(tmp_pdbfile, tmp_saxsfile, verbose)


        return chisq

    # -------------------------------------------------------------------------

    def run_cryson(self, pdb_fname, saxs_fname, verbose=False, opts=None):
        #V2: model.pd  Dro:0.030  Ra:1.480  RGE: 7.72  RGT: 7.71  Vol:  1158.  Chi^2: 0.000

        template_v2=r".*Chi\^2\:\s+(?P<chisq>[\d\.]+)\s*"

        template_v3=r"\s*Model\:\s*(?P<fname>[\w\.]+)\s+Rg\:\s+(?P<rg>[\d\.]+)\s+Run: 00  Chi\^2\:\s+(?P<chisq>[\d\.]+)\s*"

        with tempfile.TemporaryDirectory() as tmp_path:

            raw_output = []

            crysol_pipe = subprocess.Popen("%s %s %s %s" % (CRYSON_BIN,
                                                            pdb_fname,
                                                            saxs_fname,
                                                            opts if opts else self.cryson_opts), \
                                                            cwd=tmp_path, \
                                                            shell=True, \
                                                            universal_newlines=True,
                                                            stdout=subprocess.PIPE, \
                                                            stderr=subprocess.PIPE)

            os.system(f'ls {tmp_path}')

            for stdout_line in iter(crysol_pipe.stdout.readline, ""):
                _m = re.match(template_v2, stdout_line)
                if verbose: print("*V2" if _m else " V2: ", stdout_line.strip())
                if _m: return float(_m.group('chisq'))

            if os.path.isfile(os.path.join(tmp_path, 'cryson_summary.txt')):
                with open(os.path.join(tmp_path, 'cryson_summary.txt'), 'r') as ifile:
                    for line in ifile.readlines():
                        _m = re.match(template_v3, line)
                        if verbose: print("*V3" if _m else " V3: ", line.strip())
                        if _m: return float(_m.group('chisq'))

        return -666

    # -------------------------------------------------------------------------


    def run_crysol_v28x(self, pdb_fname, saxs_fname, verbose=False):

        with tempfile.TemporaryDirectory() as tmp_path:
            raw_output = []

            crysol_pipe = subprocess.Popen("%s %s %s -cst" % (CRYSOL_BIN, pdb_fname, saxs_fname), \
                                                                   cwd=tmp_path, \
                                                                   shell=True, \
                                                                   stdout=subprocess.PIPE, \
                                                                   stderr=subprocess.PIPE)

            raw_output.extend(crysol_pipe.communicate()[0].split('\n'))
            retcode = subprocess.Popen.wait(crysol_pipe)


            os.unlink(pdb_fname)
            os.unlink(saxs_fname)
            self.clean_tmp_files(pdb_fname, saxs_fname, tmp_path)

            if verbose: print("\n --> ".join(raw_output))

            for line in raw_output:
                if line.find("Chi^2:")>0:
                    try:
                        return string.atof(line.split(":")[-1])
                        # filter unrealistic chi^2=0.0 for simulated dat
                        return max(1.0, string.atof(line.split(":")[-1]))
                    except:
                        return -666

        return -666

    # -------------------------------------------------------------------------


    def run_crysol(self, pdb_fname, saxs_fname, verbose=False):
        # it will check for both v2.8.x and v3.x
        #V3: Chi-square of fit ...................................... : 1.059
        #V2: model.pd  Dro:0.030  Ra:1.480  RGE: 7.72  RGT: 7.71  Vol:  1158.  Chi^2: 0.000

        template_v3=r".*Chi\-square\sof\sfit[\s\.]+\:\s*(?P<chisq>[\d\.]+)\s*"
        template_v2=r".*Chi\^2\:\s+(?P<chisq>[\d\.]+)\s*"
        chisq=-666
        with tempfile.TemporaryDirectory() as tmp_path:
            raw_output = []
            crysol_pipe = subprocess.Popen("%s %s %s" % (CRYSOL_BIN, pdb_fname, saxs_fname),
                                                                   cwd=tmp_path,
                                                                   shell=True,
                                                                   universal_newlines=True,
                                                                   stdout=subprocess.PIPE,
                                                                   stderr=subprocess.PIPE)

            for stdout_line in iter(crysol_pipe.stdout.readline, ""):
                _m = re.match(template_v2, stdout_line)
                if _m: chisq=float(_m.group('chisq'))

                _m = re.match(template_v3, stdout_line)
                if _m: chisq=float(_m.group('chisq'))

        return chisq

    # -------------------------------------------------------------------------

    def run_foxs(self, pdb_fname, saxs_fname, verbose=False):
        #examples/1EHZ.pdb examples/tRNA_saxs.dat Chi = 1.2318 c1 = 0.95 c2 = -0.8528 default chi = 1.44192
        template=r".+Chi\s+\=\s+(?P<chisq>[\d\.]+)\s+.+"
        chisq=-666
        with tempfile.TemporaryDirectory() as tmp_path:
            raw_output = []

            foxs_pipe = subprocess.Popen("%s %s %s" % (self.foxs_path, pdb_fname, saxs_fname),
                                                                   shell=True,
                                                                   universal_newlines=True,
                                                                   stdout=subprocess.PIPE, 
                                                                   stderr=subprocess.PIPE)

            for stdout_line in iter(foxs_pipe.stdout.readline, ""):
                _m = re.match(template, stdout_line)
                if _m: chisq=float(_m.group('chisq'))

        return chisq


    # -------------------------------------------------------------------------



# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------


def test():
    co = pysaxs(foxs_path = None)#"../lib/foxs")
    print(co.check(with_dummy_data=True))

    try:
        assert co.run( pdb_test, zlib.decompress(base64.b64decode(test_saxs_data)) )<5e-2
    except:
        print("ERROR")
        exit(0)

    print("OK")

if __name__ == "__main__":
    test()

