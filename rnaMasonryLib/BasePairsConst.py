# -*- coding: utf-8 -*-
# =============================================================================
# Created at:
#  International Institute of Molecular and Cell Biology in Warsaw, Poland
# Author:
#  Grzegorz Chojnowski - gchojnowski@genesilico.pl
#  Kasia Mikołajczak - kmikolajczak@genesilico.pl
# =============================================================================



base_pairs_families = (

## ----------------------------------------   WWC

('AAWW_cis', [("N6", "N1"), ("N1", "C2")]),
('CCWW_cis', [("O2", "N3"), ("N3", "N4")]),
# last one is a water-mediated interaction
('UUWW_cis', [("O4", "N3"), ("N3", "O2"), ("O2", "O2'")]),

('ACWW_cis', [("N6", "N3"), ("N1", "O2")]),
('CAWW_cis', [("N3", "N6"), ("O2", "N1")]),
('GAWW_cis', [("O6", "N6"), ("N1", "N1")]),
('AGWW_cis', [("N6", "O6"), ("N1", "N1")]),
('GCWW_cis', [("O6", "N4"), ("N1", "N3"), ("N2", "O2")]),
('CGWW_cis', [("N4", "O6"), ("N3", "N1"), ("O2", "N2")]),
('UAWW_cis', [("O4", "N6"), ("N3", "N1"), ("O2", "C2")]),
('AUWW_cis', [("N6", "O4"), ("N1", "N3"), ("C2", "O2")]),
# last one is a water-mediated interaction
('UCWW_cis', [("O4", "N4"), ("N3", "N3")]),
('CUWW_cis', [("N4", "O4"), ("N3", "N3")]),
# last one is a water-mediated interaction CHECK!
('GUWW_cis', [("O6", "N3"), ("N1", "O2"), ("N2", "O2'")]),
('UGWW_cis', [("N3", "O6"), ("O2", "N1"), ("O2'", "N2")]),
#('UGWW_cis', [("N3", "O6"), ("O2", "N1"), ("O2'", "N2")]),



## ----------------------------------------   WWT

('AAWW_tran', [("N6", "N1"), ("N1", "N6")]),
('UUWW_tran', [("O4", "N3"), ("N3", "O4")]),
('GGWW_tran', [("O6", "N1"), ("N1", "O6")]),
('CCWW_tran', [("N4", "O2"), ("N3", "N3"), ("O2", "N4")]),

('ACWW_tran', [("N6", "N3"), ("N1", "N4")]),
('CAWW_tran', [("N3", "N6"), ("N4", "N1")]),
('GCWW_tran', [("N1", "O2"), ("N2", "N3")]),
('CGWW_tran', [("N3", "N2"), ("O2", "N1")]),
('UAWW_tran', [("O4", "C2"), ("N3", "N1"), ("O2", "N6")]),
('AUWW_tran', [("N6", "O2"), ("N1", "N3"), ("C2", "O4")]),
('UCWW_tran', [("O4", "O2"), ("N3", "N3"), ("O2", "N4")]),
# last one is a water-mediated interaction
('CUWW_tran', [("N4", "O2"), ("N3", "N3"), ("O2", "O4")]),
('GUWW_tran', [("O6", "N3"), ("N1", "O2")]),
# CHECK!
('UGWW_tran', [("O2", "N1"), ("N3", "O6")]),
#('UGWW_tran', [("O4", "N1"), ("N3", "O6")]),

## ----------------------------------------   WHC

('CCWH_cis', [("N3", "N4"), ("O2", "C5")]),
('GGWH_cis', [("N1", "O6"), ("N2", "N7")]),
('UUWH_cis', [("N3", "O4"), ("O2", "C5")]),

('GAWH_cis', [("O6", "N6"), ("N1", "N7")]),
('AGWH_cis', [("N6", "O6"), ("N1", "N7")]),
('UAWH_cis', [("O4", "N6"), ("N3", "N7"), ("O2", "C8")]),
('AUWH_cis', [("N6", "O4"), ("N1", "C5")]),
('CGWH_cis', [("N4", "O6"), ("N3", "N7")]),
('CUWH_cis', [("N4", "O4"), ("N3", "C5")]),
('UGWH_cis', [("N3", "N7"), ("O2", "C8")]),


## ----------------------------------------   WHT

('AAWH_tran', [("N6", "N7"), ("N1", "N6")]),
('UUWH_tran', [("O4", "C5"), ("N3", "O4")]),
('CCWH_tran', [("N3", "C5"), ("O2", "N4")]),
('GGWH_tran', [("N1", "N7"), ("N2", "O6")]),

('CAWH_tran', [("N4", "N7"), ("N3", "N6")]),
# last hbond C-H ... O is NOT indicated in the paper
('UAWH_tran', [("N3", "N7"), ("O2", "N6"), ("O4", "C8")]),
('AGWH_tran', [("N6", "N7"), ("N1", "O6")]),
('CGWH_tran', [("N4", "N7"), ("N3", "O6")]),
('GUWH_tran', [("O6", "C5"), ("N1", "O4")]),
('UGWH_tran', [("O4", "C8"), ("N3", "N7")]),

## ----------------------------------------   WSC

('AAWS_cis', [("N6", "N3"), ("N1", "O2'")]),
('CCWS_cis', [("N4", "O2"), ("N3", "O2'")]),
('GGWS_cis', [("O6", "N2"), ("N1", "N3"), ("N2", "O2'")]),
('UUWS_cis', [("N3", "O2"), ("O2", "O2'")]),

('ACWS_cis', [("N6",  "O2"), ("N1", "O2'")]),
('CAWS_cis', [("N4", "N3"), ("N3", "O2'")]),
('GAWS_cis', [("O6", "C2"), ("N1", "N3"), ("N2", "O2'")]),
('GCWS_cis', [("N1", "O2"), ("N2", "O2'")]),
('UAWS_cis', [("O4", "C2"), ("N3", "N3"), ("O2", "O2'")]),
('UCWS_cis', [("N3", "O2"), ("O2", "O2'")]),
('AGWS_cis', [("N6", "N3"), ("N1", "O2'")]),
('AUWS_cis', [("N6", "O2"), ("N1", "O2'")]),
('CGWS_cis', [("N4", "N3"), ("N3", "O2'")]),
('CUWS_cis', [("N4", "O2"), ("N3", "O2'")]),
# last one is a water-mediated interaction
('GUWS_cis', [("N1", "O2"), ("N2", "O2'")]),
('UGWS_cis', [("O4", "N2"), ("N3", "N3")]),

## ----------------------------------------   WST

('AAWS_tran', [("N6", "N3"), ("N1", "C2")]),
('UUWS_tran', [("O4", "O2'"), ("N3", "O2")]),
('CCWS_tran', [("N4", "O2'"), ("N4", "O2")]),


('ACWS_tran', [("N6", "O2'"), ("N6", "O2")]),
('CAWS_tran', [("N4", "O2'"), ("N4", "N3")]),
('GCWS_tran', [("O6", "O2'"), ("N1", "O2")]),
('UAWS_tran', [("N3", "N3"), ("O2", "C2")]),
('UCWS_tran', [("O4", "O2'"), ("N3", "O2")]),
('AGWS_tran', [("N6", "O2'"), ("N6", "N3"), ("N1", "N2")]),
('AUWS_tran', [("N6", "O2'"), ("N6", "O2")]),
('CGWS_tran', [("N4", "O2'"), ("N4", "N3"), ("N3", "N2")]),
('CUWS_tran', [("N4", "O2'"), ("N4", "O2")]),
('GUWS_tran', [("O6", "O2'"), ("N1", "O2")]),
# last two are water-mediated interactions
('UGWS_tran', [("O2", "N2"), ("N2", "N3"), ("N2", "O2'")]),


## ----------------------------------------   HHC

('GGHH_cis', [("C8", "O6"), ("OP1", "O3'")]),

('GCHH_cis', [("O6", "C6"), ("OP1", "O3'")]),
('CGHH_cis', [("C6", "O6"), ("O3'", "OP1")]),

## ----------------------------------------   HHT

('AAHH_tran', [("N6", "N7"), ("N7", "N6"), ("OP1", "N6")]),


## ----------------------------------------   HSC

('CCHS_cis', [("O2", "N4"), ("O2'", "C6")]),
# last one is a water mediated
('UUHS_cis', [("O2", "C5"), ("O2", "O4")]),


# last one is a water mediated
('UCHS_cis', [("C5", "O2"), ("O4", "O2")]),

## ----------------------------------------   HST

('AAHS_tran', [("C2", "N7"), ("N3", "N6"), ("O2'", "N6")]),
#CHECK!
('CCHS_tran', [("N4", "O2"), ("N4", "O2'")]),
#('CCHS_tran', [("O2", "N4"), ("O2'", "N4")]),

('ACHS_tran', [("N6", "O2"), ("N6", "O2'")]),
('CAHS_tran', [("N4", "N3"), ("N4", "O2'")]),
('AGHS_tran', [("N7", "N2"), ("N6", "N3"), ("N6", "O2'")]),
('AUHS_tran', [("N6", "O2"), ("N6", "O2'")]),
('CUHS_tran', [("N4", "O2"), ("N4", "O2'")]),

## ----------------------------------------   SSC

('AASS_cis', [("O2'", "O2'"), ("O2'", "N3"), ("N3", "C2")]),
('CCSS_cis', [("O2'", "O2'"), ("O2'", "O2")]),
('GGSS_cis', [("O2'", "O2'"), ("O2'", "N3"), ("N3", "N2")]),
('UUSS_cis', [("O2'", "O2'"), ("O2'", "O2")]),

('ACSS_cis', [("O2'", "O2'"), ("O2'", "O2")]),
# last one is a water-mediated interaction
('CASS_cis', [("O2'", "O2'"), ("O2'", "N3"), ("O2", "C2")]),
('GASS_cis', [("O2'", "O2'"), ("O2'", "N3"), ("N3", "C2"), ("N2", "N1")]),
('GCSS_cis', [("O2'", "O2'"), ("O2'", "O2")]),
('UASS_cis', [("O2'", "O2'"), ("O2'", "N3"), ("O2", "C2")]),
# last one is a water-mediated interaction
('UCSS_cis', [("O2'", "O2'"), ("O2'", "O2"), ("O2", "O2")]),
('CUSS_cis', [("O2'", "O2'"), ("O2'", "O2")]),
('AGSS_cis', [("O2'", "O2'"), ("O2'", "N3"), ("N3", "N2")]),
('AUSS_cis', [("O2'", "O2'"), ("O2'", "O2")]),
('CGSS_cis', [("O2'", "O2'"), ("O2'", "N3"), ("O2", "N2")]),
('GUSS_cis', [("O2'", "O2'"), ("O2'", "O2")]),
('UGSS_cis', [("O2'", "O2'"), ("O2'", "N3"), ("O2", "N2")]),

## ----------------------------------------   SST

('AASS_tran', [("N1", "O2'"), ("C2", "N3"), ("N3", "C2")]),
('GGSS_tran', [("N2", "O2'"), ("N2", "N3"), ("N3", "N2"), ("O2'", "N2")]),

('ACSS_tran', [("N1", "O2'"), ("C2", "O2")]),
('GASS_tran', [("N2", "O2'"), ("N2", "N3"), ("N3", "C2")]),
('GCSS_tran', [("N2", "O2'"), ("N2", "O2")]),
('AGSS_tran', [("N1", "O2'"), ("C2", "N3"), ("N3", "N2")]),
('AUSS_tran', [("N1", "O2'"), ("C2", "O2")]),
('GUSS_tran', [("N2", "O2'"), ("N2", "O2")]),
)
