'''
Created on May 25, 2015

@author: gchojnowski
'''
import sys
import os
import json
import copy
import shutil
import traceback
import urllib2, urllib
import tarfile

import glob

sys.path.append(os.path.join("..", "lib"))
from rnamasonry_config import ROOT
sys.path.append(os.path.join(ROOT, "..", "lib"))

from itertools import product
from optparse import OptionParser
from multiprocessing import Process, Lock, Manager
from progressbar import ProgressBar, Percentage, Bar, ETA

# -------------- CCTBX
from iotbx.pdb import input as pdb_input
from scitbx.math import superpose
from scitbx import matrix
from scitbx.array_family import flex
import iotbx

# --------------------

import pickle, gzip

# =============================================================================

def align(ref_xyz, mov_xyz):
    superposition = superpose.least_squares_fit(ref_xyz, mov_xyz, method=["kearsley", "kabsch"][0])

    rtmx = matrix.rt((superposition.r, superposition.t))

    x = ref_xyz
    y = rtmx * mov_xyz

    d_sq = (x-y).dot()

    return rtmx, flex.mean(d_sq)**0.5


# ---------------------------------------------------------


def get_ph(_fname):
    with open(_fname, 'r') as f:
        return pdb_input(source_info = None, lines = f.read()).construct_hierarchy()
    return None


# ---------------------------------------------------------


def get_ref_atoms(ph, sel_string = "name P"):
    sel_cache = ph.atom_selection_cache()

    assert sel_cache.n_seq == ph.atoms_size()
    isel = sel_cache.iselection
    return ph.select(isel(sel_string)).atoms().extract_xyz()


# ---------------------------------------------------------


def merge(dirname = "./tmp/"):
    pdb_files = sorted([os.path.basename(f) for f in glob.glob("%s/*_min.pdb"%dirname)], \
                            key=lambda x: int(x.split('_')[0]))

    output_structure = iotbx.pdb.hierarchy.root()


    ref_ph = get_ph(os.path.join(dirname, pdb_files[0]))
    ref_xyz = get_ref_atoms(ref_ph)

    output_structure.append_model(iotbx.pdb.hierarchy.model(id="0"))
    output_structure.models()[-1].append_chain(ref_ph.only_chain().detached_copy())


    #for idx,fname in enumerate(pdb_files[1:]):
    for idx in xrange(1, len(pdb_files), 1):
        mov_ph = get_ph(os.path.join(dirname, pdb_files[idx]))
        mov_xyz = get_ref_atoms(mov_ph)
        rtmx,rmsd = align(ref_xyz, mov_xyz)
        print pdb_files[idx], mov_xyz.size(), rmsd

        new_chain = mov_ph.only_chain().detached_copy()
        #new_chain.atoms().set_xyz(new_xyz=(rtmx * new_chain.atoms().extract_xyz()))

        output_structure.append_model(iotbx.pdb.hierarchy.model(id=str(idx)))
        output_structure.models()[-1].append_chain(new_chain)

        #ref_ph, ref_xyz = mov_ph, mov_xyz


    output_structure.write_pdb_file(os.path.join(dirname, 'mov.pdb'))


# ---------------------------------------------------------


def main():
    merge(sys.argv[1])


if __name__=="__main__":
    main()



