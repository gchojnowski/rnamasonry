'''
Created on Jun 17, 2015

@author: gchojnowski
'''

import sys
import os


# -------------- CCTBX
from iotbx.pdb import input as pdb_input
from scitbx.math import superpose
from scitbx import matrix
from scitbx.array_family import flex
import iotbx
# --------------------


import gzip



def calc_rg(fname):

    with gzip.open(fname, 'r') as f:
        ph = pdb_input(source_info = None, lines = f.read()).construct_hierarchy()

    xyz = ph.atoms().extract_xyz()
    cm = flex.vec3_double(xyz.size()*[xyz.mean()])
    print os.path.basename(fname).split('_')[0], ph.only_chain().residue_groups_size(), xyz.rms_difference(cm)



def main(fname):
    calc_rg(fname)





if __name__=="__main__":
    main(sys.argv[1])
