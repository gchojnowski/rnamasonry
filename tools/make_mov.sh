#!/bin/sh

ABSPATH=$(cd "$(dirname "$0")"; pwd)

if [ ! -f $ABSPATH/virtualenv/bin/activate ] ; then
    cd $ABSPATH
    rm -rf virtualenv
    ./_create_virtualenv

    if [ $? -gt 0 ]; then
        echo "pip failed, check your system settings..."
        exit 1;
    fi

    . $ABSPATH/virtualenv/bin/activate
    cd $ABSPATH
    ./_install_cctbx

    if [ $? -gt 0 ]; then
        echo "CCTBX installation failed..."
        exit 1;
    fi

    # try to compile the python modeule pysimrna.so
    $ABSPATH/virtualenv/cctbx_build/bin/libtbx.scons
     if [ $? -gt 0 ]; then
        echo "Failed to compile pySimRNA module..."
        exit 1;
    fi

    echo "Installation complete"
fi

. $ABSPATH/virtualenv/bin/activate
. $ABSPATH/virtualenv/cctbx_build/setpaths.sh



libtbx.python $ABSPATH/make_mov.py $@
